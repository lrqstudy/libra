
import java.util.List;
import java.util.Map;

/**
 * ${classModel.desc!''} 操作接口
 */
public interface ${classModel.uname!''}Dao {

    /**
     * 保存${classModel.desc!''}数据
     *
     * @param model
     */
    public int save${classModel.uname!''}Model(${classModel.uname!''}Model model);

    /**
     * 批量保存${classModel.desc!''}数据
     *
     * @param list
     */
    public void save${classModel.uname!''}ModelBatch(List${r'<'}${classModel.uname!''}Model${r'>'} list);

    /**
     * 删除${classModel.desc!''}数据
     *
     * @param id 操作条数
     */
    public int delete${classModel.uname!''}Model(int id);

    /**
     * 批量删除${classModel.desc!''}数据
     *
     * @param list
     * @return int 操作条数
     */
    public int delete${classModel.uname!''}ModelByIds(List${r'<Integer>'} list);

    /**
     * 更新${classModel.desc!''}数据
     *
     * @param model
     */
    public int update${classModel.uname!''}Model(${classModel.uname!''}Model model);

    /**
     * 根据id获取${classModel.desc!''}数据
     *
     * @param id
     */
    public ${classModel.uname!''}Model get${classModel.uname!''}ModelById(int id);

    /**
     * 分页获取${classModel.desc!''}数据
     * @param searchBox
     */
    public List${r'<'}${classModel.uname!''}Model${r'>'} get${classModel.uname!''}ModelForPage(${classModel.uname!''}ModelSearchBox searchBox);

    /**
     * 分页获取${classModel.desc!''}数据总条数
     * @param searchBox
     */
    public int getCountForPage(${classModel.uname!''}ModelSearchBox searchBox);

}