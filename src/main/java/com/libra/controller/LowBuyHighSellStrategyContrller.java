package com.libra.controller;

import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.libra.common.consts.CurrencyType;
import com.libra.common.consts.PlatformEnum;
import com.libra.common.util.BeanUtilExt;
import com.libra.common.util.CommonJsonResult;
import com.libra.common.util.ProjectUtils;
import com.libra.exchange.strategy.model.LowBuyHighSellStrategyModel;
import com.libra.exchange.strategy.service.LowBuyHighSellStrategyService;
import com.libra.exchange.strategy.vo.LowBuyHighSellSerachBox;
import com.libra.exchange.strategy.vo.LowBuyHighSellStrategyVO;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * Created by rongqiang.li on 17/5/14.
 */
@Controller
@RequestMapping("libra/lowbuyhighsell")
public class LowBuyHighSellStrategyContrller {

    @Resource
    private LowBuyHighSellStrategyService lowBuyHighSellStrategyService;

    private List<String> allowedCashCurrencyTypeList = Lists.newArrayList(CurrencyType.CNY.getCode());
    private List<String> allowedPlatformList = Lists.newArrayList(PlatformEnum.YUNBI.getCode(),PlatformEnum.POLONIEX.getCode());
    private List<String> allowedTargetCodeList = Lists.newArrayList(CurrencyType.ETH.getCode(), CurrencyType.SC.getCode(),
            CurrencyType.REP.getCode(), CurrencyType.GNT.getCode(),
            CurrencyType.ZEC.getCode(),  CurrencyType.BTS.getCode());

    @ResponseBody
    @RequestMapping("/addNewLowBuyHighSell")
    public Map<String, Object> addNewLowBuyHighSell(LowBuyHighSellStrategyVO lowBuyHighSellStrategyVO) {
        String userCode = ProjectUtils.getCurrentUserCode();
        if (Strings.isNullOrEmpty(userCode)) {
            //如果当前用户编码未空,直接跳转到登录页面
            return CommonJsonResult.create().data("请登录后操作").buildError();
        }
        String errMsg = validateVO(lowBuyHighSellStrategyVO,userCode);
        if (!Strings.isNullOrEmpty(errMsg)){
            return CommonJsonResult.create().data(errMsg).buildError();
        }
        LowBuyHighSellStrategyModel model = new LowBuyHighSellStrategyModel();
        lowBuyHighSellStrategyService.saveLowBuyHighSellStrategyModel(model);
        return CommonJsonResult.create().data("添加成功").buildSuccess();
    }

    private String validateVO(LowBuyHighSellStrategyVO vo,String userCode) {
        if (allowedCashCurrencyTypeList.contains(vo.getCashCurrency())) {
            //现金账户必须是CNY
            return "现金账户必须是CNY";
        }

        if (allowedPlatformList.contains(vo.getPlatform())) {
            return "只允许以下交易平台" + allowedPlatformList.toString();
        }

        if (allowedTargetCodeList.contains(vo.getTargetCode())) {
            return "只允许交易以下品种" + allowedTargetCodeList.toString();

        }

        BigDecimal changeRate = new BigDecimal(vo.getChangeRate());
        if (changeRate.compareTo(BigDecimal.ZERO) <= 0 && changeRate.compareTo(BigDecimal.ONE) >= 0) {
            return "波动比率必须是在 大于0并且小于1 ,建议0.02-0.04之间";
        }

        LowBuyHighSellSerachBox searchBox=new LowBuyHighSellSerachBox();
        searchBox.setPlatform(vo.getPlatform());
        searchBox.setUserCode(userCode);
        searchBox.setTargetCode(vo.getTargetCode());
        Integer strategyExist = lowBuyHighSellStrategyService.isStrategyExist(searchBox);
        if (strategyExist!=null&&strategyExist>=1){
            return "同一个平台的一个品种只允许配置一个策略";
        }

        return null;
    }

    private LowBuyHighSellStrategyModel buildModelFromVO(LowBuyHighSellStrategyVO vo, String userCode) {
        LowBuyHighSellStrategyModel model = new LowBuyHighSellStrategyModel();
        model.setUserCode(userCode);
        model.setPlatform(vo.getPlatform());
        model.setChangeRate(new BigDecimal(vo.getChangeRate()));
        model.setActiveStatus(0);//激活状态是0
        model.setPerBuyAmount(new BigDecimal(vo.getPerBuyAmount()));
        model.setPerSellAmount(new BigDecimal(vo.getPerSellAmount()));
        model.setTargetCode(vo.getTargetCode());
        model.setCashCurrency(vo.getCashCurrency());
        BeanUtilExt.fillNullProperty(model);
        return model;
    }


}
