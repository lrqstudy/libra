package com.libra.exchange.strategy.vo;

/**
 * Created by rongqiang.li on 17/5/14.
 */
public class LowBuyHighSellStrategyVO {

    /**
     * 平台编码
     */
    private String platform;

    /**
     * 交易品种编码
     */
    private String targetCode;
    /**
     * 现金账户编码
     */
    private String cashCurrency;
    /**
     * 获取的交易深度
     */
    private Integer orderBookDepth = 5;
    /**
     * 波动比率
     */
    private String changeRate = "0.02";

    /**
     * 每次买入数量
     */
    private String perBuyAmount;

    /**
     * 每次卖出数量
     */
    private String perSellAmount;

    public String getTargetCode() {
        return targetCode;
    }

    public void setTargetCode(String targetCode) {
        this.targetCode = targetCode;
    }

    public String getCashCurrency() {
        return cashCurrency;
    }

    public void setCashCurrency(String cashCurrency) {
        this.cashCurrency = cashCurrency;
    }

    public Integer getOrderBookDepth() {
        return orderBookDepth;
    }

    public void setOrderBookDepth(Integer orderBookDepth) {
        this.orderBookDepth = orderBookDepth;
    }

    public String getChangeRate() {
        return changeRate;
    }

    public void setChangeRate(String changeRate) {
        this.changeRate = changeRate;
    }

    public String getPerBuyAmount() {
        return perBuyAmount;
    }

    public void setPerBuyAmount(String perBuyAmount) {
        this.perBuyAmount = perBuyAmount;
    }

    public String getPerSellAmount() {
        return perSellAmount;
    }

    public void setPerSellAmount(String perSellAmount) {
        this.perSellAmount = perSellAmount;
    }

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }
}
