package com.libra.exchange.strategy.vo;

import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Created by rongqiang.li on 17/5/14.
 */
public class LowBuyHighSellSerachBox {
    /**
     * 平台编码
     */
    private String platform;

    /**
     * 交易品种编码
     */
    private String targetCode;

    private String userCode;

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public String getTargetCode() {
        return targetCode;
    }

    public void setTargetCode(String targetCode) {
        this.targetCode = targetCode;
    }

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
