package com.libra.exchange.strategy.vo;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Created by rongqiang.li on 17/12/29.
 */
public class AltTransferBalanceSnapshotResultVO implements Serializable{
    /**
     * 平台名称
     */
    private String platform;
    /**
     * 现金账户可用余额
     */
    private BigDecimal cashCurrencyBalanceAmount;
    /**
     * 目标账户可用余额
     */
    private BigDecimal targetCodeBalanceAmount;
    /**
     * 手续费账户
     */
    private BigDecimal feeBalanceAmount;
    /**
     * 未成交的买单,我们假设其很快就将成交,则计算时: 应该用标的账户加上这部分数量
     */
    private BigDecimal unexecutedOfBuyQuantity;
    /**
     * 未成交的买单,我们假设其很快就将成交,则计算时: 应该用现金账户金额减去 这部分金额
     */
    private BigDecimal unexecutedOfBuyAmount;

    /**
     * 未成交的卖单,我们假设其很快就将成交,则计算标的账户余额时,应该用标的账户减去这部分数量
     */
    private BigDecimal unexecutedOfSellQuantity;

    /**
     * 未成交的卖单,我们假设其很快就将成交,则计算余额时,应该用现金账户加上这部分金额
     */
    private BigDecimal unexecutedOfSellAmount;
    /**
     * 标的品种价格
     */
    private BigDecimal targetCodePrice;

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public BigDecimal getCashCurrencyBalanceAmount() {
        return cashCurrencyBalanceAmount;
    }

    public void setCashCurrencyBalanceAmount(BigDecimal cashCurrencyBalanceAmount) {
        this.cashCurrencyBalanceAmount = cashCurrencyBalanceAmount;
    }

    public BigDecimal getTargetCodeBalanceAmount() {
        return targetCodeBalanceAmount;
    }

    public void setTargetCodeBalanceAmount(BigDecimal targetCodeBalanceAmount) {
        this.targetCodeBalanceAmount = targetCodeBalanceAmount;
    }

    public BigDecimal getFeeBalanceAmount() {
        return feeBalanceAmount;
    }

    public void setFeeBalanceAmount(BigDecimal feeBalanceAmount) {
        this.feeBalanceAmount = feeBalanceAmount;
    }

    public BigDecimal getUnexecutedOfBuyQuantity() {
        return unexecutedOfBuyQuantity;
    }

    public void setUnexecutedOfBuyQuantity(BigDecimal unexecutedOfBuyQuantity) {
        this.unexecutedOfBuyQuantity = unexecutedOfBuyQuantity;
    }

    public BigDecimal getUnexecutedOfBuyAmount() {
        return unexecutedOfBuyAmount;
    }

    public void setUnexecutedOfBuyAmount(BigDecimal unexecutedOfBuyAmount) {
        this.unexecutedOfBuyAmount = unexecutedOfBuyAmount;
    }

    public BigDecimal getUnexecutedOfSellQuantity() {
        return unexecutedOfSellQuantity;
    }

    public void setUnexecutedOfSellQuantity(BigDecimal unexecutedOfSellQuantity) {
        this.unexecutedOfSellQuantity = unexecutedOfSellQuantity;
    }

    public BigDecimal getUnexecutedOfSellAmount() {
        return unexecutedOfSellAmount;
    }

    public void setUnexecutedOfSellAmount(BigDecimal unexecutedOfSellAmount) {
        this.unexecutedOfSellAmount = unexecutedOfSellAmount;
    }

    public BigDecimal getTargetCodePrice() {
        return targetCodePrice;
    }

    public void setTargetCodePrice(BigDecimal targetCodePrice) {
        this.targetCodePrice = targetCodePrice;
    }
}
