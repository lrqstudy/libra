package com.libra.exchange.strategy.service;

import com.libra.exchange.strategy.model.SmsInfoModel;

import java.util.List;

/**
 * 短信信息表 操作接口
 */
public interface SmsInfoService {

    /**
     * 保存短信信息表数据
     *
     * @param model
     */
    public int saveSmsInfoModel(SmsInfoModel model);

    /**
     * 批量保存短信信息表数据
     *
     * @param list
     */
    public void saveSmsInfoModelBatch(List<SmsInfoModel> list);

    /**
     * 删除短信信息表数据
     *
     * @param id
     */
    public boolean deleteSmsInfoModel(int id);

    /**
     * 批量删除短信信息表数据
     *
     * @param list
     * @return boolean
     */
    public boolean deleteSmsInfoModelByIds(List<Integer> list);

    /**
     * 更新短信信息表数据
     *
     * @param model
     */
    public boolean updateSmsInfoModel(SmsInfoModel model);

    /**
     * 根据id获取短信信息表数据
     *
     * @param id
     */
    public SmsInfoModel getSmsInfoModelById(int id);

    /**
     * 分页获取短信信息表数据
     *
     * @param model
     * @param page
     * @param pageSize
     */
    public List<SmsInfoModel> getSmsInfoModelForPage(SmsInfoModel model, int page, int pageSize);

    /**
     * 获取需要处理的 list
     * @return
     */
    public List<SmsInfoModel> getNeedHandleModelList();

    /**
     * 根据 id 更新短信发送处理状态
     * @param id
     * @param resultMessage
     * @param processFlag
     * @return
     */
    public Integer updateProcessFlagById(Integer id,String resultMessage,Integer processFlag);

}