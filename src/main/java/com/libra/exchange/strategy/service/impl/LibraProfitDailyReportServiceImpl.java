package com.libra.exchange.strategy.service.impl;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.libra.exchange.strategy.dao.libra.LibraProfitDailyReportDao;
import com.libra.exchange.strategy.model.LibraProfitDailyReportModel;
import com.libra.exchange.strategy.service.LibraProfitDailyReportService;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * 搬砖利润日报表 操作接口实现类
 */
@Service
public class LibraProfitDailyReportServiceImpl implements LibraProfitDailyReportService {

    private Logger logger = LoggerFactory.getLogger(LibraProfitDailyReportServiceImpl.class);

    @Resource
    private LibraProfitDailyReportDao libraProfitDailyReportDao;

    /**
     * 保存搬砖利润日报表数据
     *
     * @param model
     */
    @Override
    public int saveLibraProfitDailyReportModel(LibraProfitDailyReportModel model) {
        if (model == null) {
            logger.info("保存搬砖利润日报表失败，id={}", -1);
            return -1;
        }
        libraProfitDailyReportDao.saveLibraProfitDailyReportModel(model);
        logger.info("保存搬砖利润日报表成功，id={}", model.getId());
        return model.getId();
    }

    /**
     * 批量保存搬砖利润日报表数据
     *
     * @param list
     */
    @Override
    public void saveLibraProfitDailyReportModelBatch(List<LibraProfitDailyReportModel> list) {
       if (CollectionUtils.isEmpty(list)) {
            logger.info("批量保存搬砖利润日报表失败，size={}", 0);
            return;
        }
        libraProfitDailyReportDao.saveLibraProfitDailyReportModelBatch(list);
        logger.info("批量保存搬砖利润日报表成功，size={}", list.size());
    }

    /**
     * 删除搬砖利润日报表数据
     *
     * @param id
     */
    @Override
    public boolean deleteLibraProfitDailyReportModel(int id) {
        if (id <= 0) {
            logger.info("删除搬砖利润日报表失败，id={},count={}", 0, 0);
            return false;
        }
        int count = libraProfitDailyReportDao.deleteLibraProfitDailyReportModel(id);
        logger.info("删除搬砖利润日报表成功，id={},count={}", id, count);
        return count > 0;
    }

    /**
     * 批量删除搬砖利润日报表数据
     *
     * @param list
     * @return boolean
     */
    @Override
    public boolean deleteLibraProfitDailyReportModelByIds(List<Integer> list){
        if (CollectionUtils.isEmpty(list)) {
            logger.info("删除搬砖利润日报表失败,count={}",0);
            return false;
        }
        int count = libraProfitDailyReportDao.deleteLibraProfitDailyReportModelByIds(list);
        logger.info("删除搬砖利润日报表成功,count={}", count);
        return count==list.size();
    }

    /**
     * 更新搬砖利润日报表数据
     *
     * @param model
     */
    @Override
    public boolean updateLibraProfitDailyReportModel(LibraProfitDailyReportModel model) {
        if (model == null || model.getId() <= 0) {
            logger.info("更新搬砖利润日报表失败，参数不合法");
            return false;
        }
        int count = libraProfitDailyReportDao.updateLibraProfitDailyReportModel(model);
        logger.info("更新搬砖利润日报表成功，id={},count={}", model.getId(), count);
        return count > 0;
    }

    /**
     * 根据id获取搬砖利润日报表数据
     *
     * @param id
     */
    @Override
    public LibraProfitDailyReportModel getLibraProfitDailyReportModelById(int id){
        if (id <= 0) {
            logger.info("根据id获取搬砖利润日报表失败，id={}", id);
            return null;
        }
        LibraProfitDailyReportModel model = libraProfitDailyReportDao.getLibraProfitDailyReportModelById(id);
        logger.info("根据id获取搬砖利润日报表成功，id={}", id);
        return model;
    }

    /**
     * 分页获取搬砖利润日报表数据
     *
     * @param model
     * @param page
     * @param pageSize
     */
    public List<LibraProfitDailyReportModel> getLibraProfitDailyReportModelForPage(LibraProfitDailyReportModel model,int page,int pageSize){
        Preconditions.checkArgument(page >= 0 && pageSize >0 && model != null, "参数不合法");
        Map<String,Object> params = Maps.newHashMap();
        params.put("page", page);
        params.put("pageSize", pageSize);
        List<LibraProfitDailyReportModel> modelList = libraProfitDailyReportDao.getLibraProfitDailyReportModelForPage(params);
        if (CollectionUtils.isEmpty(modelList)) {
            modelList = Lists.newArrayList();
        }
        logger.info("分页获取搬砖利润日报表数据，page={},pageSize={}", page, pageSize);
        return modelList;
    }
}