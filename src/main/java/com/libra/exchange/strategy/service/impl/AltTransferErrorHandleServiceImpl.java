package com.libra.exchange.strategy.service.impl;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.libra.exchange.strategy.dao.libra.AltTransferErrorHandleDao;
import com.libra.exchange.strategy.model.AltTransferErrorHandleModel;
import com.libra.exchange.strategy.service.AltTransferErrorHandleService;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * 搬砖错误单报错处理 操作接口实现类
 */
@Service
public class AltTransferErrorHandleServiceImpl implements AltTransferErrorHandleService {

    private Logger logger = LoggerFactory.getLogger(AltTransferErrorHandleServiceImpl.class);

    @Resource
    private AltTransferErrorHandleDao altTransferErrorHandleDao;

    /**
     * 保存搬砖错误单报错处理数据
     *
     * @param model
     */
    @Override
    public int saveAltTransferErrorHandleModel(AltTransferErrorHandleModel model) {
        if (model == null) {
            logger.info("保存搬砖错误单报错处理失败，id={}", -1);
            return -1;
        }
        altTransferErrorHandleDao.saveAltTransferErrorHandleModel(model);
        logger.info("保存搬砖错误单报错处理成功，id={}", model.getId());
        return model.getId();
    }

    /**
     * 批量保存搬砖错误单报错处理数据
     *
     * @param list
     */
    @Override
    public void saveAltTransferErrorHandleModelBatch(List<AltTransferErrorHandleModel> list) {
       if (CollectionUtils.isEmpty(list)) {
            logger.info("批量保存搬砖错误单报错处理失败，size={}", 0);
            return;
        }
        altTransferErrorHandleDao.saveAltTransferErrorHandleModelBatch(list);
        logger.info("批量保存搬砖错误单报错处理成功，size={}", list.size());
    }

    /**
     * 删除搬砖错误单报错处理数据
     *
     * @param id
     */
    @Override
    public boolean deleteAltTransferErrorHandleModel(int id) {
        if (id <= 0) {
            logger.info("删除搬砖错误单报错处理失败，id={},count={}", 0, 0);
            return false;
        }
        int count = altTransferErrorHandleDao.deleteAltTransferErrorHandleModel(id);
        logger.info("删除搬砖错误单报错处理成功，id={},count={}", id, count);
        return count > 0;
    }

    /**
     * 批量删除搬砖错误单报错处理数据
     *
     * @param list
     * @return boolean
     */
    @Override
    public boolean deleteAltTransferErrorHandleModelByIds(List<Integer> list){
        if (CollectionUtils.isEmpty(list)) {
            logger.info("删除搬砖错误单报错处理失败,count={}",0);
            return false;
        }
        int count = altTransferErrorHandleDao.deleteAltTransferErrorHandleModelByIds(list);
        logger.info("删除搬砖错误单报错处理成功,count={}", count);
        return count==list.size();
    }

    /**
     * 更新搬砖错误单报错处理数据
     *
     * @param model
     */
    @Override
    public boolean updateAltTransferErrorHandleModel(AltTransferErrorHandleModel model) {
        if (model == null || model.getId() <= 0) {
            logger.info("更新搬砖错误单报错处理失败，参数不合法");
            return false;
        }
        int count = altTransferErrorHandleDao.updateAltTransferErrorHandleModel(model);
        logger.info("更新搬砖错误单报错处理成功，id={},count={}", model.getId(), count);
        return count > 0;
    }

    /**
     * 根据id获取搬砖错误单报错处理数据
     *
     * @param id
     */
    @Override
    public AltTransferErrorHandleModel getAltTransferErrorHandleModelById(int id){
        if (id <= 0) {
            logger.info("根据id获取搬砖错误单报错处理失败，id={}", id);
            return null;
        }
        AltTransferErrorHandleModel model = altTransferErrorHandleDao.getAltTransferErrorHandleModelById(id);
        logger.info("根据id获取搬砖错误单报错处理成功，id={}", id);
        return model;
    }

    /**
     * 分页获取搬砖错误单报错处理数据
     *
     * @param model
     * @param page
     * @param pageSize
     */
    public List<AltTransferErrorHandleModel> getAltTransferErrorHandleModelForPage(AltTransferErrorHandleModel model,int page,int pageSize){
        Preconditions.checkArgument(page >= 0 && pageSize >0 && model != null, "参数不合法");
        Map<String,Object> params = Maps.newHashMap();
        params.put("page", page);
        params.put("pageSize", pageSize);
        List<AltTransferErrorHandleModel> modelList = altTransferErrorHandleDao.getAltTransferErrorHandleModelForPage(params);
        if (CollectionUtils.isEmpty(modelList)) {
            modelList = Lists.newArrayList();
        }
        logger.info("分页获取搬砖错误单报错处理数据，page={},pageSize={}", page, pageSize);
        return modelList;
    }

    @Override
    public List<AltTransferErrorHandleModel> getNeedHandleModelList() {
        List<AltTransferErrorHandleModel> needHandleModelList = altTransferErrorHandleDao.getNeedHandleModelList();
        logger.info(" get need handle modellise size ={}",needHandleModelList.size());
        return needHandleModelList;
    }

    @Override
    public Integer updateResultIdAndProcessFlag(Integer id, Integer processFlag, Integer handleType, String resultId) {
        Integer count = altTransferErrorHandleDao.updateResultIdAndProcessFlag(id, processFlag, handleType, resultId);
        logger.info(" update processFlag by id={} ,processFlag={},handleType={},resultId={},  count={}",id,processFlag,handleType,resultId,count);
        return count;
    }
}