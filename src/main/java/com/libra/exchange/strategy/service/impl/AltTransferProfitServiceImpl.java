package com.libra.exchange.strategy.service.impl;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.libra.exchange.strategy.dao.libra.AltTransferProfitDao;
import com.libra.exchange.strategy.model.AltTransferProfitModel;
import com.libra.exchange.strategy.service.AltTransferProfitService;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 *  搬砖利润统计表操作接口实现类
 */
@Service
public class AltTransferProfitServiceImpl implements AltTransferProfitService {

    private Logger logger = LoggerFactory.getLogger(AltTransferProfitServiceImpl.class);

    @Resource
    private AltTransferProfitDao altTransferProfitDao;

    /**
     * 保存数据
     *
     * @param model
     */
    @Override
    public int saveAltTransferProfitModel(AltTransferProfitModel model) {
        if (model == null) {
            logger.info("保存失败，id={}", -1);
            return -1;
        }
        altTransferProfitDao.saveAltTransferProfitModel(model);
        logger.info("保存成功，id={}", model.getId());
        return model.getId();
    }

    /**
     * 批量保存数据
     *
     * @param list
     */
    @Override
    public void saveAltTransferProfitModelBatch(List<AltTransferProfitModel> list) {
       if (CollectionUtils.isEmpty(list)) {
            logger.info("批量保存失败，size={}", 0);
            return;
        }
        altTransferProfitDao.saveAltTransferProfitModelBatch(list);
        logger.info("批量保存成功，size={}", list.size());
    }

    /**
     * 删除数据
     *
     * @param id
     */
    @Override
    public boolean deleteAltTransferProfitModel(int id) {
        if (id <= 0) {
            logger.info("删除失败，id={},count={}", 0, 0);
            return false;
        }
        int count = altTransferProfitDao.deleteAltTransferProfitModel(id);
        logger.info("删除成功，id={},count={}", id, count);
        return count > 0;
    }

    /**
     * 批量删除数据
     *
     * @param list
     * @return boolean
     */
    @Override
    public boolean deleteAltTransferProfitModelByIds(List<Integer> list){
        if (CollectionUtils.isEmpty(list)) {
            logger.info("删除失败,count={}",0);
            return false;
        }
        int count = altTransferProfitDao.deleteAltTransferProfitModelByIds(list);
        logger.info("删除成功,count={}", count);
        return count==list.size();
    }

    /**
     * 更新数据
     *
     * @param model
     */
    @Override
    public boolean updateAltTransferProfitModel(AltTransferProfitModel model) {
        if (model == null || model.getId() <= 0) {
            logger.info("更新失败，参数不合法");
            return false;
        }
        int count = altTransferProfitDao.updateAltTransferProfitModel(model);
        logger.info("更新成功，id={},count={}", model.getId(), count);
        return count > 0;
    }

    /**
     * 根据id获取数据
     *
     * @param id
     */
    @Override
    public AltTransferProfitModel getAltTransferProfitModelById(int id){
        if (id <= 0) {
            logger.info("根据id获取失败，id={}", id);
            return null;
        }
        AltTransferProfitModel model = altTransferProfitDao.getAltTransferProfitModelById(id);
        logger.info("根据id获取成功，id={}", id);
        return model;
    }

    /**
     * 分页获取数据
     *
     * @param model
     * @param page
     * @param pageSize
     */
    public List<AltTransferProfitModel> getAltTransferProfitModelForPage(AltTransferProfitModel model,int page,int pageSize){
        Preconditions.checkArgument(page >= 0 && pageSize >0 && model != null, "参数不合法");
        Map<String,Object> params = Maps.newHashMap();
        params.put("page", page);
        params.put("pageSize", pageSize);
        List<AltTransferProfitModel> modelList = altTransferProfitDao.getAltTransferProfitModelForPage(params);
        if (CollectionUtils.isEmpty(modelList)) {
            modelList = Lists.newArrayList();
        }
        logger.info("分页获取数据，page={},pageSize={}", page, pageSize);
        return modelList;
    }
}