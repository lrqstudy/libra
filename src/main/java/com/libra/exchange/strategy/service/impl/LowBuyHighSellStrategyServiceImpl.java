package com.libra.exchange.strategy.service.impl;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.libra.exchange.strategy.dao.libra.LowBuyHighSellStrategyDao;
import com.libra.exchange.strategy.model.LowBuyHighSellStrategyModel;
import com.libra.exchange.strategy.service.LowBuyHighSellStrategyService;
import com.libra.exchange.strategy.vo.LowBuyHighSellSerachBox;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * 低买高卖策略配置表 操作接口实现类
 */
@Service
public class LowBuyHighSellStrategyServiceImpl implements LowBuyHighSellStrategyService {


    private Logger logger = LoggerFactory.getLogger(LowBuyHighSellStrategyServiceImpl.class);

    @Resource
    private LowBuyHighSellStrategyDao lowBuyHighSellStrategyDao;

    /**
     * 保存低买高卖策略配置表数据
     *
     * @param model
     */
    @Override
    public int saveLowBuyHighSellStrategyModel(LowBuyHighSellStrategyModel model) {
        if (model == null) {
            logger.info("保存低买高卖策略配置表失败，id={}", -1);
            return -1;
        }
        lowBuyHighSellStrategyDao.saveLowBuyHighSellStrategyModel(model);
        logger.info("保存低买高卖策略配置表成功，id={}", model.getId());
        return model.getId();
    }

    /**
     * 批量保存低买高卖策略配置表数据
     *
     * @param list
     */
    @Override
    public void saveLowBuyHighSellStrategyModelBatch(List<LowBuyHighSellStrategyModel> list) {
        if (CollectionUtils.isEmpty(list)) {
            logger.info("批量保存低买高卖策略配置表失败，size={}", 0);
            return;
        }
        lowBuyHighSellStrategyDao.saveLowBuyHighSellStrategyModelBatch(list);
        logger.info("批量保存低买高卖策略配置表成功，size={}", list.size());
    }

    /**
     * 删除低买高卖策略配置表数据
     *
     * @param id
     */
    @Override
    public boolean deleteLowBuyHighSellStrategyModel(int id) {
        if (id <= 0) {
            logger.info("删除低买高卖策略配置表失败，id={},count={}", 0, 0);
            return false;
        }
        int count = lowBuyHighSellStrategyDao.deleteLowBuyHighSellStrategyModel(id);
        logger.info("删除低买高卖策略配置表成功，id={},count={}", id, count);
        return count > 0;
    }

    /**
     * 批量删除低买高卖策略配置表数据
     *
     * @param list
     * @return boolean
     */
    @Override
    public boolean deleteLowBuyHighSellStrategyModelByIds(List<Integer> list) {
        if (CollectionUtils.isEmpty(list)) {
            logger.info("删除低买高卖策略配置表失败,count={}", 0);
            return false;
        }
        int count = lowBuyHighSellStrategyDao.deleteLowBuyHighSellStrategyModelByIds(list);
        logger.info("删除低买高卖策略配置表成功,count={}", count);
        return count == list.size();
    }

    /**
     * 更新低买高卖策略配置表数据
     *
     * @param model
     */
    @Override
    public boolean updateLowBuyHighSellStrategyModel(LowBuyHighSellStrategyModel model) {
        if (model == null || model.getId() <= 0) {
            logger.info("更新低买高卖策略配置表失败，参数不合法");
            return false;
        }
        int count = lowBuyHighSellStrategyDao.updateLowBuyHighSellStrategyModel(model);
        logger.info("更新低买高卖策略配置表成功，id={},count={}", model.getId(), count);
        return count > 0;
    }

    /**
     * 根据id获取低买高卖策略配置表数据
     *
     * @param id
     */
    @Override
    public LowBuyHighSellStrategyModel getLowBuyHighSellStrategyModelById(int id) {
        if (id <= 0) {
            logger.info("根据id获取低买高卖策略配置表失败，id={}", id);
            return null;
        }
        LowBuyHighSellStrategyModel model = lowBuyHighSellStrategyDao.getLowBuyHighSellStrategyModelById(id);
        logger.info("根据id获取低买高卖策略配置表成功，id={}", id);
        return model;
    }

    /**
     * 分页获取低买高卖策略配置表数据
     *
     * @param model
     * @param page
     * @param pageSize
     */
    public List<LowBuyHighSellStrategyModel> getLowBuyHighSellStrategyModelForPage(LowBuyHighSellStrategyModel model, int page, int pageSize) {
        Preconditions.checkArgument(page >= 0 && pageSize > 0 && model != null, "参数不合法");
        Map<String, Object> params = Maps.newHashMap();
        params.put("page", page);
        params.put("pageSize", pageSize);
        List<LowBuyHighSellStrategyModel> modelList = lowBuyHighSellStrategyDao.getLowBuyHighSellStrategyModelForPage(params);
        if (CollectionUtils.isEmpty(modelList)) {
            modelList = Lists.newArrayList();
        }
        logger.info("分页获取低买高卖策略配置表数据，page={},pageSize={}", page, pageSize);
        return modelList;
    }

    @Override
    public LowBuyHighSellStrategyModel getStrategyModelByTargetCode(String targetCode) {
        LowBuyHighSellStrategyModel model = lowBuyHighSellStrategyDao.getStrategyModelByTargetCode(targetCode);
        logger.info(" 根据 targetCode ={} 查询策略model id ={}", targetCode, model == null ? null : model.getId());
        return model;
    }

    @Override
    public List<LowBuyHighSellStrategyModel> listAllActiveStrategyModelList() {
        List<LowBuyHighSellStrategyModel> list = lowBuyHighSellStrategyDao.listAllActiveStrategyModelList();
        logger.info(" 列出所有生效的交易策略 size ={} ", list.size());
        return list;
    }

    @Override
    public List<LowBuyHighSellStrategyModel> getAllModelListByUserCode(String userCode) {
        List<LowBuyHighSellStrategyModel> modelList = lowBuyHighSellStrategyDao.getAllModelListByUserCode(userCode);
        logger.info(" 根据用户编码={}, 查询 配置list size={}",userCode,modelList.size());
        return modelList;
    }

    @Override
    public Map<Pair<String, String>, LowBuyHighSellStrategyModel> getPlatformAndUserCodeModelMap() {
        List<LowBuyHighSellStrategyModel> modelList = listAllActiveStrategyModelList();
        if (CollectionUtils.isEmpty(modelList)){
            return Maps.newHashMap();
        }
        Map<Pair<String, String>, LowBuyHighSellStrategyModel> platformAndUserCodeModelMap=Maps.newHashMap();
        for (LowBuyHighSellStrategyModel model : modelList) {
            platformAndUserCodeModelMap.put(Pair.of(model.getPlatform(), model.getUserCode()), model);
        }
        return platformAndUserCodeModelMap;
    }

    @Override
    public Integer isStrategyExist(LowBuyHighSellSerachBox serachBox) {
        Integer strategyExist = lowBuyHighSellStrategyDao.isStrategyExist(serachBox);
        logger.info(" 根据 serachBox ={} ,查询策略是否存在 count ={} ", serachBox.toString(), strategyExist);
        return strategyExist;
    }
}