package com.libra.exchange.strategy.service.impl;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.libra.exchange.strategy.dao.libra.SmsInfoDao;
import com.libra.exchange.strategy.model.SmsInfoModel;
import com.libra.exchange.strategy.service.SmsInfoService;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * 短信信息表 操作接口实现类
 */
@Service
public class SmsInfoServiceImpl implements SmsInfoService {

    private Logger logger = LoggerFactory.getLogger(SmsInfoServiceImpl.class);

    @Resource
    private SmsInfoDao smsInfoDao;

    /**
     * 保存短信信息表数据
     *
     * @param model
     */
    @Override
    public int saveSmsInfoModel(SmsInfoModel model) {
        if (model == null) {
            logger.info("保存短信信息表失败，id={}", -1);
            return -1;
        }
        smsInfoDao.saveSmsInfoModel(model);
        logger.info("保存短信信息表成功，id={}", model.getId());
        return model.getId();
    }

    /**
     * 批量保存短信信息表数据
     *
     * @param list
     */
    @Override
    public void saveSmsInfoModelBatch(List<SmsInfoModel> list) {
        if (CollectionUtils.isEmpty(list)) {
            logger.info("批量保存短信信息表失败，size={}", 0);
            return;
        }
        smsInfoDao.saveSmsInfoModelBatch(list);
        logger.info("批量保存短信信息表成功，size={}", list.size());
    }

    /**
     * 删除短信信息表数据
     *
     * @param id
     */
    @Override
    public boolean deleteSmsInfoModel(int id) {
        if (id <= 0) {
            logger.info("删除短信信息表失败，id={},count={}", 0, 0);
            return false;
        }
        int count = smsInfoDao.deleteSmsInfoModel(id);
        logger.info("删除短信信息表成功，id={},count={}", id, count);
        return count > 0;
    }

    /**
     * 批量删除短信信息表数据
     *
     * @param list
     * @return boolean
     */
    @Override
    public boolean deleteSmsInfoModelByIds(List<Integer> list) {
        if (CollectionUtils.isEmpty(list)) {
            logger.info("删除短信信息表失败,count={}", 0);
            return false;
        }
        int count = smsInfoDao.deleteSmsInfoModelByIds(list);
        logger.info("删除短信信息表成功,count={}", count);
        return count == list.size();
    }

    /**
     * 更新短信信息表数据
     *
     * @param model
     */
    @Override
    public boolean updateSmsInfoModel(SmsInfoModel model) {
        if (model == null || model.getId() <= 0) {
            logger.info("更新短信信息表失败，参数不合法");
            return false;
        }
        int count = smsInfoDao.updateSmsInfoModel(model);
        logger.info("更新短信信息表成功，id={},count={}", model.getId(), count);
        return count > 0;
    }

    /**
     * 根据id获取短信信息表数据
     *
     * @param id
     */
    @Override
    public SmsInfoModel getSmsInfoModelById(int id) {
        if (id <= 0) {
            logger.info("根据id获取短信信息表失败，id={}", id);
            return null;
        }
        SmsInfoModel model = smsInfoDao.getSmsInfoModelById(id);
        logger.info("根据id获取短信信息表成功，id={}", id);
        return model;
    }

    /**
     * 分页获取短信信息表数据
     *
     * @param model
     * @param page
     * @param pageSize
     */
    public List<SmsInfoModel> getSmsInfoModelForPage(SmsInfoModel model, int page, int pageSize) {
        Preconditions.checkArgument(page >= 0 && pageSize > 0 && model != null, "参数不合法");
        Map<String, Object> params = Maps.newHashMap();
        params.put("page", page);
        params.put("pageSize", pageSize);
        List<SmsInfoModel> modelList = smsInfoDao.getSmsInfoModelForPage(params);
        if (CollectionUtils.isEmpty(modelList)) {
            modelList = Lists.newArrayList();
        }
        logger.info("分页获取短信信息表数据，page={},pageSize={}", page, pageSize);
        return modelList;
    }

    @Override
    public List<SmsInfoModel> getNeedHandleModelList() {
        List<SmsInfoModel> needHandleModelList = smsInfoDao.getNeedHandleModelList();
        logger.info(" get needed handle modelList size={}", needHandleModelList.size());
        return needHandleModelList;
    }

    @Override
    public Integer updateProcessFlagById(Integer id, String resultMessage, Integer processFlag) {
        Integer count = smsInfoDao.updateProcessFlagById(id, resultMessage, processFlag);
        logger.info(" update processFlag by id, id={},resultMessage={},processFlag={},count={}  ", id, resultMessage, processFlag, count);
        return count;
    }
}