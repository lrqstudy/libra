package com.libra.exchange.strategy.service;

import com.libra.exchange.strategy.model.AutoTransferConfigModel;

import java.util.List;

/**
 * 自动搬砖配置 操作接口
 */
public interface AutoTransferConfigService {

    /**
     * 保存自动搬砖配置数据
     *
     * @param model
     */
    public int saveAutoTransferConfigModel(AutoTransferConfigModel model);

    /**
     * 批量保存自动搬砖配置数据
     *
     * @param list
     */
    public void saveAutoTransferConfigModelBatch(List<AutoTransferConfigModel> list);

    /**
     * 删除自动搬砖配置数据
     *
     * @param id
     */
    public boolean deleteAutoTransferConfigModel(int id);

    /**
     * 批量删除自动搬砖配置数据
     *
     * @param list
     * @return boolean
     */
    public boolean deleteAutoTransferConfigModelByIds(List<Integer> list);

    /**
     * 更新自动搬砖配置数据
     *
     * @param model
     */
    public boolean updateAutoTransferConfigModel(AutoTransferConfigModel model);

    /**
     * 根据id获取自动搬砖配置数据
     *
     * @param id
     */
    public AutoTransferConfigModel getAutoTransferConfigModelById(int id);

    /**
     * 分页获取自动搬砖配置数据
     *
     * @param model
     * @param page
     * @param pageSize
     */
    public List<AutoTransferConfigModel> getAutoTransferConfigModelForPage(AutoTransferConfigModel model, int page, int pageSize);

    /**
     * 查询所有激活的自动搬砖的配置策略
     * @return
     */
    public List<AutoTransferConfigModel> listAll();
}