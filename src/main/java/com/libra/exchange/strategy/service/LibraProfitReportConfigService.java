package com.libra.exchange.strategy.service;

import com.libra.exchange.strategy.model.LibraProfitReportConfigModel;

import java.util.List;

/**
 * 搬砖利润日报配置表 操作接口
 */
public interface LibraProfitReportConfigService {

    /**
     * 保存搬砖利润日报配置表数据
     *
     * @param model
     */
    public int saveLibraProfitReportConfigModel(LibraProfitReportConfigModel model);

    /**
     * 批量保存搬砖利润日报配置表数据
     *
     * @param list
     */
    public void saveLibraProfitReportConfigModelBatch(List<LibraProfitReportConfigModel> list);

    /**
     * 删除搬砖利润日报配置表数据
     *
     * @param id
     */
    public boolean deleteLibraProfitReportConfigModel(int id);

    /**
     * 批量删除搬砖利润日报配置表数据
     *
     * @param list
     * @return boolean
     */
    public boolean deleteLibraProfitReportConfigModelByIds(List<Integer> list);

    /**
     * 更新搬砖利润日报配置表数据
     *
     * @param model
     */
    public boolean updateLibraProfitReportConfigModel(LibraProfitReportConfigModel model);

    /**
     * 根据id获取搬砖利润日报配置表数据
     *
     * @param id
     */
    public LibraProfitReportConfigModel getLibraProfitReportConfigModelById(int id);

    /**
     * 分页获取搬砖利润日报配置表数据
     *
     * @param model
     * @param page
     * @param pageSize
     */
    public List<LibraProfitReportConfigModel> getLibraProfitReportConfigModelForPage(LibraProfitReportConfigModel model, int page, int pageSize);

    /**
     * @return
     */
    public List<LibraProfitReportConfigModel> getNeedProcessConfigModelList();
}