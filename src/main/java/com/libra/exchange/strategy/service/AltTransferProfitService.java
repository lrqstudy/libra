package com.libra.exchange.strategy.service;

import com.libra.exchange.strategy.model.AltTransferProfitModel;

import java.util.List;

/**
 *  操作接口
 */
public interface AltTransferProfitService {

    /**
     * 保存数据
     *
     * @param model
     */
    public int saveAltTransferProfitModel(AltTransferProfitModel model);

    /**
     * 批量保存数据
     *
     * @param list
     */
    public void saveAltTransferProfitModelBatch(List<AltTransferProfitModel> list);

    /**
     * 删除数据
     *
     * @param id
     */
    public boolean deleteAltTransferProfitModel(int id);

    /**
     * 批量删除数据
     *
     * @param list
     * @return boolean
     */
    public boolean deleteAltTransferProfitModelByIds(List<Integer> list);

    /**
     * 更新数据
     *
     * @param model
     */
    public boolean updateAltTransferProfitModel(AltTransferProfitModel model);

    /**
     * 根据id获取数据
     *
     * @param id
     */
    public AltTransferProfitModel getAltTransferProfitModelById(int id);

    /**
     * 分页获取数据
     *
     * @param model
     * @param page
     * @param pageSize
     */
    public List<AltTransferProfitModel> getAltTransferProfitModelForPage(AltTransferProfitModel model, int page, int pageSize);

}