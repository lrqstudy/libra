package com.libra.exchange.strategy.service;

import com.libra.exchange.strategy.model.LowBuyHighSellStrategyModel;
import com.libra.exchange.strategy.vo.LowBuyHighSellSerachBox;
import org.apache.commons.lang3.tuple.Pair;

import java.util.List;
import java.util.Map;

/**
 * 低买高卖策略配置表 操作接口
 */
public interface LowBuyHighSellStrategyService {
    /**
     * 保存低买高卖策略配置表数据
     *
     * @param model
     */
    public int saveLowBuyHighSellStrategyModel(LowBuyHighSellStrategyModel model);

    /**
     * 批量保存低买高卖策略配置表数据
     *
     * @param list
     */
    public void saveLowBuyHighSellStrategyModelBatch(List<LowBuyHighSellStrategyModel> list);

    /**
     * 删除低买高卖策略配置表数据
     *
     * @param id
     */
    public boolean deleteLowBuyHighSellStrategyModel(int id);

    /**
     * 批量删除低买高卖策略配置表数据
     *
     * @param list
     * @return boolean
     */
    public boolean deleteLowBuyHighSellStrategyModelByIds(List<Integer> list);

    /**
     * 更新低买高卖策略配置表数据
     *
     * @param model
     */
    public boolean updateLowBuyHighSellStrategyModel(LowBuyHighSellStrategyModel model);

    /**
     * 根据id获取低买高卖策略配置表数据
     *
     * @param id
     */
    public LowBuyHighSellStrategyModel getLowBuyHighSellStrategyModelById(int id);

    /**
     * 分页获取低买高卖策略配置表数据
     *
     * @param model
     * @param page
     * @param pageSize
     */
    public List<LowBuyHighSellStrategyModel> getLowBuyHighSellStrategyModelForPage(LowBuyHighSellStrategyModel model, int page, int pageSize);

    /**
     * 根据交易编码获取策略model
     *
     * @param targetCode
     * @return
     */
    public LowBuyHighSellStrategyModel getStrategyModelByTargetCode(String targetCode);


    /**
     * 列出所有生效的交易策略
     *
     * @return
     */
    public List<LowBuyHighSellStrategyModel> listAllActiveStrategyModelList();


    /**
     *  key pair: left platform,  rigth usercode
     *  value : model
     * @return
     */
    public Map<Pair<String,String>,LowBuyHighSellStrategyModel> getPlatformAndUserCodeModelMap();

    /**
     * 根据用户编码查询该用户的配置策略
     * @param userCode
     * @return
     */
    public List<LowBuyHighSellStrategyModel> getAllModelListByUserCode(String userCode);
    /**
     * 策略是否存在
     *
     * @param serachBox
     * @return
     */
    public Integer isStrategyExist(LowBuyHighSellSerachBox serachBox);
}