package com.libra.exchange.strategy.service.impl;

import com.google.common.base.Preconditions;
import com.libra.exchange.strategy.dao.libra.AutoTransferConfigDao;
import com.libra.exchange.strategy.model.AutoTransferConfigModel;
import com.libra.exchange.strategy.service.AutoTransferConfigService;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import javax.annotation.Resource;
import java.util.List;

import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * 自动搬砖配置 操作接口实现类
 */
@Service
public class AutoTransferConfigServiceImpl implements AutoTransferConfigService {

    private Logger logger = LoggerFactory.getLogger(AutoTransferConfigServiceImpl.class);

    @Resource
    private AutoTransferConfigDao autoTransferConfigDao;

    /**
     * 保存自动搬砖配置数据
     *
     * @param model
     */
    @Override
    public int saveAutoTransferConfigModel(AutoTransferConfigModel model) {
        if (model == null) {
            logger.info("保存自动搬砖配置失败，id={}", -1);
            return -1;
        }
        autoTransferConfigDao.saveAutoTransferConfigModel(model);
        logger.info("保存自动搬砖配置成功，id={}", model.getId());
        return model.getId();
    }

    /**
     * 批量保存自动搬砖配置数据
     *
     * @param list
     */
    @Override
    public void saveAutoTransferConfigModelBatch(List<AutoTransferConfigModel> list) {
        if (CollectionUtils.isEmpty(list)) {
            logger.info("批量保存自动搬砖配置失败，size={}", 0);
            return;
        }
        autoTransferConfigDao.saveAutoTransferConfigModelBatch(list);
        logger.info("批量保存自动搬砖配置成功，size={}", list.size());
    }

    /**
     * 删除自动搬砖配置数据
     *
     * @param id
     */
    @Override
    public boolean deleteAutoTransferConfigModel(int id) {
        if (id <= 0) {
            logger.info("删除自动搬砖配置失败，id={},count={}", 0, 0);
            return false;
        }
        int count = autoTransferConfigDao.deleteAutoTransferConfigModel(id);
        logger.info("删除自动搬砖配置成功，id={},count={}", id, count);
        return count > 0;
    }

    /**
     * 批量删除自动搬砖配置数据
     *
     * @param list
     * @return boolean
     */
    @Override
    public boolean deleteAutoTransferConfigModelByIds(List<Integer> list) {
        if (CollectionUtils.isEmpty(list)) {
            logger.info("删除自动搬砖配置失败,count={}", 0);
            return false;
        }
        int count = autoTransferConfigDao.deleteAutoTransferConfigModelByIds(list);
        logger.info("删除自动搬砖配置成功,count={}", count);
        return count == list.size();
    }

    /**
     * 更新自动搬砖配置数据
     *
     * @param model
     */
    @Override
    public boolean updateAutoTransferConfigModel(AutoTransferConfigModel model) {
        if (model == null || model.getId() <= 0) {
            logger.info("更新自动搬砖配置失败，参数不合法");
            return false;
        }
        int count = autoTransferConfigDao.updateAutoTransferConfigModel(model);
        logger.info("更新自动搬砖配置成功，id={},count={}", model.getId(), count);
        return count > 0;
    }

    /**
     * 根据id获取自动搬砖配置数据
     *
     * @param id
     */
    @Override
    public AutoTransferConfigModel getAutoTransferConfigModelById(int id) {
        if (id <= 0) {
            logger.info("根据id获取自动搬砖配置失败，id={}", id);
            return null;
        }
        AutoTransferConfigModel model = autoTransferConfigDao.getAutoTransferConfigModelById(id);
        logger.info("根据id获取自动搬砖配置成功，id={}", id);
        return model;
    }

    /**
     * 分页获取自动搬砖配置数据
     *
     * @param model
     * @param page
     * @param pageSize
     */
    public List<AutoTransferConfigModel> getAutoTransferConfigModelForPage(AutoTransferConfigModel model, int page, int pageSize) {
        Preconditions.checkArgument(page >= 0 && pageSize > 0 && model != null, "参数不合法");
        Map<String, Object> params = Maps.newHashMap();
        params.put("page", page);
        params.put("pageSize", pageSize);
        List<AutoTransferConfigModel> modelList = autoTransferConfigDao.getAutoTransferConfigModelForPage(params);
        if (CollectionUtils.isEmpty(modelList)) {
            modelList = Lists.newArrayList();
        }
        logger.info("分页获取自动搬砖配置数据，page={},pageSize={}", page, pageSize);
        return modelList;
    }

    @Override
    public List<AutoTransferConfigModel> listAll() {
        List<AutoTransferConfigModel> modelList = autoTransferConfigDao.listAll();
        logger.info(" list all size ={} ", modelList.size());
        return modelList;
    }
}