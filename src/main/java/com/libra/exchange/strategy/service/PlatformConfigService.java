package com.libra.exchange.strategy.service;

import com.libra.exchange.strategy.model.PlatformConfigModel;
import org.apache.commons.lang3.tuple.Pair;

import java.util.List;
import java.util.Map;

/**
 *  操作接口
 */
public interface PlatformConfigService {

    /**
     * 保存数据
     *
     * @param model
     */
    public int savePlatformConfigModel(PlatformConfigModel model);

    /**
     * 批量保存数据
     *
     * @param list
     */
    public void savePlatformConfigModelBatch(List<PlatformConfigModel> list);

    /**
     * 删除数据
     *
     * @param id
     */
    public boolean deletePlatformConfigModel(int id);

    /**
     * 批量删除数据
     *
     * @param list
     * @return boolean
     */
    public boolean deletePlatformConfigModelByIds(List<Integer> list);

    /**
     * 更新数据
     *
     * @param model
     */
    public boolean updatePlatformConfigModel(PlatformConfigModel model);

    /**
     * 根据id获取数据
     *
     * @param id
     */
    public PlatformConfigModel getPlatformConfigModelById(int id);

    /**
     * 分页获取数据
     *
     * @param model
     * @param page
     * @param pageSize
     */
    public List<PlatformConfigModel> getPlatformConfigModelForPage(PlatformConfigModel model, int page, int pageSize);

    /**
     * 根据用户编码查询平台配置信息
     * @param userCode
     * @return
     */
    public List<PlatformConfigModel> getPlatConfigModelByUserCode(String userCode);

    /**
     * key pair: left: platform   right:user_code
     * value model
     * @return
     */
    public Map<Pair<String,String>,PlatformConfigModel> getPlatformUserCodeAndModelMap();

    /**
     * 根据平台编码查询平台配置
     * @param platform
     * @return
     */
    public List<PlatformConfigModel> getPlatformConfigModelByPlatform(String platform);
}