package com.libra.exchange.strategy.service;

import com.libra.exchange.strategy.model.AltTransferErrorHandleModel;

import java.util.List;

/**
 * 搬砖错误单报错处理 操作接口
 */
public interface AltTransferErrorHandleService {

    /**
     * 保存搬砖错误单报错处理数据
     *
     * @param model
     */
    public int saveAltTransferErrorHandleModel(AltTransferErrorHandleModel model);

    /**
     * 批量保存搬砖错误单报错处理数据
     *
     * @param list
     */
    public void saveAltTransferErrorHandleModelBatch(List<AltTransferErrorHandleModel> list);

    /**
     * 删除搬砖错误单报错处理数据
     *
     * @param id
     */
    public boolean deleteAltTransferErrorHandleModel(int id);

    /**
     * 批量删除搬砖错误单报错处理数据
     *
     * @param list
     * @return boolean
     */
    public boolean deleteAltTransferErrorHandleModelByIds(List<Integer> list);

    /**
     * 更新搬砖错误单报错处理数据
     *
     * @param model
     */
    public boolean updateAltTransferErrorHandleModel(AltTransferErrorHandleModel model);

    /**
     * 根据id获取搬砖错误单报错处理数据
     *
     * @param id
     */
    public AltTransferErrorHandleModel getAltTransferErrorHandleModelById(int id);

    /**
     * 分页获取搬砖错误单报错处理数据
     *
     * @param model
     * @param page
     * @param pageSize
     */
    public List<AltTransferErrorHandleModel> getAltTransferErrorHandleModelForPage(AltTransferErrorHandleModel model, int page, int pageSize);

    /**
     *
     * 获取需要补单的交易
     * @return
     */
    public List<AltTransferErrorHandleModel> getNeedHandleModelList();

    /**
     * 回写处理标记
     * @param id
     * @param processFlag
     * @param handleType
     * @param resultId
     * @return
     */
    public Integer updateResultIdAndProcessFlag(Integer id,Integer processFlag,Integer handleType,String resultId);
}