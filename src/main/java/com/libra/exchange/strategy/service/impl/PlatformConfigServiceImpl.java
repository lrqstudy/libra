package com.libra.exchange.strategy.service.impl;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.libra.common.util.Encode;
import com.libra.exchange.strategy.dao.libra.PlatformConfigDao;
import com.libra.exchange.strategy.model.PlatformConfigModel;
import com.libra.exchange.strategy.service.PlatformConfigService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 *  操作接口实现类
 */
@Service
public class PlatformConfigServiceImpl implements PlatformConfigService {

    private Logger logger = LoggerFactory.getLogger(PlatformConfigServiceImpl.class);

    @Resource
    private PlatformConfigDao platformConfigDao;

    /**
     * 保存数据
     *
     * @param model
     */
    @Override
    public int savePlatformConfigModel(PlatformConfigModel model) {
        String apiKey=Encode.encode(model.getApiKey());
        String apiSecret=Encode.encode(model.getApiSecret());
        model.setApiKey(apiKey);
        model.setApiSecret(apiSecret);
        if (model == null) {
            logger.info("保存失败，id={}", -1);
            return -1;
        }
        platformConfigDao.savePlatformConfigModel(model);
        logger.info("保存成功，id={}", model.getId());
        return model.getId();
    }

    /**
     * 批量保存数据
     *
     * @param list
     */
    @Override
    public void savePlatformConfigModelBatch(List<PlatformConfigModel> list) {
       if (CollectionUtils.isEmpty(list)) {
            logger.info("批量保存失败，size={}", 0);
            return;
        }
        for (PlatformConfigModel model : list) {
            String apiKey=Encode.encode(model.getApiKey());
            String apiSecret=Encode.encode(model.getApiSecret());
            model.setApiKey(apiKey);
            model.setApiSecret(apiSecret);
        }
        platformConfigDao.savePlatformConfigModelBatch(list);
        logger.info("批量保存成功，size={}", list.size());
    }

    /**
     * 删除数据
     *
     * @param id
     */
    @Override
    public boolean deletePlatformConfigModel(int id) {
        if (id <= 0) {
            logger.info("删除失败，id={},count={}", 0, 0);
            return false;
        }
        int count = platformConfigDao.deletePlatformConfigModel(id);
        logger.info("删除成功，id={},count={}", id, count);
        return count > 0;
    }

    /**
     * 批量删除数据
     *
     * @param list
     * @return boolean
     */
    @Override
    public boolean deletePlatformConfigModelByIds(List<Integer> list){
        if (CollectionUtils.isEmpty(list)) {
            logger.info("删除失败,count={}",0);
            return false;
        }
        int count = platformConfigDao.deletePlatformConfigModelByIds(list);
        logger.info("删除成功,count={}", count);
        return count==list.size();
    }

    /**
     * 更新数据
     *
     * @param model
     */
    @Override
    public boolean updatePlatformConfigModel(PlatformConfigModel model) {
        if (model == null || model.getId() <= 0) {
            logger.info("更新失败，参数不合法");
            return false;
        }
        int count = platformConfigDao.updatePlatformConfigModel(model);
        logger.info("更新成功，id={},count={}", model.getId(), count);
        return count > 0;
    }

    /**
     * 根据id获取数据
     *
     * @param id
     */
    @Override
    public PlatformConfigModel getPlatformConfigModelById(int id){
        if (id <= 0) {
            logger.info("根据id获取失败，id={}", id);
            return null;
        }
        PlatformConfigModel model = platformConfigDao.getPlatformConfigModelById(id);
        logger.info("根据id获取成功，id={}", id);
        return model;
    }

    /**
     * 分页获取数据
     *
     * @param model
     * @param page
     * @param pageSize
     */
    public List<PlatformConfigModel> getPlatformConfigModelForPage(PlatformConfigModel model,int page,int pageSize){
        Preconditions.checkArgument(page >= 0 && pageSize >0 && model != null, "参数不合法");
        Map<String,Object> params = Maps.newHashMap();
        params.put("page", page);
        params.put("pageSize", pageSize);
        List<PlatformConfigModel> modelList = platformConfigDao.getPlatformConfigModelForPage(params);
        if (CollectionUtils.isEmpty(modelList)) {
            modelList = Lists.newArrayList();
        }
        logger.info("分页获取数据，page={},pageSize={}", page, pageSize);
        return modelList;
    }

    @Override
    public List<PlatformConfigModel> getPlatConfigModelByUserCode(String userCode) {
        List<PlatformConfigModel> platformConfigModelList = platformConfigDao.getPlatConfigModelByUserCode(userCode);
        logger.info(" 根据 userCode={} 查询 config list size ={}",userCode,platformConfigModelList.size());
        if (CollectionUtils.isEmpty(platformConfigModelList)){
            return Lists.newArrayList();
        }
        decode(platformConfigModelList);
        return platformConfigModelList;
    }

    private void decode(List<PlatformConfigModel> platformConfigModelList ){
        for (PlatformConfigModel configModel : platformConfigModelList) {
            decode(configModel);
        }
    }

    private void decode(PlatformConfigModel configModel) {
        String apiKey = Encode.decode(configModel.getApiKey());
        String apiSecret= Encode.decode(configModel.getApiSecret());
        configModel.setApiKey(apiKey);
        configModel.setApiSecret(apiSecret);
    }

    @Override
    public Map<Pair<String, String>, PlatformConfigModel> getPlatformUserCodeAndModelMap() {
        List<PlatformConfigModel> platformConfigModels = platformConfigDao.listAll();
        decode(platformConfigModels);
        if (CollectionUtils.isEmpty(platformConfigModels)){
            return Maps.newHashMap();
        }
        Map<Pair<String, String>, PlatformConfigModel> platformUserCodeAndModelMap=Maps.newHashMap();
        for (PlatformConfigModel platformConfigModel : platformConfigModels) {
            platformUserCodeAndModelMap.put(Pair.of(platformConfigModel.getPlatform(),platformConfigModel.getUserCode()),platformConfigModel);
        }
        return platformUserCodeAndModelMap;
    }

    @Override
    public List<PlatformConfigModel> getPlatformConfigModelByPlatform(String platform) {
        List<PlatformConfigModel> platformConfigModelList = platformConfigDao.getPlatformConfigModelByPlatform(platform);
        decode(platformConfigModelList);
        logger.info(" get config use platform={} , result size ={} ",platform,platformConfigModelList.size());
        return platformConfigModelList;
    }
}