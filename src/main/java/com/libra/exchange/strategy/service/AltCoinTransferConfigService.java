package com.libra.exchange.strategy.service;

import com.libra.exchange.strategy.model.AltCoinTransferConfigModel;

import java.util.List;

/**
 * altcoin 自动搬砖配置 操作接口
 */
public interface AltCoinTransferConfigService {

    /**
     * 保存altcoin 自动搬砖配置数据
     *
     * @param model
     */
    public int saveAltCoinTransferConfigModel(AltCoinTransferConfigModel model);

    /**
     * 批量保存altcoin 自动搬砖配置数据
     *
     * @param list
     */
    public void saveAltCoinTransferConfigModelBatch(List<AltCoinTransferConfigModel> list);

    /**
     * 删除altcoin 自动搬砖配置数据
     *
     * @param id
     */
    public boolean deleteAltCoinTransferConfigModel(int id);

    /**
     * 批量删除altcoin 自动搬砖配置数据
     *
     * @param list
     * @return boolean
     */
    public boolean deleteAltCoinTransferConfigModelByIds(List<Integer> list);

    /**
     * 更新altcoin 自动搬砖配置数据
     *
     * @param model
     */
    public boolean updateAltCoinTransferConfigModel(AltCoinTransferConfigModel model);

    /**
     * 根据id获取altcoin 自动搬砖配置数据
     *
     * @param id
     */
    public AltCoinTransferConfigModel getAltCoinTransferConfigModelById(int id);

    /**
     * 分页获取altcoin 自动搬砖配置数据
     *
     * @param model
     * @param page
     * @param pageSize
     */
    public List<AltCoinTransferConfigModel> getAltCoinTransferConfigModelForPage(AltCoinTransferConfigModel model, int page, int pageSize);


    /**
     * 查找所有的配置
     * @return
     */
    public List<AltCoinTransferConfigModel> listAll();


    /**
     * 查询某个用户的所有配置
     * @return
     */
    public List<AltCoinTransferConfigModel> getAllModelListByUserCode(String userCode);

    /**
     * 根据 idlist 更新激活状态
     * @param idList
     * @param activeStatus
     * @return
     */
    public Integer updateActiveStatusByIdList(List<Integer> idList,Integer activeStatus);

    /**
     *
     * @param activeStatus
     * @return
     */
    public List<AltCoinTransferConfigModel> getModdelListByStatus(Integer activeStatus);
}