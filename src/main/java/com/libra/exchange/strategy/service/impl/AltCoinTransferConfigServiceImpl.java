package com.libra.exchange.strategy.service.impl;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.libra.exchange.strategy.dao.libra.AltCoinTransferConfigDao;
import com.libra.exchange.strategy.model.AltCoinTransferConfigModel;
import com.libra.exchange.strategy.service.AltCoinTransferConfigService;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * altcoin 自动搬砖配置 操作接口实现类
 */
@Service
public class AltCoinTransferConfigServiceImpl implements AltCoinTransferConfigService {

    private Logger logger = LoggerFactory.getLogger(AltCoinTransferConfigServiceImpl.class);

    @Resource
    private AltCoinTransferConfigDao altCoinTransferConfigDao;

    /**
     * 保存altcoin 自动搬砖配置数据
     *
     * @param model
     */
    @Override
    public int saveAltCoinTransferConfigModel(AltCoinTransferConfigModel model) {
        if (model == null) {
            logger.info("保存altcoin 自动搬砖配置失败，id={}", -1);
            return -1;
        }
        altCoinTransferConfigDao.saveAltCoinTransferConfigModel(model);
        logger.info("保存altcoin 自动搬砖配置成功，id={}", model.getId());
        return model.getId();
    }

    /**
     * 批量保存altcoin 自动搬砖配置数据
     *
     * @param list
     */
    @Override
    public void saveAltCoinTransferConfigModelBatch(List<AltCoinTransferConfigModel> list) {
       if (CollectionUtils.isEmpty(list)) {
            logger.info("批量保存altcoin 自动搬砖配置失败，size={}", 0);
            return;
        }
        altCoinTransferConfigDao.saveAltCoinTransferConfigModelBatch(list);
        logger.info("批量保存altcoin 自动搬砖配置成功，size={}", list.size());
    }

    /**
     * 删除altcoin 自动搬砖配置数据
     *
     * @param id
     */
    @Override
    public boolean deleteAltCoinTransferConfigModel(int id) {
        if (id <= 0) {
            logger.info("删除altcoin 自动搬砖配置失败，id={},count={}", 0, 0);
            return false;
        }
        int count = altCoinTransferConfigDao.deleteAltCoinTransferConfigModel(id);
        logger.info("删除altcoin 自动搬砖配置成功，id={},count={}", id, count);
        return count > 0;
    }

    /**
     * 批量删除altcoin 自动搬砖配置数据
     *
     * @param list
     * @return boolean
     */
    @Override
    public boolean deleteAltCoinTransferConfigModelByIds(List<Integer> list){
        if (CollectionUtils.isEmpty(list)) {
            logger.info("删除altcoin 自动搬砖配置失败,count={}",0);
            return false;
        }
        int count = altCoinTransferConfigDao.deleteAltCoinTransferConfigModelByIds(list);
        logger.info("删除altcoin 自动搬砖配置成功,count={}", count);
        return count==list.size();
    }

    /**
     * 更新altcoin 自动搬砖配置数据
     *
     * @param model
     */
    @Override
    public boolean updateAltCoinTransferConfigModel(AltCoinTransferConfigModel model) {
        if (model == null || model.getId() <= 0) {
            logger.info("更新altcoin 自动搬砖配置失败，参数不合法");
            return false;
        }
        int count = altCoinTransferConfigDao.updateAltCoinTransferConfigModel(model);
        logger.info("更新altcoin 自动搬砖配置成功，id={},count={}", model.getId(), count);
        return count > 0;
    }

    /**
     * 根据id获取altcoin 自动搬砖配置数据
     *
     * @param id
     */
    @Override
    public AltCoinTransferConfigModel getAltCoinTransferConfigModelById(int id){
        if (id <= 0) {
            logger.info("根据id获取altcoin 自动搬砖配置失败，id={}", id);
            return null;
        }
        AltCoinTransferConfigModel model = altCoinTransferConfigDao.getAltCoinTransferConfigModelById(id);
        logger.info("根据id获取altcoin 自动搬砖配置成功，id={}", id);
        return model;
    }

    /**
     * 分页获取altcoin 自动搬砖配置数据
     *
     * @param model
     * @param page
     * @param pageSize
     */
    public List<AltCoinTransferConfigModel> getAltCoinTransferConfigModelForPage(AltCoinTransferConfigModel model,int page,int pageSize){
        Preconditions.checkArgument(page >= 0 && pageSize >0 && model != null, "参数不合法");
        Map<String,Object> params = Maps.newHashMap();
        params.put("page", page);
        params.put("pageSize", pageSize);
        List<AltCoinTransferConfigModel> modelList = altCoinTransferConfigDao.getAltCoinTransferConfigModelForPage(params);
        if (CollectionUtils.isEmpty(modelList)) {
            modelList = Lists.newArrayList();
        }
        logger.info("分页获取altcoin 自动搬砖配置数据，page={},pageSize={}", page, pageSize);
        return modelList;
    }

    @Override
    public List<AltCoinTransferConfigModel> listAll() {
        List<AltCoinTransferConfigModel> modelList = altCoinTransferConfigDao.listAll();
        logger.info(" list all size ={} ",modelList.size());
        return modelList;
    }

    @Override
    public List<AltCoinTransferConfigModel> getAllModelListByUserCode(String userCode) {
        List<AltCoinTransferConfigModel> configModelList = altCoinTransferConfigDao.getAllModelListByUserCode(userCode);
        logger.info(" 根据 userCode ={} 查询该用户的所有配置list size={}",userCode,configModelList.size());
        return configModelList;
    }

    @Override
    public Integer updateActiveStatusByIdList(List<Integer> idList, Integer activeStatus) {
        Integer count = altCoinTransferConfigDao.updateActiveStatusByIdList(idList, activeStatus);
        logger.info(" update status by id list ={}, activeStatus={}, result ={}",idList,activeStatus,count);
        return count;
    }

    @Override
    public List<AltCoinTransferConfigModel> getModdelListByStatus(Integer activeStatus) {
        List<AltCoinTransferConfigModel> modelList = altCoinTransferConfigDao.getModdelListByStatus(activeStatus);
        logger.info(" get list by activeStatus={}, result size={}",activeStatus,modelList.size());
        return modelList;
    }
}