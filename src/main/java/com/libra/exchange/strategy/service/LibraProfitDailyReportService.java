package com.libra.exchange.strategy.service;

import com.libra.exchange.strategy.model.LibraProfitDailyReportModel;

import java.util.List;

/**
 * 搬砖利润日报表 操作接口
 */
public interface LibraProfitDailyReportService {

    /**
     * 保存搬砖利润日报表数据
     *
     * @param model
     */
    public int saveLibraProfitDailyReportModel(LibraProfitDailyReportModel model);

    /**
     * 批量保存搬砖利润日报表数据
     *
     * @param list
     */
    public void saveLibraProfitDailyReportModelBatch(List<LibraProfitDailyReportModel> list);

    /**
     * 删除搬砖利润日报表数据
     *
     * @param id
     */
    public boolean deleteLibraProfitDailyReportModel(int id);

    /**
     * 批量删除搬砖利润日报表数据
     *
     * @param list
     * @return boolean
     */
    public boolean deleteLibraProfitDailyReportModelByIds(List<Integer> list);

    /**
     * 更新搬砖利润日报表数据
     *
     * @param model
     */
    public boolean updateLibraProfitDailyReportModel(LibraProfitDailyReportModel model);

    /**
     * 根据id获取搬砖利润日报表数据
     *
     * @param id
     */
    public LibraProfitDailyReportModel getLibraProfitDailyReportModelById(int id);

    /**
     * 分页获取搬砖利润日报表数据
     *
     * @param model
     * @param page
     * @param pageSize
     */
    public List<LibraProfitDailyReportModel> getLibraProfitDailyReportModelForPage(LibraProfitDailyReportModel model, int page, int pageSize);

}