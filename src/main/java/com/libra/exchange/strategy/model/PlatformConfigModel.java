package com.libra.exchange.strategy.model;

/**
 * 
 */
public class PlatformConfigModel {

    /**
     * 主键id
     */
    private Integer id;

    /**
     * 用户编码
     */
    private String userCode;

    /**
     * 平台名称
     */
    private String platform;

    /**
     * apikey
     */
    private String apiKey;

    /**
     * 
     */
    private String apiSecret;

    /**
     * 激活状态:0=未激活,1=已激活
     */
    private Integer activeStatus;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public String getApiSecret() {
        return apiSecret;
    }

    public void setApiSecret(String apiSecret) {
        this.apiSecret = apiSecret;
    }

    public Integer getActiveStatus() {
        return activeStatus;
    }

    public void setActiveStatus(Integer activeStatus) {
        this.activeStatus = activeStatus;
    }

}
