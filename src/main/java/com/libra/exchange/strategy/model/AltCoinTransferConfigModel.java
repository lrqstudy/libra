package com.libra.exchange.strategy.model;

import java.math.BigDecimal;

/**
 * altcoin 自动搬砖配置
 */
public class AltCoinTransferConfigModel {

    /**
     * 主键id
     */
    private Integer id;
    /**
     * 用户编码
     */
    private String userCode;

    /**
     * 交易平台
     */
    private String platform;

    /**
     * 现金账户编码
     */
    private String cashCurrency;

    /**
     * 现金账户充值地址
     */
    private String cashCurrencyDepositAddress;

    /**
     * 现金账户充值备注
     */
    private String cashCurrencyDepositMemo;

    /**
     * 现金账户提现阈值
     */
    private BigDecimal cashCurrencyWithdrawLimitAmount;

    /**
     * 交易品种编码
     */
    private String targetCode;

    /**
     * 交易品种充值地址
     */
    private String targetCodeDepositAddress;

    /**
     * 交易品种充值备注
     */
    private String targetCodeDepositMemo;

    /**
     * 交易品种提现阈值
     */
    private BigDecimal targetCodeWithdrawLimitAmount;

    /**
     * 最大卖出量
     */
    private BigDecimal maxSellAmount;

    /**
     * 最大买入数量
     */
    private BigDecimal maxBuyAmount;

    /**
     * 交易对
     */
    private String currencyPair;

    /**
     * 激活状态 0=未激活,1=已激活
     */
    private Integer activeStatus;
    /**
     * 差价比率
     */
    private BigDecimal diffRate;

    /**
     * 现金账户最低限额,超过这个任务停止
     */
    private BigDecimal stopCashLimitAmount;

    /**
     * 标的账户最低限额,超过这个任务停止
     */
    private BigDecimal stopTargetCodeLimitAmount;
    /**
     * 精度位数
     */
    private Integer precisionSize;
    /**
     * 最小的交易量
     */
    private BigDecimal minTradeAmount;



    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public String getCashCurrency() {
        return cashCurrency;
    }

    public void setCashCurrency(String cashCurrency) {
        this.cashCurrency = cashCurrency;
    }

    public String getCashCurrencyDepositAddress() {
        return cashCurrencyDepositAddress;
    }

    public void setCashCurrencyDepositAddress(String cashCurrencyDepositAddress) {
        this.cashCurrencyDepositAddress = cashCurrencyDepositAddress;
    }

    public String getCashCurrencyDepositMemo() {
        return cashCurrencyDepositMemo;
    }

    public void setCashCurrencyDepositMemo(String cashCurrencyDepositMemo) {
        this.cashCurrencyDepositMemo = cashCurrencyDepositMemo;
    }

    public BigDecimal getCashCurrencyWithdrawLimitAmount() {
        return cashCurrencyWithdrawLimitAmount;
    }

    public void setCashCurrencyWithdrawLimitAmount(BigDecimal cashCurrencyWithdrawLimitAmount) {
        this.cashCurrencyWithdrawLimitAmount = cashCurrencyWithdrawLimitAmount;
    }

    public String getTargetCode() {
        return targetCode;
    }

    public void setTargetCode(String targetCode) {
        this.targetCode = targetCode;
    }

    public String getTargetCodeDepositAddress() {
        return targetCodeDepositAddress;
    }

    public void setTargetCodeDepositAddress(String targetCodeDepositAddress) {
        this.targetCodeDepositAddress = targetCodeDepositAddress;
    }

    public String getTargetCodeDepositMemo() {
        return targetCodeDepositMemo;
    }

    public void setTargetCodeDepositMemo(String targetCodeDepositMemo) {
        this.targetCodeDepositMemo = targetCodeDepositMemo;
    }

    public BigDecimal getTargetCodeWithdrawLimitAmount() {
        return targetCodeWithdrawLimitAmount;
    }

    public void setTargetCodeWithdrawLimitAmount(BigDecimal targetCodeWithdrawLimitAmount) {
        this.targetCodeWithdrawLimitAmount = targetCodeWithdrawLimitAmount;
    }

    public BigDecimal getMaxSellAmount() {
        return maxSellAmount;
    }

    public void setMaxSellAmount(BigDecimal maxSellAmount) {
        this.maxSellAmount = maxSellAmount;
    }

    public BigDecimal getMaxBuyAmount() {
        return maxBuyAmount;
    }

    public void setMaxBuyAmount(BigDecimal maxBuyAmount) {
        this.maxBuyAmount = maxBuyAmount;
    }

    public String getCurrencyPair() {
        return currencyPair;
    }

    public void setCurrencyPair(String currencyPair) {
        this.currencyPair = currencyPair;
    }

    public Integer getActiveStatus() {
        return activeStatus;
    }

    public void setActiveStatus(Integer activeStatus) {
        this.activeStatus = activeStatus;
    }

    public BigDecimal getDiffRate() {
        return diffRate;
    }

    public void setDiffRate(BigDecimal diffRate) {
        this.diffRate = diffRate;
    }

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }

    public BigDecimal getStopCashLimitAmount() {
        return stopCashLimitAmount;
    }

    public void setStopCashLimitAmount(BigDecimal stopCashLimitAmount) {
        this.stopCashLimitAmount = stopCashLimitAmount;
    }

    public BigDecimal getStopTargetCodeLimitAmount() {
        return stopTargetCodeLimitAmount;
    }

    public void setStopTargetCodeLimitAmount(BigDecimal stopTargetCodeLimitAmount) {
        this.stopTargetCodeLimitAmount = stopTargetCodeLimitAmount;
    }

    public Integer getPrecisionSize() {
        return precisionSize;
    }

    public void setPrecisionSize(Integer precisionSize) {
        this.precisionSize = precisionSize;
    }

    public BigDecimal getMinTradeAmount() {
        return minTradeAmount;
    }

    public void setMinTradeAmount(BigDecimal minTradeAmount) {
        this.minTradeAmount = minTradeAmount;
    }
}
