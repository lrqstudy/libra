package com.libra.exchange.strategy.model;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 搬砖利润统计表
 */
public class AltTransferProfitModel {

    /**
     * 主键 id
     */
    private Integer id;

    /**
     * 用户编码
     */
    private String userCode;

    /**
     * 买入平台
     */
    private String buyPlatform;

    /**
     * 买入价
     */
    private BigDecimal buyPrice;

    /**
     * 交易对
     */
    private String currencyPair;

    /**
     * 现金账户
     */
    private String cashCurrency;

    /**
     * 标的账户
     */
    private String targetCode;

    /**
     * 卖出平台
     */
    private String sellPlatform;
    /**
     * 卖出价
     */
    private BigDecimal sellPrice;

    /**
     * 交易量
     */
    private BigDecimal tradeAmount;

    /**
     * 买单 id
     */
    private String buyResultId;

    /**
     * 卖单 id
     */
    private String sellResultId;

    /**
     * 利润
     */
    private BigDecimal profitAmount;

    /**
     * 处理状态：0=未处理，1=已处理
     */
    private Integer processFlag;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 差价比率
     */
    private BigDecimal diffRate;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }

    public String getBuyPlatform() {
        return buyPlatform;
    }

    public void setBuyPlatform(String buyPlatform) {
        this.buyPlatform = buyPlatform;
    }

    public BigDecimal getBuyPrice() {
        return buyPrice;
    }

    public void setBuyPrice(BigDecimal buyPrice) {
        this.buyPrice = buyPrice;
    }

    public String getCurrencyPair() {
        return currencyPair;
    }

    public void setCurrencyPair(String currencyPair) {
        this.currencyPair = currencyPair;
    }

    public String getCashCurrency() {
        return cashCurrency;
    }

    public void setCashCurrency(String cashCurrency) {
        this.cashCurrency = cashCurrency;
    }

    public String getTargetCode() {
        return targetCode;
    }

    public void setTargetCode(String targetCode) {
        this.targetCode = targetCode;
    }

    public String getSellPlatform() {
        return sellPlatform;
    }

    public void setSellPlatform(String sellPlatform) {
        this.sellPlatform = sellPlatform;
    }

    public BigDecimal getTradeAmount() {
        return tradeAmount;
    }

    public void setTradeAmount(BigDecimal tradeAmount) {
        this.tradeAmount = tradeAmount;
    }

    public String getBuyResultId() {
        return buyResultId;
    }

    public void setBuyResultId(String buyResultId) {
        this.buyResultId = buyResultId;
    }

    public String getSellResultId() {
        return sellResultId;
    }

    public void setSellResultId(String sellResultId) {
        this.sellResultId = sellResultId;
    }

    public BigDecimal getProfitAmount() {
        return profitAmount;
    }

    public void setProfitAmount(BigDecimal profitAmount) {
        this.profitAmount = profitAmount;
    }

    public Integer getProcessFlag() {
        return processFlag;
    }

    public void setProcessFlag(Integer processFlag) {
        this.processFlag = processFlag;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public BigDecimal getDiffRate() {
        return diffRate;
    }

    public void setDiffRate(BigDecimal diffRate) {
        this.diffRate = diffRate;
    }

    public BigDecimal getSellPrice() {
        return sellPrice;
    }

    public void setSellPrice(BigDecimal sellPrice) {
        this.sellPrice = sellPrice;
    }
}
