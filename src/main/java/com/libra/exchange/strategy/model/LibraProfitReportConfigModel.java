package com.libra.exchange.strategy.model;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 搬砖利润日报配置表
 */
public class LibraProfitReportConfigModel {

    /**
     * 主键 id
     */
    private Integer id;

    /**
     * 用户编码
     */
    private String userCode;

    /**
     * 交易对
     */
    private String currencyPair;

    /**
     * 现金账户
     */
    private String cashCurrency;

    /**
     * 标的账户
     */
    private String targetCode;


    /**
     * 处理状态：0=未处理，1=已处理
     */
    private Integer processFlag;

    /**
     * 标的账户期初金额
     */
    private BigDecimal beginTargetCodeAmount;

    /**
     * 现金账户期初金额
     */
    private BigDecimal beginCashCurrencyAmount;

    /**
     * 期初手续费金额
     */
    private BigDecimal beginFeeAmount;

    /**
     * 周期
     */
    private Integer period;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 最后更新时间
     */
    private Date lastUpdateTime;
    /**
     * 法币计算手续费折算比特币价格
     */
    private BigDecimal fiatFeePrice;
    /**
     * 平台代币计算手续费比特币价格
     */
    private BigDecimal platformFeePrice;
    /**
     * 短信接收人 list
     */
    private String messageReceiverList;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }

    public String getCurrencyPair() {
        return currencyPair;
    }

    public void setCurrencyPair(String currencyPair) {
        this.currencyPair = currencyPair;
    }

    public String getCashCurrency() {
        return cashCurrency;
    }

    public void setCashCurrency(String cashCurrency) {
        this.cashCurrency = cashCurrency;
    }

    public String getTargetCode() {
        return targetCode;
    }

    public void setTargetCode(String targetCode) {
        this.targetCode = targetCode;
    }


    public Integer getProcessFlag() {
        return processFlag;
    }

    public void setProcessFlag(Integer processFlag) {
        this.processFlag = processFlag;
    }

    public BigDecimal getBeginTargetCodeAmount() {
        return beginTargetCodeAmount;
    }

    public void setBeginTargetCodeAmount(BigDecimal beginTargetCodeAmount) {
        this.beginTargetCodeAmount = beginTargetCodeAmount;
    }

    public BigDecimal getBeginCashCurrencyAmount() {
        return beginCashCurrencyAmount;
    }

    public void setBeginCashCurrencyAmount(BigDecimal beginCashCurrencyAmount) {
        this.beginCashCurrencyAmount = beginCashCurrencyAmount;
    }

    public BigDecimal getBeginFeeAmount() {
        return beginFeeAmount;
    }

    public void setBeginFeeAmount(BigDecimal beginFeeAmount) {
        this.beginFeeAmount = beginFeeAmount;
    }

    public Integer getPeriod() {
        return period;
    }

    public void setPeriod(Integer period) {
        this.period = period;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getLastUpdateTime() {
        return lastUpdateTime;
    }

    public void setLastUpdateTime(Date lastUpdateTime) {
        this.lastUpdateTime = lastUpdateTime;
    }

    public BigDecimal getFiatFeePrice() {
        return fiatFeePrice;
    }

    public void setFiatFeePrice(BigDecimal fiatFeePrice) {
        this.fiatFeePrice = fiatFeePrice;
    }

    public BigDecimal getPlatformFeePrice() {
        return platformFeePrice;
    }

    public void setPlatformFeePrice(BigDecimal platformFeePrice) {
        this.platformFeePrice = platformFeePrice;
    }

    public String getMessageReceiverList() {
        return messageReceiverList;
    }

    public void setMessageReceiverList(String messageReceiverList) {
        this.messageReceiverList = messageReceiverList;
    }
}
