package com.libra.exchange.strategy.model;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 搬砖错误单报错处理
 */
public class AltTransferErrorHandleModel {

    /**
     * 主键 id
     */
    private Integer id;

    /**
     * 用户编码
     */
    private String userCode;

    /**
     * 交易平台
     */
    private String platform;

    /**
     * 交易价格
     */
    private BigDecimal tradePrice;
    /**
     * 交易方向: buy or sell
     */
    private String side;

    /**
     * 交易对
     */
    private String currencyPair;

    /**
     * 现金账户
     */
    private String cashCurrency;

    /**
     * 标的账户
     */
    private String targetCode;

    /**
     * 交易量
     */
    private BigDecimal tradeAmount;

    /**
     * 买单 id
     */
    private String resultId;

    /**
     * 处理状态：0=未处理，1=已处理
     */
    private Integer processFlag;

    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 最后更新时间
     */
    private Date lastUpdateTime;
    /**
     * 精度保留几位小数
     */
    private Integer precisionSize;
    /**
     * 错误消息
     */
    private String errorMessage;
    /**
     * 处理方式:
     */
    private Integer handleType;
    /**
     * 订单来源
     */
    private Integer orderSource;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public String getSide() {
        return side;
    }

    public void setSide(String side) {
        this.side = side;
    }

    public String getCurrencyPair() {
        return currencyPair;
    }

    public void setCurrencyPair(String currencyPair) {
        this.currencyPair = currencyPair;
    }

    public String getCashCurrency() {
        return cashCurrency;
    }

    public void setCashCurrency(String cashCurrency) {
        this.cashCurrency = cashCurrency;
    }

    public String getTargetCode() {
        return targetCode;
    }

    public void setTargetCode(String targetCode) {
        this.targetCode = targetCode;
    }

    public BigDecimal getTradeAmount() {
        return tradeAmount;
    }

    public void setTradeAmount(BigDecimal tradeAmount) {
        this.tradeAmount = tradeAmount;
    }

    public String getResultId() {
        return resultId;
    }

    public void setResultId(String resultId) {
        this.resultId = resultId;
    }

    public Integer getProcessFlag() {
        return processFlag;
    }

    public void setProcessFlag(Integer processFlag) {
        this.processFlag = processFlag;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getLastUpdateTime() {
        return lastUpdateTime;
    }

    public void setLastUpdateTime(Date lastUpdateTime) {
        this.lastUpdateTime = lastUpdateTime;
    }

    public BigDecimal getTradePrice() {
        return tradePrice;
    }

    public void setTradePrice(BigDecimal tradePrice) {
        this.tradePrice = tradePrice;
    }

    public Integer getPrecisionSize() {
        return precisionSize;
    }

    public void setPrecisionSize(Integer precisionSize) {
        this.precisionSize = precisionSize;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public Integer getHandleType() {
        return handleType;
    }

    public void setHandleType(Integer handleType) {
        this.handleType = handleType;
    }

    public Integer getOrderSource() {
        return orderSource;
    }

    public void setOrderSource(Integer orderSource) {
        this.orderSource = orderSource;
    }
}
