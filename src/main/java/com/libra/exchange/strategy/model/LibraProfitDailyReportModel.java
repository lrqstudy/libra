package com.libra.exchange.strategy.model;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 搬砖利润日报表
 */
public class LibraProfitDailyReportModel {

    /**
     * 主键 id
     */
    private Integer id;

    /**
     * 用户编码
     */
    private String userCode;

    /**
     * 交易对
     */
    private String currencyPair;

    /**
     * 现金账户
     */
    private String cashCurrency;

    /**
     * 标的账户
     */
    private String targetCode;

    /**
     * 利润
     */
    private BigDecimal currentProfitAmount;

    /**
     * 
     */
    private String resultJson;

    /**
     * 处理状态：0=未处理，1=已处理
     */
    private Integer processFlag;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 最后更新时间
     */
    private Date lastUpdateTime;
    /**
     * 标的账户变动
     */
    private BigDecimal targetCodeChange;
    /**
     * 标的账户变动
     */
    private BigDecimal cashCurrencyChange;

    /**
     * 当前标的账户剩余
     */
    private BigDecimal targetCodeBalanceAmount;

    /**
     * 当前现金账户余额
     */
    private BigDecimal cashCurrencyBalanceAmount;
    /**
     * 消息接收人 list
     */
    private String messageReceiverList;




    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }

    public String getCurrencyPair() {
        return currencyPair;
    }

    public void setCurrencyPair(String currencyPair) {
        this.currencyPair = currencyPair;
    }

    public String getCashCurrency() {
        return cashCurrency;
    }

    public void setCashCurrency(String cashCurrency) {
        this.cashCurrency = cashCurrency;
    }

    public String getTargetCode() {
        return targetCode;
    }

    public void setTargetCode(String targetCode) {
        this.targetCode = targetCode;
    }

    public BigDecimal getCurrentProfitAmount() {
        return currentProfitAmount;
    }

    public void setCurrentProfitAmount(BigDecimal currentProfitAmount) {
        this.currentProfitAmount = currentProfitAmount;
    }

    public String getResultJson() {
        return resultJson;
    }

    public void setResultJson(String resultJson) {
        this.resultJson = resultJson;
    }

    public Integer getProcessFlag() {
        return processFlag;
    }

    public void setProcessFlag(Integer processFlag) {
        this.processFlag = processFlag;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getLastUpdateTime() {
        return lastUpdateTime;
    }

    public void setLastUpdateTime(Date lastUpdateTime) {
        this.lastUpdateTime = lastUpdateTime;
    }

    public BigDecimal getTargetCodeChange() {
        return targetCodeChange;
    }

    public void setTargetCodeChange(BigDecimal targetCodeChange) {
        this.targetCodeChange = targetCodeChange;
    }

    public BigDecimal getCashCurrencyChange() {
        return cashCurrencyChange;
    }

    public void setCashCurrencyChange(BigDecimal cashCurrencyChange) {
        this.cashCurrencyChange = cashCurrencyChange;
    }

    public BigDecimal getTargetCodeBalanceAmount() {
        return targetCodeBalanceAmount;
    }

    public void setTargetCodeBalanceAmount(BigDecimal targetCodeBalanceAmount) {
        this.targetCodeBalanceAmount = targetCodeBalanceAmount;
    }

    public BigDecimal getCashCurrencyBalanceAmount() {
        return cashCurrencyBalanceAmount;
    }

    public void setCashCurrencyBalanceAmount(BigDecimal cashCurrencyBalanceAmount) {
        this.cashCurrencyBalanceAmount = cashCurrencyBalanceAmount;
    }

    public String getMessageReceiverList() {
        return messageReceiverList;
    }

    public void setMessageReceiverList(String messageReceiverList) {
        this.messageReceiverList = messageReceiverList;
    }
}
