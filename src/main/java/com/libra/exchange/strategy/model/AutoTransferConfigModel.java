package com.libra.exchange.strategy.model;

import java.math.BigDecimal;

/**
 * 自动搬砖配置
 */
public class AutoTransferConfigModel {

    /**
     * 主键id
     */
    private Integer id;

    /**
     * first平台
     */
    private String firstPlatform;

    /**
     * first交易品种
     */
    private String firstTargetCode;

    /**
     * first现金账户品种
     */
    private String firstCashCurrency;

    /**
     * first市场编码
     */
    private String firstMarket;

    /**
     * 品种地址
     */
    private String firstWithdrawAddress;

    /**
     * first提现memo
     */
    private String firstWithdrawMemo;

    /**
     * first最小交易量
     */
    private BigDecimal firstMinExhangeAmount;

    /**
     * first品种最大交易量
     */
    private BigDecimal firstMaxExchangeAmount;

    /**
     * 提现限额
     */
    private BigDecimal firstWithdrawAmount;

    /**
     * second交易平台
     */
    private String secondPlatform;

    /**
     * second交易品种
     */
    private String secondTargetCode;

    /**
     * second现金账户
     */
    private String secondCashCurrency;

    /**
     * second市场编码
     */
    private String secondMarket;

    /**
     * second交易地址
     */
    private String secondWithdrawAddress;

    /**
     * second交易备注
     */
    private String secondWithdrawMemo;

    /**
     * second最小交易额
     */
    private BigDecimal secondMinExhangeAmount;

    /**
     * second最大交易额
     */
    private BigDecimal secondMaxExchangeAmount;

    /**
     * second提现限额
     */
    private BigDecimal secondWithdrawAmount;
    /**
     * 激活状态
     */
    private Integer activeStatus;
    /**
     * diffRate 差价比例可以搬砖
     */
    private BigDecimal diffRate;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstPlatform() {
        return firstPlatform;
    }

    public void setFirstPlatform(String firstPlatform) {
        this.firstPlatform = firstPlatform;
    }

    public String getFirstTargetCode() {
        return firstTargetCode;
    }

    public void setFirstTargetCode(String firstTargetCode) {
        this.firstTargetCode = firstTargetCode;
    }

    public String getFirstCashCurrency() {
        return firstCashCurrency;
    }

    public void setFirstCashCurrency(String firstCashCurrency) {
        this.firstCashCurrency = firstCashCurrency;
    }

    public String getFirstMarket() {
        return firstMarket;
    }

    public void setFirstMarket(String firstMarket) {
        this.firstMarket = firstMarket;
    }

    public String getFirstWithdrawAddress() {
        return firstWithdrawAddress;
    }

    public void setFirstWithdrawAddress(String firstWithdrawAddress) {
        this.firstWithdrawAddress = firstWithdrawAddress;
    }

    public String getFirstWithdrawMemo() {
        return firstWithdrawMemo;
    }

    public void setFirstWithdrawMemo(String firstWithdrawMemo) {
        this.firstWithdrawMemo = firstWithdrawMemo;
    }

    public BigDecimal getFirstMinExhangeAmount() {
        return firstMinExhangeAmount;
    }

    public void setFirstMinExhangeAmount(BigDecimal firstMinExhangeAmount) {
        this.firstMinExhangeAmount = firstMinExhangeAmount;
    }

    public BigDecimal getFirstMaxExchangeAmount() {
        return firstMaxExchangeAmount;
    }

    public void setFirstMaxExchangeAmount(BigDecimal firstMaxExchangeAmount) {
        this.firstMaxExchangeAmount = firstMaxExchangeAmount;
    }

    public BigDecimal getFirstWithdrawAmount() {
        return firstWithdrawAmount;
    }

    public void setFirstWithdrawAmount(BigDecimal firstWithdrawAmount) {
        this.firstWithdrawAmount = firstWithdrawAmount;
    }

    public String getSecondPlatform() {
        return secondPlatform;
    }

    public void setSecondPlatform(String secondPlatform) {
        this.secondPlatform = secondPlatform;
    }

    public String getSecondTargetCode() {
        return secondTargetCode;
    }

    public void setSecondTargetCode(String secondTargetCode) {
        this.secondTargetCode = secondTargetCode;
    }

    public String getSecondCashCurrency() {
        return secondCashCurrency;
    }

    public void setSecondCashCurrency(String secondCashCurrency) {
        this.secondCashCurrency = secondCashCurrency;
    }

    public String getSecondMarket() {
        return secondMarket;
    }

    public void setSecondMarket(String secondMarket) {
        this.secondMarket = secondMarket;
    }

    public String getSecondWithdrawAddress() {
        return secondWithdrawAddress;
    }

    public void setSecondWithdrawAddress(String secondWithdrawAddress) {
        this.secondWithdrawAddress = secondWithdrawAddress;
    }

    public String getSecondWithdrawMemo() {
        return secondWithdrawMemo;
    }

    public void setSecondWithdrawMemo(String secondWithdrawMemo) {
        this.secondWithdrawMemo = secondWithdrawMemo;
    }

    public BigDecimal getSecondMinExhangeAmount() {
        return secondMinExhangeAmount;
    }

    public void setSecondMinExhangeAmount(BigDecimal secondMinExhangeAmount) {
        this.secondMinExhangeAmount = secondMinExhangeAmount;
    }

    public BigDecimal getSecondMaxExchangeAmount() {
        return secondMaxExchangeAmount;
    }

    public void setSecondMaxExchangeAmount(BigDecimal secondMaxExchangeAmount) {
        this.secondMaxExchangeAmount = secondMaxExchangeAmount;
    }

    public BigDecimal getSecondWithdrawAmount() {
        return secondWithdrawAmount;
    }

    public void setSecondWithdrawAmount(BigDecimal secondWithdrawAmount) {
        this.secondWithdrawAmount = secondWithdrawAmount;
    }

    public Integer getActiveStatus() {
        return activeStatus;
    }

    public void setActiveStatus(Integer activeStatus) {
        this.activeStatus = activeStatus;
    }

    public BigDecimal getDiffRate() {
        return diffRate;
    }

    public void setDiffRate(BigDecimal diffRate) {
        this.diffRate = diffRate;
    }
}
