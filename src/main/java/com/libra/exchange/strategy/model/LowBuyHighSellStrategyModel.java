package com.libra.exchange.strategy.model;

import java.math.BigDecimal;

/**
 * 低买高卖策略配置表
 */
public class LowBuyHighSellStrategyModel {

    /**
     *
     */
    private Integer id;
    /**
     * 平台编码
     */
    private String platform;

    /**
     * 用户编码
     */
    private String userCode;

    /**
     * 交易品种编码
     */
    private String targetCode;

    /**
     * 现金账户编码
     */
    private String cashCurrency;

    /**
     * 获取的交易深度
     */
    private Integer orderBookDepth=5;

    /**
     * 卖单量限制大小
     */
    private Integer sellOrderLimitSize;

    /**
     * 卖单量限制大小
     */
    private Integer buyOrderLimitSize;

    /**
     * 波动比率
     */
    private BigDecimal changeRate;

    /**
     * 每次买入数量
     */
    private BigDecimal perBuyAmount;

    /**
     * 每次卖出数量
     */
    private BigDecimal perSellAmount;

    /**
     * 0=未激活,1=已激活
     */
    private Integer activeStatus;
    /**
     * 精度大小
     */
    private Integer precisionSize=0;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }

    public String getTargetCode() {
        return targetCode;
    }

    public void setTargetCode(String targetCode) {
        this.targetCode = targetCode;
    }

    public String getCashCurrency() {
        return cashCurrency;
    }

    public void setCashCurrency(String cashCurrency) {
        this.cashCurrency = cashCurrency;
    }

    public Integer getOrderBookDepth() {
        return orderBookDepth;
    }

    public void setOrderBookDepth(Integer orderBookDepth) {
        this.orderBookDepth = orderBookDepth;
    }

    public Integer getSellOrderLimitSize() {
        return sellOrderLimitSize;
    }

    public void setSellOrderLimitSize(Integer sellOrderLimitSize) {
        this.sellOrderLimitSize = sellOrderLimitSize;
    }

    public Integer getBuyOrderLimitSize() {
        return buyOrderLimitSize;
    }

    public void setBuyOrderLimitSize(Integer buyOrderLimitSize) {
        this.buyOrderLimitSize = buyOrderLimitSize;
    }

    public BigDecimal getChangeRate() {
        return changeRate;
    }

    public void setChangeRate(BigDecimal changeRate) {
        this.changeRate = changeRate;
    }

    public BigDecimal getPerBuyAmount() {
        return perBuyAmount;
    }

    public void setPerBuyAmount(BigDecimal perBuyAmount) {
        this.perBuyAmount = perBuyAmount;
    }

    public BigDecimal getPerSellAmount() {
        return perSellAmount;
    }

    public void setPerSellAmount(BigDecimal perSellAmount) {
        this.perSellAmount = perSellAmount;
    }

    public Integer getActiveStatus() {
        return activeStatus;
    }

    public void setActiveStatus(Integer activeStatus) {
        this.activeStatus = activeStatus;
    }

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public Integer getPrecisionSize() {
        return precisionSize;
    }

    public void setPrecisionSize(Integer precisionSize) {
        this.precisionSize = precisionSize;
    }
}
