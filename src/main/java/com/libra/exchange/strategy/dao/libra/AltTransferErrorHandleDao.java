package com.libra.exchange.strategy.dao.libra;

import com.libra.exchange.strategy.model.AltTransferErrorHandleModel;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 搬砖错误单报错处理 操作接口
 */
public interface AltTransferErrorHandleDao {

    /**
     * 保存搬砖错误单报错处理数据
     *
     * @param model
     */
    public int saveAltTransferErrorHandleModel(AltTransferErrorHandleModel model);

    /**
     * 批量保存搬砖错误单报错处理数据
     *
     * @param list
     */
    public void saveAltTransferErrorHandleModelBatch(List<AltTransferErrorHandleModel> list);

    /**
     * 删除搬砖错误单报错处理数据
     *
     * @param id 操作条数
     */
    public int deleteAltTransferErrorHandleModel(int id);

    /**
     * 批量删除搬砖错误单报错处理数据
     *
     * @param list
     * @return int 操作条数
     */
    public int deleteAltTransferErrorHandleModelByIds(List<Integer> list);

    /**
     * 更新搬砖错误单报错处理数据
     *
     * @param model
     */
    public int updateAltTransferErrorHandleModel(AltTransferErrorHandleModel model);

    /**
     * 根据id获取搬砖错误单报错处理数据
     *
     * @param id
     */
    public AltTransferErrorHandleModel getAltTransferErrorHandleModelById(int id);

    /**
     * 分页获取搬砖错误单报错处理数据
     *
     * @param map
     */
    public List<AltTransferErrorHandleModel> getAltTransferErrorHandleModelForPage(Map<String, Object> map);

    /**
     *
     * 获取需要补单的交易
     * @return
     */
    public List<AltTransferErrorHandleModel> getNeedHandleModelList();


    /**
     * 回写处理标记
     * @param id
     * @param processFlag
     * @param handleType
     * @param resultId
     * @return
     */
    public Integer updateResultIdAndProcessFlag(@Param("id") Integer id, @Param("processFlag")Integer processFlag,@Param("handleType") Integer handleType, @Param("resultId")String resultId);
}