package com.libra.exchange.strategy.dao.libra;

import com.libra.exchange.strategy.model.LowBuyHighSellStrategyModel;
import com.libra.exchange.strategy.vo.LowBuyHighSellSerachBox;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 低买高卖策略配置表 操作接口
 */
public interface LowBuyHighSellStrategyDao {

    /**
     * 保存低买高卖策略配置表数据
     *
     * @param model
     */
    public int saveLowBuyHighSellStrategyModel(LowBuyHighSellStrategyModel model);

    /**
     * 批量保存低买高卖策略配置表数据
     *
     * @param list
     */
    public void saveLowBuyHighSellStrategyModelBatch(List<LowBuyHighSellStrategyModel> list);

    /**
     * 删除低买高卖策略配置表数据
     *
     * @param id 操作条数
     */
    public int deleteLowBuyHighSellStrategyModel(int id);

    /**
     * 批量删除低买高卖策略配置表数据
     *
     * @param list
     * @return int 操作条数
     */
    public int deleteLowBuyHighSellStrategyModelByIds(List<Integer> list);

    /**
     * 更新低买高卖策略配置表数据
     *
     * @param model
     */
    public int updateLowBuyHighSellStrategyModel(LowBuyHighSellStrategyModel model);

    /**
     * 根据id获取低买高卖策略配置表数据
     *
     * @param id
     */
    public LowBuyHighSellStrategyModel getLowBuyHighSellStrategyModelById(int id);

    /**
     * 分页获取低买高卖策略配置表数据
     *
     * @param map
     */
    public List<LowBuyHighSellStrategyModel> getLowBuyHighSellStrategyModelForPage(Map<String, Object> map);

    /**
     * 根据交易编码获取策略model
     * @param targetCode
     * @return
     */
    public LowBuyHighSellStrategyModel getStrategyModelByTargetCode(@Param("targetCode") String targetCode);

    /**
     * 列出所有生效的交易策略
     * @return
     */
    public List<LowBuyHighSellStrategyModel> listAllActiveStrategyModelList();


    /**
     * 策略是否存在
     * @param sellSerachBox
     * @return
     */
    public Integer isStrategyExist(LowBuyHighSellSerachBox sellSerachBox);

    /**
     * 根据用户编码查询该用户的配置策略
     * @param userCode
     * @return
     */
    public List<LowBuyHighSellStrategyModel> getAllModelListByUserCode(@Param("userCode") String userCode);
}