package com.libra.exchange.strategy.dao.libra;

import com.libra.exchange.strategy.model.LibraProfitDailyReportModel;

import java.util.List;
import java.util.Map;

/**
 * 搬砖利润日报表 操作接口
 */
public interface LibraProfitDailyReportDao {

    /**
     * 保存搬砖利润日报表数据
     *
     * @param model
     */
    public int saveLibraProfitDailyReportModel(LibraProfitDailyReportModel model);

    /**
     * 批量保存搬砖利润日报表数据
     *
     * @param list
     */
    public void saveLibraProfitDailyReportModelBatch(List<LibraProfitDailyReportModel> list);

    /**
     * 删除搬砖利润日报表数据
     *
     * @param id 操作条数
     */
    public int deleteLibraProfitDailyReportModel(int id);

    /**
     * 批量删除搬砖利润日报表数据
     *
     * @param list
     * @return int 操作条数
     */
    public int deleteLibraProfitDailyReportModelByIds(List<Integer> list);

    /**
     * 更新搬砖利润日报表数据
     *
     * @param model
     */
    public int updateLibraProfitDailyReportModel(LibraProfitDailyReportModel model);

    /**
     * 根据id获取搬砖利润日报表数据
     *
     * @param id
     */
    public LibraProfitDailyReportModel getLibraProfitDailyReportModelById(int id);

    /**
     * 分页获取搬砖利润日报表数据
     *
     * @param map
     */
    public List<LibraProfitDailyReportModel> getLibraProfitDailyReportModelForPage(Map<String, Object> map);

}