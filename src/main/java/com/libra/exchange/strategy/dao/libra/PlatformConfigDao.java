package com.libra.exchange.strategy.dao.libra;

import com.libra.exchange.strategy.model.PlatformConfigModel;

import java.util.List;
import java.util.Map;

/**
 * 操作接口
 */
public interface PlatformConfigDao {

    /**
     * 保存数据
     *
     * @param model
     */
    public int savePlatformConfigModel(PlatformConfigModel model);

    /**
     * 批量保存数据
     *
     * @param list
     */
    public void savePlatformConfigModelBatch(List<PlatformConfigModel> list);

    /**
     * 删除数据
     *
     * @param id 操作条数
     */
    public int deletePlatformConfigModel(int id);

    /**
     * 批量删除数据
     *
     * @param list
     * @return int 操作条数
     */
    public int deletePlatformConfigModelByIds(List<Integer> list);

    /**
     * 更新数据
     *
     * @param model
     */
    public int updatePlatformConfigModel(PlatformConfigModel model);

    /**
     * 根据id获取数据
     *
     * @param id
     */
    public PlatformConfigModel getPlatformConfigModelById(int id);

    /**
     * 分页获取数据
     *
     * @param map
     */
    public List<PlatformConfigModel> getPlatformConfigModelForPage(Map<String, Object> map);


    /**
     * 根据用户编码查询平台配置信息
     *
     * @param userCode
     * @return
     */
    public List<PlatformConfigModel> getPlatConfigModelByUserCode(String userCode);


    /** 获取所有的
     * @return
     */
    public List<PlatformConfigModel> listAll();

    /**
     * 根据平台编码查询平台配置
     * @param platform
     * @return
     */
    public List<PlatformConfigModel> getPlatformConfigModelByPlatform(String platform);


}