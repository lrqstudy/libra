package com.libra.exchange.strategy.dao.libra;

import com.libra.exchange.strategy.model.LibraProfitReportConfigModel;

import java.util.List;
import java.util.Map;

/**
 * 搬砖利润日报配置表 操作接口
 */
public interface LibraProfitReportConfigDao {

    /**
     * 保存搬砖利润日报配置表数据
     *
     * @param model
     */
    public int saveLibraProfitReportConfigModel(LibraProfitReportConfigModel model);

    /**
     * 批量保存搬砖利润日报配置表数据
     *
     * @param list
     */
    public void saveLibraProfitReportConfigModelBatch(List<LibraProfitReportConfigModel> list);

    /**
     * 删除搬砖利润日报配置表数据
     *
     * @param id 操作条数
     */
    public int deleteLibraProfitReportConfigModel(int id);

    /**
     * 批量删除搬砖利润日报配置表数据
     *
     * @param list
     * @return int 操作条数
     */
    public int deleteLibraProfitReportConfigModelByIds(List<Integer> list);

    /**
     * 更新搬砖利润日报配置表数据
     *
     * @param model
     */
    public int updateLibraProfitReportConfigModel(LibraProfitReportConfigModel model);

    /**
     * 根据id获取搬砖利润日报配置表数据
     *
     * @param id
     */
    public LibraProfitReportConfigModel getLibraProfitReportConfigModelById(int id);

    /**
     * 分页获取搬砖利润日报配置表数据
     *
     * @param map
     */
    public List<LibraProfitReportConfigModel> getLibraProfitReportConfigModelForPage(Map<String, Object> map);

    /**
     * @return
     */
    public List<LibraProfitReportConfigModel> getNeedProcessConfigModelList();

}