package com.libra.exchange.strategy.dao.libra;

import com.libra.exchange.strategy.model.AltTransferProfitModel;

import java.util.List;
import java.util.Map;

/**
 *  搬砖利润统计表操作接口
 */
public interface AltTransferProfitDao {

    /**
     * 保存数据
     *
     * @param model
     */
    public int saveAltTransferProfitModel(AltTransferProfitModel model);

    /**
     * 批量保存数据
     *
     * @param list
     */
    public void saveAltTransferProfitModelBatch(List<AltTransferProfitModel> list);

    /**
     * 删除数据
     *
     * @param id 操作条数
     */
    public int deleteAltTransferProfitModel(int id);

    /**
     * 批量删除数据
     *
     * @param list
     * @return int 操作条数
     */
    public int deleteAltTransferProfitModelByIds(List<Integer> list);

    /**
     * 更新数据
     *
     * @param model
     */
    public int updateAltTransferProfitModel(AltTransferProfitModel model);

    /**
     * 根据id获取数据
     *
     * @param id
     */
    public AltTransferProfitModel getAltTransferProfitModelById(int id);

    /**
     * 分页获取数据
     *
     * @param map
     */
    public List<AltTransferProfitModel> getAltTransferProfitModelForPage(Map<String, Object> map);

}