package com.libra.exchange.strategy.dao.libra;

import com.libra.exchange.strategy.model.AutoTransferConfigModel;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * 自动搬砖配置 操作接口
 */
@Repository
public interface AutoTransferConfigDao {

    /**
     * 保存自动搬砖配置数据
     *
     * @param model
     */
    public int saveAutoTransferConfigModel(AutoTransferConfigModel model);

    /**
     * 批量保存自动搬砖配置数据
     *
     * @param list
     */
    public void saveAutoTransferConfigModelBatch(List<AutoTransferConfigModel> list);

    /**
     * 删除自动搬砖配置数据
     *
     * @param id 操作条数
     */
    public int deleteAutoTransferConfigModel(int id);

    /**
     * 批量删除自动搬砖配置数据
     *
     * @param list
     * @return int 操作条数
     */
    public int deleteAutoTransferConfigModelByIds(List<Integer> list);

    /**
     * 更新自动搬砖配置数据
     *
     * @param model
     */
    public int updateAutoTransferConfigModel(AutoTransferConfigModel model);

    /**
     * 根据id获取自动搬砖配置数据
     *
     * @param id
     */
    public AutoTransferConfigModel getAutoTransferConfigModelById(int id);

    /**
     * 分页获取自动搬砖配置数据
     *
     * @param map
     */
    public List<AutoTransferConfigModel> getAutoTransferConfigModelForPage(Map<String, Object> map);

    /**
     * 查询所有激活的自动搬砖的配置策略
     * @return
     */
    public List<AutoTransferConfigModel> listAll();
}