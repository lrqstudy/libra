package com.libra.exchange.strategy.dao.libra;

import com.libra.exchange.strategy.model.SmsInfoModel;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 短信信息表 操作接口
 */
public interface SmsInfoDao {

    /**
     * 保存短信信息表数据
     *
     * @param model
     */
    public int saveSmsInfoModel(SmsInfoModel model);

    /**
     * 批量保存短信信息表数据
     *
     * @param list
     */
    public void saveSmsInfoModelBatch(List<SmsInfoModel> list);

    /**
     * 删除短信信息表数据
     *
     * @param id 操作条数
     */
    public int deleteSmsInfoModel(int id);

    /**
     * 批量删除短信信息表数据
     *
     * @param list
     * @return int 操作条数
     */
    public int deleteSmsInfoModelByIds(List<Integer> list);

    /**
     * 更新短信信息表数据
     *
     * @param model
     */
    public int updateSmsInfoModel(SmsInfoModel model);

    /**
     * 根据id获取短信信息表数据
     *
     * @param id
     */
    public SmsInfoModel getSmsInfoModelById(int id);

    /**
     * 分页获取短信信息表数据
     *
     * @param map
     */
    public List<SmsInfoModel> getSmsInfoModelForPage(Map<String, Object> map);

    /**
     * 获取需要处理的 list
     * @return
     */
    public List<SmsInfoModel> getNeedHandleModelList();

    /**
     * 根据 id 更新短信发送处理状态
     * @param id
     * @param resultMessage
     * @param processFlag
     * @return
     */
    public Integer updateProcessFlagById(@Param("id") Integer id,@Param("resultMessage") String resultMessage,@Param("processFlag") Integer processFlag);

}