package com.libra.exchange.strategy.dao.libra;

import com.libra.exchange.strategy.model.AltCoinTransferConfigModel;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * altcoin 自动搬砖配置 操作接口
 */
public interface AltCoinTransferConfigDao {

    /**
     * 保存altcoin 自动搬砖配置数据
     *
     * @param model
     */
    public int saveAltCoinTransferConfigModel(AltCoinTransferConfigModel model);

    /**
     * 批量保存altcoin 自动搬砖配置数据
     *
     * @param list
     */
    public void saveAltCoinTransferConfigModelBatch(List<AltCoinTransferConfigModel> list);

    /**
     * 删除altcoin 自动搬砖配置数据
     *
     * @param id 操作条数
     */
    public int deleteAltCoinTransferConfigModel(int id);

    /**
     * 批量删除altcoin 自动搬砖配置数据
     *
     * @param list
     * @return int 操作条数
     */
    public int deleteAltCoinTransferConfigModelByIds(List<Integer> list);

    /**
     * 更新altcoin 自动搬砖配置数据
     *
     * @param model
     */
    public int updateAltCoinTransferConfigModel(AltCoinTransferConfigModel model);

    /**
     * 根据id获取altcoin 自动搬砖配置数据
     *
     * @param id
     */
    public AltCoinTransferConfigModel getAltCoinTransferConfigModelById(int id);

    /**
     * 分页获取altcoin 自动搬砖配置数据
     *
     * @param map
     */
    public List<AltCoinTransferConfigModel> getAltCoinTransferConfigModelForPage(Map<String, Object> map);

    /**
     * 查找所有的配置
     * @return
     */
    public List<AltCoinTransferConfigModel> listAll();

    /**
     * 查询某个用户的所有配置
     * @return
     */
    public List<AltCoinTransferConfigModel> getAllModelListByUserCode(@Param("userCode") String userCode);

    /**
     * 根据 idlist 更新激活状态
     * @param idList
     * @param activeStatus
     * @return
     */
    public Integer updateActiveStatusByIdList(@Param("idList")List<Integer> idList,@Param("activeStatus")Integer activeStatus);

    /**
     *
     * @param activeStatus
     * @return
     */
    public List<AltCoinTransferConfigModel> getModdelListByStatus(@Param("activeStatus")Integer activeStatus);
}