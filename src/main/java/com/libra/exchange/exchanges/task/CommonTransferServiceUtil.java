package com.libra.exchange.exchanges.task;

import com.google.api.client.util.Maps;
import com.libra.common.consts.PlatformEnum;
import com.libra.exchange.exchanges.common.CommonTransferService;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.util.Map;

/**
 * Created by rongqiang.li on 17/5/13.
 */
@Repository
public class CommonTransferServiceUtil {
    @Resource
    private CommonTransferService poloniexTransferServiceImpl;
    //@Resource
   // private CommonTransferService yunbiTransferServiceImpl;

    @Resource
    private CommonTransferService bitfinexTransferServiceImpl;

    @Resource
    private CommonTransferService binanceTransferServiceImpl;

    @Resource
    private CommonTransferService bittrexTransferServiceImpl;


    @Resource
    private CommonTransferService bigOneTransferServiceImpl;

    @Resource
    private CommonTransferService biboxTransferServiceImpl;



    @Resource
    Map<String, CommonTransferService> platformAndServiceMap = Maps.newHashMap();

    @PostConstruct
    public void init() {
        //platformAndServiceMap.put(PlatformEnum.YUNBI.getCode(), yunbiTransferServiceImpl);
        platformAndServiceMap.put(PlatformEnum.POLONIEX.getCode(), poloniexTransferServiceImpl);
        platformAndServiceMap.put(PlatformEnum.BITFINEX.getCode(), bitfinexTransferServiceImpl);
        platformAndServiceMap.put(PlatformEnum.BINANCE.getCode(), binanceTransferServiceImpl);
        platformAndServiceMap.put(PlatformEnum.BITTREX.getCode(), bittrexTransferServiceImpl);
        platformAndServiceMap.put(PlatformEnum.BIGONE.getCode(), bigOneTransferServiceImpl);
        platformAndServiceMap.put(PlatformEnum.BIBOX.getCode(), biboxTransferServiceImpl);
    }

    public Map<String, CommonTransferService> getPlatformAndServiceMap() {
        return platformAndServiceMap;
    }
}
