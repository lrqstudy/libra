package com.libra.exchange.exchanges.task;

import com.libra.exchange.exchanges.common.CommonTransferService;
import com.libra.exchange.strategy.model.AltCoinTransferConfigModel;
import com.libra.exchange.strategy.model.PlatformConfigModel;

import java.util.List;
import java.util.Map;

/**
 * Created by rongqiang.li on 17/5/13.
 */
public class AutoTransferContextVO {
    private List<AltCoinTransferConfigModel> altCoinTransferConfigModelList;
    private Map<String, CommonTransferService> platformAndServiceMap;
    private Map<String, PlatformConfigModel> platformConfigModelMap;
    private String targetCode;
}
