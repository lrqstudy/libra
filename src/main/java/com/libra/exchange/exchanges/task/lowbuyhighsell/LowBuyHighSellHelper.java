package com.libra.exchange.exchanges.task.lowbuyhighsell;

import com.libra.common.consts.OrderSource;
import com.libra.common.util.ExchangeMathUtils;
import com.libra.exchange.exchanges.common.CommonMarketUtils;
import com.libra.exchange.exchanges.common.CommonOpenOrderVO;
import com.libra.exchange.exchanges.common.CommonOrderBook;
import com.libra.exchange.exchanges.common.CommonTransferService;
import com.libra.exchange.exchanges.task.CommonTransferServiceUtil;
import com.libra.exchange.strategy.model.LowBuyHighSellStrategyModel;
import com.libra.exchange.strategy.model.PlatformConfigModel;
import com.libra.exchange.strategy.service.LowBuyHighSellStrategyService;
import com.libra.exchange.strategy.service.PlatformConfigService;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * Created by rongqiang.li on 17/5/17.
 */
@Service
public class LowBuyHighSellHelper {

    @Resource
    private LowBuyHighSellStrategyService lowBuyHighSellStrategyService;
    @Resource
    private PlatformConfigService platformConfigService;
    @Resource
    private CommonTransferServiceUtil commonTransferServiceUtil;
    @Resource
    private CommonMarketUtils commonMarketUtils;

    private Logger logger = LoggerFactory.getLogger(LowBuyHighSellHelper.class);


    public void autoLowBuyHighSell() {
        Map<Pair<String, String>, PlatformConfigModel> platformUserCodeAndModelMap = platformConfigService.getPlatformUserCodeAndModelMap();
        Map<String, CommonTransferService> platformAndServiceMap = commonTransferServiceUtil.getPlatformAndServiceMap();

        List<LowBuyHighSellStrategyModel> lowBuyHighSellStrategyModelList = lowBuyHighSellStrategyService.listAllActiveStrategyModelList();
        for (LowBuyHighSellStrategyModel lowBuyHighSellStrategyModel : lowBuyHighSellStrategyModelList) {
            Pair<String, String> platformAndUserCodePair = Pair.of(lowBuyHighSellStrategyModel.getPlatform(), lowBuyHighSellStrategyModel.getUserCode());
            CommonTransferService commonTransferService = platformAndServiceMap.get(platformAndUserCodePair.getLeft());
            if (commonTransferService == null) {
                continue;
            }
            PlatformConfigModel configModel = platformUserCodeAndModelMap.get(platformAndUserCodePair);
            if (configModel == null) {
                continue;
            }
            try {
                autoExchange(lowBuyHighSellStrategyModel, commonTransferService, configModel);
            } catch (Exception e) {
                logger.error(e.getMessage(), e);
            }
        }
    }


    private void autoExchange(LowBuyHighSellStrategyModel model, CommonTransferService commonTransferService, PlatformConfigModel platformConfigModel) {
        List<CommonOpenOrderVO> openOrderList = commonTransferService.getOpenOrderList(platformConfigModel, model.getTargetCode(), model.getCashCurrency());
        Integer totalTradeSizeLimit = model.getBuyOrderLimitSize() + model.getSellOrderLimitSize();

        if (openOrderList.size() > totalTradeSizeLimit) {
            logger.info(" current open order size over limit ");
            return;
        }

        CommonOrderBook orderBook = commonTransferService.getOrderBook(model.getTargetCode(), model.getCashCurrency(), model.getOrderBookDepth());
        BigDecimal weightedBuyPrice = commonMarketUtils.getWeightedPrice(orderBook.getBuyOrderVOList());
        // 执行买入api
        BigDecimal buyPriceOfRate = ExchangeMathUtils.getBuyPriceOfRate(weightedBuyPrice, model.getChangeRate());
        Map<String, BigDecimal> accountBalanceMap = commonTransferService.getBalanceMap(platformConfigModel);//Maps.newHashMap(); //= poloniexExchangeServiceImpl.getAccountBalance();

        BigDecimal targetBalance = accountBalanceMap.get(model.getTargetCode());
        BigDecimal cashBalance = accountBalanceMap.get(model.getCashCurrency());

        if (cashBalance.compareTo(buyPriceOfRate.multiply(model.getPerBuyAmount())) < 0 || targetBalance.compareTo(model.getPerSellAmount()) < 0) {
            //余额不足,程序退出
            logger.info(" targetCode balance or cashCurrency balance is insufficient");
            return;
        }

        //如果现金账户中余额足够就执行买入api
        commonTransferService.buy(model.getTargetCode(), model.getCashCurrency(), buyPriceOfRate, model.getPerBuyAmount(),model.getPrecisionSize(), platformConfigModel, OrderSource.LOW_BUY_HIGH_SELL.getId());

        BigDecimal weightedSellPrice = commonMarketUtils.getWeightedPrice(orderBook.getSellOrderVOList());
        BigDecimal sellPriceOfRate = ExchangeMathUtils.getSellPriceOfRate(weightedSellPrice, model.getChangeRate());
        // 如果标的账户中余额足够就执行卖出api
        commonTransferService.sell(model.getTargetCode(), model.getCashCurrency(), sellPriceOfRate, model.getPerSellAmount(),model.getPrecisionSize(), platformConfigModel,OrderSource.LOW_BUY_HIGH_SELL.getId());

    }


}
