package com.libra.exchange.exchanges.task;

import com.google.api.client.util.Lists;
import com.google.api.client.util.Maps;
import com.libra.common.consts.OrderSource;
import com.libra.common.util.BeanUtilExt;
import com.libra.exchange.exchanges.common.CommonOrderBook;
import com.libra.exchange.exchanges.common.CommonOrderVO;
import com.libra.exchange.exchanges.common.CommonTransferService;
import com.libra.exchange.strategy.model.AltCoinTransferConfigModel;
import com.libra.exchange.strategy.model.AltTransferProfitModel;
import com.libra.exchange.strategy.model.PlatformConfigModel;
import com.libra.exchange.strategy.service.AltTransferProfitService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.task.TaskExecutor;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * Created by rongqiang.li on 17/5/11.
 */
@Repository
public class CommonTransferHelper {
    private final int DEFAULT_DEPTH = 1;

    private Logger logger = LoggerFactory.getLogger(CommonTransferHelper.class);

    @Resource
    private TaskExecutor taskExecutor;
    @Resource
    private AltTransferProfitService altTransferProfitService;


    public void transfer(List<AltCoinTransferConfigModel> altCoinTransferConfigModelList, Map<String, CommonTransferService> platformAndServiceMap, Map<String, PlatformConfigModel> platformConfigModelMap, String targetCode) {
        //USER_CODE
        List<AltCoinTransferConfigModel> configModelList = filterConfigModelListByTargetCode(altCoinTransferConfigModelList, targetCode);

        Map<String, CommonOrderBook> platformAndOrderBookMap = Maps.newHashMap();
        for (AltCoinTransferConfigModel transferConfigModel : configModelList) {
            String platform = transferConfigModel.getPlatform();
            CommonTransferService commonTransferService = platformAndServiceMap.get(platform);
            CommonOrderBook orderBook = commonTransferService.getOrderBook(transferConfigModel.getTargetCode(), transferConfigModel.getCashCurrency(), DEFAULT_DEPTH);
            platformAndOrderBookMap.put(platform, orderBook);
        }

        BigDecimal minSellOnePrice = new BigDecimal(Float.MAX_VALUE);
        String minSellOnePlatForm = "";
        BigDecimal maxBuyPrice = BigDecimal.ZERO;
        String maxBuyPlatForm = "";
        for (Map.Entry<String, CommonOrderBook> entry : platformAndOrderBookMap.entrySet()) {
            String platform = entry.getKey();
            CommonOrderBook orderBook = entry.getValue();
            BigDecimal buyOnePrice = orderBook.getBuyOrderVOList().get(0).getPrice();
            BigDecimal sellOnePrice = orderBook.getSellOrderVOList().get(0).getPrice();

            if (minSellOnePrice.compareTo(sellOnePrice) >= 0) {
                minSellOnePrice = sellOnePrice;//求卖一价最小值
                minSellOnePlatForm = platform;//卖一价格最小值平台
            }
            if (maxBuyPrice.compareTo(buyOnePrice) <= 0) {
                maxBuyPrice = buyOnePrice;//求买一价格最大值
                maxBuyPlatForm = platform;//买一价格最大值平台
            }
        }

        BigDecimal diffRate = configModelList.get(0).getDiffRate();

        Map<String, AltCoinTransferConfigModel> platformConfigMap = Maps.newHashMap();
        for (AltCoinTransferConfigModel configModel : configModelList) {
            platformConfigMap.put(configModel.getPlatform(), configModel);
        }

        BigDecimal realDiffRate = maxBuyPrice.subtract(minSellOnePrice).divide(minSellOnePrice, 4, BigDecimal.ROUND_CEILING);
        if (realDiffRate.compareTo(diffRate) < 0) {
            logger.info(" can buy  platform={},market ={},buy price = {},can sell price platform={}, market={},sell price={}, realDiffRate={}", minSellOnePlatForm, platformConfigMap.get(minSellOnePlatForm).getCurrencyPair(), minSellOnePrice, maxBuyPlatForm, platformConfigMap.get(maxBuyPlatForm).getCurrencyPair(), maxBuyPrice, realDiffRate);
            return;
        }

        //说明买卖价差存在套利空间,执行套利
        final PlatformConfigModel buyPlatFormConfigModel = platformConfigModelMap.get(minSellOnePlatForm);
        final PlatformConfigModel sellPlatFormConfigModel = platformConfigModelMap.get(maxBuyPlatForm);

        //价格高的地方执行卖出操作, 如果cashCurrency超过设定值,则执行提现操作
        final CommonTransferService sellPlatFormService = platformAndServiceMap.get(maxBuyPlatForm);

        final CommonOrderBook minSellPlatformCommonOrderBook = platformAndOrderBookMap.get(minSellOnePlatForm);
        final CommonOrderVO minSellPlatformSellOrderVO = minSellPlatformCommonOrderBook.getSellOrderVOList().get(0);//最低卖价的平台买单 价和量

        CommonOrderBook maxBuyPlatformCommonOrderBook = platformAndOrderBookMap.get(maxBuyPlatForm);
        final CommonOrderVO maxBuyPlatformBuyOrderVO = maxBuyPlatformCommonOrderBook.getBuyOrderVOList().get(0);//最高买价的平台买单价和量


        final AltCoinTransferConfigModel sellConfigModel = platformConfigMap.get(maxBuyPlatForm);
        final AltCoinTransferConfigModel buyConfigModel = platformConfigMap.get(minSellOnePlatForm);


        BigDecimal sellAmount = getTheMinValue(maxBuyPlatformBuyOrderVO.getVolume(), minSellPlatformSellOrderVO.getVolume(), sellConfigModel.getMaxSellAmount());
        BigDecimal buyAmount = getTheMinValue(minSellPlatformSellOrderVO.getVolume(), maxBuyPlatformBuyOrderVO.getVolume(), buyConfigModel.getMaxBuyAmount());

        final BigDecimal tradeAmount = getTradeAmount(sellAmount, buyAmount, buyConfigModel, sellConfigModel);
        //先执行买入操作
        final CommonTransferService buyPlatformService = platformAndServiceMap.get(minSellOnePlatForm);

        try {
            taskExecutor.execute(new Runnable() {
                @Override
                public void run() {
                    try {
                        buyPlatformService.buy(buyConfigModel.getTargetCode(), buyConfigModel.getCashCurrency(), minSellPlatformSellOrderVO.getPrice(), tradeAmount, buyConfigModel.getPrecisionSize(), buyPlatFormConfigModel, OrderSource.TRANSFER.getId());
                    } catch (Exception e) {
                        logger.info(" buy error occur" + e.getMessage(), e);
                    }
                }
            });
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }

        //后执行卖出操作
        try {
            taskExecutor.execute(new Runnable() {
                @Override
                public void run() {
                    try {
                        sellPlatFormService.sell(sellConfigModel.getTargetCode(), sellConfigModel.getCashCurrency(), maxBuyPlatformBuyOrderVO.getPrice(), tradeAmount, sellConfigModel.getPrecisionSize(), sellPlatFormConfigModel, OrderSource.TRANSFER.getId());
                    } catch (Exception e) {
                        logger.info(" buy error occur" + e.getMessage(), e);
                    }
                }
            });
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        BigDecimal diffPrice = maxBuyPlatformBuyOrderVO.getPrice().subtract(minSellPlatformSellOrderVO.getPrice());
        BigDecimal profitValue = diffPrice.multiply(tradeAmount).multiply(new BigDecimal(0.997));
        profitValue = profitValue.setScale(5, BigDecimal.ROUND_FLOOR);
        logger.info(" symbol={},buy platform={},price ={}, sell platform={},sell price={},tradeAmount={},realDiffRate={}, profitValue={} ,{} ", buyConfigModel.getCurrencyPair(), buyConfigModel.getPlatform(), minSellPlatformSellOrderVO.getPrice(), sellConfigModel.getPlatform(), maxBuyPlatformBuyOrderVO.getPrice(), tradeAmount, realDiffRate, profitValue, sellConfigModel.getCashCurrency());
        //启动线程去保存结果 TODO

        final AltTransferProfitModel profitModel = new AltTransferProfitModel();
        profitModel.setUserCode(buyConfigModel.getUserCode());
        profitModel.setBuyPlatform(buyPlatFormConfigModel.getPlatform());
        profitModel.setBuyPrice(minSellPlatformSellOrderVO.getPrice());
        profitModel.setBuyResultId("");//TODO
        profitModel.setCashCurrency(buyConfigModel.getCashCurrency());
        profitModel.setCurrencyPair(buyConfigModel.getCurrencyPair());
        profitModel.setDiffRate(realDiffRate);
        profitModel.setProcessFlag(0);
        profitModel.setProfitAmount(profitValue);
        profitModel.setSellPlatform(sellConfigModel.getPlatform());
        profitModel.setSellResultId("");//TODO
        profitModel.setTargetCode(buyConfigModel.getTargetCode());
        profitModel.setTradeAmount(tradeAmount);
        profitModel.setSellPrice(maxBuyPlatformBuyOrderVO.getPrice());//卖出价格
        BeanUtilExt.fillNullProperty(profitModel);

        taskExecutor.execute(new Runnable() {
            @Override
            public void run() {
                altTransferProfitService.saveAltTransferProfitModel(profitModel);
            }
        });

    }


    private BigDecimal getTradeAmount(BigDecimal buyAmount, BigDecimal sellAmount, AltCoinTransferConfigModel sellConfigModel, AltCoinTransferConfigModel buyConfigModel) {
        BigDecimal minValue = buyAmount.compareTo(sellAmount) >= 0 ? sellAmount : buyAmount;//取买单和卖单中的最小值
        /**
         * 然后把两个平台的最小值和 minValue 比对,取最大值, 避免有交易所报错
         */
        BigDecimal maxValue = minValue.compareTo(sellConfigModel.getMinTradeAmount()) > 0 ? minValue : sellConfigModel.getMinTradeAmount();

        maxValue = maxValue.compareTo(buyConfigModel.getMinTradeAmount()) > 0 ? maxValue : buyConfigModel.getMinTradeAmount();


        return maxValue;//两者取最小值
    }


    private BigDecimal getTheMinValue(BigDecimal buyOneVolume, BigDecimal sellOneVolume, BigDecimal halfBalanceAmount) {
        BigDecimal min = buyOneVolume.compareTo(sellOneVolume) >= 0 ? sellOneVolume : buyOneVolume;
        return min.compareTo(halfBalanceAmount) >= 0 ? halfBalanceAmount : min;
    }

    private List<AltCoinTransferConfigModel> filterConfigModelListByTargetCode(List<AltCoinTransferConfigModel> altCoinTransferConfigModels, final String targetCode) {
        List<AltCoinTransferConfigModel> needConfigModelList = Lists.newArrayList();
        for (AltCoinTransferConfigModel altCoinTransferConfigModel : altCoinTransferConfigModels) {
            if (altCoinTransferConfigModel.getTargetCode().equals(targetCode)) {
                needConfigModelList.add(altCoinTransferConfigModel);
            }
        }
        return needConfigModelList;
    }


}
