package com.libra.exchange.exchanges.task;

import com.google.api.client.util.Maps;
import com.google.common.base.Function;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.libra.exchange.exchanges.common.CommonTransferService;
import com.libra.exchange.strategy.model.AltCoinTransferConfigModel;
import com.libra.exchange.strategy.model.PlatformConfigModel;
import com.libra.exchange.strategy.service.AltCoinTransferConfigService;
import com.libra.exchange.strategy.service.PlatformConfigService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Nullable;
import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * Created by rongqiang.li on 17/5/13.
 */
@Service
public class AutoTransferHelper {
    @Resource
    private AltCoinTransferConfigService altCoinTransferConfigService;
    @Resource
    private PlatformConfigService platformConfigService;
    @Resource
    private CommonTransferServiceUtil commonTransferServiceUtil;
    @Resource
    private CommonTransferHelper commonTransferHelper;
    private Logger logger = LoggerFactory.getLogger(AutoTransferHelper.class);


    public void autoTransfer(String userCode) {
        List<PlatformConfigModel> platformConfigModelList = platformConfigService.getPlatConfigModelByUserCode(userCode);
        Map<String, PlatformConfigModel> platformConfigModelMap = Maps.newHashMap();
        for (PlatformConfigModel platformConfigModel : platformConfigModelList) {
            platformConfigModelMap.put(platformConfigModel.getPlatform(), platformConfigModel);
        }
        List<AltCoinTransferConfigModel> altCoinTransferConfigModelList = altCoinTransferConfigService.getAllModelListByUserCode(userCode);
        List<String> uniqueTargetCodeList = getUniqueTargetCodeList(altCoinTransferConfigModelList);
        Map<String, CommonTransferService> platformAndServiceMap = commonTransferServiceUtil.getPlatformAndServiceMap();
        for (String targetCode : uniqueTargetCodeList) {
            try {
                commonTransferHelper.transfer(altCoinTransferConfigModelList, platformAndServiceMap, platformConfigModelMap, targetCode);
            } catch (Exception e) {
                logger.error(e.getMessage(),e);
            }
        }
    }


    private List<String> getUniqueTargetCodeList(List<AltCoinTransferConfigModel> allModelListByUserCode) {
        List<String> allTargetCodeList = Lists.transform(allModelListByUserCode, new Function<AltCoinTransferConfigModel, String>() {
            @Nullable
            @Override
            public String apply(@Nullable AltCoinTransferConfigModel altCoinTransferConfigModel) {
                return altCoinTransferConfigModel.getTargetCode();
            }
        });
        return Lists.newArrayList(Sets.newHashSet(allTargetCodeList));
    }

    public void autoTransfer(Map<String, List<AltCoinTransferConfigModel>> userCodeConfigModelListMap) {
        for (Map.Entry<String, List<AltCoinTransferConfigModel>> entry : userCodeConfigModelListMap.entrySet()) {
            List<PlatformConfigModel> platformConfigModelList = platformConfigService.getPlatConfigModelByUserCode(entry.getKey());
            Map<String, PlatformConfigModel> platformConfigModelMap = Maps.newHashMap();
            for (PlatformConfigModel platformConfigModel : platformConfigModelList) {
                platformConfigModelMap.put(platformConfigModel.getPlatform(), platformConfigModel);
            }
            List<AltCoinTransferConfigModel> altCoinTransferConfigModelList =entry.getValue();
            List<String> uniqueTargetCodeList = getUniqueTargetCodeList(altCoinTransferConfigModelList);
            Map<String, CommonTransferService> platformAndServiceMap = commonTransferServiceUtil.getPlatformAndServiceMap();
            for (String targetCode : uniqueTargetCodeList) {
                try {
                    commonTransferHelper.transfer(altCoinTransferConfigModelList, platformAndServiceMap, platformConfigModelMap, targetCode);
                } catch (Exception e) {
                    logger.error(e.getMessage(),e);
                }
            }
        }
    }
}
