package com.libra.exchange.exchanges.cryptopia;


import com.google.gson.JsonObject;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.net.ssl.HttpsURLConnection;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.util.Base64;

/**
 *
 * @author Sampey
 */

public class HttpCryptopia {
    
    public String privateUrl = "https://www.cryptopia.co.nz/Api/";
    public String publicKey = "YOUR_PUBLIC_KEY";
    public String privateKey = "YOUR_PRIVATE_KEY";
    
    public HttpCryptopia(String method,String jSonPostParam) throws Exception {
        String urlMethod = privateUrl + method;
        String nonce     = String.valueOf(System.currentTimeMillis());
         
        String reqSignature = 
                publicKey 
                + "POST" 
                + URLEncoder.encode(urlMethod,StandardCharsets.UTF_8.toString()).toLowerCase() 
                + nonce 
                + getMD5_B64(jSonPostParam);
        
        String AUTH = "amx " 
                + publicKey
                +":"
                + this.sha256_B64(reqSignature) 
                +":"
                + nonce;

        String response = this.continueForHttp(urlMethod, jSonPostParam,AUTH);
        
        System.out.println("API RESPONSE : " + response);
    }
    
    private String continueForHttp(String urlMethod, String postParam,String AUTH) throws Exception {
        URLConnection con = new URL(urlMethod).openConnection(); // CREATE POST CONNECTION
        con.setDoOutput(true);
        
        HttpsURLConnection httpsConn = (HttpsURLConnection) con;
        httpsConn.setRequestMethod("POST");
        httpsConn.setInstanceFollowRedirects(true);

        con.setRequestProperty("Authorization", AUTH);
        con.setRequestProperty("Content-Type", "application/json");
        
        // WRITE POST PARAMS
        DataOutputStream wr = new DataOutputStream(con.getOutputStream());
        wr.writeBytes(postParam);
        wr.flush();
        wr.close();

        // READ THE RESPONSE
        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuilder response = new StringBuilder();
        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        return response.toString();
    }
    
    private String getMD5_B64(String postParameter) throws Exception {
        return Base64.getEncoder().encodeToString(MessageDigest.getInstance("MD5").digest(postParameter.getBytes("UTF-8")));
    }
        
    private String sha256_B64(String msg) throws Exception {
        Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
        SecretKeySpec secret_key = new SecretKeySpec(Base64.getDecoder().decode(privateKey), "HmacSHA256");
        sha256_HMAC.init(secret_key);
        return Base64.getEncoder().encodeToString(sha256_HMAC.doFinal(msg.getBytes("UTF-8")));
    }
    
/*-------------------- // MAIN IS TO AUTO-EXECUTE THIS CLASS -----------------------------------*/    
    
    public static void main(String[] args) {
        try {
            // YOU CAN FORMAT THE JSON STRING USING THE LIBRARY YOU PREFER (OR MANUALLY)
            JsonObject jo = new JsonObject();
            jo.addProperty("Currency", "DOT"); // EXAMPLE {"Currency":"DOT"}
            new HttpCryptopia("GetBalance", jo.toString());
        } catch (Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
    }
}