package com.libra.exchange.exchanges.common;

import com.libra.exchange.strategy.model.PlatformConfigModel;
import org.knowm.xchange.Exchange;
import org.knowm.xchange.ExchangeFactory;
import org.knowm.xchange.bitfinex.v1.BitfinexExchange;
import org.knowm.xchange.service.marketdata.MarketDataService;

import java.lang.reflect.ParameterizedType;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * Created by rongqiang.li on 18/1/1.
 */
public abstract class CommonTransferServiceImpl<T> implements CommonTransferService{

    private Class<T> clazz;

    public CommonTransferServiceImpl() {
        ParameterizedType parameterizedType = (ParameterizedType) this.clazz.getGenericSuperclass();
        clazz= (Class<T>) parameterizedType.getActualTypeArguments()[0];

    }

    @Override
    public CommonOrderBook getOrderBook(String targetCode, String cashCurrency, int depth) {
        return null;
    }

    @Override
    public Map<String, BigDecimal> getBalanceMap(PlatformConfigModel configModel) {
        return null;
    }


    @Override
    public String buy(String targetCode, String cashCurrency, BigDecimal price, BigDecimal amount, Integer precisionSize, PlatformConfigModel configModel, Integer orderSource) {

        return null;
    }

    @Override
    public String sell(String targetCode, String cashCurrency, BigDecimal price, BigDecimal amount, Integer precisionSize, PlatformConfigModel configModel, Integer orderSource) {
        return null;
    }

    @Override
    public String withdraw(String targetCode, String address, String memo, BigDecimal amount, PlatformConfigModel configModel) {
        return null;
    }

    @Override
    public List<CommonOpenOrderVO> getOpenOrderList(PlatformConfigModel platformConfigModel, String targetCode, String cashCurrency) {
        return null;
    }


    public CommonTickerVO getTickerBySymbol(String targetCode, String cashCurrency) {
        // Use the factory to get Bitfinex exchange API using default settings
        Exchange bitfinex = ExchangeFactory.INSTANCE.createExchange(BitfinexExchange.class.getName());

        // Interested in the public market data feed (no authentication)
        MarketDataService marketDataService = bitfinex.getMarketDataService();

        return null;
    }
}
