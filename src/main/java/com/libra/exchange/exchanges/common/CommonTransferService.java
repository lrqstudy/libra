package com.libra.exchange.exchanges.common;

import com.libra.exchange.strategy.model.PlatformConfigModel;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * Created by rongqiang.li on 17/5/11.
 */
public interface CommonTransferService {
    /**
     * 获取挂单
     *
     * @param targetCode
     * @param cashCurrency
     * @param depth
     * @return
     */
    public CommonOrderBook getOrderBook(String targetCode, String cashCurrency, int depth);

    /**
     * 查询余额
     *
     * @return
     */
    public Map<String, BigDecimal> getBalanceMap(PlatformConfigModel configModel);

    /**
     * 获取交易对
     *
     * @param targetCode
     * @param cashCurrency
     * @return
     */
    public String getCurrencyPair(String targetCode, String cashCurrency);


    /**
     * 买
     *
     * @param targetCode
     * @param cashCurrency
     * @param price
     * @param amount
     * @return
     */
    public String buy(String targetCode, String cashCurrency, BigDecimal price, BigDecimal amount, Integer precisionSize, PlatformConfigModel configModel, Integer orderSource);

    /**
     * 卖
     *
     * @param targetCode
     * @param cashCurrency
     * @param price
     * @param amount
     * @return
     */
    public String sell(String targetCode, String cashCurrency, BigDecimal price, BigDecimal amount, Integer precisionSize, PlatformConfigModel configModel, Integer orderSource);

    /**
     * 提现
     *
     * @param targetCode
     * @return
     */
    public String withdraw(String targetCode, String address, String memo, BigDecimal amount, PlatformConfigModel configModel);

    /**
     * 获取对应交易对的开放未成交订单
     *
     * @param targetCode
     * @param cashCurrency
     * @return
     */
    public List<CommonOpenOrderVO> getOpenOrderList(PlatformConfigModel platformConfigModel, String targetCode, String cashCurrency);

    /**
     * get ticker
     *
     * @param targetCode
     * @param cashCurrency
     * @return
     */
    public CommonTickerVO getTickerBySymbol(String targetCode, String cashCurrency);
}
