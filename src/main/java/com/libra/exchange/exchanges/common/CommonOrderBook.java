package com.libra.exchange.exchanges.common;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.List;

/**
 * Created by rongqiang.li on 17/4/15.
 */
public class CommonOrderBook {
    private List<CommonOrderVO> buyOrderVOList;
    private List<CommonOrderVO> sellOrderVOList;

    public List<CommonOrderVO> getBuyOrderVOList() {
        return buyOrderVOList;
    }

    public void setBuyOrderVOList(List<CommonOrderVO> buyOrderVOList) {
        this.buyOrderVOList = buyOrderVOList;
    }

    public List<CommonOrderVO> getSellOrderVOList() {
        return sellOrderVOList;
    }

    public void setSellOrderVOList(List<CommonOrderVO> sellOrderVOList) {
        this.sellOrderVOList = sellOrderVOList;
    }
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
