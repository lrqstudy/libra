package com.libra.exchange.exchanges.common;

/**
 * Created by rongqiang.li on 18/2/26.
 */
public interface CommonMarketService {

    /**
     * 获取公开 book
     * @param targetCode
     * @param cashCurrency
     * @param symbol
     * @param depth
     * @return
     */
    CommonOrderBook getOrderBook(String targetCode, String cashCurrency, String symbol, int depth);



}
