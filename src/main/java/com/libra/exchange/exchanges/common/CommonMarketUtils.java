package com.libra.exchange.exchanges.common;

import com.google.common.collect.Lists;
import org.knowm.xchange.dto.marketdata.OrderBook;
import org.knowm.xchange.dto.trade.LimitOrder;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by rongqiang.li on 2017/5/1.
 */
@Repository
public class CommonMarketUtils {

    public List<CommonOrderVO> buildOrderVOListFromArray(Object[][] orders, String priceCurrencyType) {
        //如果是比特币价格
        List<CommonOrderVO> commonOrderVOList = Lists.newArrayList();
        for (Object[] order : orders) {
            CommonOrderVO orderVO = new CommonOrderVO();
            orderVO.setPrice(new BigDecimal(order[0].toString()));
            orderVO.setBtcPrice(orderVO.getPrice());
            orderVO.setVolume(new BigDecimal(order[1].toString()));
            commonOrderVOList.add(orderVO);
        }
        return commonOrderVOList;
    }

    public BigDecimal getWeightedPrice(List<CommonOrderVO> voList) {
        BigDecimal totalPrice = BigDecimal.ZERO;
        BigDecimal totalVolum = BigDecimal.ZERO;
        for (CommonOrderVO orderVO : voList) {
            totalPrice = totalPrice.add(orderVO.getPrice().multiply(orderVO.getVolume()));
            totalVolum = totalVolum.add(orderVO.getVolume());
        }
        BigDecimal weightedPrice = totalPrice.divide(totalVolum, 12, BigDecimal.ROUND_HALF_EVEN);
        return weightedPrice;
    }

    public CommonOrderBook buildCommonOrderBook(OrderBook orderBook) {
        CommonOrderBook commonOrderBook = new CommonOrderBook();
        List<LimitOrder> asks = orderBook.getAsks();
        List<LimitOrder> bids = orderBook.getBids();

        CommonOrderVO buyOrderVO = new CommonOrderVO();
        buyOrderVO.setPrice(bids.get(0).getLimitPrice());
        buyOrderVO.setVolume(bids.get(0).getOriginalAmount());
        ArrayList<CommonOrderVO> buyOrderList = com.google.api.client.util.Lists.newArrayList();
        buyOrderList.add(buyOrderVO);


        CommonOrderVO sellOrderVO = new CommonOrderVO();
        sellOrderVO.setPrice(asks.get(0).getLimitPrice());
        sellOrderVO.setVolume(asks.get(0).getOriginalAmount());
        ArrayList<CommonOrderVO> sellOrderList = com.google.api.client.util.Lists.newArrayList();
        sellOrderList.add(sellOrderVO);

        commonOrderBook.setBuyOrderVOList(buyOrderList);
        commonOrderBook.setSellOrderVOList(sellOrderList);
        return commonOrderBook;
    }

    public CommonOrderBook buildCommonOrderBook(OrderBook orderBook, int depth) {
        CommonOrderBook commonOrderBook = new CommonOrderBook();
        List<LimitOrder> asks = orderBook.getAsks();
        List<LimitOrder> bids = orderBook.getBids();
        depth = asks.size() >= depth ? depth : asks.size();
        ArrayList<CommonOrderVO> buyOrderList = com.google.api.client.util.Lists.newArrayList();

        for (int i = 0; i < depth; i++) {
            CommonOrderVO buyOrderVO = new CommonOrderVO();
            buyOrderVO.setPrice(bids.get(i).getLimitPrice());
            buyOrderVO.setVolume(bids.get(i).getOriginalAmount());
            buyOrderList.add(buyOrderVO);
        }


        ArrayList<CommonOrderVO> sellOrderList = com.google.api.client.util.Lists.newArrayList();
        for (int i = 0; i < depth; i++) {
            CommonOrderVO sellOrderVO = new CommonOrderVO();
            sellOrderVO.setPrice(asks.get(i).getLimitPrice());
            sellOrderVO.setVolume(asks.get(i).getOriginalAmount());
            sellOrderList.add(sellOrderVO);
        }

        commonOrderBook.setBuyOrderVOList(buyOrderList);
        commonOrderBook.setSellOrderVOList(sellOrderList);
        return commonOrderBook;
    }


}
