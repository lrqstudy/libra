package com.libra.exchange.exchanges.common;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by rongqiang.li on 18/1/14.
 */
public class CommonWithdrawHistory implements Serializable {
    /**
     * 本次提现 id
     */
    private String withdrawId;
    /**
     * 提现品种
     */
    private String targetCode;
    /**
     * 提现目的地地址
     */
    private String destinationAddress;
    /**
     * 提现目的地 Id
     */
    private String destinationPaymentId;

    /**
     * 提现数量
     */
    private BigDecimal amount;
    /**
     * 申请时间
     */
    private Date applyTime;
    /**
     * 成功时间
     */
    private Date successTime;
    /**
     * txid
     */
    private String txId;

    public String getWithdrawId() {
        return withdrawId;
    }

    public void setWithdrawId(String withdrawId) {
        this.withdrawId = withdrawId;
    }

    public String getTargetCode() {
        return targetCode;
    }

    public void setTargetCode(String targetCode) {
        this.targetCode = targetCode;
    }

    public String getDestinationAddress() {
        return destinationAddress;
    }

    public void setDestinationAddress(String destinationAddress) {
        this.destinationAddress = destinationAddress;
    }

    public String getDestinationPaymentId() {
        return destinationPaymentId;
    }

    public void setDestinationPaymentId(String destinationPaymentId) {
        this.destinationPaymentId = destinationPaymentId;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Date getApplyTime() {
        return applyTime;
    }

    public void setApplyTime(Date applyTime) {
        this.applyTime = applyTime;
    }

    public Date getSuccessTime() {
        return successTime;
    }

    public void setSuccessTime(Date successTime) {
        this.successTime = successTime;
    }

    public String getTxId() {
        return txId;
    }

    public void setTxId(String txId) {
        this.txId = txId;
    }
}
