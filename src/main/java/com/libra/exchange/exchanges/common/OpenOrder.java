package com.libra.exchange.exchanges.common;

/**
 * Created by rongqiang.li on 17/4/15.
 */
public class OpenOrder{

    //[{"orderNumber":"40852492792","type":"sell","rate":"0.06900000","startingAmount":"0.01000000","amount":"0.01000000","total":"0.00069000","date":"2017-04-15 06:15:39","margin":0},{"orderNumber":"40853440843","type":"sell","rate":"0.06900000","startingAmount":"0.01000000","amount":"0.01000000","total":"0.00069000","date":"2017-04-15 06:18:30","margin":0}]
    private String orderNumber;
    private String type;
    private String rate;
    private String startingAmount;
    private String amount;
    private String total;
    private String margin;
    private String date;

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getStartingAmount() {
        return startingAmount;
    }

    public void setStartingAmount(String startingAmount) {
        this.startingAmount = startingAmount;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getMargin() {
        return margin;
    }

    public void setMargin(String margin) {
        this.margin = margin;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

}
