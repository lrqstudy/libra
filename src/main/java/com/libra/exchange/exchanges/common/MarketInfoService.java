package com.libra.exchange.exchanges.common;

/**
 * Created by rongqiang.li on 2017/4/28.
 */
public interface MarketInfoService {
    /**
     * 根据深度范围买卖单
     * @param currencyPair
     * @param depth
     * @return
     */
    public String returnOrderBook(String currencyPair, int depth);

    /**
     * 对应交易品种的市场信息
     * @return
     */
    public String returnTicker(String targetCode,String cashCurrrency);
}
