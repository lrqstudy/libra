package com.libra.exchange.exchanges.bitfinex;

import com.google.api.client.repackaged.com.google.common.base.Joiner;
import com.google.api.client.util.Lists;
import com.google.api.client.util.Maps;
import com.google.common.base.Strings;
import com.libra.exchange.exchanges.common.*;
import com.libra.exchange.exchanges.poloniex.PoloniexExchangeServiceImpl;
import com.libra.exchange.strategy.model.PlatformConfigModel;
import org.knowm.xchange.Exchange;
import org.knowm.xchange.ExchangeFactory;
import org.knowm.xchange.bitfinex.v1.BitfinexExchange;
import org.knowm.xchange.currency.CurrencyPair;
import org.knowm.xchange.dto.marketdata.Ticker;
import org.knowm.xchange.service.marketdata.MarketDataService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * Created by rongqiang.li on 17/10/21.
 */
@Service
public class BitfinexTransferServiceImpl implements CommonTransferService {

    @Resource
    private BitfinexUtils bitfinexUtils;
    @Resource
    private BitfinexMarketApiUtil bitfinexMarketApiUtil;
    @Resource
    private BitfinexSuperUtils bitfinexSuperUtils;

    private Logger logger = LoggerFactory.getLogger(PoloniexExchangeServiceImpl.class);

    private Map<String, CurrencyPair> symbolCurrencyPair;

    private Map<String, String> currencyPairMap;
    private Map<String, String> targetCodeMap;

    private Map<String, String> bitfinexSpecialTargetCodeAndCommonTargetCodeMap;

    @PostConstruct
    public void init() {
        symbolCurrencyPair = Maps.newHashMap();
        currencyPairMap = Maps.newHashMap();
        targetCodeMap = Maps.newHashMap();
        bitfinexSpecialTargetCodeAndCommonTargetCodeMap = Maps.newHashMap();

        /**
         * SPECIAL currency pair
         */
        symbolCurrencyPair.put("QTUMETH", new CurrencyPair("QTM", "ETH"));
        symbolCurrencyPair.put("QTUMBTC", new CurrencyPair("QTM", "BTC"));

        symbolCurrencyPair.put("DASHETH", new CurrencyPair("DSH", "ETH"));
        symbolCurrencyPair.put("DASHBTC", new CurrencyPair("DSH", "BTC"));
        symbolCurrencyPair.put("DASHUSD", new CurrencyPair("DSH", "USD"));

        symbolCurrencyPair.put("IOTAETH", new CurrencyPair("IOT", "ETH"));
        symbolCurrencyPair.put("IOTABTC", new CurrencyPair("IOT", "BTC"));
        symbolCurrencyPair.put("IOTAUSD", new CurrencyPair("IOT", "USD"));

        symbolCurrencyPair.put("YOYOETH", new CurrencyPair("YYW", "ETH"));
        symbolCurrencyPair.put("YOYOBTC", new CurrencyPair("YYW", "BTC"));


        symbolCurrencyPair.put("MANAETH", new CurrencyPair("MNA", "ETH"));
        symbolCurrencyPair.put("MANABTC", new CurrencyPair("MNA", "BTC"));


        symbolCurrencyPair.put("EOSBTC", CurrencyPair.EOS_BTC);
        symbolCurrencyPair.put("EOSETH", CurrencyPair.EOS_ETH);
        symbolCurrencyPair.put("EOSUSD", new CurrencyPair("EOS", "USD"));


        symbolCurrencyPair.put("ZECBTC", CurrencyPair.ZEC_BTC);
        symbolCurrencyPair.put("ZECETH", new CurrencyPair("ZEC", "ETH"));
        symbolCurrencyPair.put("ZECUSD", new CurrencyPair("ZEC", "USD"));

        symbolCurrencyPair.put("ETCBTC", CurrencyPair.ETC_BTC);
        symbolCurrencyPair.put("ETCETH", CurrencyPair.ETC_ETH);


        symbolCurrencyPair.put("LTCETH", new CurrencyPair("LTC", "ETH"));
        symbolCurrencyPair.put("LTCBTC", new CurrencyPair("LTC", "BTC"));
        symbolCurrencyPair.put("LTCUSD", new CurrencyPair("LTC", "USD"));


        symbolCurrencyPair.put("TNBETH", new CurrencyPair("TNB", "ETH"));
        symbolCurrencyPair.put("TNBBTC", new CurrencyPair("TNB", "BTC"));

        symbolCurrencyPair.put("SNTETH", new CurrencyPair("SNT", "ETH"));
        symbolCurrencyPair.put("SNTBTC", new CurrencyPair("SNT", "BTC"));

        symbolCurrencyPair.put("EDOETH", new CurrencyPair("EDO", "ETH"));
        symbolCurrencyPair.put("EDOBTC", new CurrencyPair("EDO", "BTC"));

        symbolCurrencyPair.put("AVTETH", new CurrencyPair("AVT", "ETH"));
        symbolCurrencyPair.put("AVTBTC", new CurrencyPair("AVT", "BTC"));

        symbolCurrencyPair.put("NEOETH", new CurrencyPair("NEO", "ETH"));
        symbolCurrencyPair.put("NEOBTC", new CurrencyPair("NEO", "BTC"));

        symbolCurrencyPair.put("ZRXETH", new CurrencyPair("ZRX", "ETH"));
        symbolCurrencyPair.put("ZRXBTC", new CurrencyPair("ZRX", "BTC"));

        symbolCurrencyPair.put("RLCETH", new CurrencyPair("RLC", "ETH"));
        symbolCurrencyPair.put("RLCBTC", new CurrencyPair("RLC", "BTC"));

        symbolCurrencyPair.put("RCNETH", new CurrencyPair("RCN", "ETH"));
        symbolCurrencyPair.put("RCNBTC", new CurrencyPair("RCN", "BTC"));

        symbolCurrencyPair.put("TRXETH", new CurrencyPair("TRX", "ETH"));
        symbolCurrencyPair.put("TRXBTC", new CurrencyPair("TRX", "BTC"));

        symbolCurrencyPair.put("SANETH", new CurrencyPair("SAN", "ETH"));
        symbolCurrencyPair.put("SANBTC", new CurrencyPair("SAN", "BTC"));

        symbolCurrencyPair.put("BATETH", new CurrencyPair("BAT", "ETH"));
        symbolCurrencyPair.put("BATBTC", new CurrencyPair("BAT", "BTC"));

        symbolCurrencyPair.put("FUNETH", new CurrencyPair("FUN", "ETH"));
        symbolCurrencyPair.put("FUNBTC", new CurrencyPair("FUN", "BTC"));

        symbolCurrencyPair.put("OMGETH", new CurrencyPair("OMG", "ETH"));
        symbolCurrencyPair.put("OMGBTC", new CurrencyPair("OMG", "BTC"));

        symbolCurrencyPair.put("ELFETH", new CurrencyPair("ELF", "ETH"));
        symbolCurrencyPair.put("ELFBTC", new CurrencyPair("ELF", "BTC"));

        symbolCurrencyPair.put("REPETH", new CurrencyPair("REP", "ETH"));
        symbolCurrencyPair.put("REPBTC", new CurrencyPair("REP", "BTC"));

        symbolCurrencyPair.put("REPETH", new CurrencyPair("REP", "ETH"));
        symbolCurrencyPair.put("REPBTC", new CurrencyPair("REP", "BTC"));


        /**
         * BITFINEX transfer some currencyPair
         */
        currencyPairMap.put("QTUMETH", "QTMETH");
        currencyPairMap.put("QTUMBTC", "QTMBTC");
        currencyPairMap.put("QTUMUSD", "QTMUSD");

        currencyPairMap.put("DASHETH", "DSHETH");
        currencyPairMap.put("DASHBTC", "DSHBTC");
        currencyPairMap.put("DASHUSD", "DSHUSD");

        currencyPairMap.put("IOTAETH", "IOTETH");
        currencyPairMap.put("IOTABTC", "IOTBTC");
        currencyPairMap.put("IOTAUSD", "IOTUSD");

        currencyPairMap.put("YOYOETH", "YYWETH");
        currencyPairMap.put("YOYOBTC", "YYWBTC");

        currencyPairMap.put("MANAETH", "MNAETH");
        currencyPairMap.put("MANABTC", "MNABTC");


        /**
         *
         */
        targetCodeMap.put("QTUM", "QTM");
        targetCodeMap.put("DASH", "DSH");
        targetCodeMap.put("IOTA", "IOT");
        targetCodeMap.put("YOYO", "YYW");
        targetCodeMap.put("MANA", "MNA");


        bitfinexSpecialTargetCodeAndCommonTargetCodeMap.put("QTM", "QTUM");
        bitfinexSpecialTargetCodeAndCommonTargetCodeMap.put("DSH", "DASH");
        bitfinexSpecialTargetCodeAndCommonTargetCodeMap.put("IOT", "IOTA");
        bitfinexSpecialTargetCodeAndCommonTargetCodeMap.put("YYW", "YOYO");
        bitfinexSpecialTargetCodeAndCommonTargetCodeMap.put("MNA", "MANA");
    }


    @Override
    public CommonOrderBook getOrderBook(String targetCode, String cashCurrency, int depth) {
        String currencyPair = getCurrencyPair(targetCode, cashCurrency);
        String symbol = currencyPairMap.get(currencyPair);
        if (!Strings.isNullOrEmpty(symbol)) {
            currencyPair = symbol;
        }
        BitfinexOrderBookVO orderBook = bitfinexMarketApiUtil.getOrderBook(currencyPair, depth, depth);
        CommonOrderBook commonOrderBook = new CommonOrderBook();
        commonOrderBook.setBuyOrderVOList(buildCommonOrderVO(orderBook.getBids()));
        commonOrderBook.setSellOrderVOList(buildCommonOrderVO(orderBook.getAsks()));
        return commonOrderBook;
    }

    private List<CommonOrderVO> buildCommonOrderVO(List<BitfinexOrderBookJsonVO> jsonVOList) {
        List<CommonOrderVO> commonOrderVOList = Lists.newArrayList();
        for (BitfinexOrderBookJsonVO bookJsonVO : jsonVOList) {
            CommonOrderVO commonOrderVO = new CommonOrderVO();
            BigDecimal btcPrice = new BigDecimal(bookJsonVO.getPrice());
            commonOrderVO.setBtcPrice(btcPrice);
            commonOrderVO.setPrice(btcPrice);
            commonOrderVO.setVolume(new BigDecimal(bookJsonVO.getAmount()));
            commonOrderVOList.add(commonOrderVO);
        }
        return commonOrderVOList;
    }


    @Override
    public Map<String, BigDecimal> getBalanceMap(PlatformConfigModel configModel) {
        Map<BalanceTypeKey, BigDecimal> allBalanceMap = bitfinexUtils.getBalance(configModel);
        Map<String, BigDecimal> balanceMap = Maps.newHashMap();
        for (Map.Entry<BalanceTypeKey, BigDecimal> entry : allBalanceMap.entrySet()) {
            entry.getKey().getType();
            if (BalanceInfoType.EXCHANGE.getName().equals(entry.getKey().getType())) {
                String currency = entry.getKey().getCurrency();
                balanceMap.put(currency, entry.getValue());
                String commonTargetCode = bitfinexSpecialTargetCodeAndCommonTargetCodeMap.get(currency);
                if (!Strings.isNullOrEmpty(commonTargetCode)) {
                    balanceMap.put(commonTargetCode, entry.getValue());
                }
            }
        }
        logger.info(" bitfinex get balance size ={}", allBalanceMap.size());
        return balanceMap;
    }

    @Override
    public String getCurrencyPair(String targetCode, String cashCurrency) {
        return Joiner.on("").join(targetCode.toUpperCase(), cashCurrency.toUpperCase());
    }


    @Override
    public String buy(String targetCode, String cashCurrency, BigDecimal price, BigDecimal amount, Integer precisionSize, PlatformConfigModel configModel, Integer orderSource) {
        logger.info("bitfinex buy param  targetCode={}, cashCurrency={}, price={}, amount={},precisionSize={},orderSource={} ", targetCode, cashCurrency, price, amount, precisionSize, orderSource);
        String result = bitfinexSuperUtils.buy(targetCode, cashCurrency, configModel, symbolCurrencyPair.get(getCurrencyPair(targetCode, cashCurrency)), price, amount, precisionSize, orderSource);
        return result;
    }

    @Override
    public String sell(String targetCode, String cashCurrency, BigDecimal price, BigDecimal amount, Integer precisionSize, PlatformConfigModel configModel, Integer orderSource) {
        logger.info("bitfinex sell param  targetCode={}, cashCurrency={}, price={}, amount={},precisionSize={},orderSource={} ", targetCode, cashCurrency, price, amount, precisionSize, orderSource);
        String result = bitfinexSuperUtils.sell(targetCode, cashCurrency, configModel, symbolCurrencyPair.get(getCurrencyPair(targetCode, cashCurrency)), price, amount, precisionSize, orderSource);
        return result;
    }


    @Override
    public String withdraw(String targetCode, String address, String memo, BigDecimal amount, PlatformConfigModel configModel) {
        return bitfinexSuperUtils.withdraw(configModel, targetCode, amount, address, memo);
    }

    @Override
    public List<CommonOpenOrderVO> getOpenOrderList(PlatformConfigModel platformConfigModel, String targetCode, String cashCurrency) {
        CurrencyPair currencyPair = symbolCurrencyPair.get(getCurrencyPair(targetCode, cashCurrency));
        List<CommonOpenOrderVO> openOrderList = bitfinexSuperUtils.getOpenOrderList(platformConfigModel, targetCode, cashCurrency, currencyPair);
        logger.info(" bitfinex get targetCode={},cashCurrency={}  open order list size ={}", targetCode, cashCurrency, openOrderList.size());
        return openOrderList;
    }


    public CommonTickerVO getTickerBySymbol(String targetCode, String cashCurrency) {
        try {
            String currencyPairStr = getCurrencyPair(targetCode, cashCurrency);
            CurrencyPair currencyPair = symbolCurrencyPair.get(currencyPairStr);
            Exchange bitfinex = ExchangeFactory.INSTANCE.createExchange(BitfinexExchange.class.getName());
            MarketDataService marketDataService = bitfinex.getMarketDataService();
            Ticker ticker = marketDataService.getTicker(currencyPair);
            CommonTickerVO commonTickerVO = new CommonTickerVO();
            commonTickerVO.setSymbol(currencyPairStr);
            BeanUtils.copyProperties(ticker, commonTickerVO);
            return commonTickerVO;
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return null;
        }
    }
}
