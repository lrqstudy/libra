package com.libra.exchange.exchanges.bitfinex;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;

/**
 * Created by rongqiang.li on 17/10/21.
 */
public class BitfinexOrderBookJsonVO implements Serializable{
    //price":"0.00008837","amount":"1473.54985926","timestamp
    private String price;
    private String amount;
    private String timestamp;

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
