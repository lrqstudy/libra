package com.libra.exchange.exchanges.bitfinex;

import com.libra.common.util.JsonUtil;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;
import java.util.List;

/**
 * Created by rongqiang.li on 17/10/21.
 */
public class BitfinexOrderBookVO implements Serializable {
    /*
    {"bids":[{"price":"0.00008837","amount":"1473.54985926","timestamp":"1508555306.0"}],]
    "asks":[{"price":"0.00008872","amount":"32.85948876","timestamp":"1508555306.0"}]}
    * */
    private List<BitfinexOrderBookJsonVO> asks;
    private List<BitfinexOrderBookJsonVO> bids;

    public List<BitfinexOrderBookJsonVO> getAsks() {
        return asks;
    }

    public void setAsks(List<BitfinexOrderBookJsonVO> asks) {
        this.asks = asks;
    }

    public List<BitfinexOrderBookJsonVO> getBids() {
        return bids;
    }

    public void setBids(List<BitfinexOrderBookJsonVO> bids) {
        this.bids = bids;
    }
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }


    public static void main(String[] args) {
        String json="{\"bids\":[{\"price\":\"0.00008837\",\"amount\":\"1473.54985926\",\"timestamp\":\"1508555306.0\"}],\"asks\":[{\"price\":\"0.00008872\",\"amount\":\"32.85948876\",\"timestamp\":\"1508555306.0\"}]}";
        BitfinexOrderBookVO bitfinexOrderBookVO = JsonUtil.jsonToObject(json, BitfinexOrderBookVO.class);
        System.out.println(bitfinexOrderBookVO.asks);
        test();
    }

    public static void test(){

        String json="[{\"type\":\"deposit\",\"currency\":\"btc\",\"amount\":\"0.0\",\"available\":\"0.0\"},{\"type\":\"deposit\",\"currency\":\"usd\",\"amount\":\"1.0\",\"available\":\"1.0\"},{\"type\":\"exchange\",\"currency\":\"btc\",\"amount\":\"1\",\"available\":\"1\"},{\"type\":\"exchange\",\"currency\":\"usd\",\"amount\":\"1\",\"available\":\"1\"},{\"type\":\"trading\",\"currency\":\"btc\",\"amount\":\"1\",\"available\":\"1\"},{\"type\":\"trading\",\"currency\":\"usd\",\"amount\":\"1\",\"available\":\"1\"}]";

        BalanceDetatilVO[] balanceWalletVOs = JsonUtil.jsonToObject(json, BalanceDetatilVO[].class);
        List<BalanceDetatilVO> balanceDetatilVOs = JsonUtil.jsonToObjectList(json, BalanceDetatilVO.class);
        System.out.println(balanceWalletVOs);


        /*
                    {
              "type":"deposit",
              "currency":"btc",
              "amount":"0.0",
              "available":"0.0"
            },{
              "type":"deposit",
              "currency":"usd",
              "amount":"1.0",
              "available":"1.0"
            },{
              "type":"exchange",
              "currency":"btc",
              "amount":"1",
              "available":"1"
            },{
              "type":"exchange",
              "currency":"usd",
              "amount":"1",
              "available":"1"
            },{
              "type":"trading",
              "currency":"btc",
              "amount":"1",
              "available":"1"
            },{
              "type":"trading",
              "currency":"usd",
              "amount":"1",
              "available":"1"
            },
            ...]
        * */
    }
}
