package com.libra.exchange.exchanges.bitfinex;

/**
 * Created by rongqiang.li on 17/10/21.
 */
public enum BalanceInfoType {
    TRADING(1, "trading", "trading"),
    DEPOSIT(2, "deposit", "deposit"),
    EXCHANGE(3, "exchange", "exchange"),;


    BalanceInfoType(int id, String name, String desc) {
        this.id = id;
        this.name = name;
        this.desc = desc;
    }

    private int id;
    private String name;
    private String desc;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
