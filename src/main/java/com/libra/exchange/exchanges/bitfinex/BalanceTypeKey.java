package com.libra.exchange.exchanges.bitfinex;

import java.io.Serializable;

/**
 * Created by rongqiang.li on 17/10/21.
 */
public class BalanceTypeKey implements Serializable {
    private String type;
    private String currency;

    public BalanceTypeKey(String type, String currency) {
        this.type=type;
        this.currency=currency;
    }


    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BalanceTypeKey that = (BalanceTypeKey) o;

        if (type != null ? !type.equals(that.type) : that.type != null) return false;
        return !(currency != null ? !currency.equals(that.currency) : that.currency != null);

    }

    @Override
    public int hashCode() {
        int result = type != null ? type.hashCode() : 0;
        result = 31 * result + (currency != null ? currency.hashCode() : 0);
        return result;
    }

    public static BalanceTypeKey build(String type,String currency){
        return new BalanceTypeKey(type,currency);
    }
}
