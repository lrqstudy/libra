package com.libra.exchange.exchanges.bitfinex;

import com.google.api.client.util.Maps;
import com.libra.common.exceptions.BTCTTSException;
import com.libra.common.util.JsonUtil;
import com.libra.exchange.strategy.model.PlatformConfigModel;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.Base64Utils;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.io.*;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.Map;

@Component
public class BitfinexUtils {

    private static final String TAG = BitfinexUtils.class.getSimpleName();

    private static final String ALGORITHM_HMACSHA384 = "HmacSHA384";

    private static Logger logger = LoggerFactory.getLogger(BitfinexUtils.class);


    /**
     * public access only
     */
    public BitfinexUtils() {

    }

    public Map<BalanceTypeKey, BigDecimal> getBalance(PlatformConfigModel configModel) {
        Map<String, Object> map = Maps.newHashMap();
        String json = executeCommand(configModel, "POST", "/v1/balances", map);
        List<BalanceDetatilVO> balanceDetatilVOList = JsonUtil.jsonToObjectList(json, BalanceDetatilVO.class);
        Map<BalanceTypeKey, BigDecimal> balanceMap = Maps.newHashMap();
        for (BalanceDetatilVO balanceDetatilVO : balanceDetatilVOList) {
            balanceMap.put(BalanceTypeKey.build(balanceDetatilVO.getType(), balanceDetatilVO.getCurrency().toUpperCase()), new BigDecimal(balanceDetatilVO.getAvailable()));
        }
        return balanceMap;
    }


    public String executeCommand(PlatformConfigModel configModel, String method, String command, Map<String, Object> parameterMaps) {
        logger.info(" bitfinex paramValues = {} ", parameterMaps);
        return authenticaRequestWithFormData(configModel, method, command, parameterMaps);
    }


    /**
     * Creates an authenticated request WITHOUT request parameters. Send a request for Balances.
     *
     * @return Response string if request successfull
     * @throws IOException
     */
    private String authenticaRequestWithFormData(PlatformConfigModel configModel, String method, String urlPath, Map<String, Object> paramValues) {
        String sResponse;
        HttpURLConnection conn = null;
        try {
            URL url = new URL("https://api.bitfinex.com" + urlPath);
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod(method);

            if (isAccessPublicOnly()) {
                String msg = "Authenticated access not possible, because key and secret was not initialized: use right constructor.";
                logger.error(TAG, msg);
                return msg;
            }

            conn.setDoOutput(true);
            conn.setDoInput(true);

            JSONObject jo = new JSONObject();
            jo.put("request", urlPath);
            jo.put("nonce", Long.toString(System.currentTimeMillis()));
            for (Map.Entry<String, Object> entry : paramValues.entrySet()) {
                jo.put(entry.getKey(), entry.getValue());
            }
            // API v1
            String payload = jo.toString();

            // this is usage for Base64 Implementation in Android. For pure java you can use java.util.Base64.Encoder
            // Base64.NO_WRAP: Base64-string have to be as one line string
            String payload_base64 = Base64Utils.encodeToString(payload.getBytes());
            //String payload_base64 = Base64.encodeToString(payload.getBytes(), Base64.NO_WRAP);


            String payload_sha384hmac = hmacDigest(payload_base64, configModel.getApiSecret(), ALGORITHM_HMACSHA384);

            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json");
            conn.addRequestProperty("X-BFX-APIKEY", configModel.getApiKey());
            conn.addRequestProperty("X-BFX-PAYLOAD", payload_base64);
            conn.addRequestProperty("X-BFX-SIGNATURE", payload_sha384hmac);

            // read the response
            InputStream in = new BufferedInputStream(conn.getInputStream());
            return convertStreamToString(in);

        } catch (MalformedURLException e) {
            throw new BTCTTSException(e.getClass().getName(), e);
        } catch (ProtocolException e) {
            throw new BTCTTSException(e.getClass().getName(), e);
        } catch (IOException e) {

            String errMsg = e.getLocalizedMessage();

            if (conn != null) {
                try {
                    sResponse = convertStreamToString(conn.getErrorStream());
                    errMsg += " -> " + sResponse;
                    logger.error(TAG, errMsg, e);
                    return sResponse;
                } catch (IOException e1) {
                    errMsg += " Error on reading error-stream. -> " + e1.getLocalizedMessage();
                    logger.error(TAG, errMsg, e);
                    throw new BTCTTSException(e.getClass().getName(), e1);
                }
            } else {
                throw new BTCTTSException(e.getClass().getName(), e);
            }
        } catch (JSONException e) {
            String msg = "Error on setting up the connection to server";
            throw new BTCTTSException(msg, e);
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }
    }


    private String convertStreamToString(InputStream is) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();

        String line;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line).append('\n');
            }
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }


    public boolean isAccessPublicOnly() {
        return false;
    }

    public static String hmacDigest(String msg, String keyString, String algo) {
        String digest = null;
        try {
            SecretKeySpec key = new SecretKeySpec((keyString).getBytes("UTF-8"), algo);
            Mac mac = Mac.getInstance(algo);
            mac.init(key);

            byte[] bytes = mac.doFinal(msg.getBytes("ASCII"));

            StringBuffer hash = new StringBuffer();
            for (int i = 0; i < bytes.length; i++) {
                String hex = Integer.toHexString(0xFF & bytes[i]);
                if (hex.length() == 1) {
                    hash.append('0');
                }
                hash.append(hex);
            }
            digest = hash.toString();
        } catch (UnsupportedEncodingException e) {
            logger.error(TAG, "Exception: " + e.getMessage());
        } catch (InvalidKeyException e) {
            logger.error(TAG, "Exception: " + e.getMessage());
        } catch (NoSuchAlgorithmException e) {
            logger.error(TAG, "Exception: " + e.getMessage());
        }
        return digest;
    }
}