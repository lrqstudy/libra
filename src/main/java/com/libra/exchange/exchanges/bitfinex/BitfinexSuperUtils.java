package com.libra.exchange.exchanges.bitfinex;

import com.google.api.client.repackaged.com.google.common.base.Joiner;
import com.google.api.client.repackaged.com.google.common.base.Strings;
import com.google.api.client.util.Lists;
import com.google.api.client.util.Maps;
import com.libra.common.consts.PlatformEnum;
import com.libra.common.consts.SystemConsts;
import com.libra.common.util.BeanUtilExt;
import com.libra.exchange.exchanges.common.CommonOpenOrderVO;
import com.libra.exchange.exchanges.common.CommonTickerVO;
import com.libra.exchange.exchanges.poloniex.PoloniexExchangeServiceImpl;
import com.libra.exchange.strategy.model.AltTransferErrorHandleModel;
import com.libra.exchange.strategy.model.PlatformConfigModel;
import com.libra.exchange.strategy.service.AltTransferErrorHandleService;
import com.libra.exchange.strategy.service.PlatformConfigService;
import org.knowm.xchange.Exchange;
import org.knowm.xchange.ExchangeFactory;
import org.knowm.xchange.ExchangeSpecification;
import org.knowm.xchange.bitfinex.v1.BitfinexExchange;
import org.knowm.xchange.bitfinex.v1.BitfinexOrderType;
import org.knowm.xchange.bitfinex.v1.dto.trade.BitfinexOrderStatusResponse;
import org.knowm.xchange.bitfinex.v1.service.BitfinexTradeServiceRaw;
import org.knowm.xchange.currency.CurrencyPair;
import org.knowm.xchange.dto.Order;
import org.knowm.xchange.dto.marketdata.Ticker;
import org.knowm.xchange.dto.trade.LimitOrder;
import org.knowm.xchange.service.marketdata.MarketDataService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by rongqiang.li on 17/10/22.
 */
@Component
public class BitfinexSuperUtils {

    private Logger logger = LoggerFactory.getLogger(PoloniexExchangeServiceImpl.class);

    private Map<String, String> currencyTypeAndWithdrawTypeMap = Maps.newHashMap();


    @Resource
    private PlatformConfigService platformConfigService;

    private Map<String, BitfinexTradeServiceRaw> userCodeBitfinexServiceMap = new ConcurrentHashMap<String, BitfinexTradeServiceRaw>();
    private Map<String, Exchange> userCodeBitfinexExchangeServiceMap = new ConcurrentHashMap<String, Exchange>();
    @Resource
    private AltTransferErrorHandleService altTransferErrorHandleService;

    @PostConstruct
    public void init() {
        //can be one of the following ['bitcoin', 'litecoin', 'ethereum', 'ethereumc', 'mastercoin', 'zcash', 'monero', 'wire', 'dash', 'ripple', 'eos', 'neo']
        currencyTypeAndWithdrawTypeMap.put("BTC", "bitcoin");
        currencyTypeAndWithdrawTypeMap.put("LTC", "litecoin");
        currencyTypeAndWithdrawTypeMap.put("EOS", "eos");
        currencyTypeAndWithdrawTypeMap.put("ETH", "ethereum");
        currencyTypeAndWithdrawTypeMap.put("ETC", "ethereumc");
        currencyTypeAndWithdrawTypeMap.put("ZEC", "zcash");
        currencyTypeAndWithdrawTypeMap.put("NEO", "neo");

        List<PlatformConfigModel> platformConfigModelByPlatform = platformConfigService.getPlatformConfigModelByPlatform(PlatformEnum.BITFINEX.getCode());
        for (PlatformConfigModel configModel : platformConfigModelByPlatform) {
            try {
                userCodeBitfinexExchangeServiceMap.put(configModel.getUserCode(), getExchange(configModel));
                userCodeBitfinexServiceMap.put(configModel.getUserCode(), buildBitfinexTradeServiceRaw(configModel));
                Thread.sleep(5000);
            } catch (Exception e) {
                logger.error(e.getMessage(), e);
            }
        }
    }


    public void getBalanceMap(PlatformConfigModel platformConfigModel) {

    }


    public String buy(String targetCode, String cashCurrency, PlatformConfigModel platformConfigModel, CurrencyPair currencyPair, BigDecimal price, BigDecimal amount, Integer precisionSize, Integer orderSource) {
        try {
            //amount = amount.multiply(new BigDecimal(1.002));//把手续费加上
            amount = amount.setScale(precisionSize, BigDecimal.ROUND_CEILING);
            LimitOrder limitOrder = new LimitOrder.Builder(Order.OrderType.BID, currencyPair).limitPrice(price)
                    .originalAmount(amount).build();
            BitfinexTradeServiceRaw bitfinexTradeServiceRaw = getBitfinexTradeServiceRaw(platformConfigModel);
            BitfinexOrderStatusResponse response = bitfinexTradeServiceRaw.placeBitfinexLimitOrder(limitOrder, BitfinexOrderType.LIMIT);
            logger.info(" success id={}", response.getId());
            return "success id=" + response.getId();
        } catch (Exception e) {
            logger.error(" buy error" + e.getMessage(), e);
            recordErrorOrder(targetCode, cashCurrency, platformConfigModel, currencyPair, price, amount, "buy", precisionSize, e, orderSource);
            return "error";
        }
    }

    private BitfinexTradeServiceRaw getBitfinexTradeServiceRaw(PlatformConfigModel platformConfigModel) {
        BitfinexTradeServiceRaw bitfinexTradeServiceRaw = userCodeBitfinexServiceMap.get(platformConfigModel.getUserCode());
        if (bitfinexTradeServiceRaw == null) {
            bitfinexTradeServiceRaw = buildBitfinexTradeServiceRaw(platformConfigModel);
            userCodeBitfinexServiceMap.put(platformConfigModel.getUserCode(), bitfinexTradeServiceRaw);
        }
        return bitfinexTradeServiceRaw;
    }

    public String sell(String targetCode, String cashCurrency, PlatformConfigModel platformConfigModel, CurrencyPair currencyPair, BigDecimal price, BigDecimal amount, Integer precisionSize, Integer orderSource) {
        try {
            amount = amount.setScale(precisionSize, BigDecimal.ROUND_CEILING);
            LimitOrder limitOrder = new LimitOrder.Builder(Order.OrderType.ASK, currencyPair).limitPrice(price)
                    .originalAmount(amount).build();

            BitfinexTradeServiceRaw bitfinexTradeServiceRaw = getBitfinexTradeServiceRaw(platformConfigModel);
            BitfinexOrderStatusResponse response = bitfinexTradeServiceRaw.placeBitfinexLimitOrder(limitOrder, BitfinexOrderType.LIMIT);
            long id = response.getId();
            logger.info(" success id={}", id);
            return "success id=" + id;
        } catch (Exception e) {
            logger.error(" sell error" + e.getMessage(), e);
            recordErrorOrder(targetCode, cashCurrency, platformConfigModel, currencyPair, price, amount, "sell", precisionSize, e, orderSource);
            return "error";
        }
    }

    private void recordErrorOrder(String targetCode, String cashCurrency, PlatformConfigModel platformConfigModel, CurrencyPair currencyPair, BigDecimal price, BigDecimal amount, String sell, Integer precisionSize, Exception e, Integer orderSource) {
        AltTransferErrorHandleModel model = new AltTransferErrorHandleModel();
        model.setCashCurrency(cashCurrency);
        model.setCurrencyPair(currencyPair.toString());
        model.setPlatform(platformConfigModel.getPlatform());
        model.setResultId("");
        model.setSide(sell);
        model.setTargetCode(targetCode);
        model.setTradeAmount(amount);
        model.setTradePrice(price);
        model.setUserCode(platformConfigModel.getUserCode());
        model.setPrecisionSize(precisionSize);
        String errorMsg = e.getMessage();
        model.setErrorMessage(errorMsg.substring(0, errorMsg.length() > SystemConsts.EXCEPTION_ERROR_MESSAGE_SIZE ? SystemConsts.EXCEPTION_ERROR_MESSAGE_SIZE : errorMsg.length()));
        model.setOrderSource(orderSource);
        BeanUtilExt.fillNullProperty(model);
        altTransferErrorHandleService.saveAltTransferErrorHandleModel(model);
    }


    private Exchange getExchange(PlatformConfigModel platformConfigModel) {
        Exchange exchange = userCodeBitfinexExchangeServiceMap.get(platformConfigModel.getUserCode());
        if (exchange == null) {
            exchange = buildExchangeInstance(platformConfigModel);
            userCodeBitfinexExchangeServiceMap.put(platformConfigModel.getUserCode(), exchange);
        }
        return exchange;
    }

    private Exchange buildExchangeInstance(PlatformConfigModel platformConfigModel) {
        Exchange bfx = ExchangeFactory.INSTANCE.createExchange(BitfinexExchange.class.getName());
        ExchangeSpecification bfxSpec = bfx.getDefaultExchangeSpecification();
        bfxSpec.setApiKey(platformConfigModel.getApiKey());
        bfxSpec.setSecretKey(platformConfigModel.getApiSecret());
        bfx.applySpecification(bfxSpec);
        return bfx;
    }


    private BitfinexTradeServiceRaw buildBitfinexTradeServiceRaw(PlatformConfigModel platformConfigModel) {
        Exchange exchange = buildExchangeInstance(platformConfigModel);
        BitfinexTradeServiceRaw tradeService = (BitfinexTradeServiceRaw) exchange.getTradeService();
        return tradeService;
    }

    public String withdraw(PlatformConfigModel platformConfigModel, String currencyType, BigDecimal amount, String address, String memo) {
        try {
            logger.info("currencyType={}, amount={}, address={},memo={} ", currencyType, address, address, memo);
            String withdrawId = "";
            BitfinexTradeServiceRaw bitfinexTradeServiceRaw = userCodeBitfinexServiceMap.get(platformConfigModel.getUserCode());
            if (!Strings.isNullOrEmpty(memo)) {
                withdrawId = bitfinexTradeServiceRaw.withdraw(currencyTypeAndWithdrawTypeMap.get(currencyType), "exchange", amount, address, memo);
            } else {
                withdrawId = bitfinexTradeServiceRaw.withdraw(currencyTypeAndWithdrawTypeMap.get(currencyType), "exchange", amount, address);
            }
            logger.info("withdraw result currencyType={}, amount={}, address={},memo={},withdrawId={} ", currencyType, address, address, memo, withdrawId);
            return withdrawId;
        } catch (Exception e) {
            logger.error(" withdral error " + e.getMessage(), e);
            return null;
        }
    }


    public List<CommonOpenOrderVO> getOpenOrderList(PlatformConfigModel platformConfigModel, String targetCode, String cashCurrency, CurrencyPair currencyPair) {
        try {
            List<CommonOpenOrderVO> commonOpenOrderVOList = Lists.newArrayList();
            String symbol = Joiner.on("").join(targetCode, cashCurrency);
            BitfinexOrderStatusResponse[] bitfinexOpenOrders = userCodeBitfinexServiceMap.get(platformConfigModel.getUserCode()).getBitfinexOpenOrders();
            for (BitfinexOrderStatusResponse bitfinexOpenOrder : bitfinexOpenOrders) {
                //IOTBTC     IOTA BTC
                String openOrderSymbol = bitfinexOpenOrder.getSymbol();
                if (openOrderSymbol.toUpperCase().contains(currencyPair.base.getCurrencyCode()) && openOrderSymbol.toUpperCase().contains(currencyPair.counter.getCurrencyCode())) {
                    CommonOpenOrderVO commonOpenOrderVO = new CommonOpenOrderVO();
                    commonOpenOrderVO.setOrderId(String.valueOf(bitfinexOpenOrder.getId()));
                    commonOpenOrderVO.setOriginAmount(bitfinexOpenOrder.getOriginalAmount());
                    commonOpenOrderVO.setPrice(bitfinexOpenOrder.getPrice());
                    commonOpenOrderVO.setRemainAmount(bitfinexOpenOrder.getRemainingAmount());
                    commonOpenOrderVO.setSide(bitfinexOpenOrder.getSide());
                    commonOpenOrderVO.setSymbol(symbol);
                    commonOpenOrderVO.setType(bitfinexOpenOrder.getType());
                    commonOpenOrderVOList.add(commonOpenOrderVO);
                }

            }
            return commonOpenOrderVOList;
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return null;
        }
    }


    public CommonTickerVO getTickerBySymbol(String targetCode, String cashCurrency, CurrencyPair currencyPair) {
        try {
            CommonTickerVO commonTickerVO = new CommonTickerVO();
            // Use the factory to get Bitfinex exchange API using default settings
            Exchange bitfinex = ExchangeFactory.INSTANCE.createExchange(BitfinexExchange.class.getName());
            // Interested in the public market data feed (no authentication)
            MarketDataService marketDataService = bitfinex.getMarketDataService();
            // Get the latest ticker data showing BTC to USD
            Ticker ticker = marketDataService.getTicker(currencyPair);
            System.out.println(ticker.toString());
            BeanUtils.copyProperties(ticker,commonTickerVO);
            return commonTickerVO;
        } catch (Exception e) {
            return null;
        }
    }

}
