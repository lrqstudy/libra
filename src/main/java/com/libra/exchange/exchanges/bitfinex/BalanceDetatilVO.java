package com.libra.exchange.exchanges.bitfinex;

import java.io.Serializable;

/**
 * Created by rongqiang.li on 17/10/21.
 */
public class BalanceDetatilVO implements Serializable{
    /*
      "type":"deposit",
              "currency":"btc",
              "amount":"0.0",
              "available":"0.0"
    * */

    private String type;
    private String currency;
    private String amount;
    private String available;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getAvailable() {
        return available;
    }

    public void setAvailable(String available) {
        this.available = available;
    }
}
