package com.libra.exchange.exchanges.bitfinex;

import com.libra.exchange.exchanges.poloniex.PoloniexExchangeServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;

/**
 * Created by rongqiang.li on 17/10/21.
 */
@Component
public class BitfinexMarketApiUtil {

    public static final String BITFINEX_URL = "https://api.bitfinex.com/v1/";

    @Resource
    private RestTemplate restTemplate;
    private Logger logger = LoggerFactory.getLogger(PoloniexExchangeServiceImpl.class);


    public BitfinexOrderBookVO getOrderBook(String symbol,Integer limitBids,Integer limitAsks){
        StringBuilder sb=new StringBuilder();
        sb.append(BITFINEX_URL).append("book/").append(symbol).append("?").append("limit_bids=").
                append(limitBids).append("&").append("limit_asks=").append(limitAsks);
        BitfinexOrderBookVO bitfinexOrderBookVO = restTemplate.getForObject(sb.toString(), BitfinexOrderBookVO.class);
        logger.info(" query symbol={} limit {} ,result ={}  ",symbol,limitAsks,bitfinexOrderBookVO.toString());
        return bitfinexOrderBookVO;
    }


}
