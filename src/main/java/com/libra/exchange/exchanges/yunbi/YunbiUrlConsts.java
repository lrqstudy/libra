package com.libra.exchange.exchanges.yunbi;

/**
 * Created by rongqiang.li on 2017/3/30.
 */
public class YunbiUrlConsts {
    /**
     * 获取当前账户信息
     */
    final public static String GET_MY_SELF = "/api/v2/members/me.json";
    //api/v2/members/me.json
    /**
     * 获取云币网当前所有的市场行情信息
     */
    final public static String GET_ALL_TICKERS = "/api/v2/tickers.json";
    /**
     * 获取云币当前支持的交易对
     */
    final public static String GET_ALL_MARKET_CODE = "/api/v2/markets.json";
    /**
     * 获取所有充值记录
     */
    final public static String GET_DEPOSIT_HISTORY = "/api/v2/deposits.json";
    /**
     * 根据交易id 获取充值历史记录
     */
    final public static String GET_DEPOSIT_HISTORY_BY_TXID = "/api/v2/deposit.json";
    /**
     * 获取新的区块链资产充值地址
     */
    final public static String GET_NEW_DEPOSIT_ADDRESS = "/api/v2/deposit_address.json";
    /**
     * 根据orderId获取指定订单信息
     */
    final public static String GET_ORDER_DETAIL_BY_ORDER_ID = "/api/v2/order.json";
    /**
     * 根据orderId 取消指定订单: POST method
     */
    final public static String CANCEL_ORDER_BY_ORDER_ID = "/api/v2/order/delete.json";
    /**
     * 获取指定市场的委托单信息
     */
    final public static String GET_SPECIFIC_MARKET_ORDER_BOOK_INFO = "/api/v2/order_book.json";
    /**
     * 获取你所有的订单信息
     */
    final public static String GET_ALL_YOUR_ORDERS_INFO = "/api/v2/orders.json";
    /**
     * 创建单个订单订单:POST
     */
    final public static String CREATE_SINGLE_NEW_ORDER = "/api/v2/orders.json";
    /**
     * 创建多个新订单:POST
     */
    final public static String CREATE_MULTI_NEW_ORDER = "/api/v2/orders/multi.json";
    /**
     * 取消你所有的订单:POST
     */
    final public static String CANCLE_ALL_ORDERS = "/api/v2/orders/clear.json";
    /**
     * 获取指定市场的交易深度
     */
    final public static String GET_SPECIFIC_MARKET_DEPTH = "/api/v2/depth.json";
    /**
     * 获取公开的交易历史记录
     */
    final public static String GET_TRADE_HISTORY = "/api/v2/trades.json";
    /**
     * 获取我的公开历史交易记录
     */
    final public static String GET_MY_TRADE_HISTORY = "/api/v2/trades/my.json";
    /**
     * 获取当前的时间戳
     */
    final public static String GET_CURRENT_TIMESTAMP = "/api/v2/timestamp.json";
    /**
     * 创建新的提现申请:POST
     */
    final public static String GENERATE_WITHDRAW = "/api/v2/withdraw.json";

}
