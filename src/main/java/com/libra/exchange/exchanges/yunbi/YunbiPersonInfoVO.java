package com.libra.exchange.exchanges.yunbi;

import com.libra.common.util.JsonUtil;

import java.io.Serializable;
import java.util.List;

/**
 * Created by rongqiang.li on 2017/5/1.
 */
public class YunbiPersonInfoVO implements Serializable{
    private String sn;
    private String name;
    private String email;
    private String activated;
    private String memo;
    private List<YunbiBalanceVO> accounts;

    public String getSn() {
        return sn;
    }

    public void setSn(String sn) {
        this.sn = sn;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getActivated() {
        return activated;
    }

    public void setActivated(String activated) {
        this.activated = activated;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public List<YunbiBalanceVO> getAccounts() {
        return accounts;
    }

    public void setAccounts(List<YunbiBalanceVO> accounts) {
        this.accounts = accounts;
    }

    public static void main(String[] args) {
        String jsonStr="{\"sn\":\"PEAS5OASPWYTIO\",\"name\":\"李荣强\",\"email\":\"lrqstudy@gmail.com\",\"activated\":true,\"memo\":\"13670027\",\"accounts\":[{\"currency\":\"cny\",\"balance\":\"12173.140724991\",\"locked\":\"0.0\"},{\"currency\":\"btc\",\"balance\":\"0.0000478\",\"locked\":\"0.0\"},{\"currency\":\"ltc\",\"balance\":\"0.0\",\"locked\":\"0.0\"},{\"currency\":\"doge\",\"balance\":\"0.0\",\"locked\":\"0.0\"},{\"currency\":\"bts\",\"balance\":\"18715.50245\",\"locked\":\"0.0\"},{\"currency\":\"bitcny\",\"balance\":\"0.0\",\"locked\":\"0.0\"},{\"currency\":\"bitusd\",\"balance\":\"0.0\",\"locked\":\"0.0\"},{\"currency\":\"bitbtc\",\"balance\":\"0.0\",\"locked\":\"0.0\"},{\"currency\":\"note\",\"balance\":\"0.0\",\"locked\":\"0.0\"},{\"currency\":\"pls\",\"balance\":\"0.0\",\"locked\":\"0.0\"},{\"currency\":\"nxt\",\"balance\":\"0.0\",\"locked\":\"0.0\"},{\"currency\":\"eth\",\"balance\":\"0.2802941009882196\",\"locked\":\"0.0\"},{\"currency\":\"sc\",\"balance\":\"0.0\",\"locked\":\"0.0\"},{\"currency\":\"dgd\",\"balance\":\"0.0000456\",\"locked\":\"0.0\"},{\"currency\":\"dcs\",\"balance\":\"0.0\",\"locked\":\"0.0\"},{\"currency\":\"dao\",\"balance\":\"0.0\",\"locked\":\"0.0\"},{\"currency\":\"etc\",\"balance\":\"0.0\",\"locked\":\"0.0\"},{\"currency\":\"amp\",\"balance\":\"0.0\",\"locked\":\"0.0\"},{\"currency\":\"1st\",\"balance\":\"0.00092\",\"locked\":\"0.0\"},{\"currency\":\"rep\",\"balance\":\"0.0\",\"locked\":\"0.0\"},{\"currency\":\"ans\",\"balance\":\"0.0\",\"locked\":\"0.0\"},{\"currency\":\"zec\",\"balance\":\"0.0009632900756631\",\"locked\":\"0.0\"},{\"currency\":\"zmc\",\"balance\":\"0.0020315591364111\",\"locked\":\"0.0\"},{\"currency\":\"gnt\",\"balance\":\"0.917\",\"locked\":\"0.0\"},{\"currency\":\"yun\",\"balance\":\"0.0\",\"locked\":\"0.0\"}]}";
        YunbiPersonInfoVO yunbiPersonInfoVO = JsonUtil.jsonToObject(jsonStr, YunbiPersonInfoVO.class);
        System.out.println(yunbiPersonInfoVO.getName());
    }
}
