package com.libra.exchange.exchanges.yunbi;

import com.google.common.base.Strings;
import com.google.common.collect.Maps;
import com.libra.common.exceptions.BTCTTSException;
import com.libra.common.util.JsonUtil;
import com.libra.exchange.strategy.model.PlatformConfigModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.math.BigDecimal;
import java.net.*;
import java.util.List;
import java.util.Map;

/**
 * Created by rongqiang.li on 2017/5/1.
 */
@Service
public class YunbiExchangeHelper {

    private static final String BASE_URL = "https://yunbi.com/";
    private static final String USER_AGENT = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36";
    private Logger logger = LoggerFactory.getLogger(YunbiExchangeHelper.class);

    private static final String poloniexGntAddress = "";

    private static final double MIN_TRADE_VOLUME = 0.001D;


    @Resource
    private RestTemplate restTemplate;

    //curl -X POST --header 'Content-Type: application/x-www-form-urlencoded'
    // --header 'Accept: application/json' -d
    // 'access_key=aaaaa&tonce=bbbbb&signature=sssss&market=sccny&side=sell&volume=10&price=0.0089' 'https://yunbi.com//api/v2/orders.json'
    public String trade(String market, String side, BigDecimal price, BigDecimal volume, PlatformConfigModel configModel) {

        if (volume.compareTo(new BigDecimal(MIN_TRADE_VOLUME)) <= 0) {
            logger.info("market ={},side={}, price={},volume={} will be refused ,because trade volume less than " + MIN_TRADE_VOLUME,market,side,price,volume);
            return "";
        }
        //String.valueOf(sellPrice.doubleValue())
        long tonce = System.currentTimeMillis();
        Map<String, String> paramStrMap = Maps.newHashMap();
        paramStrMap.put("access_key", configModel.getApiKey());
        paramStrMap.put("tonce", String.valueOf(tonce));
        paramStrMap.put("market", market);
        paramStrMap.put("price", String.valueOf(price.doubleValue()));
        paramStrMap.put("volume", String.valueOf(volume.doubleValue()));
        paramStrMap.put("side", side);
        String result = authRequest("POST", YunbiUrlConsts.CREATE_SINGLE_NEW_ORDER, paramStrMap, configModel);
        return result;
    }

    public String withdraw(BigDecimal amount, String addressId, String memo, PlatformConfigModel configModel) {
        //地址
        Map<String, String> paramStrMap = Maps.newHashMap();
        long tonce = System.currentTimeMillis();
        paramStrMap.put("access_key", configModel.getApiKey());
        paramStrMap.put("tonce", String.valueOf(tonce));
        paramStrMap.put("amount", String.valueOf(amount.doubleValue()));// 提现数量
        paramStrMap.put("id", addressId);//提现地址id
        paramStrMap.put("memo", memo);//提现备注
        String result = authRequest("POST", YunbiUrlConsts.GENERATE_WITHDRAW, paramStrMap, configModel);
        return result;
    }

    public Map<String, BigDecimal> getBalanceMap(PlatformConfigModel configModel) {
        long tonce = System.currentTimeMillis();
        Map<String, String> paramStrMap = Maps.newHashMap();
        paramStrMap.put("access_key", configModel.getApiKey());
        paramStrMap.put("tonce", String.valueOf(tonce));
        String result = authRequestGet("GET", "/api/v2/members/me.json", paramStrMap, configModel);
        YunbiPersonInfoVO yunbiPersonInfoVO = JsonUtil.jsonToObject(result, YunbiPersonInfoVO.class);
        List<YunbiBalanceVO> accounts = yunbiPersonInfoVO.getAccounts();
        Map<String, BigDecimal> balanceMap = Maps.newHashMap();
        for (YunbiBalanceVO account : accounts) {
            balanceMap.put(account.getCurrency().toUpperCase(), new BigDecimal(account.getBalance()));
        }
        return balanceMap;
    }


    public String authRequest(String method, String api, Map<String, String> paramStrMap, PlatformConfigModel configModel) {
        logger.info(" request param method ={}, api={},paramStrMap={} ", method, api, paramStrMap.toString());
        StringBuilder paramStr = new StringBuilder();
        for (Map.Entry<String, String> entry : paramStrMap.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();
            if (!Strings.isNullOrEmpty(paramStr.toString())) {
                paramStr.append("&");
            }
            paramStr.append(key).append("=").append(URLEncoder.encode(value));
        }
        String signatureStr = YunbiSignatureUtils.getSignatureStr(method, api, paramStr.toString(), configModel.getApiSecret());
        paramStr.append("&").append("signature=").append(signatureStr);
        HttpURLConnection conn = null;
        StringBuilder response = new StringBuilder();
        try {
            URL url = new URL(BASE_URL + api);
            conn = (HttpURLConnection) url.openConnection();
            conn.setUseCaches(false);
            conn.setDoOutput(true);//Accept: application/json
            conn.setRequestProperty("Accept", "application/json");
            conn.setRequestMethod(method);
            //conn.setRequestProperty("Sign", toHex(mac.doFinal(postData.getBytes("UTF-8"))));
            conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            //conn.setRequestProperty("User-Agent", USER_AGENT);

            OutputStreamWriter out = new OutputStreamWriter(conn.getOutputStream());
            out.write(paramStr.toString());
            out.close();
            BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String line = null;
            while ((line = in.readLine()) != null) {
                response.append(line);
            }
            in.close();
            logger.info(" 请求结果json ={}", response);
            return response.toString();
        } catch (MalformedURLException e) {
            logger.error(e.getMessage(), e);
            throw new BTCTTSException(e.getMessage(), e);
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
            throw new BTCTTSException(e.getMessage(), e);
        }
    }


    public String authRequestGet(String method, String api, Map<String, String> paramStrMap, PlatformConfigModel configModel) {
        logger.info(" request param method ={}, api={},paramStrMap={} ", method, api, paramStrMap.toString());
        StringBuilder paramStr = new StringBuilder();
        for (Map.Entry<String, String> entry : paramStrMap.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();
            if (!Strings.isNullOrEmpty(paramStr.toString())) {
                paramStr.append("&");
            }
            paramStr.append(key).append("=").append(URLEncoder.encode(value));
        }
        String signatureStr = YunbiSignatureUtils.getSignatureStr(method, api, paramStr.toString(), configModel.getApiSecret());

        paramStr.append("&").append("signature=").append(signatureStr);
        HttpURLConnection conn = null;
        StringBuilder response = new StringBuilder();
        try {
            URL url = new URL(BASE_URL + api + "?" + paramStr.toString());
            conn = (HttpURLConnection) url.openConnection();
            conn.setUseCaches(false);
            conn.setDoOutput(true);//Accept: application/json
            conn.setRequestProperty("Accept", "application/json");
            conn.setRequestMethod(method);
            //conn.setRequestProperty("Sign", toHex(mac.doFinal(postData.getBytes("UTF-8"))));
            conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            //conn.setRequestProperty("User-Agent", USER_AGENT);

            BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String line = null;
            while ((line = in.readLine()) != null) {
                response.append(line);
            }
            in.close();
            logger.info(" 请求结果json ={}", response);
            return response.toString();
        } catch (MalformedURLException e) {
            logger.error(e.getMessage(), e);
            throw new BTCTTSException(e.getMessage(), e);
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
            throw new BTCTTSException(e.getMessage(), e);
        }
    }


}
