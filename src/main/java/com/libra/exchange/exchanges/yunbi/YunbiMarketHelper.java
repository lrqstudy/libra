package com.libra.exchange.exchanges.yunbi;

import com.libra.common.consts.CurrencyType;
import com.libra.common.exceptions.BTCTTSException;
import com.libra.common.util.JsonUtil;
import com.libra.exchange.exchanges.common.CommonMarketUtils;
import com.libra.exchange.exchanges.common.CommonOrderBook;
import com.libra.exchange.exchanges.common.CommonOrderVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;

/**
 * Created by rongqiang.li on 2017/5/1.
 */
@Service
public class YunbiMarketHelper {
    private static final String USER_AGENT = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36";
    private Logger logger = LoggerFactory.getLogger(YunbiExchangeHelper.class);

    public static final String GET_ORDER_BOOK_URL = "https://yunbi.com//api/v2/depth.json";

    @Resource
    private CommonMarketUtils commonMarketUtils;

    //https://yunbi.com//api/v2/depth.json?market=sccny&limit=2
    public CommonOrderBook getOrderBook(String market, int depth) {
        URLConnection conn;
        StringBuilder response = new StringBuilder();
        try {
            URL url = new URL(GET_ORDER_BOOK_URL + "?market=" + market + "&limit=" + depth);
            conn = url.openConnection();
            conn.setUseCaches(false);
            conn.setDoOutput(true);
            conn.setRequestProperty("Accept", "application/json");
            conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            conn.setRequestProperty("User-Agent", USER_AGENT);
            BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String line;
            while ((line = in.readLine()) != null) {
                response.append(line);
            }
            in.close();
            logger.info(" yunbi  market={} ,depth={} ,result ={}",market,depth,response.toString());
            YunbiOrderBookJsonVO yunbiOrderBookJsonVO = JsonUtil.jsonToObject(response.toString(), YunbiOrderBookJsonVO.class);
            Object[][] asks = yunbiOrderBookJsonVO.getAsks();
            List<CommonOrderVO> sellCommonderOrderList = commonMarketUtils.buildOrderVOListFromArray(asks,CurrencyType.CNY.getCode());
            Object[][] bids = yunbiOrderBookJsonVO.getBids();
            List<CommonOrderVO> buyCommonderOrderList = commonMarketUtils.buildOrderVOListFromArray(bids,CurrencyType.CNY.getCode());

            CommonOrderBook commonOrderBook=new CommonOrderBook();
            commonOrderBook.setBuyOrderVOList(buyCommonderOrderList);
            commonOrderBook.setSellOrderVOList(sellCommonderOrderList);
            return commonOrderBook;
        } catch (MalformedURLException e) {
            logger.error(e.getMessage(), e);
            throw new BTCTTSException("Internal error.", e);
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
            throw new BTCTTSException("Error connecting to yunbi.com ", e);
        }
    }


}
