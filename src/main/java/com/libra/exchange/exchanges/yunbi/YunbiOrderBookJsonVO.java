package com.libra.exchange.exchanges.yunbi;

/**
 * Created by rongqiang.li on 2017/5/1.
 */
public class YunbiOrderBookJsonVO {
    private long timestamp;
    private Object[][] asks;
    private Object[][] bids;

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public Object[][] getAsks() {
        return asks;
    }

    public void setAsks(Object[][] asks) {
        this.asks = asks;
    }

    public Object[][] getBids() {
        return bids;
    }

    public void setBids(Object[][] bids) {
        this.bids = bids;
    }
}
