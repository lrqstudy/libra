package com.libra.exchange.exchanges.yunbi;

import java.io.Serializable;

/**
 * Created by rongqiang.li on 2017/5/1.
 */
public class YunbiBalanceVO implements Serializable {
    private String currency;
    private String balance;
    private String locked;

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public String getLocked() {
        return locked;
    }

    public void setLocked(String locked) {
        this.locked = locked;
    }
}
