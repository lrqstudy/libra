package com.libra.exchange.exchanges.yunbi;

import com.google.common.base.Joiner;
import com.google.common.base.Strings;
import com.libra.common.exceptions.BTCTTSException;
import org.apache.commons.codec.binary.Hex;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.util.Arrays;

/**
 * Created by rongqiang.li on 2017/3/30.
 */

public class YunbiSignatureUtils {
     public static final String BASE_YUNBI_URL="https://yunbi.com/";


    public static String getSignatureStr(String method,String apiUrl,String paramsStr,String apiSecreKey){
        try {
            String payload = Joiner.on("|").join(method, apiUrl,sortParamStr(paramsStr));
            Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
            SecretKeySpec secretKeys = new SecretKeySpec(apiSecreKey.getBytes("UTF-8"), "HmacSHA256");
            sha256_HMAC.init(secretKeys);
            String signature = Hex.encodeHexString(sha256_HMAC.doFinal(payload.getBytes("UTF-8")));
            return signature;
        }catch (Exception e){
            throw new BTCTTSException(e.getMessage(),e);
        }
    }

    private static String sortParamStr(String paramsStr){
        String[] split = paramsStr.split("&");
        Arrays.sort(split);
        StringBuilder sb=new StringBuilder();
        for (String s : split) {
            if (!Strings.isNullOrEmpty(sb.toString())){
                sb.append("&");
            }
            sb.append(s);
        }
        return sb.toString();
    }

}
