package com.libra.exchange.exchanges.bittrex;

import java.io.Serializable;

/**
 * Created by rongqiang.li on 17/10/22.
 */
public class BittrexOrderBookJsonVO implements Serializable {
    private String quantity;
    private String rate;

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }
}
