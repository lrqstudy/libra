package com.libra.exchange.exchanges.bittrex;

import java.io.Serializable;
import java.util.List;

/**
 * Created by rongqiang.li on 17/10/22.
 */
public class BittrexOrderBookResultVO implements Serializable{
    private List<BittrexOrderBookJsonVO> buy;
    private List<BittrexOrderBookJsonVO> sell;

    public List<BittrexOrderBookJsonVO> getBuy() {
        return buy;
    }

    public void setBuy(List<BittrexOrderBookJsonVO> buy) {
        this.buy = buy;
    }

    public List<BittrexOrderBookJsonVO> getSell() {
        return sell;
    }

    public void setSell(List<BittrexOrderBookJsonVO> sell) {
        this.sell = sell;
    }
}
