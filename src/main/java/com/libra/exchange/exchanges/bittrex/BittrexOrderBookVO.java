package com.libra.exchange.exchanges.bittrex;

import java.io.Serializable;

/**
 * Created by rongqiang.li on 17/10/22.
 */
public class BittrexOrderBookVO implements Serializable{
    private String success;
    private String message;
    private BittrexOrderBookResultVO result;


    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public BittrexOrderBookResultVO getResult() {
        return result;
    }

    public void setResult(BittrexOrderBookResultVO result) {
        this.result = result;
    }
}
