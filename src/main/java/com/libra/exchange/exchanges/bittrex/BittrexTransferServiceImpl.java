package com.libra.exchange.exchanges.bittrex;

import com.libra.exchange.exchanges.common.CommonOpenOrderVO;
import com.libra.exchange.exchanges.common.CommonOrderBook;
import com.libra.exchange.exchanges.common.CommonTickerVO;
import com.libra.exchange.exchanges.common.CommonTransferService;
import com.libra.exchange.exchanges.poloniex.PoloniexExchangeServiceImpl;
import com.libra.exchange.strategy.model.PlatformConfigModel;
import org.knowm.xchange.Exchange;
import org.knowm.xchange.ExchangeFactory;
import org.knowm.xchange.bittrex.Bittrex;
import org.knowm.xchange.currency.CurrencyPair;
import org.knowm.xchange.dto.marketdata.Ticker;
import org.knowm.xchange.service.marketdata.MarketDataService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.Map;

/**
 * Created by rongqiang.li on 17/10/25.
 */
@Service
public class BittrexTransferServiceImpl implements CommonTransferService {

    @Resource
    private BittrexMarketApiUtils bittrexMarketApiUtils;

    @Resource
    private BittrexUtils bittrexUtils;
    private Logger logger = LoggerFactory.getLogger(PoloniexExchangeServiceImpl.class);


    @Override
    public CommonOrderBook getOrderBook(String targetCode, String cashCurrency, int depth) {
        return bittrexMarketApiUtils.getOrderBook(targetCode, cashCurrency, depth);
    }

    @Override
    public Map<String, BigDecimal> getBalanceMap(PlatformConfigModel configModel) {
        return null;
    }

    @Override
    public String getCurrencyPair(String targetCode, String cashCurrency) {
        return null;
    }



    @Override
    public String buy(String targetCode, String cashCurrency, BigDecimal price, BigDecimal amount, Integer precisionSize, PlatformConfigModel configModel,Integer orderSource) {
        //amount=amount.multiply(new BigDecimal(1.002));
        amount=amount.setScale(precisionSize, RoundingMode.CEILING);
        return bittrexUtils.buy(configModel, targetCode, cashCurrency, price, amount,precisionSize,orderSource);
    }

    @Override
    public String sell(String targetCode, String cashCurrency, BigDecimal price, BigDecimal amount, Integer precisionSize, PlatformConfigModel configModel,Integer orderSource) {
        amount=amount.setScale(precisionSize,BigDecimal.ROUND_FLOOR);
        return bittrexUtils.sell(configModel, targetCode, cashCurrency, price, amount,precisionSize,orderSource);
    }



    @Override
    public String withdraw(String targetCode, String address, String memo, BigDecimal amount, PlatformConfigModel configModel) {
        return bittrexUtils.withdraw(configModel, targetCode, amount, address, memo);
    }

    @Override
    public List<CommonOpenOrderVO> getOpenOrderList(PlatformConfigModel platformConfigModel, String targetCode, String cashCurrency) {
        List<CommonOpenOrderVO> commonOpenOrderVOList = bittrexUtils.getCommonOpenOrderVOList(platformConfigModel, targetCode, cashCurrency);
        logger.info(" bittrex get open orderList  targetCode ={},cashCurrency={} ,size ={}", targetCode, cashCurrency, commonOpenOrderVOList.size());
        return commonOpenOrderVOList;
    }

    @Override
    public CommonTickerVO getTickerBySymbol(String targetCode, String cashCurrency) {
        try {
            CurrencyPair currencyPair=  new CurrencyPair(targetCode,cashCurrency);
            Exchange bitfinex = ExchangeFactory.INSTANCE.createExchange(Bittrex.class.getName());
            MarketDataService marketDataService = bitfinex.getMarketDataService();
            Ticker ticker = marketDataService.getTicker(currencyPair);
            CommonTickerVO commonTickerVO = new CommonTickerVO();
            BeanUtils.copyProperties(ticker, commonTickerVO);
            return commonTickerVO;
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return null;
        }
    }
}
