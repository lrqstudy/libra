package com.libra.exchange.exchanges.bittrex;

import com.google.api.client.repackaged.com.google.common.base.Joiner;
import com.google.api.client.util.Lists;
import com.google.common.collect.ImmutableMap;
import com.libra.common.consts.SystemConsts;
import com.libra.common.util.BeanUtilExt;
import com.libra.exchange.exchanges.common.CommonOpenOrderVO;
import com.libra.exchange.exchanges.poloniex.PoloniexExchangeServiceImpl;
import com.libra.exchange.strategy.model.AltTransferErrorHandleModel;
import com.libra.exchange.strategy.model.PlatformConfigModel;
import com.libra.exchange.strategy.service.AltTransferErrorHandleService;
import org.knowm.xchange.Exchange;
import org.knowm.xchange.ExchangeFactory;
import org.knowm.xchange.ExchangeSpecification;
import org.knowm.xchange.bittrex.BittrexExchange;
import org.knowm.xchange.bittrex.dto.trade.BittrexOpenOrder;
import org.knowm.xchange.bittrex.service.BittrexAccountService;
import org.knowm.xchange.bittrex.service.BittrexTradeServiceRaw;
import org.knowm.xchange.currency.CurrencyPair;
import org.knowm.xchange.dto.Order;
import org.knowm.xchange.dto.trade.LimitOrder;
import org.knowm.xchange.service.account.AccountService;
import org.knowm.xchange.service.trade.TradeService;
import org.knowm.xchange.service.trade.params.orders.DefaultOpenOrdersParamCurrencyPair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * Created by rongqiang.li on 17/10/22.
 */
@Component
public class BittrexUtils {
    private Logger logger = LoggerFactory.getLogger(PoloniexExchangeServiceImpl.class);

    @Resource
    private AltTransferErrorHandleService altTransferErrorHandleService;

    private Map<Order.OrderType,String> orderTypeStringMap= ImmutableMap.of(Order.OrderType.BID,"buy", Order.OrderType.ASK,"sell");

    public String buy(PlatformConfigModel configModel, String targetCode, String cashCurrencyType, BigDecimal price, BigDecimal amount,Integer precisionSize,Integer orderSource) {
        return trade(configModel, targetCode, cashCurrencyType, Order.OrderType.BID, price, amount,precisionSize,orderSource);
    }

    public String sell(PlatformConfigModel configModel, String targetCode, String cashCurrencyType, BigDecimal price, BigDecimal amount,Integer precisionSize,Integer orderSource) {
        return trade(configModel, targetCode, cashCurrencyType, Order.OrderType.ASK, price, amount,precisionSize,orderSource);
    }

    public String trade(PlatformConfigModel configModel, String targetCode, String cashCurrencyType, Order.OrderType orderType, BigDecimal price, BigDecimal amount,Integer precisionSize,Integer orderSource) {
        try {
            logger.info("trade param targetCode={},cashCurrencyType={},amount={},price={}  ", targetCode, cashCurrencyType, amount, price);
            TradeService tradeService = buildTradeService(configModel);
            CurrencyPair pair = new CurrencyPair(targetCode, cashCurrencyType);
            LimitOrder limitOrder = new LimitOrder.Builder(orderType, pair).limitPrice(price).originalAmount(amount)
                    .build();
            String resultID = tradeService.placeLimitOrder(limitOrder);
            logger.info("targetCode={},currencyType={},amount={},price={} ,resultID={} ", targetCode, cashCurrencyType, amount, price, resultID);
            return resultID;
        } catch (Exception e) {
            AltTransferErrorHandleModel model = new AltTransferErrorHandleModel();
            model.setCashCurrency(cashCurrencyType);
            model.setCurrencyPair(buildSymbol(targetCode,cashCurrencyType));
            model.setPlatform(configModel.getPlatform());
            model.setResultId("");
            model.setSide(orderTypeStringMap.get(orderType));//"buy or sell "
            model.setTargetCode(targetCode);
            model.setTradeAmount(amount);
            model.setTradePrice(price);
            model.setUserCode(configModel.getUserCode());
            model.setPrecisionSize(precisionSize);
            String errorMsg=e.getMessage();
            model.setErrorMessage(errorMsg.substring(0,errorMsg.length()> SystemConsts.EXCEPTION_ERROR_MESSAGE_SIZE ? SystemConsts.EXCEPTION_ERROR_MESSAGE_SIZE:errorMsg.length()));
            model.setOrderSource(orderSource);
            BeanUtilExt.fillNullProperty(model);
            altTransferErrorHandleService.saveAltTransferErrorHandleModel(model);
            logger.error(e.getMessage(), e);
            return "";
        }
    }

    public String withdraw(PlatformConfigModel configModel, String targetCode, BigDecimal amount, String adress, String memo) {
        try {
            BittrexAccountService accountService = (BittrexAccountService) buildAccountService(configModel);
            logger.info(" bittrex withdraw targetCode={} ,amount={}, address={}, memo={}", targetCode, amount, adress, memo);
            String withdrawId = accountService.withdraw(targetCode, amount, adress, memo);
            logger.info(" bittrex withdraw targetCode={} ,amount={}, address={}, memo={},withdrawId={}", targetCode, amount, adress, memo, withdrawId);
            return withdrawId;
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return null;
        }
    }

    private String buildSymbol(String targetCode, String cashCurrency) {
        return Joiner.on("-").join(cashCurrency, targetCode);
    }

    public List<CommonOpenOrderVO> getCommonOpenOrderVOList(PlatformConfigModel configModel, String targetCode, String cashCurrencyType) {
        try {
            BittrexTradeServiceRaw tradeService = (BittrexTradeServiceRaw) buildTradeService(configModel);
            DefaultOpenOrdersParamCurrencyPair params = new DefaultOpenOrdersParamCurrencyPair();
            params.setCurrencyPair(new CurrencyPair(targetCode, cashCurrencyType));
            List<BittrexOpenOrder> bittrexOpenOrders = tradeService.getBittrexOpenOrders(params);
            String symbol = buildSymbol(targetCode, cashCurrencyType);
            List<CommonOpenOrderVO> commonOpenOrderVOList = Lists.newArrayList();
            for (BittrexOpenOrder bittrexOpenOrder : bittrexOpenOrders) {
                CommonOpenOrderVO commonOpenOrderVO = new CommonOpenOrderVO();
                commonOpenOrderVO.setOrderId(bittrexOpenOrder.getOrderUuid());
                commonOpenOrderVO.setOriginAmount(bittrexOpenOrder.getQuantity());
                commonOpenOrderVO.setPrice(bittrexOpenOrder.getPrice());
                commonOpenOrderVO.setRemainAmount(bittrexOpenOrder.getQuantityRemaining());
                commonOpenOrderVO.setSide(bittrexOpenOrder.getOrderType());
                commonOpenOrderVO.setType(bittrexOpenOrder.getExchange());
                commonOpenOrderVO.setStatus(bittrexOpenOrder.getOpened());
                commonOpenOrderVO.setSymbol(symbol);
                commonOpenOrderVOList.add(commonOpenOrderVO);
            }
            return commonOpenOrderVOList;
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return null;
        }
    }


    private TradeService buildTradeService(PlatformConfigModel configModel) {
        ExchangeSpecification exSpec = new ExchangeSpecification(BittrexExchange.class);
        exSpec.setApiKey(configModel.getApiKey());
        exSpec.setSecretKey(configModel.getApiSecret());
        Exchange exchange = ExchangeFactory.INSTANCE.createExchange(exSpec);
        return exchange.getTradeService();
    }

    private AccountService buildAccountService(PlatformConfigModel configModel) {
        ExchangeSpecification exSpec = new ExchangeSpecification(BittrexExchange.class);
        exSpec.setApiKey(configModel.getApiKey());
        exSpec.setSecretKey(configModel.getApiSecret());
        Exchange exchange = ExchangeFactory.INSTANCE.createExchange(exSpec);
        return exchange.getAccountService();
    }

}
