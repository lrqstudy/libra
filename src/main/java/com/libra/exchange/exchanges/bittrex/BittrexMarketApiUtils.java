package com.libra.exchange.exchanges.bittrex;

import com.libra.common.exceptions.BTCTTSException;
import com.libra.exchange.exchanges.common.CommonMarketUtils;
import com.libra.exchange.exchanges.common.CommonOrderBook;
import com.libra.exchange.exchanges.poloniex.PoloniexExchangeServiceImpl;
import org.knowm.xchange.Exchange;
import org.knowm.xchange.ExchangeFactory;
import org.knowm.xchange.bittrex.BittrexExchange;
import org.knowm.xchange.currency.CurrencyPair;
import org.knowm.xchange.dto.marketdata.OrderBook;
import org.knowm.xchange.service.marketdata.MarketDataService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * Created by rongqiang.li on 17/10/22.
 */
@Component
public class BittrexMarketApiUtils {

    @Resource
    private CommonMarketUtils commonMarketUtils;

    private Logger logger = LoggerFactory.getLogger(PoloniexExchangeServiceImpl.class);


    public CommonOrderBook getOrderBook(String targetCode, String cashCurrency, Integer limit) {
        try {
            Exchange exchange = ExchangeFactory.INSTANCE.createExchange(BittrexExchange.class.getName());
            MarketDataService marketDataService = exchange.getMarketDataService();
            OrderBook orderBook = marketDataService.getOrderBook(new CurrencyPair(targetCode, cashCurrency));
            CommonOrderBook commonOrderBook = commonMarketUtils.buildCommonOrderBook(orderBook);
            logger.info(" targetCode ={},cashCurrency={}, orderBook={}", targetCode, cashCurrency, orderBook);
            return commonOrderBook;
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw new BTCTTSException(e.getMessage(), e);
        }
    }

}
