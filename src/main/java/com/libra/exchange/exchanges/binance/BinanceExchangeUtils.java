package com.libra.exchange.exchanges.binance;

import com.google.api.client.util.Lists;
import com.google.common.base.Joiner;
import com.google.common.collect.Maps;
import com.libra.common.consts.PlatformEnum;
import com.libra.common.consts.SystemConsts;
import com.libra.common.util.BeanUtilExt;
import com.libra.exchange.exchanges.common.CommonOpenOrderVO;
import com.libra.exchange.strategy.model.AltTransferErrorHandleModel;
import com.libra.exchange.strategy.model.PlatformConfigModel;
import com.libra.exchange.strategy.service.AltTransferErrorHandleService;
import com.libra.exchange.strategy.service.PlatformConfigService;
import org.knowm.xchange.Exchange;
import org.knowm.xchange.ExchangeFactory;
import org.knowm.xchange.ExchangeSpecification;
import org.knowm.xchange.binance.BinanceExchange;
import org.knowm.xchange.binance.dto.trade.*;
import org.knowm.xchange.binance.service.BinanceTradeServiceRaw;
import org.knowm.xchange.currency.Currency;
import org.knowm.xchange.currency.CurrencyPair;
import org.knowm.xchange.dto.account.AccountInfo;
import org.knowm.xchange.dto.account.Balance;
import org.knowm.xchange.dto.account.Wallet;
import org.knowm.xchange.service.account.AccountService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by rongqiang.li on 17/10/23.
 */
@Repository
public class BinanceExchangeUtils {
    private Logger logger = LoggerFactory.getLogger(BinanceExchangeUtils.class);

    private Map<String, BinanceTradeServiceRaw> userCodeBitfinexServiceMap = new ConcurrentHashMap<String, BinanceTradeServiceRaw>();

    private Map<String, Exchange> userCodeBitfinexExchangeServiceMap = new ConcurrentHashMap<String, Exchange>();
    @Resource
    private PlatformConfigService platformConfigService;
    @Resource
    private AltTransferErrorHandleService altTransferErrorHandleService;

    @PostConstruct
    public void init() {
        List<PlatformConfigModel> platformConfigModelByPlatform = platformConfigService.getPlatformConfigModelByPlatform(PlatformEnum.BINANCE.getCode());
        for (PlatformConfigModel configModel : platformConfigModelByPlatform) {
            try {
                Exchange exchangeInstance = getExchangeInstance(configModel);
                userCodeBitfinexExchangeServiceMap.put(configModel.getUserCode(), exchangeInstance);
                BinanceTradeServiceRaw tradeService = (BinanceTradeServiceRaw) exchangeInstance.getTradeService();
                userCodeBitfinexServiceMap.put(configModel.getUserCode(), tradeService);
                Thread.sleep(2000);
            } catch (Exception e) {
                logger.error(e.getMessage(), e);
            }
        }

    }


    public String buy(String targetCode, String cashCurrency, PlatformConfigModel platformConfigModel, String symbol, BigDecimal price, BigDecimal amount, Integer precisionSize, Integer orderSource) {
        //amount = amount.multiply(new BigDecimal(1.002));//把手续费加上
        amount = amount.setScale(precisionSize, BigDecimal.ROUND_CEILING);//这个取得的值比原来的值要大
        return trade(targetCode, cashCurrency, platformConfigModel, symbol, price, amount, OrderSide.BUY, precisionSize, orderSource);
    }

    public String sell(String targetCode, String cashCurrency, PlatformConfigModel platformConfigModel, String symbol, BigDecimal price, BigDecimal amount, Integer precisionSize, Integer orderSource) {
        amount = amount.setScale(precisionSize, BigDecimal.ROUND_FLOOR);//这个取得的值比原来的值要小
        return trade(targetCode, cashCurrency, platformConfigModel, symbol, price, amount, OrderSide.SELL, precisionSize, orderSource);
    }


    public String trade(String targetCode, String cashCurrency, PlatformConfigModel platformConfigModel, String symbol, BigDecimal price, BigDecimal amount, OrderSide orderSide, Integer precisionSize, Integer orderSource) {
        try {
            logger.info("binance symbol={},order_side={}, price={}, amount={} ", symbol, orderSide.name(), price, amount);
            BinanceTradeServiceRaw tradeService = getBinanceTradeServiceRaw(platformConfigModel);

            CurrencyPair currencyPair=new CurrencyPair(targetCode,cashCurrency);
            BinanceNewOrder binanceNewOrder = tradeService.newOrder(currencyPair, orderSide, OrderType.LIMIT, TimeInForce.GTC, amount, price, null, null, null, null, System.currentTimeMillis());
            logger.info(" binance symbol={},order_side={}, price={}, amount={},result ={} ", symbol, orderSide.name(), price, amount, binanceNewOrder.orderId);
            return String.valueOf(binanceNewOrder.orderId);
        } catch (Exception e) {
            logger.error("trade error " + e.getMessage(), e);
            AltTransferErrorHandleModel model = new AltTransferErrorHandleModel();
            model.setCashCurrency(cashCurrency);
            model.setCurrencyPair(symbol);
            model.setPlatform(platformConfigModel.getPlatform());
            model.setResultId("");
            model.setSide(orderSide.name().toLowerCase());
            model.setTargetCode(targetCode);
            model.setTradeAmount(amount);
            model.setTradePrice(price);
            model.setUserCode(platformConfigModel.getUserCode());
            model.setPrecisionSize(precisionSize);
            String errorMsg = e.getMessage();
            model.setErrorMessage(errorMsg.substring(0, errorMsg.length() > SystemConsts.EXCEPTION_ERROR_MESSAGE_SIZE ? SystemConsts.EXCEPTION_ERROR_MESSAGE_SIZE : errorMsg.length()));
            model.setOrderSource(orderSource);
            BeanUtilExt.fillNullProperty(model);
            altTransferErrorHandleService.saveAltTransferErrorHandleModel(model);
            return "error";
        }
    }

    private Exchange getExchange(PlatformConfigModel platformConfigModel) {
        String userCode = platformConfigModel.getUserCode();
        Exchange exchange = userCodeBitfinexExchangeServiceMap.get(userCode);
        if (exchange == null) {
            exchange = getExchangeInstance(platformConfigModel);
            userCodeBitfinexExchangeServiceMap.put(userCode, exchange);
        }
        return exchange;
    }

    private BinanceTradeServiceRaw getBinanceTradeServiceRaw(PlatformConfigModel platformConfigModel) {
        String userCode = platformConfigModel.getUserCode();
        BinanceTradeServiceRaw tradeService = userCodeBitfinexServiceMap.get(userCode);
        if (tradeService == null) {
            Exchange exchange = getExchange(platformConfigModel);
            tradeService = (BinanceTradeServiceRaw) exchange.getTradeService();
            userCodeBitfinexServiceMap.put(userCode, tradeService);
        }
        return tradeService;
    }

    public Map<String, BigDecimal> getBalanceMap(PlatformConfigModel platformConfigModel) {
        try {
            Exchange bnx = getExchange(platformConfigModel);
            AccountService accountService = bnx.getAccountService();
            AccountInfo accountInfo = accountService.getAccountInfo();
            Map<String, BigDecimal> balanceMap = Maps.newHashMap();
            Map<String, Wallet> wallets = accountInfo.getWallets();
            for (Map.Entry<String, Wallet> entry : wallets.entrySet()) {
                Map<Currency, Balance> balances = entry.getValue().getBalances();
                for (Map.Entry<Currency, Balance> balanceEntry : balances.entrySet()) {
                    balanceMap.put(balanceEntry.getKey().getCurrencyCode(), balanceEntry.getValue().getAvailable());
                }
            }
            logger.info(" binance get balance ,result size={}", balanceMap.size());
            return balanceMap;
        } catch (Exception e) {
            logger.error("binance getBalance error" + e.getMessage(), e);
            return Maps.newHashMap();
        }
    }

    public String withdraw(PlatformConfigModel platformConfigModel, String currencyType, BigDecimal amount, String address) {
        try {
            Exchange bnx = getExchangeInstance(platformConfigModel);
            AccountService accountService = bnx.getAccountService();
            String withdrawId = accountService.withdrawFunds(Currency.getInstance(currencyType), amount, address);
            logger.info(" binance withdraw currencyType={},amount={}, address={} ,withdrawId={} ", currencyType, amount, address, withdrawId);
            return withdrawId;
        } catch (Exception e) {
            logger.error(" binance withdraw error" + e.getMessage(), e);
            return null;
        }
    }

    private Exchange getExchangeInstance(PlatformConfigModel platformConfigModel) {
        Exchange bnx = ExchangeFactory.INSTANCE.createExchange(BinanceExchange.class.getName());
        ExchangeSpecification binanceSpec = bnx.getDefaultExchangeSpecification();
        binanceSpec.setApiKey(platformConfigModel.getApiKey());
        binanceSpec.setSecretKey(platformConfigModel.getApiSecret());
        bnx.applySpecification(binanceSpec);
        return bnx;
    }

    public List<CommonOpenOrderVO> getOpenOrderVOList(PlatformConfigModel platformConfigModel, String targetCode, String cashCurrency) {
        try {
            BinanceTradeServiceRaw tradeService = getBinanceTradeServiceRaw(platformConfigModel);
            List<BinanceOrder> binanceOrders = tradeService.openOrders(buildCurrencyPair(targetCode, cashCurrency), null, System.currentTimeMillis());
            List<CommonOpenOrderVO> commonOpenOrderVOList = Lists.newArrayList();
            for (BinanceOrder binanceOrder : binanceOrders) {
                CommonOpenOrderVO commonOpenOrderVO = new CommonOpenOrderVO();

                BigDecimal remainAmount = binanceOrder.origQty.subtract(binanceOrder.executedQty);
                commonOpenOrderVO.setOrderId(String.valueOf(binanceOrder.orderId));
                commonOpenOrderVO.setOriginAmount(binanceOrder.origQty);
                commonOpenOrderVO.setPrice(binanceOrder.price);
                commonOpenOrderVO.setType(binanceOrder.type.name());
                commonOpenOrderVO.setSide(binanceOrder.side.name());
                commonOpenOrderVO.setSymbol(binanceOrder.symbol);
                commonOpenOrderVO.setRemainAmount(remainAmount);
                commonOpenOrderVO.setStatus(binanceOrder.status.name());
                commonOpenOrderVOList.add(commonOpenOrderVO);
            }
            logger.info(" binance  get targetCode ={},cashCurrency={}   open order List size ={}", targetCode, cashCurrency, commonOpenOrderVOList.size());
            return commonOpenOrderVOList;
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return null;
        }
    }

    private String buildSymbol(String targetCode, String cashCurrency) {
        return Joiner.on("").join(targetCode, cashCurrency);
    }

    private CurrencyPair buildCurrencyPair(String targetCode, String cashCurrency) {
        return new CurrencyPair(targetCode,cashCurrency);
    }



}
