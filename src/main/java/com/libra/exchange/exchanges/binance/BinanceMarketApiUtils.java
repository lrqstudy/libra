package com.libra.exchange.exchanges.binance;

import com.google.api.client.util.Lists;
import com.google.api.client.util.Maps;
import com.libra.exchange.exchanges.common.CommonOrderBook;
import com.libra.exchange.exchanges.common.CommonOrderVO;
import com.libra.exchange.exchanges.poloniex.PoloniexExchangeServiceImpl;
import org.knowm.xchange.Exchange;
import org.knowm.xchange.ExchangeFactory;
import org.knowm.xchange.binance.BinanceExchange;
import org.knowm.xchange.currency.CurrencyPair;
import org.knowm.xchange.dto.marketdata.OrderBook;
import org.knowm.xchange.dto.trade.LimitOrder;
import org.knowm.xchange.service.marketdata.MarketDataService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by rongqiang.li on 17/10/21.
 */

@Component
public class BinanceMarketApiUtils {
    private Map<String, CurrencyPair> symbolCurrencyPair;

    @PostConstruct
    public void init() {

        symbolCurrencyPair = Maps.newHashMap();
        symbolCurrencyPair.put("EOSBTC", CurrencyPair.EOS_BTC);
        symbolCurrencyPair.put("EOSETH", CurrencyPair.EOS_ETH);


        symbolCurrencyPair.put("QTUMETH", new CurrencyPair("QTUM", "ETH"));
        symbolCurrencyPair.put("QTUMBTC", new CurrencyPair("QTUM", "BTC"));


        symbolCurrencyPair.put("ZECBTC", CurrencyPair.ZEC_BTC);
        symbolCurrencyPair.put("ZECETH", new CurrencyPair("ZEC", "ETH"));

        symbolCurrencyPair.put("ETHBTC", CurrencyPair.ETH_BTC);

        symbolCurrencyPair.put("SNTBTC", new CurrencyPair("SNT", "BTC"));
        symbolCurrencyPair.put("SNTETH", new CurrencyPair("SNT", "ETH"));


        symbolCurrencyPair.put("IOTABTC", new CurrencyPair("IOTA", "BTC"));
        symbolCurrencyPair.put("IOTAETH", new CurrencyPair("IOTA", "ETH"));

        symbolCurrencyPair.put("LTCBTC", new CurrencyPair("LTC", "BTC"));
        symbolCurrencyPair.put("LTCETH", new CurrencyPair("LTC", "ETH"));

        symbolCurrencyPair.put("DASHBTC", new CurrencyPair("DASH", "BTC"));
        symbolCurrencyPair.put("DASHETH", new CurrencyPair("DASH", "ETH"));


    }


    private Logger logger = LoggerFactory.getLogger(PoloniexExchangeServiceImpl.class);

    public CommonOrderBook getOrderBook(String targetCode, String cashCurrency, String symbol, int limit) {
        try {
            Exchange exchange = ExchangeFactory.INSTANCE.createExchange(BinanceExchange.class.getName());
            MarketDataService marketDataService = exchange.getMarketDataService();
            OrderBook orderBook = marketDataService.getOrderBook(getCurrencyPair(targetCode,cashCurrency,symbol));
            CommonOrderBook commonOrderBook = buildCommonOrderBook(orderBook, limit);
            logger.info(" exchange limit={}, order book ={} ", limit, commonOrderBook.toString());
            return commonOrderBook;
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return null;
        }
    }

    private CurrencyPair getCurrencyPair(String targetCode, String cashCurrency, String symbol) {
        CurrencyPair currencyPair = symbolCurrencyPair.get(symbol);
        if (currencyPair == null) {
            return new CurrencyPair(targetCode, cashCurrency);
        }
        return currencyPair;
    }


    private CommonOrderBook buildCommonOrderBook(OrderBook orderBook, int depth) {
        CommonOrderBook commonOrderBook = new CommonOrderBook();
        List<LimitOrder> asks = orderBook.getAsks();
        List<LimitOrder> bids = orderBook.getBids();
        depth = asks.size() >= depth ? depth : asks.size();
        ArrayList<CommonOrderVO> buyOrderList = Lists.newArrayList();

        for (int i = 0; i < depth; i++) {
            CommonOrderVO buyOrderVO = new CommonOrderVO();
            buyOrderVO.setPrice(bids.get(i).getLimitPrice());
            buyOrderVO.setVolume(bids.get(i).getOriginalAmount());
            buyOrderList.add(buyOrderVO);
        }


        ArrayList<CommonOrderVO> sellOrderList = Lists.newArrayList();
        for (int i = 0; i < depth; i++) {
            CommonOrderVO sellOrderVO = new CommonOrderVO();
            sellOrderVO.setPrice(asks.get(i).getLimitPrice());
            sellOrderVO.setVolume(asks.get(i).getOriginalAmount());
            sellOrderList.add(sellOrderVO);
        }

        commonOrderBook.setBuyOrderVOList(buyOrderList);
        commonOrderBook.setSellOrderVOList(sellOrderList);
        return commonOrderBook;
    }
}
