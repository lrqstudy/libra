package com.libra.exchange.exchanges.binance;

import com.google.common.base.Joiner;
import com.libra.common.util.BeanUtilExt;
import com.libra.exchange.exchanges.common.*;
import com.libra.exchange.strategy.model.PlatformConfigModel;
import org.knowm.xchange.Exchange;
import org.knowm.xchange.ExchangeFactory;
import org.knowm.xchange.binance.BinanceExchange;
import org.knowm.xchange.binance.dto.marketdata.BinanceTicker24h;
import org.knowm.xchange.binance.service.BinanceMarketDataServiceRaw;
import org.knowm.xchange.currency.CurrencyPair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * Created by rongqiang.li on 17/10/23.
 */
@Service
public class BinanceTransferServiceImpl implements CommonTransferService {
    @Resource
    private BinanceExchangeUtils binanceExchangeUtils;

    @Resource
    private BinanceMarketApiUtils binanceMarketApiUtils;

    private Logger logger = LoggerFactory.getLogger(BinanceExchangeUtils.class);

    @Override
    public CommonOrderBook getOrderBook(String targetCode, String cashCurrency, int depth) {
        CommonOrderBook orderBook = binanceMarketApiUtils.getOrderBook(targetCode, cashCurrency, getCurrencyPair(targetCode, cashCurrency), depth);
        return orderBook;
    }

    @Override
    public Map<String, BigDecimal> getBalanceMap(PlatformConfigModel configModel) {
        return binanceExchangeUtils.getBalanceMap(configModel);
    }

    @Override
    public String getCurrencyPair(String targetCode, String cashCurrency) {
        return Joiner.on("").join(targetCode, cashCurrency);
    }


    @Override
    public String buy(String targetCode, String cashCurrency, BigDecimal price, BigDecimal amount, Integer precisionSize, PlatformConfigModel configModel, Integer orderSource) {
        return binanceExchangeUtils.buy(targetCode, cashCurrency, configModel, getCurrencyPair(targetCode, cashCurrency), price, amount, precisionSize, orderSource);
    }

    @Override
    public String sell(String targetCode, String cashCurrency, BigDecimal price, BigDecimal amount, Integer precisionSize, PlatformConfigModel configModel, Integer orderSource) {
        return binanceExchangeUtils.sell(targetCode, cashCurrency, configModel, getCurrencyPair(targetCode, cashCurrency), price, amount, precisionSize, orderSource);
    }

    @Override
    public String withdraw(String targetCode, String address, String memo, BigDecimal amount, PlatformConfigModel configModel) {
        //binanceExchangeUtils.withdraw(configModel,targetCode,amount,address,memo);
        return null;
    }


    @Override
    public List<CommonOpenOrderVO> getOpenOrderList(PlatformConfigModel platformConfigModel, String targetCode, String cashCurrency) {
        List<CommonOpenOrderVO> openOrderVOList = binanceExchangeUtils.getOpenOrderVOList(platformConfigModel, targetCode, cashCurrency);
        return openOrderVOList;
    }

    @Override
    public CommonTickerVO getTickerBySymbol(String targetCode, String cashCurrency) {
        try {
            Exchange binance = ExchangeFactory.INSTANCE.createExchange(BinanceExchange.class.getName());
            String currencyPair = getCurrencyPair(targetCode, cashCurrency);
            BinanceMarketDataServiceRaw marketDataService = (BinanceMarketDataServiceRaw) binance.getMarketDataService();
            BinanceTicker24h ticker24h = marketDataService.ticker24h(new CurrencyPair(targetCode, cashCurrency));
            CommonTickerVO commonTickerVO = new CommonTickerVO();
            commonTickerVO.setAsk(ticker24h.getAskPrice());
            commonTickerVO.setBid(ticker24h.getBidPrice());
            commonTickerVO.setLast(ticker24h.getLastPrice());
            commonTickerVO.setHigh(ticker24h.getHighPrice());
            commonTickerVO.setLow(ticker24h.getLowPrice());
            commonTickerVO.setVolume(ticker24h.getVolume());
            commonTickerVO.setSymbol(currencyPair);
            BeanUtilExt.fillNullProperty(commonTickerVO);
            return commonTickerVO;
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return null;
        }
    }


}
