package com.libra.exchange.exchanges.binance;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;

/**
 * Created by rongqiang.li on 17/10/21.
 */
public class BinanceOrderBook implements Serializable {
    private long lastUpdateId;
    private Object[][] asks;
    private Object[][] bids;

    public long getLastUpdateId() {
        return lastUpdateId;
    }

    public void setLastUpdateId(long lastUpdateId) {
        this.lastUpdateId = lastUpdateId;
    }

    public Object[][] getAsks() {
        return asks;
    }

    public void setAsks(Object[][] asks) {
        this.asks = asks;
    }

    public Object[][] getBids() {
        return bids;
    }

    public void setBids(Object[][] bids) {
        this.bids = bids;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
