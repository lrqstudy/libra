/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.libra.exchange.exchanges.coincheck;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * @author Administrator
 */

public class CoinCheck {

    private final String BASE_API = "https://coincheck.com/";
    private HttpClient defaultHttpClient;
    private String accessKey;//// TODO: 17/5/13  
    private String secretKey;

    public CoinCheck() {
    }

    public CoinCheck(String accessKey, String secretKey) {
        this.accessKey = accessKey;
        this.secretKey = secretKey;
    }

    public String request(String method, String path, String params) throws IOException {
        if (!params.equals("") && method.equals("GET")) {
            path = path + "?" + params;
            params = "";
        }
        String url = BASE_API + path;
        long nonce = System.currentTimeMillis();
        String message = nonce + url + params;
        String signature = Util.createHmacSha256(message, this.secretKey);
        HttpResponse response;

        if (method.equals("GET")) {
            HttpGet getReq = new HttpGet(url);
            getReq.addHeader("Content-Type", "application/json");
            getReq.addHeader("ACCESS-KEY", this.accessKey);
            getReq.addHeader("ACCESS-NONCE", String.valueOf(nonce));
            getReq.addHeader("ACCESS-SIGNATURE", signature);
            response = this.defaultHttpClient.execute(getReq);
        } else if (method.equals("")) {
            HttpDelete deleteReq = new HttpDelete(url);
            deleteReq.addHeader("Content-Type", "application/json");
            deleteReq.addHeader("ACCESS-KEY", this.accessKey);
            deleteReq.addHeader("ACCESS-NONCE", String.valueOf(nonce));
            deleteReq.addHeader("ACCESS-SIGNATURE", signature);
            response = this.defaultHttpClient.execute(deleteReq);
        } else {
            HttpPost postReq = new HttpPost(url);
            postReq.addHeader("Content-Type", "application/json");
            postReq.addHeader("ACCESS-KEY", this.accessKey);
            postReq.addHeader("ACCESS-NONCE", String.valueOf(nonce));
            postReq.addHeader("ACCESS-SIGNATURE", signature);
            StringEntity entity = new StringEntity(params);
            postReq.setEntity(entity);
            response = this.defaultHttpClient.execute(postReq);
        }
        BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
        StringBuffer result = new StringBuffer();
        String line = "";
        while ((line = rd.readLine()) != null) {
            result.append(line);
        }
        System.out.println(result.toString());
        return result.toString();
    }

    public HttpClient getDefaultHttpClient() {
        return defaultHttpClient;
    }

    public void setDefaultHttpClient(HttpClient defaultHttpClient) {
        this.defaultHttpClient = defaultHttpClient;
    }


    public String getAccessKey() {
        return accessKey;
    }

    public void setAccessKey(String accessKey) {
        this.accessKey = accessKey;
    }

    public String getSecretKey() {
        return secretKey;
    }

    public void setSecretKey(String secretKey) {
        this.secretKey = secretKey;
    }
}
