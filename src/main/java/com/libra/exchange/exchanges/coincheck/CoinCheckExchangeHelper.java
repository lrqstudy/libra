package com.libra.exchange.exchanges.coincheck;


import com.google.api.client.repackaged.com.google.common.base.Strings;
import com.google.common.base.Joiner;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * Created by rongqiang.li on 17/4/29.
 */
@Service
public class CoinCheckExchangeHelper {

    @Resource
    private CoinCheck client;

    private Logger logger = LoggerFactory.getLogger(CoinCheckExchangeHelper.class);


    private String buildCurrencyPair(String targetCode, String cashCurrency){
        return Joiner.on("_").join(cashCurrency.toLowerCase(),targetCode.toLowerCase());
    }

    public String buy(String targetCode, String cashCurrency, BigDecimal buyPrice, BigDecimal buyAmount) {
        try {
            JSONObject orderObj = new JSONObject();
            orderObj.put("rate", String.valueOf(buyPrice.doubleValue()));
            orderObj.put("amount",String.valueOf(buyAmount.doubleValue()));
            orderObj.put("order_type", "buy");
            orderObj.put("pair",buildCurrencyPair(targetCode,cashCurrency));
            String response = this.client.request("POST", "api/exchange/orders","");
            JSONObject result = new JSONObject(response);
            logger.info(" 交易参数={} , result={}",orderObj,result);
            return result.toString();
        } catch (Exception e) {
            logger.error(e.getMessage(),e);
            return null;
        }
    }

    public String sell(String targetCode, String cashCurrency, BigDecimal sellPrice, BigDecimal sellAmount) {
        try {
            JSONObject orderObj = new JSONObject();
            orderObj.put("rate",String.valueOf(sellPrice.doubleValue()));
            orderObj.put("amount",String.valueOf(sellAmount.doubleValue()));
            orderObj.put("order_type", "buy");
            orderObj.put("pair",buildCurrencyPair(targetCode,cashCurrency));
            String response = this.client.request("POST", "api/exchange/orders","");
            JSONObject result = new JSONObject(response);
            logger.info(" 交易参数={} , result={}",orderObj,result);
            return result.toString();
        } catch (Exception e) {
            logger.error(e.getMessage(),e);
            return null;
        }
    }

    public String cancelOrder(String orderNumber) {
        try {
            String response = this.client.request("DELETE", "api/exchange/orders/" + orderNumber, "");
            JSONObject result = new JSONObject(response);
            logger.info(" 交易参数orderNumber ={} , result={}",orderNumber,result);
            return result.toString();
        } catch (Exception e) {
            logger.error(e.getMessage(),e);
            return null;
        }
    }
    private List<String> coinCheckTargetCodeList= Lists.newArrayList("jpy","btc","usd","cny","etc","lsk","fct","xmr","rep","zec","xem","ltc","dash");

    public Map<String, BigDecimal> getAccountBalance() {
        try {
            //,"jpy":"0","btc":"0","usd":"0","cny":"0","eth":"0","etc":"0","dao":"0","lsk":"0","fct":"0","xmr":"0","rep":"0","xrp":"0","zec":"0","xem":"0","ltc":"0","dash":"0","jpy_reserved":"0.0"
            String response = this.client.request("GET", "api/accounts/balance", "");
            JSONObject balance = new JSONObject(response);
            Map<String, BigDecimal> balanceMap=Maps.newHashMapWithExpectedSize(coinCheckTargetCodeList.size());
            for (String s : coinCheckTargetCodeList) {
                String balanceAmountStr = balance.getString(s);
                if (!Strings.isNullOrEmpty(balanceAmountStr)){
                    balanceMap.put(s,new BigDecimal(balanceAmountStr));
                }
            }
            logger.info(" 交易参数orderNumber ={} , result={}",balance);
            return balanceMap;
        } catch (Exception e) {
            logger.error(e.getMessage(),e);
            return null;
        }
    }

    public String getOpenOrders() {
        try {
            String response = this.client.request("GET", "api/exchange/orders/opens", "");
            logger.info(" get open order result={}",response);
            return response;
        } catch (Exception e) {
            logger.error(e.getMessage(),e);
            return null;
        }
    }

    public String getTradeHistory(String targetCode, String cashCurrency) {
        try {
            String response = this.client.request("GET", "api/exchange/orders/transactions", "");
            return response;
        } catch (Exception e) {
            logger.error(e.getMessage(),e);
            return null;
        }
    }

    public String getOrderBook(){
        try {
            String response = this.client.request("GET", "api/order_books", "");
            return response;
        } catch (Exception e) {
            logger.error(e.getMessage(),e);
            return null;
        }

    }
}
