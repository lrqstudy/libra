/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.libra.exchange.exchanges.coincheck;

import org.json.JSONObject;

import java.util.Map;

/**
 *
 * @author Administrator
 */
public class Leverage {

    private CoinCheck client;

    public Leverage(CoinCheck client) {
        this.client = client;
    }

    /**
     * Get a leverage positions list.
     *
     * @param params
     * @throws Exception
     *
     * @return JSONObject
     */
    public JSONObject positions(Map<String, String> params) throws Exception {
        String stringParam = Util.httpBuildQuery(params);
        String response = this.client.request("GET", "api/exchange/leverage/positions", stringParam);
        JSONObject jsonObj = new JSONObject(response);
        return jsonObj;
    }

}
