package com.libra.exchange.exchanges.bibox;

import com.google.common.base.Joiner;
import com.libra.exchange.exchanges.common.CommonMarketUtils;
import com.libra.exchange.exchanges.common.CommonOrderBook;
import com.libra.exchange.exchanges.common.CommonTickerVO;
import org.knowm.xchange.Exchange;
import org.knowm.xchange.ExchangeFactory;
import org.knowm.xchange.bibox.BiboxExchange;
import org.knowm.xchange.currency.CurrencyPair;
import org.knowm.xchange.dto.marketdata.OrderBook;
import org.knowm.xchange.dto.marketdata.Ticker;
import org.knowm.xchange.service.marketdata.MarketDataService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;

/**
 * Created by rongqiang.li on 18/2/26.
 */
@Repository
public class BiboxMarketApiUtils {

    @Resource
    private CommonMarketUtils commonMarketUtils;

    private Logger logger = LoggerFactory.getLogger(BiboxMarketApiUtils.class);


    public CommonTickerVO getTicker(String targetCode, String cashCurrency) {
        try {
            Exchange exchange = ExchangeFactory.INSTANCE.createExchange(BiboxExchange.class.getName());
            MarketDataService marketDataService = exchange.getMarketDataService();
            Ticker ticker = marketDataService.getTicker(new CurrencyPair(targetCode, cashCurrency));
            CommonTickerVO commonTickerVO = new CommonTickerVO();

            commonTickerVO.setAsk(ticker.getAsk());
            commonTickerVO.setBid(ticker.getBid());
            commonTickerVO.setHigh(ticker.getHigh());
            commonTickerVO.setLast(ticker.getLast());
            commonTickerVO.setLow(ticker.getLow());
            commonTickerVO.setVolume(ticker.getVolume());
            commonTickerVO.setSymbol(Joiner.on("").join(targetCode, cashCurrency));
            commonTickerVO.setVwap(ticker.getVwap());
            return commonTickerVO;
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return null;
        }
    }


    public CommonOrderBook getOrderBook(String targetCode, String cashCurrency, Integer depth) {
        try {
            Exchange exchange = ExchangeFactory.INSTANCE.createExchange(BiboxExchange.class.getName());
            MarketDataService marketDataService = exchange.getMarketDataService();
            OrderBook orderBook = marketDataService.getOrderBook(new CurrencyPair(targetCode, cashCurrency));
            CommonOrderBook commonOrderBook = commonMarketUtils.buildCommonOrderBook(orderBook, depth);
            return commonOrderBook;
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return null;
        }
    }


}
