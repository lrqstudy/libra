package com.libra.exchange.exchanges.bibox;

import com.google.common.base.Joiner;
import com.libra.exchange.exchanges.common.CommonOpenOrderVO;
import com.libra.exchange.exchanges.common.CommonOrderBook;
import com.libra.exchange.exchanges.common.CommonTickerVO;
import com.libra.exchange.exchanges.common.CommonTransferService;
import com.libra.exchange.strategy.model.PlatformConfigModel;
import org.knowm.xchange.binance.dto.trade.OrderSide;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * Created by rongqiang.li on 18/2/26.
 */

@Service
public class BiboxTransferServiceImpl implements CommonTransferService {
    @Resource
    private BiboxExchangeUtils biboxExchangeUtils;
    @Resource
    private BiboxMarketApiUtils biboxMarketApiUtils;

    private Logger logger = LoggerFactory.getLogger(BiboxTransferServiceImpl.class);

    @Override
    public CommonOrderBook getOrderBook(String targetCode, String cashCurrency, int depth) {
        CommonOrderBook orderBook = biboxMarketApiUtils.getOrderBook(targetCode, cashCurrency, 1);
        logger.info(" get targetCode={}, cashCurrency={},orderBook ={}", targetCode, cashCurrency, orderBook);
        return orderBook;
    }

    @Override
    public Map<String, BigDecimal> getBalanceMap(PlatformConfigModel configModel) {
        Map<String, BigDecimal> balanceMap = biboxExchangeUtils.getBalanceMap(configModel);
        return balanceMap;
    }

    @Override
    public String getCurrencyPair(String targetCode, String cashCurrency) {
        return Joiner.on("").join(targetCode, cashCurrency);
    }

    @Override
    public String buy(String targetCode, String cashCurrency, BigDecimal price, BigDecimal amount, Integer precisionSize, PlatformConfigModel configModel, Integer orderSource) {
        return biboxExchangeUtils.buy(targetCode, cashCurrency, configModel, getCurrencyPair(targetCode, cashCurrency), price, amount, OrderSide.BUY, orderSource, precisionSize);
    }

    @Override
    public String sell(String targetCode, String cashCurrency, BigDecimal price, BigDecimal amount, Integer precisionSize, PlatformConfigModel configModel, Integer orderSource) {
        return biboxExchangeUtils.sell(targetCode, cashCurrency, configModel, getCurrencyPair(targetCode, cashCurrency), price, amount, OrderSide.SELL, orderSource, precisionSize);
    }

    @Override
    public String withdraw(String targetCode, String address, String memo, BigDecimal amount, PlatformConfigModel configModel) {
        return null;
    }

    @Override
    public List<CommonOpenOrderVO> getOpenOrderList(PlatformConfigModel platformConfigModel, String targetCode, String cashCurrency) {
        List<CommonOpenOrderVO> openOrderVOList = biboxExchangeUtils.getOpenOrderVOList(platformConfigModel, targetCode, cashCurrency);
        logger.info(" bibox get openorder  targetCode={},cashCurrency={}, size={} ",targetCode,cashCurrency,openOrderVOList.size());
        return openOrderVOList;
    }

    @Override
    public CommonTickerVO getTickerBySymbol(String targetCode, String cashCurrency) {
        return biboxMarketApiUtils.getTicker(targetCode, cashCurrency);
    }
}
