package com.libra.exchange.exchanges.bibox;

import com.google.common.collect.*;
import com.libra.common.consts.SystemConsts;
import com.libra.common.util.BeanUtilExt;
import com.libra.exchange.exchanges.common.CommonOpenOrderVO;
import com.libra.exchange.strategy.model.AltTransferErrorHandleModel;
import com.libra.exchange.strategy.model.PlatformConfigModel;
import com.libra.exchange.strategy.service.AltTransferErrorHandleService;
import org.apache.commons.collections.CollectionUtils;
import org.knowm.xchange.Exchange;
import org.knowm.xchange.ExchangeFactory;
import org.knowm.xchange.ExchangeSpecification;
import org.knowm.xchange.bibox.BiboxExchange;
import org.knowm.xchange.bibox.dto.trade.BiboxOrder;
import org.knowm.xchange.bibox.dto.trade.BiboxOrders;
import org.knowm.xchange.bibox.service.BiboxTradeServiceRaw;
import org.knowm.xchange.binance.dto.trade.OrderSide;
import org.knowm.xchange.currency.Currency;
import org.knowm.xchange.currency.CurrencyPair;
import org.knowm.xchange.dto.Order;
import org.knowm.xchange.dto.account.Balance;
import org.knowm.xchange.dto.trade.LimitOrder;
import org.knowm.xchange.service.account.AccountService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * Created by rongqiang.li on 18/2/28.
 */
@Repository
public class BiboxExchangeUtils {
    private Logger logger = LoggerFactory.getLogger(BiboxExchangeUtils.class);
    @Resource
    private AltTransferErrorHandleService altTransferErrorHandleService;


    public Map<String, BigDecimal> getBalanceMap(PlatformConfigModel platformConfigModel) {
        try {
            Exchange exchange = getExchange(platformConfigModel);
            AccountService accountService = exchange.getAccountService();
            Map<Currency, Balance> balances = accountService.getAccountInfo().getWallet().getBalances();
            if (balances == null || balances.size() == 0) {
                return Maps.newHashMap();
            }
            Map<String, BigDecimal> balanceMap = Maps.newHashMap();
            for (Map.Entry<Currency, Balance> entry : balances.entrySet()) {
                String currencyCode = entry.getKey().getCurrencyCode();
                Balance value = entry.getValue();
                BigDecimal availableBalance = value.getAvailable();
                balanceMap.put(currencyCode, availableBalance);
            }
            logger.info(" bibox get balance size={}", balanceMap.size());
            return balanceMap;
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return Maps.newHashMap();
        }
    }


    public String buy(String targetCode, String cashCurrency, PlatformConfigModel platformConfigModel, String symbol, BigDecimal price, BigDecimal amount, OrderSide orderSide, Integer precisionSize, Integer orderSource) {
        try {
            //TODO  支持配置
            //amount = amount.multiply(new BigDecimal(1.002));
            amount = amount.setScale(precisionSize, BigDecimal.ROUND_CEILING);
            Exchange exchange = getExchange(platformConfigModel);
            BiboxTradeServiceRaw tradeService = (BiboxTradeServiceRaw) exchange.getTradeService();
            CurrencyPair currencyPair = new CurrencyPair(targetCode, cashCurrency);
            LimitOrder limitOrder = new LimitOrder.Builder(Order.OrderType.BID, currencyPair)
                    .limitPrice(price)
                    .originalAmount(amount)
                    .build();
            Integer result = tradeService.placeBiboxLimitOrder(limitOrder);
            logger.info(" bibox buy symbol={},price={},amount={}, result={} ", symbol, price, amount, result);
            return String.valueOf(result);
        } catch (Exception e) {
            logger.error("trade error " + e.getMessage(), e);
            recordErrorOrder(targetCode, cashCurrency, platformConfigModel, symbol, price, amount, orderSide, precisionSize, orderSource, e);
            return "error";
        }
    }

    private void recordErrorOrder(String targetCode, String cashCurrency, PlatformConfigModel platformConfigModel, String symbol, BigDecimal price, BigDecimal amount, OrderSide orderSide, Integer precisionSize, Integer orderSource, Exception e) {
        AltTransferErrorHandleModel model = new AltTransferErrorHandleModel();
        model.setCashCurrency(cashCurrency);
        model.setCurrencyPair(symbol);
        model.setPlatform(platformConfigModel.getPlatform());
        model.setResultId("");
        model.setSide(orderSide.name().toLowerCase());
        model.setTargetCode(targetCode);
        model.setTradeAmount(amount);
        model.setTradePrice(price);
        model.setUserCode(platformConfigModel.getUserCode());
        model.setPrecisionSize(precisionSize);
        String errorMsg = e.getMessage();
        model.setErrorMessage(errorMsg.substring(0, errorMsg.length() > SystemConsts.EXCEPTION_ERROR_MESSAGE_SIZE ? SystemConsts.EXCEPTION_ERROR_MESSAGE_SIZE : errorMsg.length()));
        model.setOrderSource(orderSource);
        BeanUtilExt.fillNullProperty(model);
        altTransferErrorHandleService.saveAltTransferErrorHandleModel(model);
    }

    public String sell(String targetCode, String cashCurrency, PlatformConfigModel platformConfigModel, String symbol, BigDecimal price, BigDecimal amount, OrderSide orderSide, Integer precisionSize, Integer orderSource) {
        try {

            amount = amount.setScale(precisionSize, BigDecimal.ROUND_CEILING);

            Exchange exchange = getExchange(platformConfigModel);
            BiboxTradeServiceRaw tradeService = (BiboxTradeServiceRaw) exchange.getTradeService();
            CurrencyPair currencyPair = new CurrencyPair(targetCode, cashCurrency);
            LimitOrder limitOrder = new LimitOrder.Builder(Order.OrderType.ASK, currencyPair)
                    .limitPrice(price)
                    .originalAmount(amount)
                    .build();
            Integer result = tradeService.placeBiboxLimitOrder(limitOrder);
            logger.info(" bibox sell symbol={},price={},amount={}, result={} ", symbol, price, amount, result);
            return String.valueOf(result);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            recordErrorOrder(targetCode, cashCurrency, platformConfigModel, symbol, price, amount, orderSide, precisionSize, orderSource, e);
            return "error";
        }
    }

    private Exchange getExchange(PlatformConfigModel platformConfigModel) {
        ExchangeSpecification exSpec = new ExchangeSpecification(BiboxExchange.class);
        exSpec.setApiKey(platformConfigModel.getApiKey());
        exSpec.setSecretKey(platformConfigModel.getApiSecret());
        return ExchangeFactory.INSTANCE.createExchange(exSpec);
    }

    public List<CommonOpenOrderVO> getOpenOrderVOList(PlatformConfigModel platformConfigModel, String targetCode, String cashCurrency) {

        Exchange exchange = getExchange(platformConfigModel);
        BiboxTradeServiceRaw tradeService = (BiboxTradeServiceRaw) exchange.getTradeService();
        BiboxOrders biboxOpenOrders = tradeService.getBiboxOpenOrders();
        List<BiboxOrder> items = biboxOpenOrders.getItems();
        if (CollectionUtils.isEmpty(items)) {
            return Lists.newArrayList();
        }
        List<CommonOpenOrderVO> openOrderVOList = Lists.newArrayList();
        for (BiboxOrder item : items) {
            if (item.getCurrencySymbol().equals(cashCurrency) && item.getCoinSymbol().equals(targetCode)) {
                CommonOpenOrderVO commonOpenOrderVO = new CommonOpenOrderVO();
                commonOpenOrderVO.setOrderId(String.valueOf(item.getId()));
                commonOpenOrderVO.setOriginAmount(item.getAmount());
                commonOpenOrderVO.setPrice(item.getPrice());
                commonOpenOrderVO.setSide(item.getOrderSide().name());
                commonOpenOrderVO.setStatus(item.getStatus().name());
                commonOpenOrderVO.setSymbol(buildCommonSymbol(targetCode, cashCurrency));
                commonOpenOrderVO.setType(item.getOrderType().name());
                commonOpenOrderVO.setRemainAmount(item.getUnexecuted());
                openOrderVOList.add(commonOpenOrderVO);
            }
        }
        return openOrderVOList;
    }

    private String buildCommonSymbol(String targetCode, String cashCurrency) {
        return com.google.common.base.Joiner.on("").join(targetCode, cashCurrency);
    }
}
