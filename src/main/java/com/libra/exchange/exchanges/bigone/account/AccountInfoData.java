package com.libra.exchange.exchanges.bigone.account;

public class AccountInfoData {
    private String type;

    private String user_id;

    private String account_id;

    private String account_type;

    private String account_name;

    private String public_key;

    private String system_account;

    private String active_balance;

    private String frozen_balance;

    private String estimated_btc_price;

    private String verification;

    private boolean has_asset_pin;


    public void setType(String type) {
        this.type = type;
    }

    public String getType() {
        return this.type;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getUser_id() {
        return this.user_id;
    }

    public void setAccount_id(String account_id) {
        this.account_id = account_id;
    }

    public String getAccount_id() {
        return this.account_id;
    }

    public void setAccount_type(String account_type) {
        this.account_type = account_type;
    }

    public String getAccount_type() {
        return this.account_type;
    }

    public void setAccount_name(String account_name) {
        this.account_name = account_name;
    }

    public String getAccount_name() {
        return this.account_name;
    }

    public void setPublic_key(String public_key) {
        this.public_key = public_key;
    }

    public String getPublic_key() {
        return this.public_key;
    }

    public void setSystem_account(String system_account) {
        this.system_account = system_account;
    }

    public String getSystem_account() {
        return this.system_account;
    }

    public void setActive_balance(String active_balance) {
        this.active_balance = active_balance;
    }

    public String getActive_balance() {
        return this.active_balance;
    }

    public void setFrozen_balance(String frozen_balance) {
        this.frozen_balance = frozen_balance;
    }

    public String getFrozen_balance() {
        return this.frozen_balance;
    }

    public void setEstimated_btc_price(String estimated_btc_price) {
        this.estimated_btc_price = estimated_btc_price;
    }

    public String getEstimated_btc_price() {
        return this.estimated_btc_price;
    }

    public void setVerification(String verification) {
        this.verification = verification;
    }

    public String getVerification() {
        return this.verification;
    }

    public void setHas_asset_pin(boolean has_asset_pin) {
        this.has_asset_pin = has_asset_pin;
    }

    public boolean getHas_asset_pin() {
        return this.has_asset_pin;
    }


}