package com.libra.exchange.exchanges.bigone.account;

import java.util.List;

/**
 * Created by rongqiang.li on 18/2/14.
 */
public class AccountInfoJsonVO {
    private List<AccountInfoData> data;


    public List<AccountInfoData> getData() {
        return data;
    }

    public void setData(List<AccountInfoData> data) {
        this.data = data;
    }
}
