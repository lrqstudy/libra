package com.libra.exchange.exchanges.bigone;

import java.io.Serializable;
import java.util.List;

/**
 * Created by rongqiang.li on 17/11/28.
 */
public class BigOneOrderBookVO implements Serializable {
    private String type;
    private List<BigOneOrderVO> bids;
    private List<BigOneOrderVO> asks;


    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<BigOneOrderVO> getBids() {
        return bids;
    }

    public void setBids(List<BigOneOrderVO> bids) {
        this.bids = bids;
    }

    public List<BigOneOrderVO> getAsks() {
        return asks;
    }

    public void setAsks(List<BigOneOrderVO> asks) {
        this.asks = asks;
    }
}
