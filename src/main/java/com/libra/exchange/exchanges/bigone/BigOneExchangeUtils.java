package com.libra.exchange.exchanges.bigone;

import com.google.api.client.util.Lists;
import com.google.api.client.util.Maps;
import com.google.common.collect.ImmutableMap;
import com.libra.common.consts.OrderSide;
import com.libra.common.consts.SystemConsts;
import com.libra.common.exceptions.BTCTTSException;
import com.libra.common.util.BeanUtilExt;
import com.libra.common.util.JsonUtil;
import com.libra.exchange.exchanges.bigone.account.AccountInfoData;
import com.libra.exchange.exchanges.bigone.account.AccountInfoJsonVO;
import com.libra.exchange.exchanges.bigone.account.BigOneOrderResultVO;
import com.libra.exchange.exchanges.common.CommonOpenOrderVO;
import com.libra.exchange.strategy.model.AltTransferErrorHandleModel;
import com.libra.exchange.strategy.model.PlatformConfigModel;
import com.libra.exchange.strategy.service.AltTransferErrorHandleService;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.io.*;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * Created by rongqiang.li on 17/11/28.
 */
@Repository
public class BigOneExchangeUtils {

    public final static String BIGONE_BASE_URL = "https://api.big.one";

    public final static String BIGONE_GET_ACCOUNT_COMMAND = "/accounts";
    public final static String BIGONE_ORDERS_COMMAND = "/orders";

    public static final Map<String, String> ORDER_SIDE_MAPPING = ImmutableMap.of("BID", OrderSide.BUY.getCode(), "bid", OrderSide.BUY.getCode(), "ASK", OrderSide.SELL.getCode(), "ask", OrderSide.SELL.getCode());


    private static final String USER_AGENT = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36";

    private static Logger logger = LoggerFactory.getLogger(BigOneExchangeUtils.class);


    @Resource
    private AltTransferErrorHandleService altTransferErrorHandleService;

    public List<CommonOpenOrderVO> getOpenOrderList(String targetCode, String cashCurrency, String currencyPair, PlatformConfigModel configModel) {
        StringBuilder sb = new StringBuilder();
        sb.append(BIGONE_BASE_URL).append(BIGONE_ORDERS_COMMAND).append("?market=").append(currencyPair);

        String result = executeCommandWithGet(sb.toString(), configModel);

        BigOneOpenOrderVO bigOneOpenOrderVO = JsonUtil.jsonToObject(result, BigOneOpenOrderVO.class);
        if (bigOneOpenOrderVO == null) {
            return Lists.newArrayList();
        }
        List<BigOneOrderResultJsonVO> data = bigOneOpenOrderVO.getData();

        List<CommonOpenOrderVO> commonOpenOrderVOList = Lists.newArrayList();
        for (BigOneOrderResultJsonVO openOrderJsonVO : data) {
            CommonOpenOrderVO commonOpenOrderVO = new CommonOpenOrderVO();
            commonOpenOrderVO.setOrderId(openOrderJsonVO.getOrder_id());
            commonOpenOrderVO.setPrice(new BigDecimal(openOrderJsonVO.getPrice()));
            commonOpenOrderVO.setSide(ORDER_SIDE_MAPPING.get(openOrderJsonVO.getOrder_side()));
            BigDecimal originAmount = new BigDecimal(openOrderJsonVO.getAmount());
            BigDecimal filledAmount = new BigDecimal(openOrderJsonVO.getFilled_amount());
            BigDecimal remainAmount = originAmount.subtract(filledAmount);
            remainAmount = remainAmount.setScale(2, BigDecimal.ROUND_CEILING);
            commonOpenOrderVO.setOriginAmount(originAmount);
            commonOpenOrderVO.setRemainAmount(remainAmount);
            commonOpenOrderVO.setSymbol(currencyPair);
            commonOpenOrderVO.setType(openOrderJsonVO.getOrder_type());
            commonOpenOrderVO.setStatus(openOrderJsonVO.getOrder_state());
            commonOpenOrderVOList.add(commonOpenOrderVO);
        }
        return commonOpenOrderVOList;
    }


    public List<AccountInfoData> getAccountInfoList(PlatformConfigModel configModel, String currencyType) {
        try {
            StringBuilder sb = new StringBuilder();
            sb.append(BIGONE_BASE_URL).
                    append(BIGONE_GET_ACCOUNT_COMMAND).
                    append("/").append(currencyType);
            String resultStr = executeCommandWithGet(sb.toString(), configModel);
            AccountInfoJsonVO accountInfoJsonVO = JsonUtil.jsonToObject(resultStr, AccountInfoJsonVO.class);
            if (accountInfoJsonVO != null) {
                return accountInfoJsonVO.getData();
            }
            return Lists.newArrayList();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return Lists.newArrayList();
        }
    }


    public String buy(String targetCode, String cashCurrency, String currrencyPair, BigDecimal price, BigDecimal amount, Integer precisionSize, PlatformConfigModel configModel, Integer orderSource) {
        //amount = amount.multiply(new BigDecimal(1.002));
        amount = amount.setScale(precisionSize, BigDecimal.ROUND_CEILING);
        String result = trade(targetCode, cashCurrency, currrencyPair, price, amount, precisionSize, configModel, orderSource, "BID");

        return result;
    }

    private String trade(String targetCode, String cashCurrency, String currencyPair, BigDecimal price, BigDecimal amount, Integer precisionSize, PlatformConfigModel configModel, Integer orderSource, String orderSide) {
        try {
            Map<String, String> params = Maps.newHashMap();
            amount = amount.setScale(precisionSize, BigDecimal.ROUND_CEILING);
            params.put("order_market", currencyPair);
            params.put("order_side", orderSide);
            params.put("price", price.toPlainString());
            params.put("amount", amount.toPlainString());

            StringBuilder sb = new StringBuilder();
            sb.append(BIGONE_BASE_URL).append(BIGONE_ORDERS_COMMAND);

            String result = executeCommandWithPost(sb.toString(), params, configModel);
            BigOneOrderResultVO bigOneOrderResultJsonVO = JsonUtil.jsonToObject(result, BigOneOrderResultVO.class);
            String resultId = bigOneOrderResultJsonVO.getData().getOrder_id();
            logger.info(" params={},resultId={}", params, resultId);
            return resultId;
        } catch (Exception e) {
            logger.error(" sell error" + e.getMessage(), e);
            recordErrorOrder(targetCode, cashCurrency, configModel, currencyPair, price, amount, orderSide, precisionSize, e, orderSource);
            return "error";
        }
    }

    private void recordErrorOrder(String targetCode, String cashCurrency, PlatformConfigModel platformConfigModel, String currencyPair, BigDecimal price, BigDecimal amount, String sell, Integer precisionSize, Exception e, Integer orderSource) {
        AltTransferErrorHandleModel model = new AltTransferErrorHandleModel();
        model.setCashCurrency(cashCurrency);
        model.setCurrencyPair(currencyPair.toString());
        model.setPlatform(platformConfigModel.getPlatform());
        model.setResultId("");
        model.setSide(sell);
        model.setTargetCode(targetCode);
        model.setTradeAmount(amount);
        model.setTradePrice(price);
        model.setUserCode(platformConfigModel.getUserCode());
        model.setPrecisionSize(precisionSize);
        String errorMsg =e.getMessage();
        if (org.apache.commons.lang3.StringUtils.isEmpty(errorMsg)){
            errorMsg="error";
        }
        model.setErrorMessage(errorMsg.substring(0, errorMsg.length() > SystemConsts.EXCEPTION_ERROR_MESSAGE_SIZE ? SystemConsts.EXCEPTION_ERROR_MESSAGE_SIZE : errorMsg.length()));
        model.setOrderSource(orderSource);
        BeanUtilExt.fillNullProperty(model);
        altTransferErrorHandleService.saveAltTransferErrorHandleModel(model);
    }


    public String sell(String targetCode, String cashCurrency, String currrencyPair, BigDecimal price, BigDecimal amount, Integer precisionSize, PlatformConfigModel configModel, Integer orderSource) {
        Map<String, String> params = Maps.newHashMap();
        amount = amount.setScale(precisionSize, BigDecimal.ROUND_CEILING);
        params.put("order_market", currrencyPair);
        params.put("order_side", "ASK");
        params.put("price", price.toPlainString());
        params.put("amount", amount.toPlainString());
        return trade(targetCode, cashCurrency, currrencyPair, price, amount, precisionSize, configModel, orderSource, "ASK");
    }


    private String executeCommandWithPost(String urlStr, Map<String, String> paramsMap, PlatformConfigModel configModel) {
        HttpURLConnection conn = null;
        try {
            URL url = new URL(urlStr);
            conn = (HttpURLConnection) url.openConnection();
            conn.setUseCaches(false);
            conn.setDoOutput(true);
            conn.setRequestProperty("Authorization", buildAuthorizationWithConfigModel(configModel));
            conn.setRequestProperty("Big-Device-Id", UUID.randomUUID().toString());
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("User-Agent", USER_AGENT);
            conn.setRequestMethod("POST");


            String postData;
            JSONObject jsonObject = new JSONObject();
            for (Map.Entry<String, String> entry : paramsMap.entrySet()) {
                jsonObject.put(entry.getKey(), entry.getValue());
            }

            postData = jsonObject.toString();
            OutputStreamWriter out = new OutputStreamWriter(conn.getOutputStream());

            out.write(postData);
            out.close();
            String responseResult = convertStreamToString(conn.getInputStream());
            logger.info(" call url={}, responseResult={} ",urlStr, responseResult);
            return responseResult;
        } catch (MalformedURLException e) {
            throw new BTCTTSException("call url  error" + e.getMessage(), e);
        } catch (Exception e) {
            throw new BTCTTSException("call url  error" + e.getMessage(), e);
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }
    }

    private String buildAuthorizationWithConfigModel(PlatformConfigModel configModel) {
        StringBuilder sb = new StringBuilder();
        sb.append("Bearer ").append(configModel.getApiKey());
        return sb.toString();
    }


    private String executeCommandWithGet(String urlStr, PlatformConfigModel configModel) {
        HttpURLConnection conn = null;
        try {
            URL url = new URL(urlStr);
            conn = (HttpURLConnection) url.openConnection();
            conn.setUseCaches(false);
            conn.setDoOutput(true);
            conn.setRequestProperty("Authorization", buildAuthorizationWithConfigModel(configModel));
            conn.setRequestProperty("Big-Device-Id", UUID.randomUUID().toString());
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("User-Agent", USER_AGENT);
            conn.setRequestMethod("GET");
            String responseResult = convertStreamToString(conn.getInputStream());
            logger.info(" call url={}, responseResult={} ", urlStr,responseResult);
            return responseResult;
        } catch (MalformedURLException e) {
            throw new BTCTTSException("call url  error" + e.getMessage(), e);
        } catch (IOException e) {
            throw new BTCTTSException("call url  error" + e.getMessage(), e);
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }
    }

    private String convertStreamToString(InputStream is) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();

        String line;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line).append('\n');
            }
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }


}
