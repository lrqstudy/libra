package com.libra.exchange.exchanges.bigone.account;

public class Withdrawal {
    private String fee;

    private String fee_type;

    public void setFee(String fee) {
        this.fee = fee;
    }

    public String getFee() {
        return this.fee;
    }

    public void setFee_type(String fee_type) {
        this.fee_type = fee_type;
    }

    public String getFee_type() {
        return this.fee_type;
    }

}