package com.libra.exchange.exchanges.bigone;

import java.io.Serializable;

/**
 * Created by rongqiang.li on 17/11/28.
 */
public class BigOneOrderBookJsonResult implements Serializable {
    private BigOneOrderBookVO data;

    public BigOneOrderBookVO getData() {
        return data;
    }

    public void setData(BigOneOrderBookVO data) {
        this.data = data;
    }
}
