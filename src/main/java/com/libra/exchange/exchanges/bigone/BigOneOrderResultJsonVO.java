package com.libra.exchange.exchanges.bigone;

public class BigOneOrderResultJsonVO {
    private String type;

    private String user_id;

    private String order_id;

    private String order_market;

    private String order_type;

    private String order_side;

    private String order_state;

    private String price;

    private String amount;

    private String filled_amount;

    private String created_at;

    private String updated_at;

    public void setType(String type) {
        this.type = type;
    }

    public String getType() {
        return this.type;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getUser_id() {
        return this.user_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getOrder_id() {
        return this.order_id;
    }

    public void setOrder_market(String order_market) {
        this.order_market = order_market;
    }

    public String getOrder_market() {
        return this.order_market;
    }

    public void setOrder_type(String order_type) {
        this.order_type = order_type;
    }

    public String getOrder_type() {
        return this.order_type;
    }

    public void setOrder_side(String order_side) {
        this.order_side = order_side;
    }

    public String getOrder_side() {
        return this.order_side;
    }

    public void setOrder_state(String order_state) {
        this.order_state = order_state;
    }

    public String getOrder_state() {
        return this.order_state;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getPrice() {
        return this.price;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getAmount() {
        return this.amount;
    }

    public void setFilled_amount(String filled_amount) {
        this.filled_amount = filled_amount;
    }

    public String getFilled_amount() {
        return this.filled_amount;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getCreated_at() {
        return this.created_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getUpdated_at() {
        return this.updated_at;
    }

}