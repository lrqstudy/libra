package com.libra.exchange.exchanges.bigone.account;
public class Deposits {
private String type;

private String deposit_id;

private String deposit_type;

private String amount;

private int confirmations;

private String state;

private String scanner_url;

private String created_at;

public void setType(String type){
this.type = type;
}
public String getType(){
return this.type;
}
public void setDeposit_id(String deposit_id){
this.deposit_id = deposit_id;
}
public String getDeposit_id(){
return this.deposit_id;
}
public void setDeposit_type(String deposit_type){
this.deposit_type = deposit_type;
}
public String getDeposit_type(){
return this.deposit_type;
}
public void setAmount(String amount){
this.amount = amount;
}
public String getAmount(){
return this.amount;
}
public void setConfirmations(int confirmations){
this.confirmations = confirmations;
}
public int getConfirmations(){
return this.confirmations;
}
public void setState(String state){
this.state = state;
}
public String getState(){
return this.state;
}
public void setScanner_url(String scanner_url){
this.scanner_url = scanner_url;
}
public String getScanner_url(){
return this.scanner_url;
}
public void setCreated_at(String created_at){
this.created_at = created_at;
}
public String getCreated_at(){
return this.created_at;
}

}