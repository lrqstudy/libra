package com.libra.exchange.exchanges.bigone.account;

import com.libra.exchange.exchanges.bigone.BigOneOrderResultJsonVO;

import java.io.Serializable;

/**
 * Created by rongqiang.li on 18/2/24.
 */
public class BigOneOrderResultVO implements Serializable{
    private BigOneOrderResultJsonVO data;


    public BigOneOrderResultJsonVO getData() {
        return data;
    }

    public void setData(BigOneOrderResultJsonVO data) {
        this.data = data;
    }
}
