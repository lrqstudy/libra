package com.libra.exchange.exchanges.bigone;

import com.google.api.client.util.Maps;
import com.google.common.base.Joiner;
import com.libra.common.util.BeanUtilExt;
import com.libra.exchange.exchanges.bigone.account.AccountInfoData;
import com.libra.exchange.exchanges.common.*;
import com.libra.exchange.strategy.model.PlatformConfigModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * Created by rongqiang.li on 17/11/28.
 */
@Service
public class BigOneTransferServiceImpl implements CommonTransferService {

    @Resource
    private BigOneExchangeUtils bigOneExchangeUtils;

    @Resource
    private BigOneMarketApi bigOneMarketApi;

    private static Logger logger = LoggerFactory.getLogger(BigOneTransferServiceImpl.class);

    @Override
    public CommonOrderBook getOrderBook(String targetCode, String cashCurrency, int depth) {
        String currencyPair = getCurrencyPair(targetCode, cashCurrency);
        CommonOrderBook orderBook = bigOneMarketApi.getOrderBook(targetCode, cashCurrency, currencyPair, depth);
        logger.info(" get currencyPair={} order book result={}", currencyPair, orderBook);
        return orderBook;
    }

    @Override
    public Map<String, BigDecimal> getBalanceMap(PlatformConfigModel configModel) {
        //TODO handle error

        List<AccountInfoData> accountInfoList = bigOneExchangeUtils.getAccountInfoList(configModel, "");
        if (org.apache.commons.collections.CollectionUtils.isEmpty(accountInfoList)) {
            return Maps.newHashMap();
        }
        Map<String, BigDecimal> balanceMap = Maps.newHashMap();
        for (AccountInfoData accountInfoData : accountInfoList) {
            balanceMap.put(accountInfoData.getAccount_type(), new BigDecimal(accountInfoData.getActive_balance()));
        }
        return balanceMap;
    }

    @Override
    public String getCurrencyPair(String targetCode, String cashCurrency) {
        return Joiner.on("-").join(targetCode, cashCurrency);
    }

    @Override
    public String buy(String targetCode, String cashCurrency, BigDecimal price, BigDecimal amount, Integer precisionSize, PlatformConfigModel configModel, Integer orderSource) {
        String currencyPair = getCurrencyPair(targetCode, cashCurrency);
        String result = bigOneExchangeUtils.buy(targetCode, cashCurrency, currencyPair, price, amount, precisionSize, configModel, orderSource);
        return result;
    }

    @Override
    public String sell(String targetCode, String cashCurrency, BigDecimal price, BigDecimal amount, Integer precisionSize, PlatformConfigModel configModel, Integer orderSource) {
        String currencyPair = getCurrencyPair(targetCode, cashCurrency);
        String result = bigOneExchangeUtils.sell(targetCode, cashCurrency, currencyPair, price, amount, precisionSize, configModel, orderSource);
        return result;
    }

    @Override
    public String withdraw(String targetCode, String address, String memo, BigDecimal amount, PlatformConfigModel configModel) {
        return null;
    }

    @Override
    public List<CommonOpenOrderVO> getOpenOrderList(PlatformConfigModel platformConfigModel, String targetCode, String cashCurrency) {
        List<CommonOpenOrderVO> openOrderList = bigOneExchangeUtils.getOpenOrderList(targetCode, cashCurrency, getCurrencyPair(targetCode, cashCurrency), platformConfigModel);
        logger.info(" big one get open order targetCode={},cashCurrency ={} ,size={}", targetCode, cashCurrency, openOrderList.size());
        return openOrderList;
    }

    @Override
    public CommonTickerVO getTickerBySymbol(String targetCode, String cashCurrency) {
        CommonOrderBook orderBook = getOrderBook(targetCode, cashCurrency, 1);
        CommonOrderVO commonOrderVO = orderBook.getBuyOrderVOList().get(0);
        CommonTickerVO tickerVO = new CommonTickerVO();
        tickerVO.setLast(commonOrderVO.getPrice());
        tickerVO.setAsk(commonOrderVO.getPrice());
        tickerVO.setSymbol(getCurrencyPair(targetCode, cashCurrency));
        tickerVO.setBid(commonOrderVO.getPrice());
        tickerVO.setVolume(commonOrderVO.getVolume());
        tickerVO.setHigh(commonOrderVO.getPrice());
        BeanUtilExt.fillNullProperty(tickerVO);
        return tickerVO;
    }
}
