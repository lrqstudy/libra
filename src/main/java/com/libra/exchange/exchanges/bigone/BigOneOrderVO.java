package com.libra.exchange.exchanges.bigone;

import java.io.Serializable;

/**
 * Created by rongqiang.li on 17/11/28.
 */
public class BigOneOrderVO implements Serializable{
    private String type;
    private String price;
    private String amount;


    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }
}
