package com.libra.exchange.exchanges.bigone;

import com.google.api.client.util.Lists;
import com.google.common.base.Joiner;
import com.libra.exchange.exchanges.common.CommonOrderBook;
import com.libra.exchange.exchanges.common.CommonOrderVO;
import com.libra.exchange.exchanges.poloniex.PoloniexExchangeServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.List;

/**
 * Created by rongqiang.li on 17/11/28.
 */
@Repository
public class BigOneMarketApi {
    @Resource
    private RestTemplate restTemplate;

    private static final String BASE_URL = "https://api.big.one";

    private static Logger logger = LoggerFactory.getLogger(PoloniexExchangeServiceImpl.class);

    public CommonOrderBook getOrderBook(String targetCode, String cashCurrency, String symbol, int depth) {
        try {
            StringBuilder sb = new StringBuilder();
            sb.append(BASE_URL).append("/markets/").append(buildSymbol(targetCode, cashCurrency)).append("/").append("book");
            BigOneOrderBookJsonResult bookJsonVO = restTemplate.getForObject(sb.toString(), BigOneOrderBookJsonResult.class);
            CommonOrderBook commonOrderBook = buildCommonOrderBook(bookJsonVO,depth);
            return commonOrderBook;
        } catch (Exception e) {
            logger.error(e.getMessage());
            return null;
        }
    }



    private String buildSymbol(String targetCode, String cashCurrency) {
        return Joiner.on("-").join(targetCode, cashCurrency);
    }

    private CommonOrderBook buildCommonOrderBook(BigOneOrderBookJsonResult bookJsonVO,int depth) {
        CommonOrderBook commonOrderBook = new CommonOrderBook();
        List<BigOneOrderVO> asks = bookJsonVO.getData().getAsks();
        List<BigOneOrderVO> bids = bookJsonVO.getData().getBids();



        /**
         * buy order list
         */
        List<CommonOrderVO> buyOrderVOList = Lists.newArrayList();
        for(int i=0;i<depth;i++){
            CommonOrderVO buyOrderVO = buildCommonOrderVOFromBigOneOrderVO(bids.get(i));
            buyOrderVOList.add(buyOrderVO);
        }

        commonOrderBook.setBuyOrderVOList(buyOrderVOList);

        List<CommonOrderVO> sellOrderVOList = Lists.newArrayList();

        for(int i=0;i<depth;i++){
            CommonOrderVO sellOrderVO = buildCommonOrderVOFromBigOneOrderVO(asks.get(i));
            sellOrderVOList.add(sellOrderVO);
        }

        commonOrderBook.setSellOrderVOList(sellOrderVOList);
        return commonOrderBook;
    }

    private CommonOrderVO buildCommonOrderVOFromBigOneOrderVO(BigOneOrderVO bigOneOrderVO) {
        CommonOrderVO sellOrderVO = new CommonOrderVO();
        sellOrderVO.setPrice(new BigDecimal(bigOneOrderVO.getPrice()));
        sellOrderVO.setVolume(new BigDecimal(bigOneOrderVO.getAmount()));
        return sellOrderVO;
    }

}
