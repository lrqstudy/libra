package com.libra.exchange.exchanges.bigone;

import java.util.List;

/**
 * Created by rongqiang.li on 18/2/15.
 */
public class BigOneOpenOrderVO {
    private List<BigOneOrderResultJsonVO> data;

    public List<BigOneOrderResultJsonVO> getData() {
        return data;
    }

    public void setData(List<BigOneOrderResultJsonVO> data) {
        this.data = data;
    }
}
