package com.libra.exchange.exchanges.bigone.account;
public class Withdrawals {
private String type;

private String withdrawal_id;

private String withdrawal_type;

private String address;

private String memo;

private String label;

private String amount;

private String state;

private String scanner_url;

private String created_at;

public void setType(String type){
this.type = type;
}
public String getType(){
return this.type;
}
public void setWithdrawal_id(String withdrawal_id){
this.withdrawal_id = withdrawal_id;
}
public String getWithdrawal_id(){
return this.withdrawal_id;
}
public void setWithdrawal_type(String withdrawal_type){
this.withdrawal_type = withdrawal_type;
}
public String getWithdrawal_type(){
return this.withdrawal_type;
}
public void setAddress(String address){
this.address = address;
}
public String getAddress(){
return this.address;
}
public void setMemo(String memo){
this.memo = memo;
}
public String getMemo(){
return this.memo;
}
public void setLabel(String label){
this.label = label;
}
public String getLabel(){
return this.label;
}
public void setAmount(String amount){
this.amount = amount;
}
public String getAmount(){
return this.amount;
}
public void setState(String state){
this.state = state;
}
public String getState(){
return this.state;
}
public void setScanner_url(String scanner_url){
this.scanner_url = scanner_url;
}
public String getScanner_url(){
return this.scanner_url;
}
public void setCreated_at(String created_at){
this.created_at = created_at;
}
public String getCreated_at(){
return this.created_at;
}

}