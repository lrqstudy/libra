package com.libra.exchange.exchanges.bitflyer;

import java.io.Serializable;

/**
 * Created by rongqiang.li on 17/5/17.
 */
public class BitflyerTickerVO implements Serializable{
    /*
    *
   {"product_code":"BTC_JPY","timestamp":"2017-05-16T22:20:37.423",
   "tick_id":53245,"best_bid":206503.0,"best_ask":206510.0,
   "best_bid_size":2.63595779,"best_ask_size":8.0,
   "total_bid_depth":5890.31593945,"total_ask_depth":1706.95740706,
   "ltp":206503.0,
   "volume":54217.48425794,"volume_by_product":10317.26799369}
    * */

    private String product_code;

    private String timestamp;

    private int tick_id;

    private double best_bid;

    private double best_ask;

    private double best_bid_size;

    private double best_ask_size;

    private double total_bid_depth;

    private double total_ask_depth;

    private double ltp;

    private double volume;

    private double volume_by_product;

    public void setProduct_code(String product_code){
        this.product_code = product_code;
    }
    public String getProduct_code(){
        return this.product_code;
    }
    public void setTimestamp(String timestamp){
        this.timestamp = timestamp;
    }
    public String getTimestamp(){
        return this.timestamp;
    }
    public void setTick_id(int tick_id){
        this.tick_id = tick_id;
    }
    public int getTick_id(){
        return this.tick_id;
    }
    public void setBest_bid(double best_bid){
        this.best_bid = best_bid;
    }
    public double getBest_bid(){
        return this.best_bid;
    }
    public void setBest_ask(double best_ask){
        this.best_ask = best_ask;
    }
    public double getBest_ask(){
        return this.best_ask;
    }
    public void setBest_bid_size(double best_bid_size){
        this.best_bid_size = best_bid_size;
    }
    public double getBest_bid_size(){
        return this.best_bid_size;
    }
    public void setBest_ask_size(double best_ask_size){
        this.best_ask_size = best_ask_size;
    }
    public double getBest_ask_size(){
        return this.best_ask_size;
    }
    public void setTotal_bid_depth(double total_bid_depth){
        this.total_bid_depth = total_bid_depth;
    }
    public double getTotal_bid_depth(){
        return this.total_bid_depth;
    }
    public void setTotal_ask_depth(double total_ask_depth){
        this.total_ask_depth = total_ask_depth;
    }
    public double getTotal_ask_depth(){
        return this.total_ask_depth;
    }
    public void setLtp(double ltp){
        this.ltp = ltp;
    }
    public double getLtp(){
        return this.ltp;
    }
    public void setVolume(double volume){
        this.volume = volume;
    }
    public double getVolume(){
        return this.volume;
    }
    public void setVolume_by_product(double volume_by_product){
        this.volume_by_product = volume_by_product;
    }
    public double getVolume_by_product(){
        return this.volume_by_product;
    }
}
