package com.libra.exchange.exchanges.poloniex;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class OrderBookJsonVO {
    private Object[][] asks;

    private Object[][] bids;

    private String isFrozen;

    private int seq;

    public Object[][] getAsks() {
        return asks;
    }

    public void setAsks(Object[][] asks) {
        this.asks = asks;
    }

    public Object[][] getBids() {
        return bids;
    }

    public void setBids(Object[][] bids) {
        this.bids = bids;
    }

    public void setIsFrozen(String isFrozen) {
        this.isFrozen = isFrozen;
    }

    public String getIsFrozen() {
        return this.isFrozen;
    }

    public void setSeq(int seq) {
        this.seq = seq;
    }

    public int getSeq() {
        return this.seq;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}