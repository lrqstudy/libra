package com.libra.exchange.exchanges.poloniex.task;

import java.io.Serializable;

/**
 * Created by rongqiang.li on 17/5/13.
 */
public class CoinBaseTicker implements Serializable{
   // {"data":{"amount":"1660.49","currency":"USD"}
     //   ,"warnings":[{"id":"missing_version","message":"Please supply API version (YYYY-MM-DD) as CB-VERSION header","url":"https://developers.coinbase.com/api#versioning"}]}
    private CoinBaseDataBean data;
    public CoinBaseDataBean getData() {
        return data;
    }
    public void setData(CoinBaseDataBean data) {
        this.data = data;
    }
}
