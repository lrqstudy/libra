package com.libra.exchange.exchanges.poloniex;

import com.google.api.client.repackaged.com.google.common.base.Strings;
import com.google.common.base.Function;
import com.google.common.collect.Maps;
import com.libra.common.consts.SystemConsts;
import com.libra.common.exceptions.BTCTTSException;
import com.libra.common.util.BeanUtilExt;
import com.libra.common.util.JsonUtil;
import com.libra.exchange.exchanges.common.OpenOrder;
import com.libra.exchange.strategy.model.AltTransferErrorHandleModel;
import com.libra.exchange.strategy.model.PlatformConfigModel;
import com.libra.exchange.strategy.service.AltTransferErrorHandleService;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * Created by rongqiang.li on 17/4/25.
 */
@Service
public class PoloniexExchangeServiceImpl {
    @Resource
    private Poloniex poloniex;
    private Logger logger = LoggerFactory.getLogger(PoloniexExchangeServiceImpl.class);
    @Resource
    private PoloniexMarketUtils poloniexMarketUtils;

    @Resource
    private AltTransferErrorHandleService altTransferErrorHandleService;

    public String buy(String targetCode, String cashCurrency, BigDecimal buyPrice, BigDecimal buyAmount, PlatformConfigModel configModel,Integer precisionSize,Integer orderSource) {
        return trade(targetCode, cashCurrency, "buy", buyPrice, buyAmount, configModel,precisionSize,orderSource);
    }


    public String trade(String targetCode, String cashCurrency, String tradeType, BigDecimal tradePrice, BigDecimal tradeAmount, PlatformConfigModel configModel,Integer precisionSize,Integer orderSource) {
        String currencyPair = poloniexMarketUtils.buildCurrencyPair(targetCode, cashCurrency);
        try {
            if (tradeAmount.compareTo(BigDecimal.ZERO) == 0) {
                return " trade amount is zero ,can't trade ";
            }
            Map<String, String> args = Maps.newHashMap();
            args.put("command", tradeType);
            args.put("currencyPair", currencyPair);
            args.put("rate", String.valueOf(tradePrice.doubleValue()));//交易价格
            args.put("amount", String.valueOf(tradeAmount.doubleValue()));//交易数量
            return executeCommand(tradeType, args, configModel);
        } catch (Exception e) {
            logger.error("trade error" + e.getMessage(), e);
            AltTransferErrorHandleModel model = new AltTransferErrorHandleModel();
            model.setCashCurrency(cashCurrency);
            model.setCurrencyPair(currencyPair);
            model.setPlatform(configModel.getPlatform());
            model.setResultId("");
            model.setSide(tradeType);
            model.setTargetCode(targetCode);
            model.setTradeAmount(tradeAmount);
            model.setTradePrice(tradePrice);
            model.setUserCode(configModel.getUserCode());
            model.setPrecisionSize(precisionSize);
            String errorMsg=e.getMessage();
            model.setErrorMessage(errorMsg.substring(0,errorMsg.length()> SystemConsts.EXCEPTION_ERROR_MESSAGE_SIZE ? SystemConsts.EXCEPTION_ERROR_MESSAGE_SIZE:errorMsg.length()));
            model.setOrderSource(orderSource);
            BeanUtilExt.fillNullProperty(model);
            altTransferErrorHandleService.saveAltTransferErrorHandleModel(model);
            return "error";
        }
    }


    public String sell(String targetCode, String cashCurrency, BigDecimal sellPrice, BigDecimal sellAmount, PlatformConfigModel configModel,Integer precisionSize,Integer orderSource) {
        return trade(targetCode, cashCurrency, "sell", sellPrice, sellAmount, configModel,precisionSize,orderSource);
    }

    public String executeCommand(String command, Map<String, String> args, PlatformConfigModel configModel) {
        try {
            logger.info(" 执行命令参数 map={} ", args);
            String result = poloniex.authrequest(command, args, configModel);
            logger.info(" 命令执行结束 result={} ", result);
            return result;
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw new BTCTTSException(e.getMessage(), e);
        }
    }

    public String cancelOrder(String targetCode, String cashCurrency, String orderNumber, PlatformConfigModel configModel) {
        Map<String, String> args = Maps.newHashMap();
        args.put("command", "cancelOrder");
        args.put("currencyPair", poloniexMarketUtils.buildCurrencyPair(targetCode, cashCurrency));
        args.put("orderNumber", orderNumber);
        return executeCommand("cancelOrder", args, configModel);
    }

    public String withdraw(String targetCode, String address, String memo, BigDecimal amount, PlatformConfigModel configModel) {
        //"currency", "amount", and "address"
        //String[] addressInfo = yunbiAddressMap.get(targetCode).split("_");
        Map<String, String> args = Maps.newHashMap();
        args.put("command", "withdraw");
        args.put("currency", targetCode);
        args.put("amount", String.valueOf(amount.doubleValue()));
        args.put("address", address);
        if (!Strings.isNullOrEmpty(memo)) {
            args.put("paymentId", memo);
        }
        return executeCommand("withdraw", args, configModel);
    }


    public Map<String, BigDecimal> getAccountBalance(PlatformConfigModel configModel) {
        Map<String, String> args = Maps.newHashMap();
        args.put("command", "returnBalances");
        String returnBalances = executeCommand("returnBalances", args, configModel);
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            Map<String, String> balanceMap = objectMapper.readValue(returnBalances, Map.class);
            Map<String, BigDecimal> balanceInfoMap = Maps.transformValues(balanceMap, new Function<String, BigDecimal>() {
                @Override
                public BigDecimal apply(String balanceStr) {
                    return new BigDecimal(balanceStr);
                }
            });
            return balanceInfoMap;
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw new BTCTTSException(e.getMessage(), e);
        }
    }


    public List<OpenOrder> getOpenOrders(String targetCode, String cashCurrency, PlatformConfigModel configModel) {
        Map<String, String> args = Maps.newHashMap();
        args.put("command", "returnOpenOrders");
        args.put("currencyPair", poloniexMarketUtils.buildCurrencyPair(targetCode, cashCurrency));
        String returnOpenOrders = executeCommand("returnOpenOrders", args, configModel);
        List<OpenOrder> openOrderList = JsonUtil.jsonToObjectList(returnOpenOrders, OpenOrder.class);
        return openOrderList;
    }

    public String getTradeHistory(String targetCode, String cashCurrency, PlatformConfigModel configModel) {
        Map<String, String> args = Maps.newHashMap();
        args.put("command", "returnTradeHistory");
        args.put("currencyPair", poloniexMarketUtils.buildCurrencyPair(targetCode, cashCurrency));
        return executeCommand("returnTradeHistory", args, configModel);
    }


}
