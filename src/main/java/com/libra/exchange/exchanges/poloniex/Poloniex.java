package com.libra.exchange.exchanges.poloniex;


import com.libra.common.exceptions.BTCTTSException;
import com.libra.exchange.strategy.model.PlatformConfigModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.io.*;
import java.math.BigInteger;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

@Service
public class Poloniex {
    private static final String API_URL = "https://poloniex.com/tradingApi";
    private static final String USER_AGENT = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36";

    private static long auth_last_request = 0;
    private static long auth_request_limit = 1000;    // request limit in milliseconds
    private boolean initialized = false;
    private Mac mac;


    private Logger logger = LoggerFactory.getLogger(Poloniex.class);

    public void setAuthKeys(PlatformConfigModel configModel) {
        SecretKeySpec keyspec = null;
        try {
            keyspec = new SecretKeySpec(configModel.getApiSecret().getBytes("UTF-8"), "HmacSHA512");
        } catch (UnsupportedEncodingException uee) {
            throw new BTCTTSException(uee.getMessage(), uee);
        }

        try {
            mac = Mac.getInstance("HmacSHA512");
        } catch (NoSuchAlgorithmException nsae) {
            throw new BTCTTSException(nsae.getMessage(), nsae);
        }

        try {
            mac.init(keyspec);
        } catch (InvalidKeyException ike) {
            throw new BTCTTSException(ike.getMessage(), ike);
        }
        initialized = true;
    }


    private final void preAuth() {
        long elapsed = System.currentTimeMillis() - auth_last_request;
        if (elapsed < auth_request_limit) {
            try {
                Thread.currentThread().sleep(auth_request_limit - elapsed);
            } catch (InterruptedException e) {
                throw new BTCTTSException(e.getMessage(), e);
            }
        }
        auth_last_request = System.currentTimeMillis();
    }

    private String toHex(byte[] b) throws UnsupportedEncodingException {
        return String.format("%040x", new BigInteger(1, b));
    }

    public String authrequest(String method, Map<String, String> args, PlatformConfigModel configModel) {
        setAuthKeys(configModel);
        if (!initialized) {
            throw new BTCTTSException("Poloniex connection not initialized.");
        }

        // prep the call
        preAuth();

        // add method and nonce to args
        if (args == null) {
            args = new HashMap<String, String>();
        }
        long nonce = System.currentTimeMillis();
        args.put("method", method);
        args.put("nonce", Long.toString(nonce));


        // create url form encoded post data
        String postData = "";
        for (Iterator<String> iter = args.keySet().iterator(); iter.hasNext(); ) {
            String arg = iter.next();
            if (postData.length() > 0) {
                postData += "&";
            }
            postData += arg + "=" + URLEncoder.encode(args.get(arg));
        }

        // create connection
        URLConnection conn;
        StringBuffer response = new StringBuffer();
        try {
            URL url = new URL(API_URL);
            conn = url.openConnection();
            conn.setUseCaches(false);
            conn.setDoOutput(true);
            conn.setRequestProperty("Key", configModel.getApiKey());
            conn.setRequestProperty("Sign", toHex(mac.doFinal(postData.getBytes("UTF-8"))));
            conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            conn.setRequestProperty("User-Agent", USER_AGENT);

            OutputStreamWriter out = new OutputStreamWriter(conn.getOutputStream());
            out.write(postData);
            out.close();

            BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String line = null;
            while ((line = in.readLine()) != null)
                response.append(line);
            in.close();
        } catch (MalformedURLException e) {
            throw new BTCTTSException("Internal error.", e);
        } catch (IOException e) {
            throw new BTCTTSException(e.getMessage(), e);
        }
        return response.toString();
    }


    public static void main(String args[]) {
        //Config.loadUserDefined();
        //Poloniex polo = new Poloniex();
        //polo.buy(0.00052, 0.2);
        //polo.getOpenOrders();
        //polo.getBalance();
        //System.out.println(Config.poloniexKey);
        //System.out.println(Config.poloniexSecret);
        //polo.cancelOrder("13589199");

        Poloniex poloniex = new Poloniex();


    }
}