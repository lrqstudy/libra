package com.libra.exchange.exchanges.poloniex;

public class Config {
    public static Integer majorVersion = 2;
    public static Integer minorVersion = 9;
    public static String version = Integer.toString(majorVersion) + "." + Integer.toString(minorVersion);
}