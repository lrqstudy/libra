package com.libra.exchange.exchanges.poloniex;

import com.google.common.base.Joiner;
import com.libra.common.consts.CurrencyType;
import com.libra.exchange.exchanges.common.CommonMarketUtils;
import com.libra.exchange.exchanges.common.CommonOrderBook;
import com.libra.exchange.exchanges.common.CommonOrderVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.List;

/**
 * Created by rongqiang.li on 17/4/11.
 */
@Component
public class PoloniexMarketUtils {

    public static final String returnTicker = "https://poloniex.com/public?command=returnTicker";
    public static final String return24Volume = "https://poloniex.com/public?command=return24hVolume";
    public static final String returnOrderBook = "https://poloniex.com/public?command=returnOrderBook";
    public static final String returnTradeHistory = "https://poloniex.com/public?command=returnTradeHistory&currencyPair=BTC_NXT&start=1410158341&end=1410499372";
    public static final String returnChartData = "https://poloniex.com/public?command=returnChartData&currencyPair=BTC_XMR&start=1405699200&end=9999999999&period=14400";
    public static final String returnCurrencies = "https://poloniex.com/public?command=returnCurrencies";
    @Resource
    private RestTemplate restTemplate;
    @Resource
    private CommonMarketUtils commonMarketUtils;

    private Logger logger = LoggerFactory.getLogger(PoloniexExchangeServiceImpl.class);


    public String buildCurrencyPair(String targetCode, String cashCurrency) {
        return Joiner.on("_").join(cashCurrency, targetCode).toUpperCase();
    }

    public BigDecimal getWeightedPrice(List<CommonOrderVO> voList) {
        BigDecimal totalPrice = BigDecimal.ZERO;
        BigDecimal totalVolum = BigDecimal.ZERO;
        for (CommonOrderVO orderVO : voList) {
            totalPrice = totalPrice.add(orderVO.getPrice().multiply(orderVO.getVolume()));
            totalVolum = totalVolum.add(orderVO.getVolume());
        }
        BigDecimal weightedPrice = totalPrice.divide(totalVolum, 12, BigDecimal.ROUND_HALF_EVEN);
        return weightedPrice;
    }



    public CommonOrderBook returnOrderBook(String currencyPair, int depth) {
        StringBuilder sb = new StringBuilder();
        sb.append(returnOrderBook).append("&").append("currencyPair=").append(currencyPair.toUpperCase()).append("&depth=").append(depth);
        OrderBookJsonVO orderBookJsonVO = restTemplate.getForObject(sb.toString(), OrderBookJsonVO.class);
        logger.info(" poloniex currencyPair ={} depth={},result={} ", currencyPair, depth, orderBookJsonVO.toString());
        Object[][] asks = orderBookJsonVO.getAsks();
        Object[][] bids = orderBookJsonVO.getBids();
        CommonOrderBook orderBook = new CommonOrderBook();
        List<CommonOrderVO> buyOrderVOList = commonMarketUtils.buildOrderVOListFromArray(bids, CurrencyType.BTC.getCode());
        orderBook.setBuyOrderVOList(buyOrderVOList);
        List<CommonOrderVO> sellOrderVOList = commonMarketUtils.buildOrderVOListFromArray(asks, CurrencyType.BTC.getCode());
        orderBook.setSellOrderVOList(sellOrderVOList);
        return orderBook;
    }
}
