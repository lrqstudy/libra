package com.libra.exchange.exchanges.poloniex.task;

import java.io.Serializable;

/**
 * Created by rongqiang.li on 2017/5/1.
 */
public class BitStampBTCTicker implements Serializable{

    //{"high": "1362.50", "last": "1360.36", "timestamp": "1493609837", "bid": "1360.37",
    // "vwap": "1337.90", "volume": "4122.58455582", "low": "1315.68", "ask": "1361.57", "open": "1348.88"}
    private String high;
    private String last;
    private String timestamp;
    private String vwap;
    private String volume;
    private String low;
    private String ask;
    private String open;

    public String getHigh() {
        return high;
    }

    public void setHigh(String high) {
        this.high = high;
    }

    public String getLast() {
        return last;
    }

    public void setLast(String last) {
        this.last = last;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getVwap() {
        return vwap;
    }

    public void setVwap(String vwap) {
        this.vwap = vwap;
    }

    public String getVolume() {
        return volume;
    }

    public void setVolume(String volume) {
        this.volume = volume;
    }

    public String getLow() {
        return low;
    }

    public void setLow(String low) {
        this.low = low;
    }

    public String getAsk() {
        return ask;
    }

    public void setAsk(String ask) {
        this.ask = ask;
    }

    public String getOpen() {
        return open;
    }

    public void setOpen(String open) {
        this.open = open;
    }
}
