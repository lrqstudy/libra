package com.libra.exchange.exchanges.poloniex;

import com.google.api.client.util.Lists;
import com.google.common.base.Joiner;
import com.libra.exchange.exchanges.common.*;
import com.libra.exchange.strategy.model.PlatformConfigModel;
import org.knowm.xchange.Exchange;
import org.knowm.xchange.ExchangeFactory;
import org.knowm.xchange.currency.CurrencyPair;
import org.knowm.xchange.dto.marketdata.Ticker;
import org.knowm.xchange.service.marketdata.MarketDataService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * Created by rongqiang.li on 17/5/11.
 */
@Service
public class PoloniexTransferServiceImpl implements CommonTransferService {
    @Resource
    private PoloniexExchangeServiceImpl poloniexExchangeServiceImpl;
    @Resource
    private PoloniexMarketUtils poloniexMarketUtils;
    private Logger logger = LoggerFactory.getLogger(PoloniexExchangeServiceImpl.class);

    @Override
    public CommonOrderBook getOrderBook(String targetCode, String cashCurrency, int depth) {
        String currencyPair = getCurrencyPair(targetCode, cashCurrency);
        return poloniexMarketUtils.returnOrderBook(currencyPair, depth);
    }

    @Override
    public Map<String, BigDecimal> getBalanceMap(PlatformConfigModel configModel) {
        return poloniexExchangeServiceImpl.getAccountBalance(configModel);
    }

    @Override
    public String getCurrencyPair(String targetCode, String cashCurrency) {
        return Joiner.on("_").join(cashCurrency.toUpperCase(), targetCode.toUpperCase());
    }



    @Override
    public String buy(String targetCode, String cashCurrency, BigDecimal price, BigDecimal amount, Integer precisionSize, PlatformConfigModel configModel,Integer orderSource) {
        amount = amount.multiply(new BigDecimal(1.002));//把手续费加上
        amount = amount.setScale(precisionSize, BigDecimal.ROUND_FLOOR);
        return poloniexExchangeServiceImpl.buy(targetCode, cashCurrency, price, amount, configModel,precisionSize,orderSource);
    }

    @Override
    public String sell(String targetCode, String cashCurrency, BigDecimal price, BigDecimal amount, Integer precisionSize, PlatformConfigModel configModel,Integer orderSource) {
        amount = amount.setScale(precisionSize, BigDecimal.ROUND_FLOOR);
        return poloniexExchangeServiceImpl.sell(targetCode, cashCurrency, price, amount, configModel,precisionSize,orderSource);
    }



    @Override
    public String withdraw(String targetCode, String address, String memo, BigDecimal amount, PlatformConfigModel configModel) {
        return poloniexExchangeServiceImpl.withdraw(targetCode, address, memo, amount, configModel);
    }

    @Override
    public List<CommonOpenOrderVO> getOpenOrderList(PlatformConfigModel platformConfigModel, String targetCode, String cashCurrency) {
        List<OpenOrder> openOrders = poloniexExchangeServiceImpl.getOpenOrders(targetCode, cashCurrency, platformConfigModel);
        String symbol = getCurrencyPair(targetCode, cashCurrency);

        List<CommonOpenOrderVO> commonOpenOrderVOList = Lists.newArrayList();
        for (OpenOrder openOrder : openOrders) {
            CommonOpenOrderVO commonOpenOrderVO = new CommonOpenOrderVO();
            commonOpenOrderVO.setOrderId(openOrder.getOrderNumber());
            commonOpenOrderVO.setOriginAmount(new BigDecimal(openOrder.getStartingAmount()));
            commonOpenOrderVO.setPrice(new BigDecimal(openOrder.getRate()));
            commonOpenOrderVO.setRemainAmount(new BigDecimal(openOrder.getAmount()));
            commonOpenOrderVO.setSide(openOrder.getType());
            commonOpenOrderVO.setStatus("");
            commonOpenOrderVO.setSymbol(symbol);
            commonOpenOrderVO.setType(openOrder.getMargin());
            commonOpenOrderVOList.add(commonOpenOrderVO);
        }

        return commonOpenOrderVOList;
    }

    @Override
    public CommonTickerVO getTickerBySymbol(String targetCode, String cashCurrency) {
        try {
            CurrencyPair currencyPair=  new CurrencyPair(targetCode,cashCurrency);
            Exchange bitfinex = ExchangeFactory.INSTANCE.createExchange(org.knowm.xchange.poloniex.Poloniex.class.getName());
            MarketDataService marketDataService = bitfinex.getMarketDataService();
            Ticker ticker = marketDataService.getTicker(currencyPair);
            CommonTickerVO commonTickerVO = new CommonTickerVO();
            BeanUtils.copyProperties(ticker, commonTickerVO);
            return commonTickerVO;
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return null;
        }
    }
}
