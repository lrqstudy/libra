package com.libra.common.exceptions;

/**
 * Created by rongqiang.li on 17/1/26.
 */
public class BTCTTSException extends RuntimeException {
    public BTCTTSException() {
    }

    public BTCTTSException(String message) {
        super(message);
    }

    public BTCTTSException(String message, Throwable cause) {
        super(message, cause);
    }

    public BTCTTSException(Throwable cause) {
        super(cause);
    }

    public BTCTTSException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
