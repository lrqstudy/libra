package com.libra.common.consts;

import com.google.common.collect.Maps;

import java.util.Map;

/**
 * Created by rongqiang.li on 18/1/2.
 */
public enum TransferStopReason {

    //get balance error
    // open order over limit
    // cash not enough
    // target_code not enough


    GET_BALANCE_ERROR(1, "get_balance_error", "GET_BALANCE_ERROR"),
    OPEN_ORDER_OVER_LIMIT(2, "open_order_over_limit", "OPEN_ORDER_OVER_LIMIT"),
    CASH_NOT_ENOUGH(3, "cash_not_enough", "CASH_NOT_ENOUGH"),
    TARGET_CODE_NOT_ENOUGH(4, "target_code_not_enough", "TARGET_CODE_NOT_ENOUGH"),;

    /**
     * id
     */
    private int id;
    /**
     * 编码
     */
    private String code;
    /**
     * 描述
     */
    private String desc;

    private static final Map<Integer, TransferStopReason> map = Maps.newHashMap();

    static {
        for (TransferStopReason e : values()) {
            map.put(e.getId(), e);
        }
    }

    public int code() {
        return this.getId();
    }

    public static TransferStopReason codeOf(int id) {
        return map.get(id);
    }


    TransferStopReason(int id, String code, String desc) {
        this.id = id;
        this.code = code;
        this.desc = desc;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
