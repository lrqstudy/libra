package com.libra.common.consts;

import com.google.common.collect.Maps;

import java.util.Map;

/**
 * Created by rongqiang.li on 17/11/24.
 */
public enum OrderSide {
    BUY(1, "buy", "buy"),
    SELL(2, "sell", "sell"),;

    /**
     * id
     */
    private int id;
    /**
     * 编码
     */
    private String code;
    /**
     * 描述
     */
    private String desc;

    private static final Map<Integer, OrderSide> map = Maps.newHashMap();

    static {
        for (OrderSide e : values()) {
            map.put(e.getId(), e);
        }
    }

    public int code() {
        return this.getId();
    }

    public static OrderSide codeOf(int id) {
        return map.get(id);
    }


    OrderSide(int id, String code, String desc) {
        this.id = id;
        this.code = code;
        this.desc = desc;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
    }
