package com.libra.common.consts;

import com.google.common.collect.Maps;

import java.util.Map;

/**
 * Created by rongqiang.li on 17/11/24.
 */
public enum ProcessFlag {
    NO(0, "未处理", "未处理"),
    YES(1, "已处理", "已处理"),
    NO_NEED(2, "不需要处理", "不需要处理"),;
    /**
     * id
     */
    private int id;
    /**
     * 编码
     */
    private String code;
    /**
     * 描述
     */
    private String desc;

    private static final Map<Integer, ProcessFlag> map = Maps.newHashMap();

    static {
        for (ProcessFlag e : values()) {
            map.put(e.getId(), e);
        }
    }

    public int code() {
        return this.getId();
    }

    public static ProcessFlag codeOf(int id) {
        return map.get(id);
    }


    ProcessFlag(int id, String code, String desc) {
        this.id = id;
        this.code = code;
        this.desc = desc;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
