package com.libra.common.consts;

/**
 * Created by rongqiang.li on 17/5/12.
 */
public enum CurrencyType {

    BTC(1, "BTC", "BTC"),
    CNY(2, "CNY", "CNY"),
    USD(3, "USD", "USD"),
    GNT(4, "GNT", "GNT"),
    ETH(5, "ETH", "ETH"),
    ZEC(6, "ZEC", "ZEC"),
    BTS(7, "BTS", "BTS"),
    REP(8, "REP", "REP"),
    SC(9, "SC", "SC"),;

    private int id;
    private String code;

    private String name;

    CurrencyType(int id, String code, String name) {
        this.id = id;
        this.code = code;
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
