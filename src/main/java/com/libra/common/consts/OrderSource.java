package com.libra.common.consts;

import com.google.common.collect.Maps;

import java.util.Map;

/**
 * Created by rongqiang.li on 17/11/24.
 */
public enum OrderSource {
    TRANSFER(1, "搬砖", "搬砖"),
    LOW_BUY_HIGH_SELL(2, "低买高卖", "低买高卖"),
    MAKE_FAILED_ORDER(3, "补单", "补单"),;
    /**
     * id
     */
    private int id;
    /**
     * 编码
     */
    private String code;
    /**
     * 描述
     */
    private String desc;

    private static final Map<Integer, OrderSource> map = Maps.newHashMap();

    static {
        for (OrderSource e : values()) {
            map.put(e.getId(), e);
        }
    }

    public int code() {
        return this.getId();
    }

    public static OrderSource codeOf(int id) {
        return map.get(id);
    }


    OrderSource(int id, String code, String desc) {
        this.id = id;
        this.code = code;
        this.desc = desc;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
