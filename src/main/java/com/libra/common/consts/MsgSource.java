package com.libra.common.consts;

import com.google.common.collect.Maps;

import java.util.Map;

/**
 * Created by rongqiang.li on 17/11/26.
 */
public enum MsgSource {
    AUTO_OFF_LINE(1, "自动下线", "自动下线"),
    MAKE_ORDER_FAILED(2, "自动补单失败", "自动补单失败"),
    LIBRA_TRANSFER_DAILY_REPORT(3, "搬砖每日结算", "搬砖每日结算"),
    AUTO_ON_LINE(4, "自动上线", "自动上线"),
    ;

    /**
     * id
     */
    private int id;
    /**
     * 编码
     */
    private String code;
    /**
     * 描述
     */
    private String desc;

    private static final Map<Integer, MsgSource> map = Maps.newHashMap();

    static {
        for (MsgSource e : values()) {
            map.put(e.getId(), e);
        }
    }

    public int code() {
        return this.getId();
    }

    public static MsgSource codeOf(int id) {
        return map.get(id);
    }


    MsgSource(int id, String code, String desc) {
        this.id = id;
        this.code = code;
        this.desc = desc;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
