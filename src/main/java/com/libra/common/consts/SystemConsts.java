package com.libra.common.consts;

import java.math.BigDecimal;

/**
 * Created by rongqiang.li on 2017/2/12.
 */
public class SystemConsts {
    public static final String LOGIN_USER_KEY="loginUser";
    public static final BigDecimal INIT_CNY_VALUE=new BigDecimal(100000.000000);
    public static final BigDecimal ZERO=new BigDecimal(0.000000);
    public static final int COIN_MARKET_CAP_GET_SIZE=50;
    public static final int EXCEPTION_ERROR_MESSAGE_SIZE=400;
}
