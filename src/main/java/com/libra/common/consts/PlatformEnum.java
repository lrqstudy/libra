package com.libra.common.consts;

/**
 * Created by rongqiang.li on 17/5/12.
 */
public enum PlatformEnum {
    YUNBI(1,"yunbi","yunbi"),
    POLONIEX(2,"poloniex","poloniex"),
    COINCHECK(3,"coincheck","coincheck"),
    BITFINEX(4,"bitfinex","bitfinex"),
    BINANCE(5,"binance","binance"),
    BITTREX(6,"bittrex","bittrex"),
    QUOINEX(7,"quoinex","quoinex"),
    BIGONE(8,"bigone","bigone"),
    HUOBI(9,"huobi","huobi"),
    BIBOX(10,"bibox","bibox"),
    ;

    private  int id;
    private  String code;

    private  String name;

    PlatformEnum(int id, String code, String name) {
        this.id = id;
        this.code = code;
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
