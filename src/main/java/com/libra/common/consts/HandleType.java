package com.libra.common.consts;

import com.google.common.collect.Maps;

import java.util.Map;

/**
 * Created by rongqiang.li on 17/11/24.
 */
public enum  HandleType {
    NO(0,"未处理","未处理"),
    MANUAL(1,"人工","人工"),
    SYSTEM(2,"系统","系统"),
            ;
    /**
     * id
     */
    private int id;
    /**
     * 编码
     */
    private String code;
    /**
     * 描述
     */
    private String desc;

    private static final Map<Integer, HandleType> map = Maps.newHashMap();

    static {
        for (HandleType e : values()) {
            map.put(e.getId(), e);
        }
    }

    public int code() {
        return this.getId();
    }

    public static HandleType codeOf(int id) {
        return map.get(id);
    }


    HandleType(int id, String code, String desc) {
        this.id = id;
        this.code = code;
        this.desc = desc;
    }



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

}
