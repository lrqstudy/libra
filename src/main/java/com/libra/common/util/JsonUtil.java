package com.libra.common.util;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.JavaType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * Created by rongqiang.li on 15-4-21.
 */
public class JsonUtil {

    private static final Logger log = LoggerFactory.getLogger(JsonUtil.class);
    private static ObjectMapper objectMapper = new ObjectMapper();

    /**
     * json字符串解析成java对象
     *
     * @param <T>
     * @param jsonStr
     * @return
     */
    public static <T> T jsonToObject(String jsonStr, Class<T> clazz) {
        try {
            return objectMapper.readValue(jsonStr, clazz);
        } catch (Exception e) {
            log.error("json解析异常,参数：{}", jsonStr, e);
            return null;
        }
    }

    /**
     * json字符串解析成java对象列表
     *
     * @param <T>
     * @param jsonStr
     * @return
     */
    public static <T> List<T> jsonToObjectList(String jsonStr, Class<T> clazz) {
        JavaType javaType = objectMapper.getTypeFactory().constructParametricType(List.class, clazz);
        try {
            return objectMapper.readValue(jsonStr, javaType);
        } catch (Exception e) {
            log.error("json解析异常,参数：{}", jsonStr, e);
            return null;
        }
    }

    /**
     * java对象转json
     *
     * @param <T>
     * @param t
     * @return
     */
    public static <T> String objectToJson(T t) {
        try {
            return objectMapper.writeValueAsString(t);
        } catch (Exception e) {
            log.error("对象转json异常,参数：{}", t, e);
            return null;
        }
    }

    /**
     * java对象列表转json
     *
     * @param <T>
     * @param
     * @return
     */
    public static <T> String objectListToJson(List<T> list) {
        try {
            return objectMapper.writeValueAsString(list);
        } catch (Exception e) {
            log.error("对象列表转json异常 {}", e.getMessage(), e);
            return null;
        }
    }

    public static Map<?, ?> jsonToMap(String jsonStr) {
        try {
            return objectMapper.readValue(jsonStr, Map.class);
        } catch (IOException e) {
            log.error("对象列表转json异常 {}", e.getMessage(), e);
            return null;
        }
    }
}
