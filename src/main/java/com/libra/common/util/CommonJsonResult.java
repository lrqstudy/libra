package com.libra.common.util;

import com.google.common.collect.Maps;

import java.util.Map;

/**
 *
 */
public class CommonJsonResult {
    private Map<String, Object> resultMap = Maps.newHashMap();
    public static final String ERR_CODE_NAME = "errcode";
    public static final String ERR_MSG_NAME = "errmsg";
    public static final String DATA_NAME = "data";
    public static final String RET_NAME = "ret";

    public static CommonJsonResult create() {
        return new CommonJsonResult();
    }

    public static CommonJsonResult create(int version) {
        return new CommonJsonResult(version);
    }

    public CommonJsonResult() {
    }

    public CommonJsonResult(int version) {
        resultMap.put("ver", version);
    }

    public CommonJsonResult ret(boolean ret) {
        resultMap.put(RET_NAME, ret);
        return this;
    }

    public CommonJsonResult errCode(int errcode) {
        resultMap.put(ERR_CODE_NAME, errcode);
        return this;
    }

    public CommonJsonResult errMsg(String errmsg) {
        resultMap.put(ERR_MSG_NAME, errmsg);
        return this;
    }

    public CommonJsonResult data(Object data) {
        resultMap.put(DATA_NAME, data);
        return this;
    }

    /**
     * 为了兼容以前的json接口（以前有version字段）
     *
     * @param version
     * @return
     */
    public CommonJsonResult version(int version) {
        resultMap.put("ver", version);
        return this;
    }

    public Map<String, Object> build() {
        if (hasError()) {
            return buildError();
        } else {
            return buildSuccess();
        }
    }

    public Map<String, Object> buildSuccess() {
        resultMap.put(RET_NAME, true);
        return resultMap;
    }

    public Map<String, Object> buildError() {
        resultMap.put(RET_NAME, false);
        return resultMap;
    }

    private boolean hasError() {
        return resultMap.get(ERR_CODE_NAME) != null || resultMap.get(ERR_MSG_NAME) != null;
    }

}
