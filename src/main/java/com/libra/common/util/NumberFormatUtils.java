package com.libra.common.util;

import java.math.BigDecimal;
import java.text.NumberFormat;

/**
 * Created by rongqiang.li on 2017/3/2.
 */
public class NumberFormatUtils {

    public static String getPercentageValue(BigDecimal roaValue,BigDecimal netAmount){
        if (netAmount.compareTo(BigDecimal.ZERO)==0){
            return "0.00%";
        }
        NumberFormat nf = NumberFormat.getPercentInstance();
        nf.setMinimumFractionDigits(2);//
        BigDecimal roa = roaValue.divide(netAmount, 6, BigDecimal.ROUND_HALF_EVEN);
        String roaStr = nf.format(roa.doubleValue());
        return roaStr;
    }


}
