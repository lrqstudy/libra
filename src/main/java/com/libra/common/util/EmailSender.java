package com.libra.common.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.util.List;

@Service
public class EmailSender {

    private static final Logger logger = LoggerFactory.getLogger(EmailSender.class);

    @Resource
    private JavaMailSender mailSender;


    /**
     * 同步发送邮件。
     *
     * @param from    发件人。
     * @param to      收件人列表。
     * @param subject 主题。
     * @param body    邮件内容。
     */
    public void sendMail(String from, List<String> to, String subject, String body) {
        MimeMessage mimeMessage = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(mimeMessage);
        try {
            helper.setFrom(from);
            String[] toList = new String[to.size()];
            helper.setTo(to.toArray(toList));
            mimeMessage.setSubject(subject, "UTF-8");
            mimeMessage.setContent(body, "text/html; charset=\"UTF-8\"");
            mimeMessage.setHeader("Content-Type", "text/html; charset=\"UTF-8\"");
            mailSender.send(mimeMessage);
        } catch (MessagingException e) {
            logger.error("邮件发送错误!", e);
        }
    }

}
