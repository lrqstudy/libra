package com.libra.common.util.message;

/**
 * Created by rongqiang.li on 17/11/25.
 */
public class SmsMessageModel {
    private String extend;
    private String smsType;
    private String smsFreeSignName;
    private String recNum;
    private String smsTemplateCode;
    private String smsParamString;

    public String getExtend() {
        return extend;
    }

    public void setExtend(String extend) {
        this.extend = extend;
    }

    public String getSmsType() {
        return smsType;
    }

    public void setSmsType(String smsType) {
        this.smsType = smsType;
    }

    public String getSmsFreeSignName() {
        return smsFreeSignName;
    }

    public void setSmsFreeSignName(String smsFreeSignName) {
        this.smsFreeSignName = smsFreeSignName;
    }

    public String getRecNum() {
        return recNum;
    }

    public void setRecNum(String recNum) {
        this.recNum = recNum;
    }

    public String getSmsTemplateCode() {
        return smsTemplateCode;
    }

    public void setSmsTemplateCode(String smsTemplateCode) {
        this.smsTemplateCode = smsTemplateCode;
    }

    public String getSmsParamString() {
        return smsParamString;
    }

    public void setSmsParamString(String smsParamString) {
        this.smsParamString = smsParamString;
    }


    //req.setExtend("123456");
    //req.setSmsType("normal");
    //req.setSmsFreeSignName("荣强搬砖");


//    req.setSmsParamString("{\"name\":\"test\",\"configId\":\"10086\"}");
    //  req.setRecNum("18612082469");
    //req.setSmsTemplateCode("SMS_113825007");


}
