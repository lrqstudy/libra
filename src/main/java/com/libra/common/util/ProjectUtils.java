package com.libra.common.util;


import com.libra.common.consts.SystemConsts;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * Created by rongqiang.li on 2017/2/15.
 */
public class ProjectUtils {


    public static String getCurrentUserCode() {
        RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
        if (requestAttributes == null) {
            return null;
        }

        if (!(requestAttributes instanceof ServletRequestAttributes)){
            return null;
        }
        HttpServletRequest request = ((ServletRequestAttributes) requestAttributes).getRequest();
        if (request==null){
            return null;
        }
        HttpSession session = request.getSession(false);
        if (session==null){
            return null;
        }
        Object userCode = session.getAttribute(SystemConsts.LOGIN_USER_KEY);
        if (userCode == null) {
            return null;
        }
        return (String) userCode;
    }


}
