package com.libra.common.util;


import com.google.common.collect.Maps;
import com.libra.common.exceptions.BTCTTSException;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.ReflectionUtils;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.util.Map;

/**
 * Created by xiaoxuan.jin on 2014/7/1.
 */
public class BeanUtilExt {

    private static Logger logger = LoggerFactory.getLogger(BeanUtilExt.class);

    /**
     * if the one of the source object isEmpty ,then not combining
     *
     * @param dst
     * @param src
     */
    public static void combineData(Object dst, Object src) {
        Field[] fields = src.getClass().getDeclaredFields();
        try {
            Map<String, Object> mprop = Maps.newHashMap();
            for (Field field : fields) {
                field.setAccessible(true);
                Object srvProValue = field.get(src);
                if (srvProValue == null) {
                    continue;
                } else if (srvProValue instanceof String && StringUtils.isEmpty((String) srvProValue)) {
                    continue;
                } else {
                    mprop.put(field.getName(), srvProValue);
                }
            }
            BeanUtils.populate(dst, mprop);
        } catch (IllegalAccessException e) {
            throw new BTCTTSException(e.getMessage(), e);
        } catch (Exception e) {
            throw new BTCTTSException(e.getMessage(), e);
        }

    }

    /**
     * 填充空值
     *
     * @param input
     */
    public static <T> T fillNullProperty(T input) {
        Method[] allDeclaredMethods = ReflectionUtils.getAllDeclaredMethods(input.getClass());
        Map<String, Method> methodMap = Maps.newHashMap();
        for (Method method : allDeclaredMethods) {
            methodMap.put(method.getName(), method);
        }
        try {
            for (Method method : allDeclaredMethods) {
                String methodName = method.getName();
                if (method.getName().startsWith("get") || method.getName().startsWith("is")) {
                    Object srvProValue = method.invoke(input, null);
                    String typeName = method.getReturnType().getSimpleName();
                    if (srvProValue == null) {
                        String setMethodName = "set" + methodName.substring(3, methodName.length());
                        Method setMethod = methodMap.get(setMethodName);
                        if (typeName.equalsIgnoreCase("string")) {
                            setMethod.invoke(input, "");
                        } else if (typeName.equalsIgnoreCase("integer")) {
                            setMethod.invoke(input, 0);
                        } else if (typeName.equalsIgnoreCase("long")) {
                            setMethod.invoke(input, 0L);
                        } else if (typeName.equalsIgnoreCase("bigDecimal")) {
                            setMethod.invoke(input, BigDecimal.ZERO);
                        } else if (typeName.equalsIgnoreCase("date")) {
                            setMethod.invoke(input, DateUtil.parse("1970-01-01", "yyyy-MM-dd"));
                        } else {
                            continue;
                        }
                    }
                }
            }
        } catch (Exception e) {
            logger.error("fillNullProperty error : {} ", e.getMessage(), e);
        }
        return input;
    }

    public static  <T> void  fillNullProperty(Iterable<T> iterable) {
        if (iterable == null) {
            return;
        }
        for (T t : iterable) {
            fillNullProperty(t);
        }
    }
}
