package com.libra.common.util;

import com.libra.common.util.message.SmsMessageModel;
import com.taobao.api.DefaultTaobaoClient;
import com.taobao.api.TaobaoClient;
import com.taobao.api.request.AlibabaAliqinFcSmsNumSendRequest;
import com.taobao.api.response.AlibabaAliqinFcSmsNumSendResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by rongqiang.li on 17/11/25.
 */

public class SendSmsMessageUtil {
    private static Logger logger = LoggerFactory.getLogger(SendSmsMessageUtil.class);
    private static TaobaoClient taobaoClient;

    static {
        try {
            taobaoClient = new DefaultTaobaoClient(GlobalConfig.getTaobaoGateWayUrl(), GlobalConfig.getApiKey(), GlobalConfig.getApiSecret());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw new ExceptionInInitializerError(e);
        }
    }


    public static String send(SmsMessageModel messageModel) {
        try {
            AlibabaAliqinFcSmsNumSendRequest req = new AlibabaAliqinFcSmsNumSendRequest();
            req.setExtend(messageModel.getExtend());
            req.setSmsType(messageModel.getSmsType());
            req.setSmsFreeSignName(messageModel.getSmsFreeSignName());
            req.setSmsParamString(messageModel.getSmsParamString());
            req.setRecNum(messageModel.getRecNum());//
            req.setSmsTemplateCode(messageModel.getSmsTemplateCode());

            AlibabaAliqinFcSmsNumSendResponse rsp = taobaoClient.execute(req);
            String result = rsp.getResult().getMsg();
            logger.info(" send success,result={} ", result);
            return result;
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return "error";
        }
    }


    public static void main(String[] args) throws Exception {
        //SMS_113350037

        //String host = "http://sms.market.alicloudapi.com";
        //http://sms.market.alicloudapi.com/singleSendSms
        //http://gw.api.taobao.com/router/rest
        //alibaba.aliqin.fc.sms.num.send
        String path = "/singleSendSms";
        TaobaoClient client = new DefaultTaobaoClient("http://gw.api.taobao.com/router/rest", "23641897", "956c3d2f0561d4e9533205317c5738fe");

        AlibabaAliqinFcSmsNumSendRequest req = new AlibabaAliqinFcSmsNumSendRequest();
        req.setExtend("123456");
        req.setSmsType("normal");
        req.setSmsFreeSignName("荣强搬砖");


        req.setSmsParamString("{\"name\":\"test\",\"configId\":\"10086\"}");
        req.setRecNum("18612082469");
        req.setSmsTemplateCode("SMS_113825007");
        AlibabaAliqinFcSmsNumSendResponse rsp = client.execute(req);
        System.out.println(rsp.getBody());


    }


}
