package com.libra.common.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Created by rongqiang.li on 2017/5/1.
 */
public class GlobalConfig {

    private static Logger logger = LoggerFactory.getLogger(GlobalConfig.class);

    static {
        init();
    }

    private static void init() {
        try {
            InputStream resourceAsStream = GlobalConfig.class.getClassLoader().getResourceAsStream("taobao_msg.properties");
            Properties properties = new Properties();
            properties.load(resourceAsStream);

            TAOBAO_GATE_WAY_URL = properties.getProperty(TAOBAO_GATE_WAY_URL_KEY_NAME);
            API_KEY = properties.getProperty(API_KEY_KEY_NAME);
            API_SECRET = properties.getProperty(API_SECRET_KEY_NAME);


            SMS_TYPE_OF_NORMAL = properties.getProperty(SMS_TYPE_OF_NORMAL_KEY_NAME);


            SMS_TEMPLATE_CODE_OF_OFF_LINE_NOTICE = properties.getProperty(SMS_TEMPLATE_CODE_OF_OFF_LINE_NOTICE_KEY_NAME);
            SMS_OFF_LINE_TEMPLATE_VARIABLE_OF_NAME = properties.getProperty(SMS_OFF_LINE_TEMPLATE_VARIABLE_OF_KEY_NAME);
            SMS_OFF_LINE_TEMPLATE_VARIABLE_OF_CONFIGID = properties.getProperty(SMS_OFF_LINE_TEMPLATE_VARIABLE_OF_CONFIGID_KEY_NAME);
            SMS_OFF_LINE_TEMPLATE_VARIABLE_OF_REASON = properties.getProperty(SMS_OFF_LINE_TEMPLATE_VARIABLE_OF_REASON_KEY_NAME);


            SMS_TEMPLATE_CODE_OF_ORDER_FAILED_NOTICE = properties.getProperty(SMS_TEMPLATE_CODE_OF_ORDER_FAILED_NOTICE_KEY_NAME);
            SMS_ORDER_FAILED_TEMPLATE_VARIABLE_OF_NAME = properties.getProperty(SMS_ORDER_FAILED_TEMPLATE_VARIABLE_OF_CONFIGID_KEY_NAME);
            SMS_ORDER_FAILED_TEMPLATE_VARIABLE_OF_ORDERID = properties.getProperty(SMS_ORDER_FAILED_TEMPLATE_VARIABLE_OF_NAME_KEY_NAME);

            SMS_TEMPLATE_CODE_OF_TRANSFER_DAILY_REPORT = properties.getProperty(SMS_TEMPLATE_CODE_OF_TRANSFER_DAILY_REPORT_NOTICE_KEY_NAME);
            SMS_TRANSFER_DAILY_REPORT_TEMPLATE_VARIABLE_OF_NAME = properties.getProperty(SMS_TRANSFER_DAILY_REPORT_TEMPLATE_VARIABLE_OF_CONFIGID_KEY_NAME);
            SMS_TRANSFER_DAILY_REPORT_TEMPLATE_VARIABLE_OF_CONFIGID = properties.getProperty(SMS_TRANSFER_DAILY_REPORT_TEMPLATE_VARIABLE_OF_NAME_KEY_NAME);


        } catch (IOException e) {
            logger.error(e.getMessage(), e);
            throw new ExceptionInInitializerError(e);
        }
    }

    private static String TAOBAO_GATE_WAY_URL;
    private static String API_KEY;
    private static String API_SECRET;

    private static String SMS_TYPE_OF_NORMAL;
    private static String SMS_FREE_SIGN_NAME="荣强搬砖";


    private static String SMS_TEMPLATE_CODE_OF_OFF_LINE_NOTICE;
    private static String SMS_OFF_LINE_TEMPLATE_VARIABLE_OF_NAME;
    private static String SMS_OFF_LINE_TEMPLATE_VARIABLE_OF_CONFIGID;
    private static String SMS_OFF_LINE_TEMPLATE_VARIABLE_OF_REASON;

    private static String SMS_TEMPLATE_CODE_OF_ORDER_FAILED_NOTICE;
    private static String SMS_ORDER_FAILED_TEMPLATE_VARIABLE_OF_NAME;
    private static String SMS_ORDER_FAILED_TEMPLATE_VARIABLE_OF_ORDERID;

    private static String SMS_TEMPLATE_CODE_OF_TRANSFER_DAILY_REPORT;
    private static String SMS_TRANSFER_DAILY_REPORT_TEMPLATE_VARIABLE_OF_NAME;
    private static String SMS_TRANSFER_DAILY_REPORT_TEMPLATE_VARIABLE_OF_CONFIGID;


    final public static String TAOBAO_GATE_WAY_URL_KEY_NAME = "TAOBAO_GATE_WAY_URL";
    final public static String API_KEY_KEY_NAME = "API_KEY";
    final public static String API_SECRET_KEY_NAME = "API_SECRET";

    final public static String SMS_TYPE_OF_NORMAL_KEY_NAME = "SMS_TYPE_OF_NORMAL";

    final public static String SMS_TEMPLATE_CODE_OF_OFF_LINE_NOTICE_KEY_NAME = "SMS_TEMPLATE_CODE_OF_OFF_LINE_NOTICE";
    final public static String SMS_OFF_LINE_TEMPLATE_VARIABLE_OF_KEY_NAME = "SMS_OFF_LINE_TEMPLATE_VARIABLE_OF_NAME";
    final public static String SMS_OFF_LINE_TEMPLATE_VARIABLE_OF_CONFIGID_KEY_NAME = "SMS_OFF_LINE_TEMPLATE_VARIABLE_OF_CONFIGID";
    final public static String SMS_OFF_LINE_TEMPLATE_VARIABLE_OF_REASON_KEY_NAME = "SMS_OFF_LINE_TEMPLATE_VARIABLE_OF_REASON";


    final public static String SMS_TEMPLATE_CODE_OF_ORDER_FAILED_NOTICE_KEY_NAME = "SMS_TEMPLATE_CODE_OF_ORDER_FAILED_NOTICE";
    final public static String SMS_ORDER_FAILED_TEMPLATE_VARIABLE_OF_CONFIGID_KEY_NAME = "SMS_ORDER_FAILED_TEMPLATE_VARIABLE_OF_NAME";
    final public static String SMS_ORDER_FAILED_TEMPLATE_VARIABLE_OF_NAME_KEY_NAME = "SMS_ORDER_FAILED_TEMPLATE_VARIABLE_OF_ORDERID";

    final public static String SMS_TEMPLATE_CODE_OF_TRANSFER_DAILY_REPORT_NOTICE_KEY_NAME = "SMS_TEMPLATE_CODE_OF_TRANSFER_DAILY_REPORT";
    final public static String SMS_TRANSFER_DAILY_REPORT_TEMPLATE_VARIABLE_OF_CONFIGID_KEY_NAME = "SMS_TRANSFER_DAILY_REPORT_TEMPLATE_VARIABLE_OF_NAME";
    final public static String SMS_TRANSFER_DAILY_REPORT_TEMPLATE_VARIABLE_OF_NAME_KEY_NAME = "SMS_TRANSFER_DAILY_REPORT_TEMPLATE_VARIABLE_OF_CONFIGID";



    public static String getTaobaoGateWayUrl() {
        return TAOBAO_GATE_WAY_URL;
    }

    public static String getApiKey() {
        return API_KEY;
    }

    public static String getApiSecret() {
        return API_SECRET;
    }

    public static String getSmsTypeOfNormal() {
        return SMS_TYPE_OF_NORMAL;
    }

    public static String getSmsFreeSignName() {
        return SMS_FREE_SIGN_NAME;
    }

    public static String getSmsTemplateCodeOfOffLineNotice() {
        return SMS_TEMPLATE_CODE_OF_OFF_LINE_NOTICE;
    }

    public static String getSmsOffLineTemplateVariableOfName() {
        return SMS_OFF_LINE_TEMPLATE_VARIABLE_OF_NAME;
    }

    public static String getSmsOffLineTemplateVariableOfConfigid() {
        return SMS_OFF_LINE_TEMPLATE_VARIABLE_OF_CONFIGID;
    }

    public static String getSmsTemplateCodeOfOrderFailedNotice() {
        return SMS_TEMPLATE_CODE_OF_ORDER_FAILED_NOTICE;
    }

    public static String getSmsOrderFailedTemplateVariableOfName() {
        return SMS_ORDER_FAILED_TEMPLATE_VARIABLE_OF_NAME;
    }

    public static String getSmsOrderFailedTemplateVariableOfOrderid() {
        return SMS_ORDER_FAILED_TEMPLATE_VARIABLE_OF_ORDERID;
    }

    public static String getSmsOffLineTemplateVariableOfReason() {
        return SMS_OFF_LINE_TEMPLATE_VARIABLE_OF_REASON;
    }


    public static String getSmsTemplateCodeOfTransferDailyReport() {
        return SMS_TEMPLATE_CODE_OF_TRANSFER_DAILY_REPORT;
    }

    public static String getSmsTransferDailyReportTemplateVariableOfName() {
        return SMS_TRANSFER_DAILY_REPORT_TEMPLATE_VARIABLE_OF_NAME;
    }

    public static String getSmsTransferDailyReportTemplateVariableOfConfigid() {
        return SMS_TRANSFER_DAILY_REPORT_TEMPLATE_VARIABLE_OF_CONFIGID;
    }
}
