package com.libra.common.util;


import com.libra.common.exceptions.BTCTTSException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sun.misc.BASE64Encoder;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by rongqiang.li on 2017/2/11.
 */
public class EncryptUtils {

    private static Logger logger = LoggerFactory.getLogger(EncryptUtils.class);

    public static String getEncryptMessage(String message) {
        try {
            MessageDigest md = MessageDigest.getInstance("md5");
            byte[] digest = md.digest(message.getBytes("UTF-8"));
            BASE64Encoder encoder=new BASE64Encoder();
            String encodeMessage = encoder.encode(digest);
            return encodeMessage;
        } catch (NoSuchAlgorithmException e) {
            logger.error(e.getMessage(),e);
            throw new BTCTTSException(e.getMessage(),e);
        } catch (UnsupportedEncodingException e) {
            logger.error(e.getMessage(), e);
            throw new BTCTTSException(e.getMessage(),e);
        }

    }


}
