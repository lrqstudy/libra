package com.libra.common.util;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.google.common.collect.Range;
import org.joda.time.DateTime;
import org.joda.time.DateTimeComparator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * @author zhangzijing
 * @version 2011-8-10
 */
public class DateUtil {

    private static final Logger logger = LoggerFactory.getLogger(DateUtil.class);
    private static final String FORMAT_DATE_PATTERN = "yyyy年MM月dd日";
    private static final String FORMAT_DATETIME_PATTERN = "yyyy年MM月dd日 HH:mm:ss";
    private static final String FORMAT_ALL_DATETIME_PATTERN = "yyyyMMddHHmmssSSSS";
    public static final String FORMAT_WITH_DATE = "yyyy-MM-dd";
    public static final String FORMAT_WITH_SECOND = "yyyy-MM-dd HH:mm:ss";

    public static String formatAllDateTime(Date date) {
        SimpleDateFormat formatDate = new SimpleDateFormat(FORMAT_ALL_DATETIME_PATTERN);
        return formatDate.format(date);
    }

    public static int date2MonthPaymentDays(Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMM");
        String yyyyMM = sdf.format(date);
        return Integer.valueOf(yyyyMM);
    }

    public static String getDateOfCurrentMonthFirstDay() {

        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.set(Calendar.DAY_OF_MONTH, 1);

        return new SimpleDateFormat("yyyy-MM-dd").format(cal.getTime());
    }

    /**
     * 将long型时间戳转化为日期(年月日)
     *
     * @param timemillis
     * @return
     */
    public static String formatLongToDate(long timemillis) {
        if (timemillis == 0) {
            return "";
        }
        SimpleDateFormat formatDate = new SimpleDateFormat(FORMAT_DATE_PATTERN);
        return formatDate.format(new Date(timemillis));
    }

    /**
     * 将long型时间戳转化为日期(年月日时分秒)
     *
     * @param timemillis
     * @return
     */
    public static String formatLongToDateTime(long timemillis) {
        if (timemillis == 0) {
            return "";
        }
        SimpleDateFormat formDateTime = new SimpleDateFormat(FORMAT_DATETIME_PATTERN);
        return formDateTime.format(new Date(timemillis));
    }

    /**
     * 将java.util.Date转化为标准日期(年月日)
     *
     * @param date
     * @return
     */
    public static String formatDateToSimpleDate(Date date) {
        SimpleDateFormat formatDate = new SimpleDateFormat(FORMAT_DATE_PATTERN);
        return formatDate.format(date);
    }

    public static int daysBetween(Date begin, Date end) {
        return (int) ((end.getTime() - begin.getTime()) / TimeUnit.DAYS.toMillis(1L));
    }

    /**
     * 将java.util.Date转化为标准日期(年月日时分秒)
     *
     * @param date
     * @return
     */
    public static String formatDateToSimpleDatetime(Date date) {
        SimpleDateFormat formDateTime = new SimpleDateFormat(FORMAT_DATETIME_PATTERN);
        return formDateTime.format(date);
    }

    /**
     * 日期转换成字符串
     *
     * @param date    日期
     * @param pattern 格式
     * @return String
     */
    public static String dateToString(Date date, String pattern) {
        if (date == null)
            return null;
        SimpleDateFormat sdf = null;
        try {
            sdf = new SimpleDateFormat(pattern);
            return sdf.format(date);
        } catch (Exception e) {
            logger.error("date转换成string出错:" + e.getMessage());
            sdf = new SimpleDateFormat(FORMAT_DATETIME_PATTERN);
            return sdf.format(date);
        }

    }

    /**
     * 字符串转换日期
     *
     * @param strDate 字符串
     * @param pattern 格式
     * @return Date
     */
    public static Date stringToDate(String strDate, String pattern) {
        if (strDate == null || strDate.trim().length() <= 0)
            return null;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(pattern);
            return sdf.parse(strDate);
        } catch (ParseException e) {
            logger.error("字符串转换日期出错:" + e.getMessage());
            return null;
        }
    }

    /**
     * 计算出两个日期之间相差的天数
     * 建议date1 大于 date2 这样计算的值为正数
     *
     * @param date1 日期1
     * @param date2 日期2
     * @return date1 - date2
     */
    public static int dateInterval(long date1, long date2) {
        if (date2 > date1) {
            date2 = date2 + date1;
            date1 = date2 - date1;
            date2 = date2 - date1;
        }  
        /* 
         * Canlendar 该类是一个抽象类  
         * 提供了丰富的日历字段 
         *  
         * 本程序中使用到了 
         * Calendar.YEAR    日期中的年份 
         * Calendar.DAY_OF_YEAR     当前年中的天数 
         * getActualMaximum(Calendar.DAY_OF_YEAR) 返回今年是 365 天还是366天 
         */
        Calendar calendar1 = Calendar.getInstance(); // 获得一个日历  
        calendar1.setTimeInMillis(date1); // 用给定的 long 值设置此 Calendar 的当前时间值。  

        Calendar calendar2 = Calendar.getInstance();
        calendar2.setTimeInMillis(date2);
        // 先判断是否同年  
        int y1 = calendar1.get(Calendar.YEAR);
        int y2 = calendar2.get(Calendar.YEAR);

        int d1 = calendar1.get(Calendar.DAY_OF_YEAR);
        int d2 = calendar2.get(Calendar.DAY_OF_YEAR);
        int maxDays = 0;
        int day = 0;
        if (y1 - y2 > 0) {
            day = numerical(maxDays, d1, d2, y1, y2, calendar2);
        } else {
            day = d1 - d2;
        }
        return day;
    }

    /**
     * 日期间隔计算
     * 计算公式(示例):
     * 20121230 - 20071001
     * 取出20121230这一年过了多少天 d1 = 365     取出20071001这一年过了多少天 d2 = 274
     * 如果2007年这一年有366天就要让间隔的天数+1，因为2月份有29日。
     *
     * @param maxDays  用于记录一年中有365天还是366天
     * @param d1       表示在这年中过了多少天
     * @param d2       表示在这年中过了多少天
     * @param y1       当前为2010年
     * @param y2       当前为2012年
     * @param calendar 根据日历对象来获取一年中有多少天
     * @return 计算后日期间隔的天数
     */
    public static int numerical(int maxDays, int d1, int d2, int y1, int y2, Calendar calendar) {
        int day = d1 - d2;
        int betweenYears = y1 - y2;
        List<Integer> d366 = new ArrayList<Integer>();

        if (calendar.getActualMaximum(Calendar.DAY_OF_YEAR) == 366) {
            System.out.println(calendar.getActualMaximum(Calendar.DAY_OF_YEAR));
            day += 1;
        }

        for (int i = 0; i < betweenYears; i++) {
            // 当年 + 1 设置下一年中有多少天  
            calendar.set(Calendar.YEAR, (calendar.get(Calendar.YEAR)) + 1);
            maxDays = calendar.getActualMaximum(Calendar.DAY_OF_YEAR);
            // 第一个 366 天不用 + 1 将所有366记录，先不进行加入然后再少加一个  
            if (maxDays != 366) {
                day += maxDays;
            } else {
                d366.add(maxDays);
            }
            // 如果最后一个 maxDays 等于366 day - 1  
            if (i == betweenYears - 1 && betweenYears > 1 && maxDays == 366) {
                day -= 1;
            }
        }

        for (int i = 0; i < d366.size(); i++) {
            // 一个或一个以上的366天  
            if (d366.size() >= 1) {
                day += d366.get(i);
            }
//          else{  
//              day -= 1;  
//          }  
        }
        return day;
    }

    /**
     * 将日期字符串装换成日期
     *
     * @param strDate 日期支持年月日 示例:yyyyMMdd
     * @return 1970年1月1日器日期的毫秒数
     */
    public static long getDateTime(String strDate) {
        return getDateByFormat(strDate, "yyyyMMdd").getTime();
    }

    /**
     * @param strDate 日期字符串
     * @param format  日期格式
     * @return Date
     */
    public static Date getDateByFormat(String strDate, String format) {
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        try {
            return (sdf.parse(strDate));
        } catch (Exception e) {
            return null;
        }
    }

    public static String getCurrentMonth(Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM");
        return sdf.format(date);
    }

    public static String getPreviousMonth(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.MONTH, -1);

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM");
        return sdf.format(cal.getTime());
    }

    public static String getPreviousMonth(Date date, String formatStr) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.MONTH, -1);

        SimpleDateFormat sdf = new SimpleDateFormat(formatStr);
        return sdf.format(cal.getTime());
    }

    public static Date getPreviousMonthCurrentDay(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.MONTH, -1);
        return cal.getTime();
    }

    public static String getMonthFirstDay(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.DAY_OF_MONTH, calendar
                .getActualMinimum(Calendar.DAY_OF_MONTH));

        return dateToString(calendar.getTime(), "yyyy-MM-dd");

    }

    public static String getMonthLastDay(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.DAY_OF_MONTH, calendar
                .getActualMaximum(Calendar.DAY_OF_MONTH));
        return dateToString(calendar.getTime(), "yyyy-MM-dd");
    }

    public static Date getMonthLastDayDate(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.DAY_OF_MONTH, calendar
                .getActualMaximum(Calendar.DAY_OF_MONTH));
        return calendar.getTime();
    }

    public static String getNextMonthFirstDay(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.MONTH, 1);
        calendar.set(Calendar.DAY_OF_MONTH, calendar
                .getActualMinimum(Calendar.DAY_OF_MONTH));

        return dateToString(calendar.getTime(), "yyyy-MM-dd");

    }

    public static boolean isSameDay(Date date,Date compareDate){
        DateTime dateTime = new DateTime(date);
        DateTime compareDateTime = new DateTime(compareDate);
        int compare = DateTimeComparator.getDateOnlyInstance().compare(dateTime, compareDateTime);
        return compare == 0;
    }
    /**
     * 获得上一天
     *
     * @param date
     * @return
     */
    public static Date getYesterday(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DAY_OF_MONTH, -1);
        return calendar.getTime();
    }

    /**
     * 获得下一天
     *
     * @param date
     * @return
     */
    public static Date getNextDay(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DAY_OF_MONTH, 1);
        return calendar.getTime();
    }

    public static String getDayAgo(String beginDate, int days) {
        Calendar calendar = Calendar.getInstance();
        try {
            Date nowDay = stringToDate(beginDate, "yyyy-MM-dd");
            calendar.setTime(nowDay);
            calendar.add(Calendar.DAY_OF_YEAR, -days);
        } catch (Exception e) {
            logger.error("getDay error:" + e.getMessage(), e);
            return null;
        }

        return dateToString(calendar.getTime(), "yyyy-MM-dd");
    }

    public static Date getDayAgo(Date beginDate, int days) {
        Calendar calendar = Calendar.getInstance();
        try {
            calendar.setTime(beginDate);
            calendar.add(Calendar.DAY_OF_YEAR, -days);
        } catch (Exception e) {
            logger.error("getDay error:" + e.getMessage(), e);
            return null;
        }
        return calendar.getTime();
    }

    public static String getCurrentStrDate() {
        return dateToString(new Date(System.currentTimeMillis()), "yyyy-MM-dd");
    }


    public static String getMonthFirstDay(Date date, int number, String pattern) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.MONTH, number);
        calendar.set(Calendar.DAY_OF_MONTH, calendar
                .getActualMinimum(Calendar.DAY_OF_MONTH));
        try {
            return dateToString(calendar.getTime(), pattern);
        } catch (Exception e) {
            logger.error("getMonthFirstDay error, " + e.getMessage(), e);
            return dateToString(calendar.getTime(), "yyyy-MM-dd");
        }

    }

    public static String getMonthLastDay(Date date, int number, String pattern) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.MONTH, number);
        calendar.set(Calendar.DAY_OF_MONTH, calendar
                .getActualMaximum(Calendar.DAY_OF_MONTH));
        try {
            return dateToString(calendar.getTime(), pattern);
        } catch (Exception e) {
            logger.error("getMonthFirstDay error, " + e.getMessage(), e);
            return dateToString(calendar.getTime(), "yyyy-MM-dd");
        }

    }

    public static String getDay(String beginDate, int days) {
        Calendar calendar = Calendar.getInstance();
        try {
            Date nowDay = stringToDate(beginDate, "yyyy-MM-dd");
            calendar.setTime(nowDay);
            calendar.add(Calendar.DAY_OF_YEAR, -days);
        } catch (Exception e) {
            logger.error("getDay error:" + e.getMessage(), e);
            return null;
        }

        return dateToString(calendar.getTime(), "yyyy-MM-dd");
    }

    public static List<String> getFormatedDateListByRange(String startDate, String endDate, String format) {
        Date start = stringToDate(startDate, format);
        Date end = stringToDate(endDate, format);

        Calendar calendar = Calendar.getInstance();
        List<String> dateList = new ArrayList<String>();
        /**
         * modify jiangzw 2013-07-30
         * 修改时间判断不当，导致else条件中死循环
         */
        if (start.after(end)) {
            logger.error("时间条件异常，开始时间要小于结束时间。");
        } else {
            while (!start.equals(end)) {
                String formatedDate = dateToString(start, format);
                dateList.add(formatedDate);
                calendar.setTime(start);
                calendar.add(Calendar.DATE, 1);
                start = calendar.getTime();
            }
            String formatedDate = dateToString(end, format);
            dateList.add(formatedDate);
        }
        return dateList;
    }

    public static boolean checkDateFormat(String strDate) {
        DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
        if (null != strDate && !strDate.equals("")) {
            if (strDate.split("/").length > 1) {
                dateFormat = new SimpleDateFormat("yyyy/MM/dd");
            }
            if (strDate.split("-").length > 1) {
                dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            }
        } else
            return false;
        try {
            dateFormat.parse(strDate);
            return true;
        } catch (Exception e) {
            return false;
        }

    }

    /**
     * add jiangzw 2013-06-09
     * 获取上个月的最后一天
     *
     * @param date
     * @return
     */
    public static Date getPreviousMonthLastDay(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.MONTH, -1);
        int lastDay = cal.getActualMaximum(Calendar.DATE);
        cal.set(Calendar.DATE, lastDay);
        return trimDate(cal.getTime());
    }

    public static Date trimDate(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }


    public static String getActionDate(int monthPaymentDays) {

        try {
            SimpleDateFormat sf = new SimpleDateFormat("yyyyMM");
            SimpleDateFormat sfy_m = new SimpleDateFormat("yyyy-MM");
            Date date = sf.parse(String.valueOf(monthPaymentDays));
            return sfy_m.format(date);
        } catch (Exception e) {
            logger.error("日期解析异常", e);
            return null;
        }
    }

    public static int getLastMonth() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MONTH, -1);
        return getMonthPaymentDays(calendar.getTime());
    }

    public static int getMonthPaymentDays(Date date) {
        SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyyMM");
        return Integer.valueOf(DATE_FORMAT.format(date));
    }

    public static String getLastMonthStr() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MONTH, -1);
        SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM");
        return DATE_FORMAT.format(calendar.getTime());
    }

    public static boolean isFirstDay(Date date) {
        if (date == null) {
            return false;
        } else {
            DateTime dateTime = new DateTime(date);
            return dateTime.getDayOfMonth() == 1;
        }
    }

    public static int getWeekDay(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DAY_OF_WEEK, -1);
        return calendar.get(Calendar.DAY_OF_WEEK);
    }

    public static String getHourAndMint(Date date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm");
        return dateFormat.format(date);
    }

    public static int monthCompare(Date date, Date anotherDate) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMM");
        try {
            Date d = dateFormat.parse(dateFormat.format(date));
            Date another = dateFormat.parse(dateFormat.format(anotherDate));
            return d.compareTo(another);
        } catch (ParseException e) {
            logger.error("解析日期格式异常" , e);
            throw new RuntimeException("解析日期格式异常");
        }
    }
    public static Date withStartOfDay(Date date){
        DateTime dateTime = new DateTime(date);
        return dateTime.withTimeAtStartOfDay().toDate();
    }

    public static Date withEndOfDay(Date date){
        DateTime dateTime = new DateTime(date);
        int hourOfDay = 23;
        int minuteOfHour = 59;
        int secondOfMinute = 59;
        int millisOfSecond = 999;
        return dateTime.withTime(hourOfDay, minuteOfHour, secondOfMinute, millisOfSecond).toDate();
    }

    public static Date withStartOfMonth(Date date){
        DateTime dateTime = new DateTime(date);
        return dateTime.dayOfMonth().withMinimumValue().toDate();
    }

    public static Date withEndOfMonth(Date date){
        DateTime dateTime = new DateTime(date);
        return dateTime.dayOfMonth().withMaximumValue().toDate();
    }

    /**
     * @param strDate   日期字符串
     * @param format    日期格式
     * @return      Date
     */
    public static Date parse(String strDate, String format) {
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        try{
            return (sdf.parse(strDate));
        }catch (Exception e){
            return null;
        }
    }


    public static String format(Date date, String format){
        SimpleDateFormat formatDate = new SimpleDateFormat(format);
        return formatDate.format(date);
    }

    public static Date getMonthLastDayReturnDate(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.DAY_OF_MONTH, calendar
                .getActualMaximum(Calendar.DAY_OF_MONTH));
        return calendar.getTime();
    }

    public static Date defaultDate() {
        return DateUtil.parse("1970-01-01", "yyyy-MM-dd");
    }

    public static boolean isSameMonth(Date date1, Date date2) {
        if (date1 == null || date2 == null) {
            throw new IllegalArgumentException("The date must not be null");
        }
        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(date1);
        Calendar cal2 = Calendar.getInstance();
        cal2.setTime(date2);
        return isSameMonth(cal1, cal2);
    }

    public static boolean isSameMonth(Calendar cal1, Calendar cal2) {
        if (cal1 == null || cal2 == null) {
            throw new IllegalArgumentException("The date must not be null");
        }
        return (cal1.get(Calendar.ERA) == cal2.get(Calendar.ERA) &&
            cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR) &&
            cal1.get(Calendar.MONTH) == cal2.get(Calendar.MONTH));
    }

    public static List<Range<Date>> splitMonth(Date begin,Date end,List<Range<Date>> list){

        if(begin.after(end)){
            return list;
        }

        if(isSameMonth(begin,end)){
            list.add(Range.closed(begin,end));
            return list;
        }

        Date lastDay = getMonthLastDayReturnDate(begin);
        list.add(Range.closed(begin,lastDay));

        return splitMonth(getDayAgo(lastDay,-1),end,list);
    }
    /**
     * 根据日期获取费用期间格式
     *
     * @param expenseDate
     * @return
     */
    public static int getGlPeriodByDate(Date expenseDate){
        String expensDateStr = DateUtil.dateToString(expenseDate, "yyyyMM");
        return Integer.parseInt(expensDateStr);
    }


    final static List<String> quartBeginningList = Lists.newArrayList("01", "04", "07", "10");

    /**
     * 是否为季度第一个月
     * @param period    会计期间 yyyyMM
     * @return
     */
    public static boolean isQuartBeginning(int period) {
        String s = String.valueOf(period);
        if (s.length() != 6)
            return false;
        String month = s.substring(4, 6);
        return quartBeginningList.contains(month);
    }

    final static Map<String, String> monthQuarterMapping = ImmutableMap.<String, String>builder()
            .put("01", "01").build();
    /**
     * 获取当前期间所在季初期间
     * @param period
     * @return
     */
    public static int getQuarterOfBeginning(int period) {
        if (isQuartBeginning(period)) {
            return period;
        }
        String year = String.valueOf(period).substring(0, 4);
        String month = String.valueOf(period).substring(4, 6);
        String result = year + monthQuarterMapping.get(month);
        return Integer.parseInt(result);
    }

    /**
     * 是否为年度第一个月
     * @param period 会计期间yyyyMM
     * @return
     */
    public static boolean isYearBeginning(int period) {
        String s = String.valueOf(period);
        if (s.length() != 6)
            return false;
        String month = String.valueOf(period).substring(4, 6);
        return "01".contains(month);
    }

}
