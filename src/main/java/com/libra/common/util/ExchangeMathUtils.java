package com.libra.common.util;

import java.math.BigDecimal;

/**
 * Created by rongqiang.li on 17/4/16.
 */
public class ExchangeMathUtils {


    public static BigDecimal getBuyPriceOfRate(BigDecimal weightedPrice, BigDecimal rate) {
        BigDecimal buyPrice = weightedPrice.multiply(BigDecimal.ONE.subtract(rate));
        buyPrice= buyPrice.setScale(8, BigDecimal.ROUND_CEILING);
        return buyPrice;
    }


    public static BigDecimal getSellPriceOfRate(BigDecimal weightedPrice, BigDecimal rate) {
        BigDecimal sellPrice = weightedPrice.multiply(BigDecimal.ONE.add(rate));
        sellPrice= sellPrice.setScale(8, BigDecimal.ROUND_CEILING);
        return sellPrice;
    }

}
