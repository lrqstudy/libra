package com.libra.common.util;

/**
 * <p>
 * <b>本类主要完成以下功能</b>
 * <p>
 * <ul>
 * <li>
 * </ul>
 * <p>
 * <p>
 * <p>
 *
 * @author 李荣强
 * @version 1.0
 * @time 2015-1-21 下午5:41:01
 * @since 1.0
 */
public class Encode {

    /**
     * 解密
     */
    public static String decode(String s) {
        if (s == null)
            return null;
        String res = "";
        DES des = new DES(getRandomNum());
        byte[] sBytes = s.getBytes();
        for (int i = 0; i < (sBytes.length / 16); i++) {
            byte[] theBytes = new byte[8];
            for (int j = 0; j <= 7; j++) {
                byte byte1 = (byte) (sBytes[16 * i + 2 * j] - 'a');
                byte byte2 = (byte) (sBytes[16 * i + 2 * j + 1] - 'a');
                theBytes[j] = (byte) (byte1 * 16 + byte2);
            }
            long x = des.bytes2long(theBytes);
            byte[] result = new byte[8];
            des.long2bytes(des.decrypt(x), result);
            res = res + (new String(result));
        }
        return res.trim();
    }

    final private static long RANDOM_NUM = 123456789;

    /**
     * 加密
     */
    public static String encode(String s) {
        if (s == null)
            return null;
        String res = "";
        DES des = new DES(getRandomNum());
        byte space = 0x20;
        byte[] sBytes = s.getBytes();
        int length = sBytes.length;
        int newLength = length + (8 - length % 8) % 8;
        byte[] newBytes = new byte[newLength];
        for (int i = 0; i < newLength; i++) {
            if (i <= length - 1) {
                newBytes[i] = sBytes[i];
            } else {
                newBytes[i] = space;
            }
        }
        for (int i = 0; i < (newLength / 8); i++) {
            byte[] theBytes = new byte[8];
            for (int j = 0; j <= 7; j++) {
                theBytes[j] = newBytes[8 * i + j];
            }
            long x = des.bytes2long(theBytes);
            byte[] result = new byte[8];
            des.long2bytes(des.encrypt(x), result);
            byte[] doubleResult = new byte[16];
            for (int j = 0; j < 8; j++) {
                doubleResult[2 * j] = (byte) (((((char) result[j]) & 0xF0) >> 4) + 'a');
                doubleResult[2 * j + 1] = (byte) ((((char) result[j]) & 0x0F) + 'a');
            }
            res = res + new String(doubleResult);
        }
        return res;
    }

    /**
     * @return long
     */
    private static long getRandomNum() {
        return RANDOM_NUM;
    }
}