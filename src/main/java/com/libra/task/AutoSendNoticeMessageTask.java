package com.libra.task;

import com.libra.common.consts.ProcessFlag;
import com.libra.common.util.SendSmsMessageUtil;
import com.libra.common.util.message.SmsMessageModel;
import com.libra.exchange.strategy.model.SmsInfoModel;
import com.libra.exchange.strategy.service.SmsInfoService;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by rongqiang.li on 17/11/26.
 */
@Repository
public class AutoSendNoticeMessageTask {

    @Resource
    private SmsInfoService smsInfoService;


    public void autoSendMsg() {
        List<SmsInfoModel> needHandleModelList = smsInfoService.getNeedHandleModelList();
        for (SmsInfoModel smsInfoModel : needHandleModelList) {
            SmsMessageModel messageModel = buildSmsMessageModelInfoModel(smsInfoModel);
            String result = SendSmsMessageUtil.send(messageModel);
            result= StringUtils.defaultString(result);
            //回写发送状态
            smsInfoService.updateProcessFlagById(smsInfoModel.getId(),result, ProcessFlag.YES.getId());
        }

    }

    private SmsMessageModel buildSmsMessageModelInfoModel(SmsInfoModel smsInfoModel) {
        try {
            SmsMessageModel messageModel = new SmsMessageModel();
            messageModel.setExtend(smsInfoModel.getExtend());
            messageModel.setRecNum(smsInfoModel.getRecNumber());
            messageModel.setSmsFreeSignName(smsInfoModel.getSmsFreeSignName());

            //JSONObject jsonObject=new JSONObject();
            //jsonObject.put("name","test");
            //jsonObject.put("configId","123456");
            //jsonObject.put("reason","balance not enough");
            messageModel.setSmsParamString(smsInfoModel.getMsgContent());
            messageModel.setSmsTemplateCode(smsInfoModel.getTemplateCode());
            messageModel.setSmsType(smsInfoModel.getSmsType());
            return messageModel;
        }catch (Exception e){
            return null;
        }

    }


}
