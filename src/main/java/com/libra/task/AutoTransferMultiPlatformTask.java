package com.libra.task;

import com.google.api.client.util.Lists;
import com.google.common.collect.Maps;
import com.libra.exchange.exchanges.task.AutoTransferHelper;
import com.libra.exchange.strategy.model.AltCoinTransferConfigModel;
import com.libra.exchange.strategy.service.AltCoinTransferConfigService;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * Created by rongqiang.li on 17/5/13.
 */
@Service
public class AutoTransferMultiPlatformTask {
    @Resource
    private AutoTransferHelper autoTransferHelper;
    @Resource
    private AltCoinTransferConfigService altCoinTransferConfigService;

    public void autoTransfer() {
        List<AltCoinTransferConfigModel> altCoinTransferConfigModelList = altCoinTransferConfigService.getModdelListByStatus(1);
        Map<String, List<AltCoinTransferConfigModel>> userCodeConfigModelListMap = Maps.newHashMap();
        for (AltCoinTransferConfigModel configModel : altCoinTransferConfigModelList) {
            List<AltCoinTransferConfigModel> userCodeConfigModelList = userCodeConfigModelListMap.get(configModel.getUserCode());
            if (CollectionUtils.isEmpty(userCodeConfigModelList)){
                userCodeConfigModelList= Lists.newArrayList();
                userCodeConfigModelList.add(configModel);
                userCodeConfigModelListMap.put(configModel.getUserCode(),userCodeConfigModelList);
                continue;
            }
            userCodeConfigModelList.add(configModel);
        }
        autoTransferHelper.autoTransfer(userCodeConfigModelListMap);
    }
}
