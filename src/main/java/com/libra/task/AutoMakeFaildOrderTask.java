package com.libra.task;

import com.libra.common.consts.*;
import com.libra.common.util.BeanUtilExt;
import com.libra.common.util.GlobalConfig;
import com.libra.exchange.exchanges.common.CommonTransferService;
import com.libra.exchange.exchanges.task.CommonTransferServiceUtil;
import com.libra.exchange.strategy.model.AltTransferErrorHandleModel;
import com.libra.exchange.strategy.model.PlatformConfigModel;
import com.libra.exchange.strategy.model.SmsInfoModel;
import com.libra.exchange.strategy.service.AltTransferErrorHandleService;
import com.libra.exchange.strategy.service.PlatformConfigService;
import com.libra.exchange.strategy.service.SmsInfoService;
import org.apache.commons.lang3.tuple.Pair;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * Created by rongqiang.li on 17/11/23.
 */
@Repository
public class AutoMakeFaildOrderTask {

    @Resource
    private CommonTransferServiceUtil commonTransferServiceUtil;

    @Resource
    private AltTransferErrorHandleService altTransferErrorHandleService;

    @Resource
    private PlatformConfigService platformConfigService;
    private Logger logger = LoggerFactory.getLogger(AutoStopTransferTask.class);

    @Resource
    private SmsInfoService smsInfoService;

    public void autoMakeFailOrder() {
        //自动补单功能
        List<AltTransferErrorHandleModel> needHandleModelList = altTransferErrorHandleService.getNeedHandleModelList();
        //left:platform  right:user_code
        Map<Pair<String, String>, PlatformConfigModel> platformUserCodeAndModelMap = platformConfigService.getPlatformUserCodeAndModelMap();

        Map<String, CommonTransferService> platformAndServiceMap = commonTransferServiceUtil.getPlatformAndServiceMap();

        for (AltTransferErrorHandleModel model : needHandleModelList) {
            PlatformConfigModel platformConfigModel = platformUserCodeAndModelMap.get(Pair.of(model.getPlatform(), model.getUserCode()));
            if (platformConfigModel == null) {
                logger.info(" alt_transfer_error id={}, cannot get config", model.getId());
                continue;
            }
            CommonTransferService commonTransferService = platformAndServiceMap.get(platformConfigModel.getPlatform());
            String resultId = "";
            try {
                //bid --> buy , ask---> sell
                if (OrderSide.BUY.getCode().equalsIgnoreCase(model.getSide()) || "BID".equalsIgnoreCase(model.getSide())) {
                    resultId = commonTransferService.buy(model.getTargetCode(), model.getCashCurrency(), model.getTradePrice(), model.getTradeAmount(), model.getPrecisionSize(), platformConfigModel, OrderSource.MAKE_FAILED_ORDER.getId());
                } else if (OrderSide.SELL.getCode().equalsIgnoreCase(model.getSide()) || "ASK".equalsIgnoreCase(model.getSide())) {
                    resultId = commonTransferService.sell(model.getTargetCode(), model.getCashCurrency(), model.getTradePrice(), model.getTradeAmount(), model.getPrecisionSize(), platformConfigModel, OrderSource.MAKE_FAILED_ORDER.getId());
                } else {
                    resultId = "order side error";
                }

            } catch (Exception e) {
                logger.error(e.getMessage(), e);
                resultId = "make error";
            }
            model.setResultId(resultId);
            if (resultId.contains("error")) {
                //补单失败
                SmsInfoModel smsInfoModel = new SmsInfoModel();
                smsInfoModel.setExtend(String.valueOf(MsgSource.MAKE_ORDER_FAILED.getId()));
                StringBuilder sb = new StringBuilder();
                sb.append(model.getId()).append(" ,").append(model.getPlatform()).append(" ").append(model.getSide())
                        .append(" price= ").append(model.getTradePrice()).append(" , amount=").append(model.getTradeAmount()).append(" ");
                smsInfoModel.setMsgContent(buildMsgContent("用户", sb.toString()));

                smsInfoModel.setMsgSource(MsgSource.MAKE_ORDER_FAILED.getId());
                smsInfoModel.setProcessFlag(ProcessFlag.NO.getId());

                smsInfoModel.setRecNumber(model.getUserCode());
                smsInfoModel.setSmsFreeSignName(GlobalConfig.getSmsFreeSignName());
                smsInfoModel.setSmsType(GlobalConfig.getSmsTypeOfNormal());
                smsInfoModel.setTemplateCode(GlobalConfig.getSmsTemplateCodeOfOrderFailedNotice());
                smsInfoModel.setUserCode(model.getUserCode());
                BeanUtilExt.fillNullProperty(smsInfoModel);
                smsInfoService.saveSmsInfoModel(smsInfoModel);
            }
            altTransferErrorHandleService.updateResultIdAndProcessFlag(model.getId(), ProcessFlag.YES.getId(), HandleType.SYSTEM.getId(), resultId);
            try {
                Thread.sleep(2000);
            } catch (Exception e) {
                logger.error("thread sleep" + e.getMessage(), e);
            }

        }

    }

    private String buildMsgContent(String name, String orderId) {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(GlobalConfig.getSmsOrderFailedTemplateVariableOfOrderid(), orderId);
            jsonObject.put(GlobalConfig.getSmsOrderFailedTemplateVariableOfName(), name);
            return jsonObject.toString();
        } catch (Exception e) {
            logger.error(e.getMessage(), "");
            return "";
        }
    }


}
