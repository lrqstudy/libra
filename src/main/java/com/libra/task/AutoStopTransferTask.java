package com.libra.task;

import com.google.api.client.util.Lists;
import com.google.api.client.util.Maps;
import com.libra.common.consts.MsgSource;
import com.libra.common.consts.ProcessFlag;
import com.libra.common.util.BeanUtilExt;
import com.libra.common.util.GlobalConfig;
import com.libra.exchange.exchanges.common.CommonOpenOrderVO;
import com.libra.exchange.exchanges.common.CommonTransferService;
import com.libra.exchange.exchanges.task.CommonTransferServiceUtil;
import com.libra.exchange.strategy.model.AltCoinTransferConfigModel;
import com.libra.exchange.strategy.model.PlatformConfigModel;
import com.libra.exchange.strategy.model.SmsInfoModel;
import com.libra.exchange.strategy.service.AltCoinTransferConfigService;
import com.libra.exchange.strategy.service.PlatformConfigService;
import com.libra.exchange.strategy.service.SmsInfoService;
import org.apache.commons.lang3.tuple.Pair;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by rongqiang.li on 17/11/14.
 */
@Service
public class AutoStopTransferTask {
    @Resource
    private CommonTransferServiceUtil commonTransferServiceUtil;

    private AtomicInteger balanceMapIsNullCount = new AtomicInteger(0);

    @Resource
    private AltCoinTransferConfigService altCoinTransferConfigService;
    @Resource
    private PlatformConfigService platformConfigService;
    private Logger logger = LoggerFactory.getLogger(AutoStopTransferTask.class);


    @Resource
    private SmsInfoService smsInfoService;


    public void autoOnLineOffLine() {
        autoStopTask();
        try {
            Thread.sleep(5000);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        autoActiveTask();
    }


    private void autoActiveTask() {
        List<AltCoinTransferConfigModel> altCoinTransferConfigModelList = altCoinTransferConfigService.getModdelListByStatus(0);//查询所有未激活
        Map<String, CommonTransferService> platformAndServiceMap = commonTransferServiceUtil.getPlatformAndServiceMap();

        Map<Pair<String, String>, PlatformConfigModel> platformUserCodeAndModelMap = platformConfigService.getPlatformUserCodeAndModelMap();
        if (platformUserCodeAndModelMap == null || platformUserCodeAndModelMap.size() == 0) {
            logger.info(" all is empty");
            return;
        }

        //避免重复查库
        Map<Integer, Map<String, BigDecimal>> platformIdBalanceMap = Maps.newHashMap();

        Map<Integer, AltCoinTransferConfigModelAndErrorReasonCompositeVO> neededActiveTransferIdMap = Maps.newHashMap();

        for (AltCoinTransferConfigModel transferConfigModel : altCoinTransferConfigModelList) {
            CommonTransferService commonTransferService = platformAndServiceMap.get(transferConfigModel.getPlatform());
            PlatformConfigModel platformConfigModel = platformUserCodeAndModelMap.get(Pair.of(transferConfigModel.getPlatform(), transferConfigModel.getUserCode()));
            if (isApiError(neededActiveTransferIdMap, transferConfigModel, platformConfigModel)) continue;


            Map<String, BigDecimal> tempBalanceMap = platformIdBalanceMap.get(platformConfigModel.getId());
            if (tempBalanceMap == null) {
                tempBalanceMap = commonTransferService.getBalanceMap(platformConfigModel);
                if (tempBalanceMap == null || tempBalanceMap.size() == 0) {
                    break;
                }
            }

            if (isTargetCodeNotEnougt(transferConfigModel, tempBalanceMap)) {
                continue;
            }

            if (isCashCurrencyNotEnough(transferConfigModel, tempBalanceMap)) {
                continue;
            }

            List<CommonOpenOrderVO> openOrderList = commonTransferService.getOpenOrderList(platformConfigModel, transferConfigModel.getTargetCode(), transferConfigModel.getCashCurrency());
            if (isOpenOrderTooMuch(openOrderList, transferConfigModel)) {
                continue;
            }
            //进到这里说明他应该恢复的
            AltCoinTransferConfigModelAndErrorReasonCompositeVO compositeVO = new AltCoinTransferConfigModelAndErrorReasonCompositeVO();
            compositeVO.setErrorReason(transferConfigModel.getPlatform() + " should auto actived " + transferConfigModel.getTargetCode());
            compositeVO.setTransferConfigModel(transferConfigModel);
            compositeVO.setType(MsgSource.AUTO_ON_LINE.getId());
            neededActiveTransferIdMap.put(transferConfigModel.getId(), compositeVO);


        }
        if (neededActiveTransferIdMap != null && neededActiveTransferIdMap.size() > 0) {
            altCoinTransferConfigService.updateActiveStatusByIdList(Lists.newArrayList(neededActiveTransferIdMap.keySet()), 1);
            List<SmsInfoModel> smsInfoModelList = buildSmsInfoModelListFromTransferHashSet(neededActiveTransferIdMap.values());
            smsInfoService.saveSmsInfoModelBatch(smsInfoModelList);
            return;
        }
        logger.info(" all is ok");

    }

    private void autoStopTask() {
        List<AltCoinTransferConfigModel> altCoinTransferConfigModelList = altCoinTransferConfigService.getModdelListByStatus(1);

        Map<String, CommonTransferService> platformAndServiceMap = commonTransferServiceUtil.getPlatformAndServiceMap();

        Map<Pair<String, String>, PlatformConfigModel> platformUserCodeAndModelMap = platformConfigService.getPlatformUserCodeAndModelMap();
        if (platformUserCodeAndModelMap == null || platformUserCodeAndModelMap.size() == 0) {
            logger.info(" all is empty");
            return;
        }
        //避免重复查库
        Map<Integer, Map<String, BigDecimal>> platformIdBalanceMap = Maps.newHashMap();

        Map<Integer, AltCoinTransferConfigModelAndErrorReasonCompositeVO> neededStopTransferIdMap = Maps.newHashMap();

        int diffCount = 0;
        for (AltCoinTransferConfigModel transferConfigModel : altCoinTransferConfigModelList) {
            CommonTransferService commonTransferService = platformAndServiceMap.get(transferConfigModel.getPlatform());
            PlatformConfigModel platformConfigModel = platformUserCodeAndModelMap.get(Pair.of(transferConfigModel.getPlatform(), transferConfigModel.getUserCode()));
            if (isApiError(neededStopTransferIdMap, transferConfigModel, platformConfigModel)) continue;


            Map<String, BigDecimal> tempBalanceMap = platformIdBalanceMap.get(platformConfigModel.getId());
            if (tempBalanceMap == null) {
                tempBalanceMap = commonTransferService.getBalanceMap(platformConfigModel);
                if (tempBalanceMap == null || tempBalanceMap.size() == 0) {
                    balanceMapIsNullCount.incrementAndGet();
                    if (balanceMapIsNullCount.get() > 3) {
                        //查询余额报错了停止
                        AltCoinTransferConfigModelAndErrorReasonCompositeVO compositeVO = new AltCoinTransferConfigModelAndErrorReasonCompositeVO();
                        compositeVO.setErrorReason(transferConfigModel.getPlatform() + " get balance error");
                        compositeVO.setTransferConfigModel(transferConfigModel);
                        compositeVO.setType(MsgSource.AUTO_OFF_LINE.getId());
                        neededStopTransferIdMap.put(transferConfigModel.getId(), compositeVO);
                        logger.info(" get balance error,should stop task");
                        continue;
                    }
                    continue;
                }
            }
            diffCount++;
            if (diffCount >= altCoinTransferConfigModelList.size()) {
                balanceMapIsNullCount.set(0);//如果每个条都执行到这里说明本次余额获取没有报错,应该把之前的报错累计去掉
            }
            if (isTargetCodeNotEnougt(transferConfigModel, tempBalanceMap)) {
                AltCoinTransferConfigModelAndErrorReasonCompositeVO compositeVO = new AltCoinTransferConfigModelAndErrorReasonCompositeVO();
                compositeVO.setErrorReason(transferConfigModel.getPlatform() + "  not enough " + transferConfigModel.getTargetCode());
                compositeVO.setTransferConfigModel(transferConfigModel);
                compositeVO.setType(MsgSource.AUTO_OFF_LINE.getId());
                neededStopTransferIdMap.put(transferConfigModel.getId(), compositeVO);
                continue;//报警
            }

            if (isCashCurrencyNotEnough(transferConfigModel, tempBalanceMap)) {
                AltCoinTransferConfigModelAndErrorReasonCompositeVO compositeVO = new AltCoinTransferConfigModelAndErrorReasonCompositeVO();
                compositeVO.setErrorReason(transferConfigModel.getPlatform() + " cash not enough " + transferConfigModel.getCashCurrency());
                compositeVO.setTransferConfigModel(transferConfigModel);
                compositeVO.setType(MsgSource.AUTO_OFF_LINE.getId());
                neededStopTransferIdMap.put(transferConfigModel.getId(), compositeVO);
                continue;//报警
            }

            List<CommonOpenOrderVO> openOrderList = commonTransferService.getOpenOrderList(platformConfigModel, transferConfigModel.getTargetCode(), transferConfigModel.getCashCurrency());
            if (isOpenOrderTooMuch(openOrderList, transferConfigModel)) {
                //是否未成交的订单过多
                AltCoinTransferConfigModelAndErrorReasonCompositeVO compositeVO = new AltCoinTransferConfigModelAndErrorReasonCompositeVO();
                compositeVO.setErrorReason(transferConfigModel.getPlatform() + " open order over limit of " + transferConfigModel.getCurrencyPair());
                compositeVO.setTransferConfigModel(transferConfigModel);
                compositeVO.setType(MsgSource.AUTO_OFF_LINE.getId());
                neededStopTransferIdMap.put(transferConfigModel.getId(), compositeVO);
                continue;
            }

        }
        if (neededStopTransferIdMap != null && neededStopTransferIdMap.size() > 0) {
            altCoinTransferConfigService.updateActiveStatusByIdList(Lists.newArrayList(neededStopTransferIdMap.keySet()), 0);
            List<SmsInfoModel> smsInfoModelList = buildSmsInfoModelListFromTransferHashSet(neededStopTransferIdMap.values());
            smsInfoService.saveSmsInfoModelBatch(smsInfoModelList);
            return;
        }
        logger.info(" all is ok");
    }

    /**
     * @param openOrderList
     * @param transferConfigModel
     * @return
     */

    private boolean isOpenOrderTooMuch(List<CommonOpenOrderVO> openOrderList, AltCoinTransferConfigModel transferConfigModel) {
        BigDecimal totalRemainAmount = new BigDecimal(0.00);
        for (CommonOpenOrderVO commonOpenOrderVO : openOrderList) {
            BigDecimal remainAmount = commonOpenOrderVO.getRemainAmount();
            totalRemainAmount = totalRemainAmount.add(remainAmount);
        }
        //如果未成交的订单数额 超过了配置的,则需要跳闸处理
        if (totalRemainAmount.compareTo(transferConfigModel.getTargetCodeWithdrawLimitAmount()) >= 0) {
            logger.info(" platform={},currency pair={},open order totalRemainAmount={}, over limit should stop task", transferConfigModel.getPlatform(), transferConfigModel.getCurrencyPair(), totalRemainAmount);
            return true;
        }
        return false;
    }


    private boolean isCashCurrencyNotEnough(AltCoinTransferConfigModel transferConfigModel, Map<String, BigDecimal> tempBalanceMap) {
        BigDecimal cashCurrencyAmount = tempBalanceMap.get(transferConfigModel.getCashCurrency());
        if (cashCurrencyAmount.compareTo(transferConfigModel.getStopCashLimitAmount()) < 0) {
            //4 如果现金账户小于最低限额,停止任务
            logger.info(" platform={},cashCurrency={},balance={}, less ={} ,should stop task", transferConfigModel.getPlatform(), transferConfigModel.getCashCurrency(), cashCurrencyAmount, transferConfigModel.getStopCashLimitAmount());
            return true;
        }
        return false;
    }

    private boolean isTargetCodeNotEnougt(AltCoinTransferConfigModel transferConfigModel, Map<String, BigDecimal> tempBalanceMap) {
        BigDecimal targetCodeBalanceAmount = tempBalanceMap.get(transferConfigModel.getTargetCode());
        if (targetCodeBalanceAmount == null || targetCodeBalanceAmount.compareTo(transferConfigModel.getStopTargetCodeLimitAmount()) < 0) {
            //3 如果目标品种小于最低限额,停止任务
            logger.info(" platform={},targetcode={},balance={}, less ={} ,should stop task", transferConfigModel.getPlatform(), transferConfigModel.getTargetCode(), targetCodeBalanceAmount, transferConfigModel.getStopTargetCodeLimitAmount());//TODO 报警
            return true;
        }
        return false;
    }


    private boolean isApiError(Map<Integer, AltCoinTransferConfigModelAndErrorReasonCompositeVO> neededStopTransferIdMap, AltCoinTransferConfigModel transferConfigModel, PlatformConfigModel platformConfigModel) {
        if (platformConfigModel == null) {
            logger.info("cannot get the config");
            AltCoinTransferConfigModelAndErrorReasonCompositeVO compositeVO = new AltCoinTransferConfigModelAndErrorReasonCompositeVO();
            compositeVO.setErrorReason(transferConfigModel.getPlatform() + "api error");
            compositeVO.setTransferConfigModel(transferConfigModel);
            compositeVO.setType(MsgSource.AUTO_OFF_LINE.getId());
            neededStopTransferIdMap.put(transferConfigModel.getId(), compositeVO);
            return true;
        }
        return false;
    }

    private List<SmsInfoModel> buildSmsInfoModelListFromTransferHashSet(Collection<AltCoinTransferConfigModelAndErrorReasonCompositeVO> configModels) {
        List<SmsInfoModel> smsInfoModelList = Lists.newArrayList();
        for (AltCoinTransferConfigModelAndErrorReasonCompositeVO compositeVO : configModels) {
            smsInfoModelList.add(buildSmsInfoModelByTransferModel(compositeVO));
        }
        return smsInfoModelList;
    }


    private SmsInfoModel buildSmsInfoModelByTransferModel(AltCoinTransferConfigModelAndErrorReasonCompositeVO compositeVO) {
        SmsInfoModel smsInfoModel = new SmsInfoModel();
        smsInfoModel.setExtend(String.valueOf(compositeVO.getType()));
        smsInfoModel.setMsgContent(buildMsgContent("用户", compositeVO.getTransferConfigModel().getCurrencyPair(), compositeVO.getErrorReason()));
        smsInfoModel.setMsgSource(compositeVO.getType());
        smsInfoModel.setProcessFlag(ProcessFlag.NO.getId());
        smsInfoModel.setRecNumber(compositeVO.getTransferConfigModel().getUserCode());
        smsInfoModel.setSmsFreeSignName(GlobalConfig.getSmsFreeSignName());
        smsInfoModel.setSmsType(GlobalConfig.getSmsTypeOfNormal());
        smsInfoModel.setTemplateCode(GlobalConfig.getSmsTemplateCodeOfOffLineNotice());
        smsInfoModel.setUserCode(compositeVO.getTransferConfigModel().getUserCode());
        BeanUtilExt.fillNullProperty(smsInfoModel);
        return smsInfoModel;
    }

    private String buildMsgContent(String name, String configId, String reason) {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(GlobalConfig.getSmsOffLineTemplateVariableOfReason(), reason);
            jsonObject.put(GlobalConfig.getSmsOffLineTemplateVariableOfConfigid(), configId);
            jsonObject.put(GlobalConfig.getSmsOffLineTemplateVariableOfName(), name);
            return jsonObject.toString();
        } catch (Exception e) {
            logger.error(e.getMessage(), "");
            return "";
        }
    }


    public static class AltCoinTransferConfigModelAndErrorReasonCompositeVO {
        private AltCoinTransferConfigModel transferConfigModel;
        private Integer type;
        private String errorReason;

        public AltCoinTransferConfigModel getTransferConfigModel() {
            return transferConfigModel;
        }

        public void setTransferConfigModel(AltCoinTransferConfigModel transferConfigModel) {
            this.transferConfigModel = transferConfigModel;
        }

        public String getErrorReason() {
            return errorReason;
        }

        public void setErrorReason(String errorReason) {
            this.errorReason = errorReason;
        }

        public Integer getType() {
            return type;
        }

        public void setType(Integer type) {
            this.type = type;
        }
    }

}
