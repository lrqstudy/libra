package com.libra.task;

import com.libra.exchange.exchanges.task.lowbuyhighsell.LowBuyHighSellHelper;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;

/**
 * Created by rongqiang.li on 17/5/17.
 */
@Repository
public class AutoBuyHighSellTask {
    @Resource
    private LowBuyHighSellHelper lowBuyHighSellHelper;

    public void executeLowBuyHighSell(){
        lowBuyHighSellHelper.autoLowBuyHighSell();
    }

}
