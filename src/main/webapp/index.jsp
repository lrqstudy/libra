<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page session="false"%>
<!DOCTYPE html>
<html>
<head>
    <title>www.btctts.com</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
    <link href="style.css" rel="stylesheet" />
</head>
<body>
<div class="container">

    <div class="masthead">
        <h3 class="muted">www.btctts.com</h3>
        <div class="navbar">
            <div class="navbar-inner">
                <div class="container">
                    <ul class="nav">
                        <li class="active"><a href="/index.jsp">首页</a></li>
                        <li><a href="./login.html">登陆</a></li>
                        <li><a href="./register.html">注册</a></li>
                        <li><a href="./listAllTradeTarget.html">开始交易</a></li>
                        <li><a href="./tradeHistory.html">我的交易</a></li>
                        <li><a href="./myAccount.html">我的账户</a></li>
                        <li><a href="./tradeStrategy.html">资产配置</a></li>
                        <li><a href="./roaCalculate.html">收益计算</a></li>
                        <li><a href="https://github.com/lrqstudy/BitcoinCommonSense/wiki">常用链接</a></li>
                    </ul>
                </div>
            </div>
        </div><!-- /.navbar -->
    </div>

    <!-- Jumbotron -->
    <div class="jumbotron">
        <h2>做合格的区块链资产投资者</h2>
        <p class="lead">珍惜每一次学习和成长的机会</p>
        <a class="btn btn-large btn-success" href="./listAllTradeTarget.html">开始交易吧，我们给每个用户提供了10W人民币的模拟额度</a>
    </div>

    <hr>

    <!-- Example row of columns -->
    <div class="row-fluid">
        <div class="span4">
            <h2>BTC</h2>
            <p>比特币(Bitcoin)的概念最初由中本聪( Satoshi Nakamoto )在 2009 年提出。
                与大多数货币不同的是，比特币不依赖于特定的中央发行机构，而是使用遍布整个 P2P 网络节点的分布式数据库来记录货币的交易，并使用密码学的设计来确保货币流通各个环节安全性，这一切依赖于根据中本聪思路设计发布的开源软件以及建构其上的 P2P 网络。
                从 2009 年至今，比特币取得了巨大的发展，目前总市值约为 70 亿美金（截止 2016 年 5 月）官网中文链接: https://bitcoin.org/zh_CN/</p>
            <p><a class="btn" href="./listAllTradeTarget.html">开始模拟盘交易 &raquo;</a></p>
        </div>
        <div class="span4">
            <h2>ETH</h2>
            <p>以太坊的目的是基于脚本、竞争币和链上元协议（on-chain meta-protocol）概念进行整合和提高，使得开发者能够创建任意的基于共识的、可扩展的、标准化的、特性完备的、易于开发的和协同的区块链应用。以太坊通过建立终极的抽象的基础层-内置有图灵完备编程语言的区块链-使得任何人都能够创建合约和去中心化应用，并在其中设立他们自由定义的所有权规则、交易方式和状态转换函数。官方网站:https://www.ethereum.org/ </p>
            <p><a class="btn" href="./listAllTradeTarget.html">开始模拟盘交易 &raquo;</a></p>
        </div>
        <div class="span4">
            <h2>ZEC</h2>
            <p>Zcash 是建立在区块链上的隐私保密技术。和比特币不同， Zcash 交易自动隐藏区块链上所有交易的发送者、接受者和数额。只用那些拥有查看秘钥的人才能看到交易的内容。用户拥有完全的控制权，用户可以自己选择性地向其他人提供查看秘钥。
                Zcash 钱包资金分 2 种：透明资金、私有资金，透明资金类似比特币资金；私有资金加强了隐私性，涉及到私有资金的交易是保密不可查的，透明资金与透明资金的交易是公开可查的。官方网站：https://z.cash/</p>
            <p><a class="btn" href="./listAllTradeTarget.html">开始模拟盘交易 &raquo;</a></p>
        </div>
    </div>

    <hr>

    <div class="footer">
        <p>&copy; www.btctts.com 2017 </p>
    </div>

</div> <!-- /container -->
<script src="http://code.jquery.com/jquery.js"></script>
<script src="js/bootstrap.min.js"></script>
</body>
</html>