package com.libra.exchange.strategy.service;

import com.libra.common.BaseSpringJUnitTest;
import com.libra.exchange.strategy.model.LibraProfitDailyReportModel;
import com.libra.exchange.strategy.vo.AltTransferBalanceSnapshotResultVO;
import org.junit.Test;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by rongqiang.li on 17/12/29.
 */
public class LibraProfitDailyReportServiceTest extends BaseSpringJUnitTest {

    @Resource
    private LibraProfitDailyReportService libraProfitDailyReportService;


    @Test
    public void testSaveLibraProfitDailyReportModel() throws Exception {
        LibraProfitDailyReportModel model=new LibraProfitDailyReportModel();

        model.setCashCurrency("BTC");
        model.setCreateTime(new Date());
        model.setCurrencyPair("EOSBTC");
        model.setCurrentProfitAmount(new BigDecimal(0.00));
        model.setProcessFlag(0);

        AltTransferBalanceSnapshotResultVO resultVO=new AltTransferBalanceSnapshotResultVO();

        model.setResultJson("");
        libraProfitDailyReportService.saveLibraProfitDailyReportModel(model);
    }
}