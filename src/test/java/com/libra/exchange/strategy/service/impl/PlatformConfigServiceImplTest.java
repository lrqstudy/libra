package com.libra.exchange.strategy.service.impl;

import com.google.api.client.util.Lists;
import com.libra.common.BaseSpringJUnitTest;
import com.libra.common.consts.PlatformEnum;
import com.libra.exchange.strategy.model.PlatformConfigModel;
import com.libra.exchange.strategy.service.PlatformConfigService;
import org.junit.Test;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by rongqiang.li on 17/5/12.
 */
public class PlatformConfigServiceImplTest extends BaseSpringJUnitTest {

    @Resource
    private PlatformConfigService platformConfigService;

    @Test
    public void testSavePlatformConfigModel() throws Exception {

    }

    @Test
    public void testSavePlatformConfigModelBatch() throws Exception {
        List<PlatformConfigModel> configModelList = Lists.newArrayList();

        PlatformConfigModel yunbi = new PlatformConfigModel();
        yunbi.setActiveStatus(1);
        yunbi.setUserCode("18720624406");
        yunbi.setApiKey("bbb");
        yunbi.setApiSecret("aaa");
        yunbi.setPlatform(PlatformEnum.YUNBI.getCode());


        PlatformConfigModel poloniex = new PlatformConfigModel();
        poloniex.setActiveStatus(1);
        poloniex.setUserCode("18720624406");
        poloniex.setApiKey("abd");
        poloniex.setApiSecret("aaa");
        poloniex.setPlatform(PlatformEnum.POLONIEX.getCode());

        configModelList.add(yunbi);
        configModelList.add(poloniex);

        platformConfigService.savePlatformConfigModelBatch(configModelList);
    }

    @Test
    public void testGetPlatConfigModelByUserCode() throws Exception {
        //private String apiKey="CNLmX6nAXGAMQRyF";
        //private String apiSecret="vWuSruA8330G-YLp9VsjsRBSLZ-ffYcY";
        PlatformConfigModel coinCheck = new PlatformConfigModel();
        coinCheck.setActiveStatus(1);
        coinCheck.setUserCode("18720624406");
        coinCheck.setApiKey("aa");
        coinCheck.setApiSecret("bbb");
        coinCheck.setPlatform(PlatformEnum.COINCHECK.getCode());

        platformConfigService.savePlatformConfigModel(coinCheck);
    }
}