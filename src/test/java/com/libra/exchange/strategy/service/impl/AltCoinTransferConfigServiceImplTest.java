package com.libra.exchange.strategy.service.impl;

import com.google.api.client.util.Lists;
import com.libra.common.BaseSpringJUnitTest;
import com.libra.common.consts.CurrencyType;
import com.libra.common.consts.PlatformEnum;
import com.libra.common.util.BeanUtilExt;
import com.libra.exchange.strategy.model.AltCoinTransferConfigModel;
import com.libra.exchange.strategy.service.AltCoinTransferConfigService;
import org.junit.Test;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.List;

/**
 * Created by rongqiang.li on 17/5/12.
 */
public class AltCoinTransferConfigServiceImplTest extends BaseSpringJUnitTest {

    @Resource
    private AltCoinTransferConfigService altCoinTransferConfigService;

    @Test
    public void testSaveAltCoinTransferConfigModel() throws Exception {

    }

    @Test
    public void testSaveAltCoinTransferConfigModelBatch() throws Exception {
        List<AltCoinTransferConfigModel> altCoinTransferConfigModelList=Lists.newArrayList();
        altCoinTransferConfigModelList.addAll(buildGNTTransferConfigBatch());
        altCoinTransferConfigModelList.addAll(buildREPTransferConfigBatch());
        altCoinTransferConfigModelList.addAll(buildBTSTransferConfigBatch());
        altCoinTransferConfigModelList.addAll(buildETHTransferConfigBatch());
        altCoinTransferConfigModelList.addAll(buildSCTransferConfigBatch());
        altCoinTransferConfigModelList.addAll(buildZECTransferConfigBatch());

        altCoinTransferConfigService.saveAltCoinTransferConfigModelBatch(altCoinTransferConfigModelList);
    }

    private List<AltCoinTransferConfigModel> buildGNTTransferConfigBatch(){
        List<AltCoinTransferConfigModel> transferConfigModelList= Lists.newArrayList();

        AltCoinTransferConfigModel yunbi=new AltCoinTransferConfigModel();

        yunbi.setActiveStatus(1);
        yunbi.setCashCurrency(CurrencyType.CNY.getCode());
        yunbi.setPlatform(PlatformEnum.YUNBI.getCode());

        yunbi.setCashCurrencyDepositAddress("1MS5dmz818DeQAgUDabg8tVmPbG7kEG2CG");
        yunbi.setCashCurrencyDepositMemo("");

        yunbi.setCashCurrencyWithdrawLimitAmount(new BigDecimal(3));
        yunbi.setCurrencyPair("gntcny");
        yunbi.setDiffRate(new BigDecimal(0.04));
        yunbi.setMaxBuyAmount(new BigDecimal(500));
        yunbi.setMaxSellAmount(new BigDecimal(500));

        yunbi.setTargetCode(CurrencyType.GNT.getCode());
        yunbi.setTargetCodeWithdrawLimitAmount(new BigDecimal(1000));

        yunbi.setTargetCodeDepositAddress("0xa8a00aaa0489e0bdab9bd8101b8af47a8ad77997");
        yunbi.setTargetCodeDepositMemo("");


        yunbi.setActiveStatus(1);
        yunbi.setUserCode("18720624406");

        BeanUtilExt.fillNullProperty(yunbi);


        AltCoinTransferConfigModel poloniex=new AltCoinTransferConfigModel();
        poloniex.setUserCode("18720624406");
        poloniex.setActiveStatus(1);
        poloniex.setCashCurrency(CurrencyType.BTC.getCode());
        poloniex.setPlatform(PlatformEnum.POLONIEX.getCode());
        poloniex.setCashCurrencyWithdrawLimitAmount(new BigDecimal(3));
        poloniex.setCashCurrencyDepositAddress("12fRCYfH1YvA4mZg7y1mNy1y421j9aQZe7");
        poloniex.setCashCurrencyDepositMemo("");

        poloniex.setCurrencyPair("BTC_GNT");
        poloniex.setTargetCodeDepositAddress("0xfad75f278a6b86a3953a8b0f4db20c7e3b5884d9");
        poloniex.setTargetCodeDepositMemo("");
        poloniex.setDiffRate(new BigDecimal(0.04));
        poloniex.setMaxBuyAmount(new BigDecimal(500));
        poloniex.setMaxSellAmount(new BigDecimal(500));
        poloniex.setActiveStatus(1);

        poloniex.setTargetCode(CurrencyType.GNT.getCode());
        poloniex.setTargetCodeWithdrawLimitAmount(new BigDecimal(1000));

        BeanUtilExt.fillNullProperty(poloniex);

        transferConfigModelList.add(yunbi);
        transferConfigModelList.add(poloniex);

        return transferConfigModelList;
    }

    private List<AltCoinTransferConfigModel> buildREPTransferConfigBatch(){
        List<AltCoinTransferConfigModel> transferConfigModelList= Lists.newArrayList();

        AltCoinTransferConfigModel yunbi=new AltCoinTransferConfigModel();

        yunbi.setActiveStatus(1);
        yunbi.setCashCurrency(CurrencyType.CNY.getCode());
        yunbi.setPlatform(PlatformEnum.YUNBI.getCode());

        yunbi.setCashCurrencyDepositAddress("1MS5dmz818DeQAgUDabg8tVmPbG7kEG2CG");
        yunbi.setCashCurrencyDepositMemo("");

        yunbi.setCashCurrencyWithdrawLimitAmount(new BigDecimal(3));
        yunbi.setCurrencyPair("repcny");
        yunbi.setDiffRate(new BigDecimal(0.04));
        yunbi.setMaxBuyAmount(new BigDecimal(10));
        yunbi.setMaxSellAmount(new BigDecimal(10));

        yunbi.setTargetCode(CurrencyType.REP.getCode());
        yunbi.setTargetCodeWithdrawLimitAmount(new BigDecimal(50));

        yunbi.setTargetCodeDepositAddress("0x0ab38887bbf81d965e0f1cbe6575a16ed954e7c2");
        yunbi.setTargetCodeDepositMemo("");


        yunbi.setActiveStatus(1);
        yunbi.setUserCode("18720624406");

        BeanUtilExt.fillNullProperty(yunbi);


        AltCoinTransferConfigModel poloniex=new AltCoinTransferConfigModel();
        poloniex.setUserCode("18720624406");
        poloniex.setActiveStatus(1);
        poloniex.setCashCurrency(CurrencyType.BTC.getCode());
        poloniex.setPlatform(PlatformEnum.POLONIEX.getCode());
        poloniex.setCashCurrencyWithdrawLimitAmount(new BigDecimal(3));
        poloniex.setCashCurrencyDepositAddress("12fRCYfH1YvA4mZg7y1mNy1y421j9aQZe7");
        poloniex.setCashCurrencyDepositMemo("");

        poloniex.setCurrencyPair("BTC_REP");
        poloniex.setTargetCodeDepositAddress("0xc3af1dfd8a60bc88f39c9c1a552fb9d5e8505877");
        poloniex.setTargetCodeDepositMemo("");
        poloniex.setDiffRate(new BigDecimal(0.04));
        poloniex.setMaxBuyAmount(new BigDecimal(10));
        poloniex.setMaxSellAmount(new BigDecimal(10));
        poloniex.setActiveStatus(1);

        poloniex.setTargetCode(CurrencyType.REP.getCode());
        poloniex.setTargetCodeWithdrawLimitAmount(new BigDecimal(50));

        BeanUtilExt.fillNullProperty(poloniex);

        transferConfigModelList.add(yunbi);
        transferConfigModelList.add(poloniex);

        return transferConfigModelList;
    }

    private List<AltCoinTransferConfigModel> buildSCTransferConfigBatch(){
        List<AltCoinTransferConfigModel> transferConfigModelList= Lists.newArrayList();

        AltCoinTransferConfigModel yunbi=new AltCoinTransferConfigModel();

        yunbi.setActiveStatus(1);
        yunbi.setCashCurrency(CurrencyType.CNY.getCode());
        yunbi.setPlatform(PlatformEnum.YUNBI.getCode());

        yunbi.setCashCurrencyDepositAddress("1MS5dmz818DeQAgUDabg8tVmPbG7kEG2CG");
        yunbi.setCashCurrencyDepositMemo("");

        yunbi.setCashCurrencyWithdrawLimitAmount(new BigDecimal(3));
        yunbi.setCurrencyPair("sccny");
        yunbi.setDiffRate(new BigDecimal(0.04));
        yunbi.setMaxBuyAmount(new BigDecimal(10000));
        yunbi.setMaxSellAmount(new BigDecimal(10000));

        yunbi.setTargetCode(CurrencyType.SC.getCode());
        yunbi.setTargetCodeWithdrawLimitAmount(new BigDecimal(100000));

        yunbi.setTargetCodeDepositAddress("547a73defcfd4012110ab633e50c95558a68966e2fb4bf6df3f2268aad93efaac83ddc10d8fe");
        yunbi.setTargetCodeDepositMemo("");


        yunbi.setActiveStatus(1);
        yunbi.setUserCode("18720624406");

        BeanUtilExt.fillNullProperty(yunbi);


        AltCoinTransferConfigModel poloniex=new AltCoinTransferConfigModel();
        poloniex.setUserCode("18720624406");
        poloniex.setActiveStatus(1);
        poloniex.setCashCurrency(CurrencyType.BTC.getCode());
        poloniex.setPlatform(PlatformEnum.POLONIEX.getCode());
        poloniex.setCashCurrencyWithdrawLimitAmount(new BigDecimal(3));
        poloniex.setCashCurrencyDepositAddress("12fRCYfH1YvA4mZg7y1mNy1y421j9aQZe7");
        poloniex.setCashCurrencyDepositMemo("");

        poloniex.setCurrencyPair("BTC_SC");
        poloniex.setTargetCodeDepositAddress("cecacf22b3fa6f1fb3f1c3eec540719f93b641a7725e516dca295855e63d68ea81c09697f474");
        poloniex.setTargetCodeDepositMemo("");
        poloniex.setDiffRate(new BigDecimal(0.04));
        poloniex.setMaxBuyAmount(new BigDecimal(10000));
        poloniex.setMaxSellAmount(new BigDecimal(10000));
        poloniex.setActiveStatus(1);

        poloniex.setTargetCode(CurrencyType.SC.getCode());
        poloniex.setTargetCodeWithdrawLimitAmount(new BigDecimal(100000));

        BeanUtilExt.fillNullProperty(poloniex);

        transferConfigModelList.add(yunbi);
        transferConfigModelList.add(poloniex);

        return transferConfigModelList;
    }

    private List<AltCoinTransferConfigModel> buildZECTransferConfigBatch(){
        List<AltCoinTransferConfigModel> transferConfigModelList= Lists.newArrayList();

        AltCoinTransferConfigModel yunbi=new AltCoinTransferConfigModel();

        yunbi.setActiveStatus(1);
        yunbi.setCashCurrency(CurrencyType.CNY.getCode());
        yunbi.setPlatform(PlatformEnum.YUNBI.getCode());

        yunbi.setCashCurrencyDepositAddress("1MS5dmz818DeQAgUDabg8tVmPbG7kEG2CG");
        yunbi.setCashCurrencyDepositMemo("");

        yunbi.setCashCurrencyWithdrawLimitAmount(new BigDecimal(3));
        yunbi.setCurrencyPair("zeccny");
        yunbi.setDiffRate(new BigDecimal(0.04));
        yunbi.setMaxBuyAmount(new BigDecimal(1));
        yunbi.setMaxSellAmount(new BigDecimal(1));

        yunbi.setTargetCode(CurrencyType.ZEC.getCode());
        yunbi.setTargetCodeWithdrawLimitAmount(new BigDecimal(10));

        yunbi.setTargetCodeDepositAddress("t1PjgkEQhHKEa7y2omN5nRCUbcj6FnLnzMk");
        yunbi.setTargetCodeDepositMemo("");


        yunbi.setActiveStatus(1);
        yunbi.setUserCode("18720624406");

        BeanUtilExt.fillNullProperty(yunbi);


        AltCoinTransferConfigModel poloniex=new AltCoinTransferConfigModel();
        poloniex.setUserCode("18720624406");
        poloniex.setActiveStatus(1);
        poloniex.setCashCurrency(CurrencyType.BTC.getCode());
        poloniex.setPlatform(PlatformEnum.POLONIEX.getCode());
        poloniex.setCashCurrencyWithdrawLimitAmount(new BigDecimal(3));
        poloniex.setCashCurrencyDepositAddress("12fRCYfH1YvA4mZg7y1mNy1y421j9aQZe7");
        poloniex.setCashCurrencyDepositMemo("");

        poloniex.setCurrencyPair("BTC_ZEC");
        poloniex.setTargetCodeDepositAddress("t1ZpfeRqFbDVGks6p6xcvbvwXQoFsWoov3y");
        poloniex.setTargetCodeDepositMemo("");
        poloniex.setDiffRate(new BigDecimal(0.04));
        poloniex.setMaxBuyAmount(new BigDecimal(1));
        poloniex.setMaxSellAmount(new BigDecimal(1));
        poloniex.setActiveStatus(1);

        poloniex.setTargetCode(CurrencyType.ZEC.getCode());
        poloniex.setTargetCodeWithdrawLimitAmount(new BigDecimal(10));

        BeanUtilExt.fillNullProperty(poloniex);

        transferConfigModelList.add(yunbi);
        transferConfigModelList.add(poloniex);

        return transferConfigModelList;
    }

    private List<AltCoinTransferConfigModel> buildETHTransferConfigBatch(){
        List<AltCoinTransferConfigModel> transferConfigModelList= Lists.newArrayList();

        AltCoinTransferConfigModel yunbi=new AltCoinTransferConfigModel();

        yunbi.setActiveStatus(1);
        yunbi.setCashCurrency(CurrencyType.CNY.getCode());
        yunbi.setPlatform(PlatformEnum.YUNBI.getCode());

        yunbi.setCashCurrencyDepositAddress("1MS5dmz818DeQAgUDabg8tVmPbG7kEG2CG");
        yunbi.setCashCurrencyDepositMemo("");

        yunbi.setCashCurrencyWithdrawLimitAmount(new BigDecimal(3));
        yunbi.setCurrencyPair("ethcny");
        yunbi.setDiffRate(new BigDecimal(0.04));
        yunbi.setMaxBuyAmount(new BigDecimal(1));
        yunbi.setMaxSellAmount(new BigDecimal(1));

        yunbi.setTargetCode(CurrencyType.ETH.getCode());
        yunbi.setTargetCodeWithdrawLimitAmount(new BigDecimal(10));

        yunbi.setTargetCodeDepositAddress("0x11bf165190905ecad230f5ddd7f1b78de303b694");
        yunbi.setTargetCodeDepositMemo("");


        yunbi.setActiveStatus(1);
        yunbi.setUserCode("18720624406");

        BeanUtilExt.fillNullProperty(yunbi);


        AltCoinTransferConfigModel poloniex=new AltCoinTransferConfigModel();
        poloniex.setUserCode("18720624406");
        poloniex.setActiveStatus(1);
        poloniex.setCashCurrency(CurrencyType.BTC.getCode());
        poloniex.setPlatform(PlatformEnum.POLONIEX.getCode());
        poloniex.setCashCurrencyWithdrawLimitAmount(new BigDecimal(3));
        poloniex.setCashCurrencyDepositAddress("12fRCYfH1YvA4mZg7y1mNy1y421j9aQZe7");
        poloniex.setCashCurrencyDepositMemo("");

        poloniex.setCurrencyPair("BTC_ETH");
        poloniex.setTargetCodeDepositAddress("0xe0e2c2870286f25ba305050691801998d1864baa");
        poloniex.setTargetCodeDepositMemo("");
        poloniex.setDiffRate(new BigDecimal(0.04));
        poloniex.setMaxBuyAmount(new BigDecimal(1));
        poloniex.setMaxSellAmount(new BigDecimal(1));
        poloniex.setActiveStatus(1);

        poloniex.setTargetCode(CurrencyType.ETH.getCode());
        poloniex.setTargetCodeWithdrawLimitAmount(new BigDecimal(10));

        BeanUtilExt.fillNullProperty(poloniex);

        transferConfigModelList.add(yunbi);
        transferConfigModelList.add(poloniex);

        return transferConfigModelList;
    }


    private List<AltCoinTransferConfigModel> buildBTSTransferConfigBatch(){
        List<AltCoinTransferConfigModel> transferConfigModelList= Lists.newArrayList();

        AltCoinTransferConfigModel yunbi=new AltCoinTransferConfigModel();

        yunbi.setActiveStatus(1);
        yunbi.setCashCurrency(CurrencyType.CNY.getCode());
        yunbi.setPlatform(PlatformEnum.YUNBI.getCode());

        yunbi.setCashCurrencyDepositAddress("1MS5dmz818DeQAgUDabg8tVmPbG7kEG2CG");
        yunbi.setCashCurrencyDepositMemo("");

        yunbi.setCashCurrencyWithdrawLimitAmount(new BigDecimal(3));
        yunbi.setCurrencyPair("btscny");
        yunbi.setDiffRate(new BigDecimal(0.04));
        yunbi.setMaxBuyAmount(new BigDecimal(5000));
        yunbi.setMaxSellAmount(new BigDecimal(5000));

        yunbi.setTargetCode(CurrencyType.BTS.getCode());
        yunbi.setTargetCodeWithdrawLimitAmount(new BigDecimal(20000));

        yunbi.setTargetCodeDepositAddress("yun-bts");
        yunbi.setTargetCodeDepositMemo("13670027");


        yunbi.setActiveStatus(1);
        yunbi.setUserCode("18720624406");

        BeanUtilExt.fillNullProperty(yunbi);


        AltCoinTransferConfigModel poloniex=new AltCoinTransferConfigModel();
        poloniex.setUserCode("18720624406");
        poloniex.setActiveStatus(1);
        poloniex.setCashCurrency(CurrencyType.BTC.getCode());
        poloniex.setPlatform(PlatformEnum.POLONIEX.getCode());
        poloniex.setCashCurrencyWithdrawLimitAmount(new BigDecimal(3));
        poloniex.setCashCurrencyDepositAddress("12fRCYfH1YvA4mZg7y1mNy1y421j9aQZe7");
        poloniex.setCashCurrencyDepositMemo("");

        poloniex.setCurrencyPair("BTC_BTS");
        poloniex.setTargetCodeDepositAddress("poloniexwallet");
        poloniex.setTargetCodeDepositMemo("965f9b7e794d0089");
        poloniex.setDiffRate(new BigDecimal(0.04));
        poloniex.setMaxBuyAmount(new BigDecimal(5000));
        poloniex.setMaxSellAmount(new BigDecimal(5000));
        poloniex.setActiveStatus(1);

        poloniex.setTargetCode(CurrencyType.BTS.getCode());
        poloniex.setTargetCodeWithdrawLimitAmount(new BigDecimal(20000));

        BeanUtilExt.fillNullProperty(poloniex);

        transferConfigModelList.add(yunbi);
        transferConfigModelList.add(poloniex);

        return transferConfigModelList;
    }



    @Test
    public void testGetAllModelListByUserCode() throws Exception {

    }
}