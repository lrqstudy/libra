package com.libra.exchange.exchanges.bigone;

import com.libra.common.BaseSpringJUnitTest;
import com.libra.exchange.exchanges.common.CommonOrderBook;
import org.junit.Test;

import javax.annotation.Resource;

/**
 * Created by rongqiang.li on 17/11/28.
 */
public class BigOneMarketApiTest extends BaseSpringJUnitTest{

    @Resource
    private BigOneMarketApi bigOneMarketApi;


    @Test
    public void testGetOrderBook() throws Exception {
        CommonOrderBook orderBook = bigOneMarketApi.getOrderBook("ETH", "BTC", "ETH-BTC", 1);
        System.out.println(orderBook.getBuyOrderVOList());
        System.out.println(orderBook.getSellOrderVOList());
    }
}