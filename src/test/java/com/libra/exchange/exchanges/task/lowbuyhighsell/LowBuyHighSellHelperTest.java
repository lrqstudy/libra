package com.libra.exchange.exchanges.task.lowbuyhighsell;

import com.libra.common.BaseSpringJUnitTest;
import org.junit.Test;

import javax.annotation.Resource;

/**
 * Created by rongqiang.li on 17/5/17.
 */
public class LowBuyHighSellHelperTest extends BaseSpringJUnitTest {

    @Resource
    private LowBuyHighSellHelper lowBuyHighSellHelper;

    @Test
    public void testAutoLowBuyHighSell() throws Exception {

        lowBuyHighSellHelper.autoLowBuyHighSell();

    }
}