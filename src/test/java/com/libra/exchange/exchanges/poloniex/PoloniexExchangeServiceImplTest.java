package com.libra.exchange.exchanges.poloniex;

import com.google.api.client.util.Maps;
import com.libra.common.BaseSpringJUnitTest;
import com.libra.common.consts.PlatformEnum;
import com.libra.exchange.exchanges.yunbi.YunbiExchangeHelper;
import com.libra.exchange.strategy.model.PlatformConfigModel;
import com.libra.exchange.strategy.service.PlatformConfigService;
import org.junit.Test;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * Created by rongqiang.li on 17/5/2.
 */
public class PoloniexExchangeServiceImplTest extends BaseSpringJUnitTest {

    @Resource
    private PoloniexExchangeServiceImpl poloniexExchangeService;

    @Resource
    private PlatformConfigService platformConfigService;
    @Resource
    private YunbiExchangeHelper yunbiExchangeHelper;

    @Test
    public void testBuy() throws Exception {
        //String buy = poloniexExchangeService.buy("SC", "BTC", new BigDecimal(0.00000077D), new BigDecimal(1D));
        //System.out.println(buy);
    }

    @Test
    public void testSell() throws Exception {

    }

    @Test
    public void testWithdraw(){
        List<PlatformConfigModel> platConfigModelByUserCode = platformConfigService.getPlatConfigModelByUserCode("18720624406");

        Map<String,PlatformConfigModel> platformConfigModelMap= Maps.newHashMap();
        for (PlatformConfigModel configModel : platConfigModelByUserCode) {
            platformConfigModelMap.put(configModel.getPlatform(),configModel);
        }


        PlatformConfigModel poloniexConfig = platformConfigModelMap.get(PlatformEnum.POLONIEX.getCode());

        Map<String, BigDecimal> poloniexAccountBalanceMap = poloniexExchangeService.getAccountBalance(poloniexConfig);
        PlatformConfigModel yunbiConfig = platformConfigModelMap.get(PlatformEnum.YUNBI.getCode());


        //Map<String, BigDecimal> yunbiAccountBalanceMap = yunbiExchangeHelper.getBalanceMap(yunbiConfig);
        //String sc = poloniexExchangeService.withdraw("BTS", "36");
        //System.out.println(sc);
        //System.out.println(yunbiAccountBalanceMap.size());
        //poloniexExchangeService.withdraw("BTS","yun-bts","13670027",new BigDecimal(1000),poloniexConfig);
        //poloniexExchangeService.withdraw("SC","547a73defcfd4012110ab633e50c95558a68966e2fb4bf6df3f2268aad93efaac83ddc10d8fe","",new BigDecimal(1000),poloniexConfig);
        poloniexExchangeService.withdraw("GNT","0xa8a00aaa0489e0bdab9bd8101b8af47a8ad77997","",new BigDecimal(10),poloniexConfig);
        //poloniexExchangeService.withdraw("GNT","0xa8a00aaa0489e0bdab9bd8101b8af47a8ad77997","",new BigDecimal(10),poloniexConfig);
    }
}