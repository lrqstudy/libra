package com.libra.exchange.exchanges.bibox;

import com.libra.common.consts.OrderSource;
import com.libra.exchange.exchanges.common.CommonOpenOrderVO;
import com.libra.exchange.strategy.model.PlatformConfigModel;
import org.junit.Before;
import org.junit.Test;
import org.knowm.xchange.binance.dto.trade.OrderSide;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * Created by rongqiang.li on 18/3/20.
 */
public class BiboxExchangeUtilsTest {

    private BiboxExchangeUtils biboxExchangeUtils;
    private PlatformConfigModel configModel;

    @Before
    public void init() {
        biboxExchangeUtils = new BiboxExchangeUtils();
        configModel = new PlatformConfigModel();
        configModel.setApiKey("abd3a0c12a63db6f26915a52f79299accd279e39");
        configModel.setApiSecret("f6bc201080cbbb5b1c7d2a67748d02278617e730");

    }

    @Test
    public void testGetBalanceMap() throws Exception {
        Map<String, BigDecimal> balanceMap = biboxExchangeUtils.getBalanceMap(configModel);
        System.out.println(balanceMap);
    }

    @Test
    public void testBuy() throws Exception {
        //biboxExchangeUtils.buy("TNB","BTC",configModel,"TNBEOS",)

        String result = biboxExchangeUtils.buy("TNB", "BTC", configModel, "TNBBTC", new BigDecimal(0.00000356), new BigDecimal(50), OrderSide.BUY, 2, OrderSource.TRANSFER.getId());
        System.out.println(result);

    }

    @Test
    public void testSell() throws Exception {
        String result = biboxExchangeUtils.sell("TNB", "BTC", configModel, "TNBBTC", new BigDecimal(0.00000358), new BigDecimal(50), OrderSide.SELL, 2, OrderSource.TRANSFER.getId());
        System.out.println(result);
    }

    @Test
    public void testGetOpenOrderVOList() throws Exception {

        for (int i = 0; i < 10; i++) {
            List<CommonOpenOrderVO> openOrderVOList = biboxExchangeUtils.getOpenOrderVOList(configModel, "EOS", "BTC");
            for (CommonOpenOrderVO commonOpenOrderVO : openOrderVOList) {
                System.out.println(commonOpenOrderVO);
            }
        }

    }
}