package com.libra.exchange.exchanges.bitfinex;

import org.knowm.xchange.Exchange;
import org.knowm.xchange.ExchangeFactory;
import org.knowm.xchange.ExchangeSpecification;
import org.knowm.xchange.bitfinex.v1.BitfinexExchange;
import org.knowm.xchange.dto.account.AccountInfo;
import org.knowm.xchange.service.account.AccountService;

/**
 * Created by rongqiang.li on 17/10/22.
 */
public class BitfinexXchangeTest {
    public static void main(String[] args)throws Exception {
        ExchangeSpecification exSpec = new BitfinexExchange().getDefaultExchangeSpecification();
        exSpec.setUserName("lrqstudy");
        exSpec.setApiKey("aa");
        exSpec.setSecretKey("bb");
        Exchange bitstamp=ExchangeFactory.INSTANCE.createExchange(exSpec);

        // Get the account information
        AccountService accountService = bitstamp.getAccountService();
        AccountInfo accountInfo = accountService.getAccountInfo();
        System.out.println(accountInfo.toString());

    }
}
