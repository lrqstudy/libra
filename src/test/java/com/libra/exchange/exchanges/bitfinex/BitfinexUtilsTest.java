package com.libra.exchange.exchanges.bitfinex;

import com.libra.common.BaseSpringJUnitTest;
import com.libra.exchange.strategy.model.PlatformConfigModel;
import org.junit.Test;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Map;

/**
 * Created by rongqiang.li on 17/10/22.
 */
public class BitfinexUtilsTest extends BaseSpringJUnitTest{

    @Resource
    private BitfinexUtils bitfinexUtils;

    @Test
    public void testGetBalance() throws Exception {
        PlatformConfigModel configModel=new PlatformConfigModel();
        Map<BalanceTypeKey, BigDecimal> balance = bitfinexUtils.getBalance(configModel);
        System.out.println(balance);
    }
    @Test
    public void testSell(){
    }

    @Test
    public void testHistory(){
    }

}