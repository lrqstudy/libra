package com.libra.exchange.exchanges.bitfinex;

import com.libra.common.BaseSpringJUnitTest;
import org.junit.Test;

import javax.annotation.Resource;

/**
 * Created by rongqiang.li on 17/10/21.
 */
public class BitfinexMarketApiUtilTest extends BaseSpringJUnitTest {

    @Resource
    private BitfinexMarketApiUtil bitfinexMarketApiUtil;


    @Test
    public void testGetOrderBook() throws Exception {
        BitfinexOrderBookVO eosbtc = bitfinexMarketApiUtil.getOrderBook("EOSBTC", 1, 1);
        System.out.println(eosbtc);

    }
}