package com.libra.exchange;

import com.libra.common.util.Encode;

/**
 * Created by rongqiang.li on 17/10/29.
 */
public class EncodeTest {
    public static void main(String[] args) {
        String apiKey="a";
        String secret="b";

        System.out.println(Encode.encode(apiKey));
        System.out.println(Encode.encode(secret));
    }
}
