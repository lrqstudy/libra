package com.libra;

import com.taobao.api.DefaultTaobaoClient;
import com.taobao.api.TaobaoClient;
import com.taobao.api.request.AlibabaAliqinFcSmsNumSendRequest;
import com.taobao.api.response.AlibabaAliqinFcSmsNumSendResponse;
import org.json.JSONObject;

/**
 * Created by rongqiang.li on 17/11/24.
 */
public class SendMessageUtils {
    public static void main(String[] args)throws Exception {
        //SMS_113350037

        //String host = "http://sms.market.alicloudapi.com";
        //http://sms.market.alicloudapi.com/singleSendSms
        //http://gw.api.taobao.com/router/rest
        //alibaba.aliqin.fc.sms.num.send
        String path = "/singleSendSms";
        TaobaoClient client = new DefaultTaobaoClient("http://gw.api.taobao.com/router/rest", "23641897", "956c3d2f0561d4e9533205317c5738fe");

        AlibabaAliqinFcSmsNumSendRequest req = new AlibabaAliqinFcSmsNumSendRequest();
        req.setExtend("123456");
        req.setSmsType("normal");
        req.setSmsFreeSignName("荣强搬砖");

        JSONObject jsonObject=new JSONObject();
        jsonObject.put("name","test");
        jsonObject.put("configId","123456");
        jsonObject.put("reason","balance not enough");
        req.setSmsParamString(jsonObject.toString());
        //req.setSmsParamString("{\"name\":\"test\",\"configId\":\"10086\"}");
        req.setRecNum("17610357877");
        req.setSmsTemplateCode("SMS_113815017");
        AlibabaAliqinFcSmsNumSendResponse rsp = client.execute(req);
        System.out.println(rsp.getBody());



    }
}
